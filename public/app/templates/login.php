<div bg set-height class="login" style="background-size: 100% 100%;">
   <div class="login-container">      
            <div class="login-box animated fadeInDown">
                
                <div class="login-body">
                <div class="login-logo"></div>
                    <div class="login-title"><strong>Log In</strong> to your account</div>
                    <form class="form-horizontal login-form" name="userForm" ng-submit="doLogin()" novalidate>
                    <div class="form-group" ng-class="{ 'has-error' : userForm.email.$invalid && !userForm.email.$pristine }">
                        <div class="col-md-12">
                            <input type="email" class="form-control" placeholder="E-mail"  name="email" class="form-control" ng-model="user.email" autofocus required  />
                            <p ng-show="userForm.email.$invalid && !userForm.email.$pristine" class="help-block">Enter a valid email.</p>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error' : userForm.password.$invalid && !userForm.password.$pristine }">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Password"  name="password" class="form-control" ng-model="user.password" required />
                            <p ng-show="userForm.password.$invalid && !userForm.password.$pristine" class="help-block">Password is Mandatory.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                        </div>
                        <div class="col-md-6">                            
                            <input type="submit" class="btn btn-info btn-block" ng-disabled="userForm.$invalid" value="Submit">
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <a href="http://www.kaspontech.com/" target="_blank"> 2017 Kaspon TechWorks Pvt Ltd </a>
                    </div>
                </div>
            </div>
            
        </div>
</div>