<div class="page-sidebar"  ng-controller="navController">
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="#"> <img src="assets/img/logo.png" alt="{{user.name}}"/></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="assets/img/man.png" alt="{{user.name}}"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="assets/img/man1.png" alt="{{user.name}}"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">{{user.name}}</div>
                    <div class="profile-data-title">{{user.user_type}}</div>
                </div>
                <div class="profile-controls">
                    <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>                                                                        
        </li>
        <div ng-repeat="link in navUrl">
            <li class="start" ng-class="checkActiveLink(link.url)" ng-if="!link.subMenu">
                <a href="#{{link.url}}" ng-click="add_active($event)">
                    <span class="{{link.icon}}"></span> {{link.link}}
                </a>                     
            </li>
            <li class="xn-openable start" ng-class="checkActiveLink(link.url)" ng-if="link.subMenu" >
                <a href="{{link.url}}" ng-click="add_active($event)">
                    <span class="{{link.icon}}"></span> {{link.link}}
                </a>
                <ul ng-if="link.subMenu" class="multi-level" role="menu" id="heightmax" aria-labelledby="dropdownMenu">
                    <li class="dropdown-submenu" ng-repeat="submenu in link.subMenu">
                        <a tabindex="-1" ng-href="#{{submenu.url}}" ng-click="add_subactive($event)">
                            <span class="{{submenu.icon}}"></span> {{submenu.link}}
                        </a>
                    </li>
                </ul>
            </li>
        </div>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->