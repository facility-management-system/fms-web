var myApp = angular.module('myApp', ['ngAnimate', 'ngRoute', 'ngCookies', 'ngMessages', 'ngTagsInput', 'ui.router', 'ui.bootstrap', 'datatables', 'angularjs-datetime-picker', 'oitozero.ngSweetAlert', 'angular-bind-html-compile', 'vsGoogleAutocomplete', 'angularFileUpload', '720kb.datepicker', 'toaster', 'ngIdle', 'ngJsTree', '720kb.datepicker', 'pdf', 'ngValidate', 'ngBootbox']);
myApp.config(function($validatorProvider) {
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block'
    });

    $validatorProvider.addMethod("emailformat", function(value, element) {
        return this.optional(element) || /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/.test(value);
    }, "Enter a valid email.");

    $validatorProvider.setDefaultMessages({
        required: "This field is required.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: $validatorProvider.format("Please enter no more than {0} characters."),
        minlength: $validatorProvider.format("Please enter at least {0} characters."),
        rangelength: $validatorProvider.format("Please enter a value between {0} and {1} characters long."),
        range: $validatorProvider.format("Please enter a value between {0} and {1}."),
        max: $validatorProvider.format("Please enter a value less than or equal to {0}."),
        min: $validatorProvider.format("Please enter a value greater than or equal to {0}.")
    });
});
myApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {

        $locationProvider.hashPrefix('');
        $routeProvider.when('/', {
            templateUrl: 'app/templates/login.php',
            controller: 'UserController',
        });
        $routeProvider.when('/dashboard', {
            templateUrl: 'app/templates/dashboard.html',
            controller: 'user_managementController',
            authenticated: true,
        });
        $routeProvider.when('/msocial_page', {
            templateUrl: 'app/templates/msocial_page.html',
            controller: 'msocial_managementController',
            authenticated: true,
        });
        $routeProvider.when('/msocial_feeds', {
            templateUrl: 'app/templates/news_feeds.html',
            controller: 'msocial_managementController',
            authenticated: true,
        });
        $routeProvider.when('/user_role', {
            templateUrl: 'app/templates/user_roles.html',
            controller: 'user_managementController',
            authenticated: true,
        });
        $routeProvider.when('/user', {
            templateUrl: 'app/templates/user.html',
            controller: 'user_managementController',
            authenticated: true,
        });
        $routeProvider.when('/technician', {
            templateUrl: 'app/templates/technician.html',
            controller: 'user_managementController',
            authenticated: true,
        });
        $routeProvider.when('/ticket', {
            templateUrl: 'app/templates/tickets.html',
            controller: 'tickets_managementController',
            authenticated: true,
        });

        $routeProvider.when('/sa_graphs', {
            templateUrl: 'app/templates/superadm_dashboard.html',
            controller: 'company_managementController',
        });
        $routeProvider.when('/superadm_project', {
            templateUrl: 'app/templates/project.html',
            controller: 'company_managementController',
        });
        $routeProvider.when('/company_creation', {
            templateUrl: 'app/templates/company.html',
            controller: 'company_managementController',
        });
        $routeProvider.when('/admin_list', {
            templateUrl: 'app/templates/admin_list.html',
            controller: 'company_managementController',
        });
        $routeProvider.when('/product', {
            templateUrl: 'app/templates/product.html',
            controller: 'product_managementController',
            authenticated: true,
        });
        /*$routeProvider.when('/customer', {
            templateUrl: 'app/templates/customer.html',
            controller: 'customer_managementController',
            authenticated: true,
        });*/
        $routeProvider.when('/customer_owner', {
            templateUrl: 'app/templates/customer_ownertypes.html',
            controller: 'customer_managementController',
            authenticated: true,
        });
        $routeProvider.when('/manage_customer', {
            templateUrl: 'app/templates/customer.html',
            controller: 'customer_managementController',
            authenticated: true,
        });

        $routeProvider.when('/contract_master', {
            templateUrl: 'app/templates/contract_master.html',
            controller: 'contract_managementController',
            authenticated: true,
        });
        $routeProvider.when('/contract_category', {
            templateUrl: 'app/templates/contract_category.html',
            controller: 'contract_managementController',
            authenticated: true,
        });
        $routeProvider.when('/support_category', {
            templateUrl: 'app/templates/support_category.html',
            controller: 'contract_managementController',
            authenticated: true,
        });
        $routeProvider.when('/contracttypes', {
            templateUrl: 'app/templates/contracttypes.html',
            controller: 'contract_managementController',
            authenticated: true,
        });
        $routeProvider.when('/maintenance', {
            templateUrl: 'app/templates/maintenance.html',
            controller: 'contract_managementController',
            authenticated: true,
        });
        $routeProvider.when('/maintanance', {
            templateUrl: 'app/templates/maintanance.html',
            controller: 'customer_managementController',
            authenticated: true,
        });
        $routeProvider.when('/new_project', {
            templateUrl: 'app/templates/project.html',
            controller: 'project_managementController',
            authenticated: true,
        });
        $routeProvider.when('/training', {
            templateUrl: 'app/templates/training.html',
            controller: 'training_managementController',
            authenticated: true,
        });
        $routeProvider.when('/assessment', {
            templateUrl: 'app/templates/assessment.html',
            controller: 'training_managementController',
            authenticated: true,
        });
        $routeProvider.when('/add_knowledge', {
            templateUrl: 'app/templates/add_knowledge.html',
            controller: 'kb_managementController',
            authenticated: true,
        });
        $routeProvider.when('/awaiting_approve', {
            templateUrl: 'app/templates/awaiting_approve.html',
            controller: 'kb_managementController',
            authenticated: true,
        });
        $routeProvider.when('/knowledgebase', {
            templateUrl: 'app/templates/list_knowledgebase.html',
            controller: 'kb_managementController',
            authenticated: true,
        });
        $routeProvider.when('/logout', {
            templateUrl: 'app/templates/logout.html',
            controller: 'user_managementController',
            authenticated: true,
        });
        $routeProvider.when('/edit_certificate/:id/:assessment_id/:title/:type/:productcategory', {
            templateUrl: 'app/templates/edit_certificate.html',
            controller: 'editsassesment_managementController',
            authenticated: true,
        });
        $routeProvider.when('/spare', {
            templateUrl: 'app/templates/spare_management.html',
            controller: 'spare_managementController',
            authenticated: true,
        });
        $routeProvider.when('/man_newtickets', {
            templateUrl: 'app/templates/man_newtickets.html',
            controller: 'tickets_managementController',
            authenticated: true,
        });
        $routeProvider.when('/man_inprogress', {
            templateUrl: 'app/templates/man_inprogress.html',
            controller: 'tickets_managementController',
            authenticated: true,
        });
        $routeProvider.when('/man_completed', {
            templateUrl: 'app/templates/man_completed.html',
            controller: 'tickets_managementController',
            authenticated: true,
        });

        $routeProvider.otherwise('/');
    }
]);
myApp.config(["KeepaliveProvider", "IdleProvider",
    function(KeepaliveProvider, IdleProvider) {
        IdleProvider.idle(500);
        IdleProvider.timeout(500);
        KeepaliveProvider.interval(10000);
    }
]);
myApp.run(['$rootScope', 'Idle', 'userModel', '$location', '$route', function($rootScope, Idle, userModel, $location, $route) {
    Idle.watch();
    $rootScope.$on('IdleStart', function() {
        Idle.watch();
    });
    $rootScope.$on('IdleTimeout', function() {
        /* Logout user */
        userModel.doUserLogout();
        $location.path('/');
    });
}]);

myApp.run(['$rootScope', '$location', 'userModel', function($rootScope, $location, userModel) {
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
        if (next.$$route.authenticated) {
            if (!userModel.getAuthStatus()) {
                $location.path('/');
            }
        }
        if (next.$$route.originalPath == '/') {
            if (userModel.getAuthStatus()) {
                $location.path(current.$$route.originalPath);
            }
        }
    })
}]);
myApp.service('myService', ['$http', function($http) {
    return {
        submitForm: function($url, $method, $data, $headers) {
            $url = 'index.php/' + $url;
            return $http({
                url: $url,
                method: $method,
                data: $data,
                dataType: 'JSON',
                headers: $headers,
            }).then(function(data) {
                return data;
            });
        }
    }
}]);
myApp.directive('loading', ['$http', function($http) {
    return {
        restrict: 'A',
        link: function(scope, elm, attrs) {
            scope.isLoading = function() {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function(v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);
myApp.directive('fileModel', ['$parse', function($parse) {

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
myApp.directive('myRepeatDone', function() {
    return function(scope, element, attrs) {
        if (!scope.link.subMenu) {
            angular.element(document.getElementsByClassName(attrs.class)).addClass('xn');
        } else {
            angular.element(document.getElementsByClassName(attrs.class)).addClass('xn-openable');
        };
    };
});