myApp
/* .service('myService', ['$http', function ($http) {
  return {
    submitForm : function ( $url, $method, $data ) {
      return $http({
        url: $url,
        data: $data,
        method: $method,
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        dataType:'JSON',
        //contentType: "application/json; charset=utf-8",
        cache: false,
      }).then(function (data) {
        return data;
      })
    }
  }
}]) */
    .controller('user_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', function($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService) {


    var status = $cookies.get('auth');
    status = JSON.parse(status);
    var comp_id = status['company_id'];
    /*For Usermanagement */
    $scope.loadData = function() {
        var vm = this;
        var resetPaging = false;
        $http({
            method: 'POST',
            url: 'index.php/getusers',
            data: { 'company_id': comp_id },
        }).then(function(response) {
            $scope.user = response.data;
            console.log($scope.user);
        });
    };
    $scope.loadData();

    $scope.tags = [
        { text: 'just' },
        { text: 'some' },
        { text: 'cool' },
        { text: 'tags' }
    ];
    $scope.loadTags = function(query) {
        return $http.get('/loadWorktype?query=' + query);
    };

    $scope.loadWorktype = function() {
        var vm = this;
        var resetPaging = false;
        $http({
            method: 'GET',
            url: 'index.php/loadWorktype',
        }).then(function(response) {
            $scope.s_types = response.data;
        });
    };
    $scope.loadWorktype();

    $scope.getuser_role = function() {
        var vm = this;
        var resetPaging = false;
        $url = 'getuser_role';
        $method = 'POST';
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            console.log(data.data);
            if (data['status'] == '200') {
                $scope.role_types = data.data;
            } else {
                $scope.role_types = data.data;
            }
        });
    };
    $scope.getuser_role();

    $scope.add_techopen = function() {
        $('#add_tech').modal('show');
    };

    $scope.insert_userrole = function() {
        $values = $scope.insert.ownertype;
        $url = 'insert_userrole';
        $method = 'POST';
        $data = { 'owner_types': $values, 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.getuser_role();
            } else {
                $scope.error = 'User Role not inserted kindly try again';
            }
        });
    };

    $scope.edit_role = function(customers) {
        $scope.insert_customer = customers;
        $('#myModal').modal('show');
    };

    $scope.update_role = function() {
        $id = $scope.insert_customer.id;
        $customer_type = $scope.insert_customer.customer_type;
        $url = 'update_role';
        $method = 'POST';
        $data = {
            'id': $id,
            'customer_type': $customer_type
        };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $('#myModal').modal('hide');
                $scope.getuser_role();
            } else {
                toaster.pop({ type: 'error', title: "User role not Updated! Kindly try again" });
            }
        });
    };

    $scope.delete_role = function(id) {
        $url = 'delete_role';
        $method = 'POST';
        $data = { 'id': id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.getuser_role();
            } else {
                $scope.error = 'User not deledted, kindly try again';
            }
        });
    };

    $scope.model1_open = function() {
        //$scope.insert_user.$setPristine();
        // $scope.insert_user.employee_id = '';
        // $scope.insert_user.first_name = '';
        // $scope.insert_user.last_name = '';
        // $scope.insert_user.email_id = '';
        // $scope.insert_user.contact_number = '';
        // $scope.insert_user.alternate_number = '';
        // $scope.insert_user.address = '';
        // $scope.insert_user.region = '';
        // $scope.insert_user.area = '';
        // $scope.insert_user.location = '';
        // $scope.insert_user.role = '';
        // $scope.insert_user.support = '';
        $('#myModal').modal('show');
    };
    var counter = 0;
    $scope.addproduct = function() {
        counter += 1;
        var el = $(`<hr class="hr_` + counter + `">
                  <div class="row fields">
                    <a class="pull-right" ng-click="enter()"><i class="fa fa-times"></i></a>
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group form-md-line-input" ng-class="{ 'has-error' : add_user.product_cat_` + counter + `.$invalid && !add_user.product_cat_` + counter + `.$pristine }">
                            <label class="col-md-3 control-label" for="form_control_1">Product Category</label>
                            <div class="col-md-9">
                              <select class="form-control" name="product_cat_` + counter + `" ng-model='add_user.product_cat_` + counter + `' ng-change() required>
                                <option ng-option selected="" disabled="" value="">Select Skill Level</option>
                                <option ng-option value="L1">L1</option>
                                <option ng-option value="L2">L2</option>
                                <option ng-option value="L3">L3</option>
                                <option ng-option value="L4">L4</option>
                              </select>
                              <p ng-show="add_user.product_cat_` + counter + `.$invalid && !add_user.product_cat_` + counter + `.$pristine" class="help-block" style="color:#e73d4a !important">Product Category is required.</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group form-md-line-input" ng-class="{ 'has-error' : add_user.sub_cat_` + counter + `.$invalid && !add_user.sub_cat_` + counter + `.$pristine }">
                            <label class="col-md-3 control-label" for="form_control_1">Sub Category</label>
                            <div class="col-md-9">
                              <select class="form-control" name="sub_cat_` + counter + `" ng-model='add_user.sub_cat_` + counter + `' ng-change() required>
                              </select>
                              <p ng-show="add_user.sub_cat_` + counter + `.$invalid && !add_user.sub_cat_` + counter + `.$pristine" class="help-block" style="color:#e73d4a !important">Sub Category is required.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group form-md-line-input" ng-class="{ 'has-error' : add_user.product_skill_` + counter + `.$invalid && !add_user.product_skill_` + counter + `.$pristine }">
                            <label class="col-md-3 control-label" for="form_control_1">Skill Level</label>
                            <div class="col-md-9">
                              <select class="form-control" name="product_skill_` + counter + `" ng-model='add_user.product_skill_` + counter + `' ng-change() required>                              
                              </select>
                              <p ng-show="add_user.product_skill_` + counter + `.$invalid && !add_user.product_skill_` + counter + `.$pristine" class="help-block" style="color:#e73d4a !important">Skill Level is required.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>`);
        //var result = angular.element(document.getElementById('insert_before')).before(el);
        var result = $(insert_before).before(el);
        $compile(result)($scope);
    };
    $scope.cancel_prod = function(id) {
        //console.log('testing');
        /*var id = id.split("_");
        console.log(id);
        $('.fields_' + id[1]).remove();
        $('.hr_' + id[1]).remove(); */
    };

    $scope.insert_tuser = function() {
        console.log($scope.insert_user.alternate_number);
        if ($scope.myFile) {
            $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var user_proof = $scope.myFile;
                console.log(user_proof);
            } else {
                toaster.pop({ type: "error", title: "Attached File should be an Image file" });
                return false;
            }
        } else {
            var user_proof = "";
        }
        if ($scope.insert_user.alternate_number != "") {
            var alternate_number = $scope.insert_user.alternate_number;
        } else {
            var alternate_number = "";
        }
        var role = "Technician";
        var formdata = new FormData();
        formdata.append("company_id", comp_id);
        formdata.append("ids", $scope.insert_user.id);
        formdata.append("employeeid", $scope.insert_user.employee_id);
        formdata.append("firstname", $scope.insert_user.first_name);
        formdata.append("lastname", $scope.insert_user.last_name);
        formdata.append("emailid", $scope.insert_user.email_id);
        formdata.append("contactnumber", $scope.insert_user.contact_number);
        formdata.append("alternatenumber", alternate_number);
        formdata.append("addresses", $scope.insert_user.address);
        formdata.append("user_proof", $scope.myFile);
        formdata.append("regions", $scope.insert_user.region);
        formdata.append("areas", $scope.insert_user.area);
        formdata.append("locations", $scope.insert_user.location);
        formdata.append("roles", role);
        formdata.append("support", $scope.insert_user.support);
        formdata.append("user_proof", user_proof);

        $url = 'insertuser';
        $method = 'POST';
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            //console.log(data.data);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#myModal').modal('hide');
                    $scope.loadData();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'User not Added, kindly try again';
            }
        });
    };

    $scope.insertuser = function() {
        if ($scope.insert_user.alternate_number != "") {
            var alternate_number = $scope.insert_user.alternate_number;
        } else {
            var alternate_number = "";
        }
        if ($scope.insert_user.role != "Technician") {
            var support = "";
        }

        if ($scope.myFile) {
            $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var user_proof = $scope.myFile;
            } else {
                toaster.pop({ type: "error", title: "Attached File should be an Image file" });
                return false;
            }
        } else {
            //console.log("working correctly");
            var user_proof = "";
            //console.log(user_proof);
        }

        var formdata = new FormData();
        formdata.append("company_id", comp_id);
        formdata.append("ids", $scope.insert_user.id);
        formdata.append("employeeid", $scope.insert_user.employee_id);
        formdata.append("firstname", $scope.insert_user.first_name);
        formdata.append("lastname", $scope.insert_user.last_name);
        formdata.append("emailid", $scope.insert_user.email_id);
        formdata.append("contactnumber", $scope.insert_user.contact_number);
        formdata.append("alternatenumber", alternate_number);
        formdata.append("addresses", $scope.insert_user.address);
        //formdata.append("user_proof",$scope.myFile);
        formdata.append("regions", $scope.insert_user.region);
        formdata.append("areas", $scope.insert_user.area);
        formdata.append("locations", $scope.insert_user.location);
        formdata.append("roles", $scope.insert_user.role);
        formdata.append("support", support);
        formdata.append("user_proof", user_proof);

        $url = 'insertuser';
        $method = 'POST';
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            //console.log(data.data);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#myModal').modal('hide');
                    $scope.loadData();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'User not Added, kindly try again';
            }
        });

        //console.log($scope.items);
        /* $scope.reset = function() {
           $scope.insert_user = angular.copy($scope.master);
         };
        $scope.reset();  */
    };

    $scope.function_techedituser = function() {
        $scope.national_file_ext = "";
        if ($scope.edit_user.alternate_number != "") {
            var alternate_number = $scope.edit_user.alternate_number;
        } else {
            var alternate_number = "";
        }

        //   if($scope.myproofFile){
        //     $scope.national_file_ext = angular.element(document.getElementById("tech_files")).val().toString().split('.').pop().toLowerCase();
        //       if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
        //           var user_proof = $scope.myproofFile;
        //         } 
        //       else {
        //          toaster.pop({ type: "error", title: "Attached File should be an Image file"});
        //          return false;
        //        }
        //  }
        //  else{
        //       var user_proof = "";
        //     }
        $url = 'insertuser';
        $method = 'POST';
        var role = "Technician";
        var fruits = $scope.edit_user.sup_name;
        if (Array.isArray(fruits)) {
            console.log($scope.edit_user.sup_name);
        } else {
            var fruits = $scope.edit_user.support;
        }
        $data = {
            "ids": $scope.edit_user.id,
            "employeeid": $scope.edit_user.employee_id,
            "firstname": $scope.edit_user.first_name,
            "lastname": $scope.edit_user.last_name,
            "emailid": $scope.edit_user.email_id,
            "contactnumber": $scope.edit_user.contact_number,
            "alternatenumber": $scope.edit_user.alternate_number,
            "addresses": $scope.edit_user.address,
            "regions": $scope.edit_user.region,
            "areas": $scope.edit_user.area,
            "locations": $scope.edit_user.location,
            "roles": role,
            "support": fruits
        };

        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {


            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#edit_modal').modal('hide');
                    $scope.loadData();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'User not Updated, kindly try again';
            }
        });
    };

    $scope.function_edituser = function() {
        alert("hello");
        //   if($scope.myFile){
        //     $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
        //       if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
        //           var user_proof = $scope.myFile;
        //         } 
        //       else {
        //          toaster.pop({ type: "error", title: "Attached File should be an Image file"});
        //          return false;
        //        }
        //  }
        //  else{
        //   var user_proof = "";
        // }
        if ($scope.edit_user.alternate_number != "") {
            var alternate_number = $scope.edit_user.alternate_number;
        } else {
            var alternate_number = "";
        }

        $url = 'insertuser';
        $method = 'POST';
        $data = {
            "ids": $scope.edit_user.id,
            "employeeid": $scope.edit_user.employee_id,
            "firstname": $scope.edit_user.first_name,
            "lastname": $scope.edit_user.last_name,
            "emailid": $scope.edit_user.email_id,
            "contactnumber": $scope.edit_user.contact_number,
            "alternatenumber": alternate_number,
            "addresses": $scope.edit_user.address,
            "regions": $scope.edit_user.region,
            "areas": $scope.edit_user.area,
            "locations": $scope.edit_user.location,
            "roles": $scope.edit_user.role,
            "support": ""
        };
        //$data = $('#insert_user').serialize();

        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {


            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#edit_modal').modal('hide');
                    //$scope.insert_user = {};
                    $scope.loadData();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'User not Added, kindly try again';
            }
        });
    }

    $scope.get_value = function() {
        var support = $scope.insert_user.support;
        alert(support);
    }

    $scope.edit_users = function(users) {
        $scope.edit_user = "";
        $input = users;
        $url = 'edit_users';
        $method = 'POST';
        $data = { 'input': $input };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.edit_user = data.data;
                // toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
            } else {
                $scope.error = 'Network error!! kindly try again';
            }
        });
        $('#edit_modal').modal('show');
    };

    $scope.change_intoselect = function() {
        $("#load_supportid").html('');

        var el = $(`<div class="form-group form-md-line-input" ng-class="{ 'has-error' : edits.support_names.$invalid && !edits.support_names.$pristine }">
                  <label class="col-md-3 control-label" for="form_control_1">Support Category</label>
                    <div class="col-md-9">
                        <select multiple class="form-control" name="support_names" ng-model="edit_user.sup_name" ng-options="load_sup.id as load_sup.support_category for load_sup in s_types" ng-dropdown required>
                        </select>
                        <p ng-show="edits.support_names.$invalid && !edits.support_names.$pristine" class="help-block">Support Category is required.</p>
                    </div>
               </div>`).appendTo("#load_supportid");;
        $compile(el)($scope);

    }

    $scope.load_supportids = function() {
        $("#load_supportids").html('');

        var el = $(`<div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_user.support_names.$invalid && !insert_user.support_names.$pristine }">
                  <label class="col-md-3 control-label" for="form_control_1">Support Category</label>
                    <div class="col-md-9">
                        <select multiple class="form-control" name="support_names" ng-model="insert_user.support" ng-options="sup.id as sup.support_category for sup in s_types" ng-dropdown required>
                        </select>
                        <p ng-show="insert_user.support_names.$invalid && !insert_user.support_names.$pristine" class="help-block">Support Category is required.</p>
                    </div>
               </div>`).appendTo("#load_supportids");;
        $compile(el)($scope);

    }

    $scope.view_users = function(users) {
        $scope.userForm = users;
        $('#view_user').modal('show');
    };

    $scope.delete_user = function(id) {
        swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this User!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function() {
            $url = 'delete_user';
            $method = 'POST';
            $data = { 'input': id };
            $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
                if (data['status'] == '200') {
                    if (data['data']['type'] == 'success') {
                        $scope.loadData();
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    } else {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    }

                } else {
                    toaster.pop({ type: "error", title: "Something went wrong!" });
                }
            });
        }, function(dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'User is safe :)',
                    'error'
                )
            }
        })
    };
    /*End of usermanagement page*/
    $scope.view_sample = function() {
        $('#sample_image').modal('show');
    };

    $scope.view_samplefile = function() {
        $('#sample_fileimage').modal('show');
    }

    $scope.clear_input = function() {
        angular.element("input[type='file']").val(null);
        $('#bulk_user').modal("hide");
    }

    $scope.clear_fileinput = function() {
        angular.element("input[type='file']").val(null);
        $('#bulk_user2').modal("hide");
    }

    $scope.clear_fields = function() {
            //var original = $scope.insert_user;
            //$scope.insert_user= angular.copy(original);
            //$scope.userForm.$setPristine();
            $scope.insert_user = {};
            //$scope.insert_user.$setPristine();
            $('#myModal').modal('hide');
        }
        /* bulk upload user */
    $scope.upload_userexcel = function() {

        var file = $scope.bulk_File;
        var formdata = new FormData();
        formdata.append('bulk_file', file);
        $url = "upload_userexcel";
        $method = "POST";
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            var number = data['data'].length;

            for (i = 0; i < number; i++) {
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        $('#bulk_user').modal('hide');
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "Something went wrong!" });
                }
            }
            // console.log(data['type']);
        });
    };
    /* end */
    /* bulk upload technicians */
    $scope.upload_techexcel = function() {
        var file = $scope.bulk_techFile;
        var formdata = new FormData();
        formdata.append('bulk_file', file);
        $url = "upload_techexcel";
        $method = "POST";
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            var number = data['data'].length;

            for (i = 0; i < number; i++) {
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        $('#bulk_user').modal('hide');
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "Something went wrong!" });
                }
            }
            // console.log(data['type']);
        });
    };

    $scope.empty_portlet = {
        "min-height": '7px !important'
    }
    $scope.display = {
        "display": 'none'
    }

    $scope.address1 = {
        name: '',
        place: '',
        components: {
            placeId: '',
            streetNumber: '',
            street: '',
            city: '',
            state: '',
            countryCode: '',
            country: '',
            postCode: '',
            district: '',
            location: {
                lat: '',
                long: ''
            }
        }
    };

    /* $scope.submitForm = function ($url, $method, $data) {
      
    }; */

}]);

// create your directive
/*myApp.directive('myDirective', function() {
    return {
      // angular passes the element reference to you
      compile: function(element) {
        $(element).DataTable();
      }
    }
});*/