myApp.controller('spare_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', '$timeout', '$routeParams', function ($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService, $timeout, $routeParams) {

    var status = $cookies.get('auth');
    status = JSON.parse(status);
    $scope.company_id = status.company_id;

    this.myDate = new Date();
    this.isOpen = false

    $scope.get_spare = function () {
        $url = "getspare";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                $scope.getspare = data.data;
            } else {
                $scope.getspare = data.data;
            }
        });
    };
    $scope.get_spare();

    $scope.get_supportmodel = function () {
        $url = "get_supportmodel";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {            
            if (data['status'] == '200') {
                $scope.get_supportmodel = data.data;
            } else {
                $scope.get_supportmodel = data.data;
            }
        });
    };
    $scope.get_supportmodel();  


    $scope.insertspare = function () {  
        if($scope.insert_spare.expiry_date=="")
        {
            var exp_date = "-";
        }
        else{
            var exp_date = $scope.insert_spare.expiry_date;
        }
        var file = $scope.insert_spare.spareimage;
        //var file = $scope.national_file;
        var formdata = new FormData();
        if($scope.insert_spare.id==""){
            formdata.append('id', $scope.insert_spare.id);
            formdata.append('spare_id', $scope.insert_spare.spare_id);
            formdata.append('sparecode', $scope.insert_spare.spare_code);
            formdata.append('sparename', $scope.insert_spare.spare_name);
            formdata.append('sparedesc', $scope.insert_spare.spare_desc);
            formdata.append('sparemodal', $scope.insert_spare.spare_modal);        
            formdata.append('sparelocation', $scope.insert_spare.spare_location);
            formdata.append('sparecategory', $scope.insert_spare.category);
            formdata.append('sparepurchase', $scope.insert_spare.p_date);
            formdata.append('spareexpairy', exp_date);
            formdata.append('spareprice', $scope.insert_spare.price);
            formdata.append('sparequantity', $scope.insert_spare.quantity_purchased);
            formdata.append('spareimage', $scope.insert_spare.image);        
            formdata.append('company_id', status.company_id);
            formdata.append('sparefile', file);
        }
        if($scope.insert_spare.id!=""){
            var fruits=$scope.insert_spare.category; 
            //console.log(fruits);
            if(!isNaN(fruits) && angular.isNumber(fruits)){
              var sparecategory= $scope.insert_spare.category;
            }
            else{
                var sparecategory = $scope.insert_spare.cat_id;
            }
            //return false;
            formdata.append('id', $scope.insert_spare.id);
            formdata.append('spare_id', $scope.insert_spare.spare_id);
            formdata.append('sparecode', $scope.insert_spare.spare_code);
            formdata.append('sparename', $scope.insert_spare.spare_name);
            formdata.append('sparedesc', $scope.insert_spare.spare_desc);
            formdata.append('sparemodal', $scope.insert_spare.spare_modal);        
            formdata.append('sparelocation', $scope.insert_spare.spare_location);
            formdata.append('sparecategory', sparecategory);
            formdata.append('sparepurchase', $scope.insert_spare.p_date);
            formdata.append('spareexpairy', exp_date);
            formdata.append('spareprice', $scope.insert_spare.price);
            formdata.append('sparequantity', $scope.insert_spare.quantity_purchased);
            formdata.append('spareimage', $scope.insert_spare.image);        
            formdata.append('company_id', status.company_id);
            formdata.append('sparefile', file);
        }        

        $url = "insertspare";
        $method = "POST";
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {            
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {  
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    angular.element(document.getElementById('spare')).modal('hide');                                      
                    $scope.reset();
                    $scope.get_spare();
                } else {
                    toaster.pop({ type: "error", title: data['data']['message'] });
                }
            } else {
                toaster.pop({ type: "error", title: data['data']['message'] });
            }
        });
    };   

    $scope.view_spare = function (image){
        $scope.image = image;
        angular.element(document.getElementById('view_image')).modal('show');
    };

    $scope.edit_spare = function(getspares){
        $scope.insert_spare = getspares;
        angular.element(document.getElementById('spare')).modal('show');
    };

    $scope.change_intoselect = function(){
        $("#load_supportid").html('');
    //     <select name='product' class="form-control" ng-model="insert_spare.category">
    //     <option ng-repeat="x in get_supportmodel" value="{{x.id}}">{{x.support_category}}</option>
    // </select>
        
        var el = $(` <select name="product" class="form-control" ng-model="insert_spare.category" ng-options="x.id as x.support_category for x in get_supportmodel" required>
                        <option value="" disabled>Select </option>
                    </select>  
                    <p ng-show="insert_spare.product.$invalid && !insert_spare.product.$pristine" class="help-block">Category is required.</p>`)
                .appendTo("#load_supportid");
        $compile(el)($scope);        
      }

    $scope.delete_spare = function(id){
        $url = "deletespare";
        $method = "POST";        
        $data = {'id' : id};
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.get_spare();
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                }
            } else {
                toaster.pop({ type: "error", title: "No Data Available" });
            }
        });
    }

    $scope.reset = function () {
        // Clean up scope before destorying
        $scope.insert_spare = {};
       // $scope.stage = "";
    };

}]);