myApp.controller('company_managementController', ['$scope', '$location', '$http', '$compile', 'toaster', '$window', 'myService', function($scope, $location, $http, $compile, toaster, $window, myService) {

    $scope.loadData = function() {
        var vm = this;
        var resetPaging = false;
        $http({
            method: 'GET',
            url: 'index.php/getcompanies',
        }).then(function(response) {
            $scope.companies = response.data;
        });
    };
    $scope.loadproject = function() {
        $url = "getproject";
        $method = "GET";
        $data = "";
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $scope.project = data.data.message;
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                }
            } else {
                toaster.pop({ type: "error", title: "No Data Available" });
            }
        });
    };
    $scope.loadproject();

    $scope.loadData();


    $scope.view_project = function(projects) {
        $scope.json_data = "";
        $scope.json_data = JSON.parse(projects.project_wbs);
        angular.element(document.getElementById("tree_sample")).jstree('destroy');
        var data = JSON.parse($scope.json_data);
        angular.element(document.getElementById("tree_sample")).jstree({
            core: {
                "data": data,
                "check_callback": true
            },
            plugins: ["themes", "json", "grid", "dnd", "contextmenu", "search", "json_data", "ui", "types", "data"],
            //plugins: ["themes","ui","json_data","json","grid","contextmenu"],
            grid: {
                columns: [
                    { width: 370, header: "Create WBS", title: "_DATA_", contextmenu: true },
                    {
                        width: 100,
                        cellClass: "col1",
                        value: "value1",
                        header: "Node Type",
                        valueClass: "spanclass"
                    },
                ],
                resizable: true,
                contextmenu: true
            },
            dnd: {
                drop_finish: function() {},
                drag_finish: function() {},
                drag_check: function(data) {
                    return {
                        after: true,
                        before: true,
                        inside: true
                    };
                }
            },
        });
        angular.element(document.getElementById("tree_sample")).jstree(true).redraw(true);
        var trees = $("#tree_sample");
        trees.bind("loaded.tree_sample", function(event, data) {
            trees.jstree("open_all");
            //tree.jstree("deselect_all");
        });
        angular.element(document.getElementById("view_projects")).modal('show');
    };

    $scope.companyadd_open = function() {
        //   alert("hello");
        $('#addcompany').modal('show');
    };
    $scope.insert_company = function() {
        $url = 'insertcompany';
        $method = 'POST';
        if ($scope.myFile) {
            $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var user_proof = $scope.myFile;
            } else {
                toaster.pop({ type: "error", title: "Attached File should be an Image file" });
                return false;
            }
        } else {
            //console.log("working correctly");
            var user_proof = "";
            //console.log(user_proof);
        }
        var formdata = new FormData();
        //  console.log("hello" + $scope.insert_customer.company_name);
        formdata.append("company_name", $scope.insert_customer.company_name);
        //  console.log(formdata);

        formdata.append("company_email", $scope.insert_customer.comp_email);
        formdata.append("company_number", $scope.insert_customer.company_number);
        formdata.append("alternate_number", $scope.insert_customer.altno);
        //  formdata.append("contactnumber", $scope.insert_customer.contact_number);
        //  formdata.append("alternatenumber", $scope.insert_customer.alternate_number);
        formdata.append("company_address", $scope.insert_customer.comp_address);
        formdata.append("user_proof", $scope.myFile);
        formdata.append("comp_adminid", $scope.insert_customer.employee_id);
        formdata.append("admin_fname", $scope.insert_customer.first_name);
        formdata.append("admin_lname", $scope.insert_customer.last_name);
        //  formdata.append("roles", "Admin");
        formdata.append("admin_emailid", $scope.insert_customer.email_id);
        // formdata.append("support", support);
        formdata.append("user_proof", user_proof);
        formdata.append("admin_contactno", $scope.insert_customer.admin_contact_number);
        formdata.append("admin_alternatecontactno", $scope.insert_customer.alternate_number);
        formdata.append("admin_address", $scope.insert_customer.address);
        formdata.append("admin_region", $scope.insert_customer.region);
        formdata.append("admin_area", $scope.insert_customer.area);
        formdata.append("admin_location", $scope.insert_customer.location);
        console.log($scope.insert_customer.admin_contact_number);
        $headers = { 'Content-Type': undefined };
        //  alert("hi");
        $data = formdata;
        // console.log($data);




        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            console.log(data.data);

            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $scope.insert_customer = {};
                    $('#addcompany').modal('hide');
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Company not Added, kindly try again';
            }
        });
    };

    $scope.model1_open = function() {
        $('#myModal').modal('show');
    };
    $scope.adminlist = [];
    $scope.getadminlist = function() {


            $http({
                method: 'POST',
                url: 'index.php/getuserlist'
            }).then(function(response) {
                $scope.user = response.data;
                console.log(response.data);
            });
        }
        // $scope.getadminlist();
    $scope.edit_users = function(users) {
        $scope.edit_user = "";
        $input = users;
        $url = 'admin_edit_users';
        $method = 'POST';
        $data = { 'input': $input };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.edit_user = data.data;
                // toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
            } else {
                $scope.error = 'Network error!! kindly try again';
            }
        });
        $('#edit_modal').modal('show');
    };



    $scope.function_editadminuser = function() {

        //   if($scope.myFile){
        //     $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
        //       if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
        //           var user_proof = $scope.myFile;
        //         } 
        //       else {
        //          toaster.pop({ type: "error", title: "Attached File should be an Image file"});
        //          return false;
        //        }
        //  }
        //  else{
        //   var user_proof = "";
        // }
        if ($scope.edit_user.alternate_number != "") {
            var alternate_number = $scope.edit_user.alternate_number;
        } else {
            var alternate_number = "";
        }

        $url = 'updateadminuser';
        $method = 'POST';
        $data = {
            "ids": $scope.edit_user.id,
            "employeeid": $scope.edit_user.employee_id,
            "firstname": $scope.edit_user.first_name,
            "lastname": $scope.edit_user.last_name,
            "emailid": $scope.edit_user.email_id,
            "contactnumber": $scope.edit_user.contact_number,
            "alternatenumber": alternate_number,
            "addresses": $scope.edit_user.address,
            "regions": $scope.edit_user.region,
            "areas": $scope.edit_user.area,
            "locations": $scope.edit_user.location,
            "roles": $scope.edit_user.role,
            "support": ""
        };
        //$data = $('#insert_user').serialize();

        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {


            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#edit_modal').modal('hide');
                    //$scope.insert_user = {};
                    $scope.getadminlist();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'User not Added, kindly try again';
            }
        });
    }
    $scope.deleteadminuser = function(id) {
        alert('hello');
        $url = 'deleteadminuser';
        $method = 'POST';
        $data = {
            "ids": id
        };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {


            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#edit_modal').modal('hide');
                    //$scope.insert_user = {};
                    $scope.getadminlist();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'User not Added, kindly try again';
            }
        });
    }

    $scope.getcompanyadminlist = function(selected_company) {
        $http({
            method: 'POST',
            url: 'index.php/getcompanybaseduserlist',
            data: { 'companyid': selected_company }
        }).then(function(response) {
            $scope.adminproject = response.data;
            console.log(response.data);
        });
    }
    $scope.getadminall = function() {
        $http({
            method: 'POST',
            url: 'index.php/getuserlist',
            data: { 'companyid': $scope.formParams.slctedCompany }
        }).then(function(response) {
            $scope.adminlist = response.data;
            console.log(response.data);
        });
    }
    $scope.insertuser = function() {
        if ($scope.insert_user.role != "Technician") {
            var support = "";
        }

        if ($scope.myFile) {
            $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var user_proof = $scope.myFile;
            } else {
                toaster.pop({ type: "error", title: "Attached File should be an Image file" });
                return false;
            }
        } else {
            //console.log("working correctly");
            var user_proof = "";
            //console.log(user_proof);
        }

        var formdata = new FormData();
        formdata.append("ids", $scope.insert_user.id);
        formdata.append("employeeid", $scope.insert_user.employee_id);
        formdata.append("firstname", $scope.insert_user.first_name);
        formdata.append("lastname", $scope.insert_user.last_name);
        formdata.append("emailid", $scope.insert_user.email_id);
        formdata.append("contactnumber", $scope.insert_user.contact_number);
        formdata.append("alternatenumber", $scope.insert_user.alternate_number);
        formdata.append("addresses", $scope.insert_user.address);
        formdata.append("user_proof", $scope.myFile);
        formdata.append("regions", $scope.insert_user.region);
        formdata.append("areas", $scope.insert_user.area);
        formdata.append("locations", $scope.insert_user.location);
        formdata.append("roles", "Admin");
        // formdata.append("project", $scope.insert_user.project_name);
        formdata.append("support", support);
        formdata.append("user_proof", user_proof);
        formdata.append("company_id", $scope.insert_user.slctedCompany);

        $url = 'insert_adminuser';
        $method = 'POST';
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            //console.log(data.data);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#myModal').modal('hide');
                    $scope.loadData();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                //$scope.error='User not Added, kindly try again';
                toaster.pop({ type: "error", title: 'User not Added, kindly try again' });
            }
        });

        //console.log($scope.items);
        /* $scope.reset = function() {
           $scope.insert_user = angular.copy($scope.master);
         };
        $scope.reset();  */
    };
    $scope.formParams = {};
    $scope.stage = "";
    $scope.formValidation = false;
    $scope.toggleJSONView = false;
    $scope.toggleFormErrorsView = false;

    $scope.formParams = {
        ccEmail: '',
        ccEmailList: []
    };
    $scope.next = function(tostage) {
        //$scope.direction = 1;
        //$scope.stage = stage;

        $scope.formValidation = true;
        console.log($scope.multiStepForm.$valid);
        console.log(tostage);
        // if ($scope.multiStepForm.$valid) {
        alert("hi");
        $scope.direction = 1;
        alert(tostage);
        $scope.stage = tostage;
        $scope.formValidation = false;
        //   }
    };
    $scope.back = function(stage) {
        $scope.direction = 0;
        $scope.stage = stage;
    };
    $scope.reset = function() {
        // Clean up scope before destorying
        $scope.formParams = {};
        $scope.stage = "";
    };
    $scope.init = function() {
        var data;
        data = [{
            text: $scope.formParams.pro_name,
            data: { value1: "Project", size: "30", spanclass: "root" },
            children: [{
                    text: "Phase-1",
                    data: { value1: "Phase", size: "50", spanclass: "first" },
                    children: [{
                            text: "Tower-1",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-1",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                    text: "Flat A1",
                                    data: { value1: "Flat", size: "50", spanclass: "third" },
                                    children: [
                                        { text: "Master Bedroom", data: { value1: "Area", size: "50", spanclass: "third" } }
                                    ]
                                }]
                            }]
                        },
                        {
                            text: "Tower-2",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-1",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                        text: "Flat A2",
                                        data: { value1: "Flat", size: "50", spanclass: "third" },
                                        children: [
                                            { text: "Balcony", data: { value1: "Area", size: "50", spanclass: "third" } }
                                        ]
                                    }

                                ]

                            }]

                        }

                    ],
                },
                {
                    text: "Phase-2",
                    data: { value1: "Phase", size: "50", spanclass: "first" },
                    children: [{
                            text: "Tower-1",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-1",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                    text: "Flat C1",
                                    data: { value1: "Flat", size: "50", spanclass: "third" },
                                    children: [
                                        { text: "Master Toilet", data: { value1: "Area", size: "50", spanclass: "third" } }
                                    ]
                                }]
                            }]
                        },
                        {
                            text: "Tower-2",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-2",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                        text: "Flat A2",
                                        data: { value1: "Flat", size: "50", spanclass: "third" },
                                        children: [
                                            { text: "Kitchen", data: { value1: "Area", size: "50", spanclass: "third" } }
                                        ]
                                    }

                                ]

                            }]

                        }

                    ],
                },
            ]
        }];

        angular.element(document.getElementById("jstree")).jstree({
            plugins: ["themes", "json", "grid", "dnd", "contextmenu", "search"],
            core: {
                "data": data,
                "check_callback": true
            },
            grid: {
                columns: [
                    { width: 370, header: "Create WBS", title: "_DATA_", contextmenu: true },
                    {
                        width: 100,
                        cellClass: "col1",
                        value: "value1",
                        header: "Node Type",
                        valueClass: "spanclass"
                    },
                ],
                resizable: true,
                contextmenu: true
            },
            dnd: {
                drop_finish: function() {},
                drag_finish: function() {},
                drag_check: function(data) {
                    return {
                        after: true,
                        before: true,
                        inside: true
                    };
                }
            },
            contextmenu: {
                'items': function(node) {
                    var tmp = $.jstree.defaults.contextmenu.items();
                    delete tmp.create.action;
                    tmp.create.label = "New";
                    tmp.create.submenu = {
                        "create_folder": {
                            "separator_after": true,
                            "label": "Block",
                            "action": function(data) {
                                var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);

                                bootbox.prompt("Please Enter Block to add", function(result) {
                                    if (result == '' || isNaN(result) == true) {
                                        toastr["error"]("Please Enter Numbers", "Warning");
                                    } else {
                                        var cfnum = parseInt(result);
                                        for (i = 1; i <= cfnum; i++) {
                                            inst.create_node(obj, { type: "default", text: "Newnode" + i }, "last", function() {
                                                setTimeout(function() {
                                                    inst.edit();
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    };

                    return tmp;
                }
            }
        });
        var tree = $("#jstree");
        tree.bind("loaded.jstree", function(event, data) {
            tree.jstree("open_all");
            //tree.jstree("deselect_all");
        });
    };
    $scope.saveWbs = function() {
        $("#jstree").jstree("deselect_all");
        var treejson = $("#jstree").jstree(true).get_json('#', { flat: true });
        treejson = JSON.stringify(treejson);
        treejson = treejson.replace(/\\/g, "");
        //treejson=treejson.serialize();
        $data = {
            'project_name': $scope.formParams.pro_name,
            'project_location': $scope.formParams.pro_location,
            // 'project_manager': $scope.formParams.pro_manager,
            'project_wbs': treejson,
            'selectedcompany': $scope.formParams.company_selected,
            'selectedadmin': $scope.formParams.slctedUser
        };

        $url = "insertproject";
        $method = "POST";
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $scope.reset();
                    //$scope.loadproject();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadproject();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadproject();
                }
            } else {
                toaster.pop({ type: "error", title: "Project not created. Kindly try again!" });
            }
        });

        //console.log($scope.formParams.pro_name);

    };
    $scope.upload_userexcel = function() {

        var file = $scope.bulk_File;
        var formdata = new FormData();
        formdata.append('bulk_file', file);
        $url = "upload_userexcel";
        $method = "POST";
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            var number = data['data'].length;

            for (i = 0; i < number; i++) {
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        $('#bulk_user').modal('hide');
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "Something went wrong!" });
                }
            }
            // console.log(data['type']);
        });
    };
}]);