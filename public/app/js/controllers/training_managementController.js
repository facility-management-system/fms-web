myApp.controller('training_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', '$timeout', function($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService, $timeout, $ngBootbox) {

    var status = $cookies.get('auth');
    status = JSON.parse(status);
    $scope.company_id = status.company_id;

    $scope.loadtraining = function() {
        $url = "gettraining";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.training = data.data;

            } else {
                $scope.training = data.data;

            }
        });
    };
    $scope.loadtraining();

    $scope.get_supportmodel = function() {
        $url = "get_supportmodel";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.get_supportmodel = data.data;
            } else {
                $scope.get_supportmodel = data.data;
            }
        });
    };
    $scope.get_supportmodel();

    $scope.loadassessment = function() {
        $url = "getassessment";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.assessment = data.data;
            } else {
                $scope.assessment = data.data;
            }
        });
    };
    $scope.loadassessment();


    $scope.inserttraining = function() {
        $scope.file = $scope.insert_trai.contentfile;
        $scope.trai_title = $scope.insert_trai.trai_title;
        $scope.trai_type = $scope.insert_trai.trai_type;
        $scope.trai_contenttype = $scope.insert_trai.trai_contenttype;
        $scope.productcategory = $scope.insert_trai.productcategory;

        if ($scope.file) {
            $scope.file_name = $scope.insert_trai.contentfile.name;
            $scope.file_type = $scope.file_name.split('.');
            $scope.file_type = $scope.file_type[1];
            console.log($scope.trai_contenttype);
            console.log($scope.file_type);
            var allowedfiletypes = [];
            if ($scope.trai_contenttype.toLowerCase() == 'pdf') {
                allowedfiletypes.push("pdf");
            } else {
                allowedfiletypes.push("mp4");
                allowedfiletypes.push("avi");
            }

            if (allowedfiletypes.indexOf($scope.file_type.toLowerCase()) < 0) {
                alert('Content Type and selected file is mismatched, Kindly select correct file!');
            } else {
                var formdata = new FormData();
                formdata.append('file', $scope.file);
                formdata.append('title', $scope.trai_title);
                formdata.append('type', $scope.trai_type);
                formdata.append('product', $scope.productcategory);
                formdata.append('content_type', $scope.trai_contenttype);
                formdata.append('company_id', $scope.company_id);

                $url = "uploadtraining";
                $method = "POST";
                $data = formdata;
                $headers = { 'Content-Type': undefined };
                $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
                    if (data['status'] == '200') {
                        if (data['data']['type'] == 'success') {
                            $scope.trai_title = "";
                            $scope.productcategory = "";
                            $scope.trai_contenttype = "";
                            $scope.file = "";
                            $('#training').modal('hide');
                            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                            $scope.loadtraining();
                        } else {
                            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                        }
                    } else {
                        $scope.error = 'Product Category is not inserted kindly try again';
                    }
                });
            }
        } else {
            alert('File is Mandatory');
        }
    }

    $scope.view = function(trainings) {
        var embedurl = "http://localhost:8080/FMS/public/assets/uploads/training/" + trainings.content;
        if (trainings.contenttype == 'pdf') {
            console.log(embedurl);
            $('#sla_confirm').modal('show');
            PDFObject.embed(embedurl, "#sla_detail_confirm");


            /* $scope.pdfUrl = trainings.content;
            angular.element(document.getElementById('sla_confirm')).modal('show'); */
        } else {
            $('#sla_confirm').modal('show');
            $('#sla_detail_confirm').append('<video width="100%" height="240" autoplay controls><source src="' + embedurl + '" type="video/mp4">Your browser does not support the video tag.</video>')
        }
        //$('#sla_confirm').modal('show');
    }

    $scope.edit_training = function(edit) {
        $scope.edit_trai = edit;
        angular.element(document.getElementById('edit_training')).modal('show');
    }
    $scope.delete_training = function(deletes) {

        $url = "deletetraining";
        $method = "POST";
        $data = { 'id': deletes };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadtraining();
                    angular.element(document.getElementById('edit_training')).modal('hide');
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went Wrong, Kindly Try again!';
            }
        });
    }
    $scope.updatetraining = function() {
        $scope.trai_title = $scope.edit_trai.title;
        $scope.trai_type = $scope.edit_trai.type;
        $scope.trai_contenttype = $scope.edit_trai.contenttype;
        $scope.productcategory = $scope.edit_trai.productcategory;
        $scope.id = $scope.edit_trai.id;
        $scope.training_id = $scope.edit_trai.training_id;
        $scope.file = $scope.edit_trai.contentfile;
        if ($scope.file) {
            $scope.file_name = $scope.edit_trai.contentfile.name;
            $scope.file_type = $scope.file_name.split('.');
            $scope.file_type = $scope.file_type[1];
            if ($scope.trai_contenttype != $scope.file_type) {
                alert('Content Type and selected file is mismatched, Kindly select correct file!');
            } else {
                var formdata = new FormData();
                formdata.append('file', $scope.file);
                formdata.append('title', $scope.trai_title);
                formdata.append('type', $scope.trai_type);
                formdata.append('product', $scope.productcategory);
                formdata.append('content_type', $scope.trai_contenttype);
                formdata.append('id', $scope.id);
                formdata.append('training_id', $scope.training_id);
                formdata.append('company_id', $scope.company_id);
                $url = "edittraining";
                $method = "POST";
                $data = formdata;
                $headers = { 'Content-Type': undefined };
                $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
                    if (data['status'] == '200') {
                        if (data['data']['type'] == 'success') {
                            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                            $scope.loadtraining();
                            angular.element(document.getElementById('edit_training')).modal('hide');
                        } else {
                            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                        }
                    } else {
                        $scope.error = 'Something went Wrong, Kindly Try again!';
                    }
                });
            }
        } else {
            var formdata = new FormData();
            formdata.append('file', '');
            formdata.append('title', $scope.trai_title);
            formdata.append('type', $scope.trai_type);
            formdata.append('product', $scope.productcategory);
            formdata.append('content_type', $scope.trai_contenttype);
            formdata.append('id', $scope.id);
            formdata.append('training_id', $scope.training_id);
            formdata.append('company_id', $scope.company_id);
            $url = "edittraining";
            $url = "edittraining";
            $method = "POST";
            $data = formdata;
            $headers = { 'Content-Type': undefined };
            $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
                if (data['status'] == '200') {
                    if (data['data']['type'] == 'success') {
                        $('#training').modal('hide');
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                        $scope.loadtraining();
                        angular.element(document.getElementById('edit_training')).modal('hide');
                    } else {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    }
                } else {
                    $scope.error = 'Something went Wrong, Kindly Try again!';
                }
            });
        }
    }

    $scope.insertassessment = function() {

        $scope.assess_title = $scope.insert_assessment.assess_title;
        $scope.assess_type = $scope.insert_assessment.assess_type;
        $scope.productcategory = $scope.insert_assessment.productcategory;

        var formdata = new FormData();
        formdata.append('assess_title', $scope.assess_title);
        formdata.append('assess_type', $scope.assess_type);
        formdata.append('assess_product', $scope.productcategory);
        formdata.append('company_id', $scope.company_id);

        $url = "uploadassessment";
        $method = "POST";
        $data = formdata;
        $headers = { 'Content-Type': undefined };
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#assesssment').modal('hide');
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadassessment();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Product Category is not inserted kindly try again';
            }
        });

    }
    $scope.edit_assessment = function(edit) {
        $scope.edit_assess = edit;
        angular.element(document.getElementById('edit_assessment')).modal('show');
    }
    $scope.updateassessment = function() {

        $scope.assess_title = $scope.edit_assess.title;
        $scope.assessment_id = $scope.edit_assess.assessment_id;
        $scope.id = $scope.edit_assess.id;
        $scope.assess_type = $scope.edit_assess.type;
        $scope.productcategory = $scope.edit_assess.productcategory;

        var formdata = new FormData();
        formdata.append('id', $scope.id);
        formdata.append('assessment_id', $scope.assessment_id);
        formdata.append('title', $scope.assess_title);
        formdata.append('type', $scope.assess_type);
        formdata.append('product', $scope.productcategory);
        formdata.append('company_id', $scope.company_id);

        $url = "editassessment";
        $method = "POST";
        $data = formdata;
        $headers = { 'Content-Type': undefined };
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadtraining();
                    angular.element(document.getElementById('edit_assessment')).modal('hide');
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went Wrong, Kindly Try again!';
            }
        });
    }
    $scope.delete_assessment = function(deletes) {
        $url = "deleteassessment";
        $method = "POST";
        $data = { 'id': deletes };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadassessment();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went Wrong, Kindly Try again!';
            }
        });
    }

    $scope.add_quizs = function() {
        $url = "deleteassessment";
        $method = "POST";
        $data = { 'id': deletes };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadassessment();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went Wrong, Kindly Try again!';
            }
        });
    };

    /* function add_quizs() {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('controller_admin/check_assesment');?>",
            data: { 'id': sParameterNames },
            datatype: "JSON",
            cache: false,
            success: function (data) {
                var data = JSON.parse(data);
                //console.log(data);
                //$('#assessments').val(data['certificate_name']);
                $('.trainings').val(data['certificate_id']);
                $.ajax({
                    url: "<?php echo base_url();?>index.php?/Controller_admin/quiz_check",
                    type: "POST",
                    data: "",
                    cache: false,
                    success: function (data) {
                        console.log(data);
                        $('#quiz_id').val(data);
                    },
                })
                $('#myquiz').modal('show');
            }
        });
    } */
}]);