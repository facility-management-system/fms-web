myApp.controller('navController', ['$scope', '$cookies', '$location', 'userModel', function($scope, $cookies, $location, userModel) {
    var status = $cookies.get('auth');
    console.log(status);
    status = JSON.parse(status);

    if (status.user_type == 0) {
        angular.extend($scope, {
            navUrl: [{
                link: 'Super Admin Dashboard',
                url: '/sa_graphs',
                icon: 'icon-home'
            }, {
                link: 'Company Creation',
                url: 'javascript:void(0)',
                icon: 'icon-book-open',
                subMenu: [{
                    link: 'Project Creation',
                    url: '/superadm_project',
                    icon: 'fa fa-user'
                }, {
                    link: 'Map Projects',
                    url: '/sa_graphs',
                    icon: 'fa fa-wrench'
                }, {
                    link: 'Company',
                    url: 'company_creation',
                    icon: 'fa fa-user',
                }],
            }, {
                link: 'Admin List',
                url: '/admin_list',
                icon: 'icon-book-open'
            }]
        });
    }
    if (status.user_type == 1) {
        angular.extend($scope, {
            user: userModel.getUserObject(),
            navUrl: [{
                    link: 'Dashboard',
                    url: '/dashboard',
                    icon: 'fa fa-desktop',
                }, {
                    link: 'M-Social',
                    url: 'javascript:void(0)',
                    icon: 'fa fa-home',
                    subMenu: [{
                        link: 'News Feeds',
                        url: '/msocial_feeds',
                        icon: 'fa fa-user'
                    }, {
                        link: 'New Posts',
                        url: '/msocial_page',
                        icon: 'fa fa-wrench'
                    }],
                }, {
                    link: 'User Management',
                    url: 'javascript:void(0)',
                    icon: 'fa fa-users',
                    subMenu: [{
                        link: 'User Roles',
                        url: '/user_role',
                        icon: 'fa fa-user'
                    }, {
                        link: 'User',
                        url: '/user',
                        icon: 'fa fa-user'
                    }, {
                        link: 'Technician',
                        url: '/technician',
                        icon: 'fa fa-wrench'
                    }],
                }, {
                    link: 'Project Planning',
                    url: '/new_project',
                    icon: 'fa fa-book'
                }, {
                    link: 'Training And Certification',
                    url: 'javascript:void(0)',
                    icon: 'fa fa-wrench',
                    subMenu: [{
                        link: 'Training',
                        url: '/training',
                        icon: 'fa fa-wrench'
                    }, {
                        link: 'Assessment',
                        url: '/assessment',
                        icon: 'fa fa-wrench'
                    }],
                }, {
                    link: 'Contract Master',
                    url: 'javascript:void(0)',
                    icon: 'fa fa-link',
                    subMenu: [{
                        link: 'Contract Category',
                        url: '/contract_category',
                        icon: 'fa fa-user'
                    }, {
                        link: 'Support Category',
                        url: '/support_category',
                        icon: 'fa fa-wrench'
                    }, {
                        link: 'Contract Types',
                        url: '/contracttypes',
                        icon: 'fa fa-wrench'
                    }, {
                        link: 'Maintenance Types',
                        url: '/maintenance',
                        icon: 'fa fa-wrench'
                    }],
                }, {
                    link: 'Spare Management',
                    url: '/spare',
                    icon: 'fa fa-wrench'
                },
                /*{
                                    link:'Customer Management',
                                    url:'/customer',
                                    icon:'fa fa-user'
                                },*/
                {
                    link: 'Customer Management',
                    url: 'javascript:void(0)',
                    icon: 'fa fa-users',
                    subMenu: [{
                        link: 'Manage Owner',
                        url: '/customer_owner',
                        icon: 'fa fa-user'
                    }, {
                        link: 'Manage Customer',
                        url: '/manage_customer',
                        icon: 'fa fa-user'
                    }],
                },
                {
                    link: 'Visitor Management',
                    url: '/visitor',
                    icon: 'fa fa-user'
                }, {
                    link: 'Car Management',
                    url: '/carparking',
                    icon: 'fa fa-user'
                }
            ]
        });
    } else if (status.user_type == 5) {
        angular.extend($scope, {
            user: userModel.getUserObject(),
            navUrl: [{
                link: 'Dashboard',
                url: '/dashboard',
                icon: 'fa fa-desktop',
            }, {
                link: 'M-Social',
                url: '/msocial_feeds',
                icon: 'icon-home',
                subMenu: [{
                    link: 'News Feeds',
                    url: '/msocial_feeds',
                    icon: 'fa fa-user'
                }, {
                    link: 'New Posts',
                    url: '/msocial_page',
                    icon: 'fa fa-wrench'
                }],
            }, {
                link: 'Tickets',
                url: '/ticket',
                icon: 'icon-home'
            }, {
                link: 'Profile',
                url: '/dashboard',
                icon: 'icon-book-open'
            }, {
                link: 'Payment',
                url: '/dashboard',
                icon: 'icon-book-open'
            }]
        });
    } else if (status.user_type == 4) {
        angular.extend($scope, {
            user: userModel.getUserObject(),
            navUrl: [{
                link: 'Dashboard',
                url: '/dashboard',
                icon: 'icon-home'
            }, {
                link: 'Tickets',
                url: '/ticket',
                icon: 'fa fa-ticket'
            }, {
                link: 'Contracts',
                url: '/ticket',
                icon: 'fa fa-ticket'
            }, {
                link: 'Knowledge Base',
                url: 'javascript:void(0)',
                icon: 'fa fa-link',
                subMenu: [{
                    link: 'Add Knowledge Base',
                    url: '/add_knowledge',
                    icon: 'fa fa-user'
                }, {
                    link: 'Awaiting Approval',
                    url: '/awaiting_approve',
                    icon: 'fa fa-wrench'
                }, {
                    link: 'Approved Knowledge Base',
                    url: '/knowledgebase',
                    icon: 'fa fa-wrench'
                }],
            }]
        });
    } else if (status.user_type == 2) {
        angular.extend($scope, {
            user: userModel.getUserObject(),
            navUrl: [{
                link: 'Dashboard',
                url: '/dashboard',
                icon: 'icon-home'
            }, {
                link: 'Project Planning',
                url: '/company_creation',
                icon: 'icon-book-open'
            }, {
                link: 'Tickets List',
                url: 'javascript:void(0)',
                icon: 'icon-book-open',
                subMenu: [{
                    link: 'New Tickets',
                    url: '/man_newtickets',
                    icon: 'fa fa-user'
                }, {
                    link: 'In Progress Tickets',
                    url: '/man_inprogress',
                    icon: 'fa fa-wrench'
                }, {
                    link: 'Completed Tickets',
                    url: '/man_completed',
                    icon: 'fa fa-wrench'
                }],
            }]
        });

    }
    angular.extend($scope, {
        doLogout: function() {
            $cookies.remove('auth');
            $location.path('/');
        },
        checkActiveLink: function(routeLink) {
            if ($location.path() == routeLink) {
                return 'active open';
            }
        }
    });

    $scope.add_active = function(event) {
        //angular.element(document.getElementsByClassName('xn-openable')).removeClass('active open');
        $(event.target).parent().addClass('active');
    };

    $scope.add_subactive = function(event) {
        //angular.element(document.getElementsByClassName('xn-openable')).addClass('active');
    };

}]);