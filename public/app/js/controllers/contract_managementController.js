myApp.controller('contract_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', function ($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService) {
    var status = $cookies.get('auth');
    status = JSON.parse(status);
	console.log(status);
	var comp_id = status['company_id'];
	console.log(comp_id);
    /*For Customermanagement */ 

    $scope.loadConttype = function () {
        var vm = this;
        var resetPaging = false;
        $url = 'loadConttype';
        $method = 'POST';
        $data = {"comp_id":comp_id};        
        $scope.welcome = myService.submitForm($url, $method, $data).then(function (data){
            console.log(data);
            if (data['status'] == '200') {
                $scope.contract = data.data;
            } else {
                $scope.contract = data.data;
            }
        });
    };
    $scope.loadConttype(); 

    $scope.loadSupporttype = function () {
        var vm = this;
        var resetPaging = false;
        $url = 'loadSupporttype';
        $method = 'POST';
        $data = {"comp_id":comp_id};      
        $scope.welcome = myService.submitForm($url, $method, $data).then(function (data){
            console.log(data);
            if (data['status'] == '200') {
                $scope.support_type = data.data;
            } else {
                $scope.support_type = data.data;
            }
        });
    };
    $scope.loadSupporttype(); 

    $scope.loadMaintaintype = function () {
        var vm = this;
        var resetPaging = false;
        $url = 'loadMaintaintype';
        $method = 'POST';
        $data = {"comp_id":comp_id};         
        $scope.welcome = myService.submitForm($url, $method, $data).then(function (data){
            console.log(data);
            if (data['status'] == '200') {
                $scope.maintain_type = data.data;
            } else {
                $scope.maintain_type = data.data;
            }
        });
    };
    $scope.loadMaintaintype(); 

    $scope.loadContracttype = function () {
        var vm = this;
        var resetPaging = false;
        $url = 'loadContracttype';
        $method = 'POST';
        $data = {"comp_id":comp_id};         
        $scope.welcome = myService.submitForm($url, $method, $data).then(function (data){
            console.log(data);
            if (data['status'] == '200') {
                $scope.contract_data = data.data;
            } else {
                $scope.contract_data = data.data;
            }
        });
    };
    $scope.loadContracttype(); 

    $scope.model1_open = function () {
        $('#myModal').modal('show');
    };
    $scope.view_example = function (){
        $('#sample_user').modal('show');
    }
    $scope.contract_type = function (){
        $('#image_conttype').modal('show');
    }
    $scope.maintanance_type = function (){
        $('#image_maintain').modal('show');
    }
    $scope.support_type = function (){
        $('#image_support').modal('show');
    }

    $scope.insertcategorytype = function () {   
         $answer=this.user['category'];
        console.log($answer);
        //alert($answer);
        //console.log($values);
        $url = 'insertcattype';
        $method = 'POST';
        $data={'value':$answer};
        //$data =$('#welcome').serialize();
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            console.log(data['data']);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadConttype();
            } else {
                $scope.error = 'Not Inserted! kindly try again';
            }
        });
    };

    $scope.insertsup_category = function () { 
        $input=this.sup_cat['support_typ'];
        // alert($input);
        // return false;
        $url = 'insertsup_category';
        $method = 'POST';
        $data ={'input':$input}; 
        //$data =$('#welcome').serialize();
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            console.log(data['data']);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadSupporttype(); 
                
            } else {
                $scope.error = 'Not Inserted! kindly try again';
            }
        });
    };

    // $scope.insert_maintanance = function () {
    //     $input=this.sup_cat['support_typ'];
    //     $input=this.sup_cat['support_typ'];
    //     $url = 'insert_maintanance';
    //     $method = 'POST';
    //     $data ={'input':$input}; 
    //     //$data =$('#welcome').serialize();
    //     $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
    //         console.log(data['data']);
    //         if (data['status'] == '200') {
    //             if (data['data']['type'] == 'success') {
    //                 toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
    //             } else {
    //                 toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
    //             }
    //             $scope.loadSupporttype(); 
                
    //         } else {
    //             $scope.error = 'Not Inserted! kindly try again';
    //         }
    //     });
    // }

    $scope.insert_maintaintype = function () { 
        $choose=this.insert['contracttype'];  
        $type=this.insert['maintanance_type'];
        //alert($type);
        $url = 'insert_maintaintype';
        $method = 'POST';
        $data ={'input':$type,'cont':$choose}; 
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            console.log(data['data']);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadMaintaintype(); 
                
            } else {
                $scope.error = 'Not Inserted! kindly try again';
            }
        });

    };

    $scope.insert_conttype = function () {
        $choose=this.insert_1['contracttype'];  
        $type=this.insert_1['contract_name'];
        //alert($type);
        $url = 'insert_conttype';
        $method = 'POST';
        $data ={'input':$type,'cont':$choose}; 
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            console.log(data['data']);
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadContracttype(); 

                
            } else {
                $scope.error = 'Not Inserted! kindly try again';
            }
        });
    };

    $scope.edit_contract = function (cont){
        $scope.insert_customer = cont;
        $('#myModal').modal('show');
    };
    $scope.update_contract = function () {
        $id = $scope.insert_customer.id;
        $customer_type = $scope.insert_customer.cat_name;
        $url = 'update_contract';
        $method = 'POST';
        $data = { 
                    'id': $id,
                    'cat_name': $customer_type
                };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $('#myModal').modal('hide');
                    $scope.loadConttype();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                
            } else {
                toaster.pop({ type: 'error', title: "User not Updated! Kindly try again" });
            }
        });
    };

    $scope.delete_owner = function (id){
        $url = 'deleteownertype';
        $method = 'POST';
        $data = { 'id': id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadData();
            } else {
                $scope.error = 'User not inserted kindly try again';
            }
        });
    }

    $scope.edit_suptype = function (sup_types){
        $scope.edit_support = sup_types;
        $('#support_modal').modal('show');
    };

    $scope.update_support = function () {
        $id = $scope.edit_support.idsup;
        $customer_type = $scope.edit_support.support_category;
        $url = 'update_support';
        $method = 'POST';
        $data = { 
                    'id': $id,
                    'sup_cat': $customer_type
                };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $('#myModal').modal('hide');
                    $scope.loadSupporttype();
                } 
                else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                
            } else {
                toaster.pop({ type: 'error', title: "Not Updated! Kindly try again" });
            }
        });
    };

    $scope.editcont_type = function (cont){
        $scope.update_cont = cont;
        $('#edit_conttype').modal('show');
    };

    $scope.load_contcats = function (){
        $("#change_onclick").html('');
        var el = $(` <div class="col-md-7 form-group form-md-line-input" ng-class="{ 'has-error' : update_cont.edit_contcat.$invalid && !update_cont.edit_contcat.$pristine }" ng-focus="load_contcats()">
                                <select class="form-control col-md-12" name="edit_contcat" ng-model="update_cont.cat_name" ng-options="cont_cat.id as cont_cat.cat_name for cont_cat in contract" ng-dropdown ng-change="update_cont.dyn_id=update_cont.cat_name" required>
                                <p ng-show="update_cont.edit_contcat.$invalid && !update_cont.edit_contcat.$pristine" class="help-block">Category is required.</p>
                                 </select>
                           
                    </div>`).appendTo("#change_onclick");

            $compile(el)($scope);
    }

    $scope.update_conttype = function () {
        $id = $scope.update_cont.id;
        $cat_name = $scope.update_cont.cat_name;
        $cont_id = $scope.update_cont.cont_cat;
        $cont_type = $scope.update_cont.contract_type;
        var dyn_id = $scope.update_cont.dyn_id;
        if(!$scope.update_cont.dyn_id){
            toaster.pop({ type: 'error', title: "No Changes made!" });
        }
        else{
            $url = 'update_contracttype';
            $method = 'POST';
            $data = { 
                        'id': $id,
                        'dyn_id': dyn_id,
                        'cont_id': $cont_id,
                        'cont_type': $cont_type,
                        'cat_name': $cat_name
                    };
            $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
                if (data['status'] == '200') {
                    if (data['data']['type'] == 'success') {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                        $('#edit_conttype').modal('hide');
                        $scope.loadContracttype();
                    } else {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    }
                    
                } else {
                    toaster.pop({ type: 'error', title: "User not Updated! Kindly try again" });
                }
            });
        }
        
    };

    $scope.edit_maintanance = function (mtype){
        $scope.maintain_data = mtype;
        $("#maintain_modal").modal('show');
    }

    $scope.load_mcontcats = function (){
        $("#change_onclick1").html('');
        var el = $(` <div class="col-md-7 form-group form-md-line-input" ng-class="{ 'has-error' : maintain_data.m_contcat.$invalid && !maintain_data.m_contcat.$pristine }" ng-focus="load_mcontcats()">
                                <select class="form-control col-md-12" name="m_contcat" ng-model="maintain_data.cat_name" ng-options="cont_cat.id as cont_cat.cat_name for cont_cat in contract" ng-dropdown ng-change="maintain_data.dyn_mid=maintain_data.cat_name" required>
                                <p ng-show="maintain_data.m_contcat.$invalid && !maintain_data.m_contcat.$pristine" class="help-block">Category is required.</p>
                                 </select>
                           
                    </div>`).appendTo("#change_onclick1");

            $compile(el)($scope);
    }
    
    $scope.update_maintain = function () {
        $id = $scope.maintain_data.id;
        $cat_name = $scope.maintain_data.cat_name;
        $cont_id = $scope.maintain_data.cont_cat;
        $cont_type = $scope.maintain_data.maintainance;
        var dyn_id = $scope.maintain_data.dyn_mid;
        if(!$scope.maintain_data.dyn_mid){
            toaster.pop({ type: 'error', title: "No Changes made!" });
        }
        else{
            
            $url = 'update_maintaintype';
            $method = 'POST';
            $data = { 
                        'id': $id,
                        'dyn_mid': dyn_id,
                        'cont_id': $cont_id,
                        'm_type': $cont_type,
                        'cat_name': $cat_name
                    };
            $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
                if (data['status'] == '200') {
                    if (data['data']['type'] == 'success') {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                            $('#maintain_modal').modal('hide');
                            $scope.loadMaintaintype();
                    } else {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    }
                    
                } else {
                    toaster.pop({ type: 'error', title: "User not Updated! Kindly try again" });
                }
            });
        }
        
    };

    $scope.delete_contract = function (cid){
       // $("#sample_modal").modal('show');
        $url = 'delete_contract';
        $method = 'POST';
        $data = { 'id': cid };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadConttype();
            } else {
                $scope.error = 'kindly try again';
            }
        }); 
    }

    $scope.delete_conttype = function (ctype_id){
        $url = 'delete_conttype';
        $method = 'POST';
        $data = { 'id': ctype_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadContracttype(); 
            } else {
                $scope.error = 'kindly try again';
            }
        });
    }

    $scope.delete_mtype = function (mid){
        $url = 'delete_mtype';
        $method = 'POST';
        $data = { 'id': mid };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadMaintaintype(); 
            } else {
                $scope.error = 'kindly try again';
            }
        });
    }

    $scope.delete_suptype = function (sid){
        $url = 'delete_suptype';
        $method = 'POST';
        $data = { 'id': sid };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadSupporttype(); 
            } else {
                $scope.error = 'kindly try again';
            }
        });
    }

    $scope.Import_excel = function (){
        var file = $scope.myFile;
        var formdata = new FormData();
        formdata.append('contract_file', file); 
        $url = "import_contract";
        $method = "POST";       
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
            $scope.result=data['data'];
            var number = data['data'].length;
            
           for(i=0;i<number;i++)
            {   
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                } 
            }
             
           // console.log(data['type']);
        }); 

    };

    $scope.upload_excel = function (){
        var file = $scope.mycont_File;
        var formdata = new FormData();
        formdata.append('contract_type', file); 
        $url = "upload_excel";
        $method = "POST";       
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
            var number = data['data'].length;
            
           for(i=0;i<number;i++)
            {   
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                } 
            }
           // console.log(data['type']);
        }); 

    };
    
    $scope.upload_maintype = function (){
        var file = $scope.main_File;
        var formdata = new FormData();
        formdata.append('main_type', file); 
        $url = "upload_maintype";
        $method = "POST";       
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
            var number = data['data'].length;
            
           for(i=0;i<number;i++)
            {   
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                } 
            }
           // console.log(data['type']);
        }); 

    };

    $scope.upload_supexcel = function (){
        var file = $scope.sup_File;
        var formdata = new FormData();
        formdata.append('support_file', file); 
        $url = "upload_supexcel";
        $method = "POST";       
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
            var number = data['data'].length;
            
           for(i=0;i<number;i++)
            {   
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "Something went wrong!" });
                } 
            }
           // console.log(data['type']);
        }); 
    };

}]);

// create your directive
/*myApp.directive('myDirective', function() {
    return {
      // angular passes the element reference to you
      compile: function(element) {
        $(element).DataTable();
      }
    }
});*/