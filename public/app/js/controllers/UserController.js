myApp.controller('UserController',['$scope', '$location', 'userModel', function($scope, $location, userModel){
    /*$scope.bgUrl = 'http://www.publicdomainpictures.net/pictures/80000/velka/blur-background.jpg';*/
	$scope.bgUrl = 'assets/img/backgrounds/login-backgrounds.jpg';
    angular.extend($scope, {
        doLogin: function(loginForm){
            var formData = {
                'email'    : $scope.user.email,
                'password'    : $scope.user.password
            };    
            userModel.doLogin(formData).then(function(){
                $location.path('/dashboard');
            })
        },
        doLogout: function() {
            userModel.doUserLogout();
            $location.path('/');
        }
    });
}]);

myApp.directive('bg', function () {
    return {
		link: function(scope, element, attrs) {
		  if(scope.bgUrl){
		    element.css("background","url("+scope.bgUrl+")")
			element.css("background-size","cover")
			element.css("background-repeat","no-repeat")			
		  } else {
		    element.addClass("bg-default")	    
		  }
		}
	}
});
myApp.directive('setHeight', function($window){
  return{
    link: function(scope, element, attrs){
        element.css('min-height', window.innerHeight + 'px');
        //element.height($window.innerHeight/3);
    }
  }
});
