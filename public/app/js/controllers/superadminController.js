myApp.controller('superadminController',['$scope', '$location', function($scope,$location){    
        angular.extend($scope, {
            navUrls: [{
                link: 'Super Admin Dashboard',
                url: '/sa_graphs',
                icon: 'icon-home'
            },{
                link: 'Company Creation',
                url:'javascript:void(0)',
                icon: 'icon-book-open',
                subMenu: [{
                    link: 'Project Creation',
                    url: '/superadm_project',
                    icon: 'fa fa-user'
                }, {
                    link: 'Map Projects',
                    url: '/sa_graphs',
                    icon: 'fa fa-wrench'
                },{
                  link: 'Company',
                  url: 'company_creation',
                  icon: 'fa fa-user',
                }],
            }, {
                link: 'Admin List',
                url: '/admin_list',
                icon: 'icon-book-open'
            }]
        });
    

    angular.extend($scope, {
       
        checkActiveLink:function(routeLink){
            if ($location.path() == routeLink) {
                return 'active open';
            }
        } 
    });

    $scope.add_active = function(event){
        //angular.element(document.getElementsByClassName('xn-openable')).removeClass('active open');
        $(event.target).parent().addClass('active');
    };

    $scope.add_subactive = function(event){
        //angular.element(document.getElementsByClassName('xn-openable')).addClass('active');
    };

}]);
