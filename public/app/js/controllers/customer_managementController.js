myApp.controller('customer_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', function($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService) {
    var status = $cookies.get('auth');
    var selected_node = "";
    var newValue, oldValue = "";
    var assigned_prjid = "";
    var assigned_prjname = "";
    var assigned_prjdetails = Array();

    status = JSON.parse(status);
    var comp_id = status['company_id'];
    /*For Customermanagement */
    $scope.loadData = function() {
        var vm = this;
        var resetPaging = false;
        $url = 'getcustomer';
        $method = 'POST';
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.customer = data.data;
            } else {
                $scope.customer = data.data;
            }
        });
    };
    $scope.loadData();
    $scope.loadassignedproject = function() {
        $url = 'get_assignedproject';
        $method = 'POST';

        $data = { 'company_id': '' };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            console.log(data.data);
            assigned_prjid = data.data['projectid'];
            assigned_prjname = data.data['projectname'];

            $scope.insert_customer.project_name = assigned_prjname;
            $scope.insert_customer.project_id = assigned_prjid;

            assigned_prjdetails = data.data['project_details'];

            // console.log('name=' + data.data['projectid'])
            //assigned_prjid=
        });
    }
    $scope.loadassignedproject();
    $scope.getcustomer = function() {

        var vm = this;
        var resetPaging = false;
        $url = 'get_customer';
        $method = 'POST';
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            console.log(data.data);
            if (data['status'] == '200') {
                $scope.get_customer = data.data;
            } else {
                $scope.get_customer = data.data;
            }
        });
    };
    $scope.getcustomer();

    $scope.getownertype = function() {
        var vm = this;
        var resetPaging = false;
        $url = 'getownertype';
        $method = 'POST';
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            console.log(data.data);
            if (data['status'] == '200') {
                $scope.get_customer = data.data;
            } else {
                $scope.get_customer = data.data;
            }
        });
    };
    $scope.getcustomer();


    $scope.get_project = function() {
        $scope.names = ["Emil", "Tobias", "Linus"];
        $url = 'getproject';
        $method = 'GET';
        $data = "";
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                $scope.project_detail = data.data.message;
            } else {
                //angular.element(document.getElementById("project_name")).append("No Projects");
            }
        });
    };
    $scope.get_project();

    $scope.model1_open = function() {
        $('#myModal').modal('show');
    };

    $scope.insertownertype = function() {
        $values = $scope.insert.ownertype;
        $url = 'insertownertype';
        $method = 'POST';
        $data = { 'owner_types': $values };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadData();
            } else {
                $scope.error = 'User not inserted kindly try again';
            }
        });
    };
    $scope.edit_owner = function(customers) {
        $scope.edit_ownertypes = customers;
        console.log(customers);
        $('#myModal').modal('show');
    };
    $scope.updateownertype = function() {
        $id = $scope.edit_ownertypes.id;
        $customer_type = $scope.edit_ownertypes.customer_type;
        $url = 'updateownertype';
        $method = 'POST';
        $data = {
            'id': $id,
            'customer_type': $customer_type
        };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $('#myModal').modal('hide');
                $scope.loadData();
            } else {
                toaster.pop({ type: 'error', title: "User not Updated! Kindly try again" });
            }
        });
    };
    $scope.delete_owner = function(id) {
        $url = 'deleteownertype';
        $method = 'POST';
        $data = { 'id': id };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                $scope.loadData();
            } else {
                $scope.error = 'User not deledted, kindly try again';
            }
        });
    };

    $scope.add_nationalid = function() {
        if ($scope.insert_customer.national_no != "" && $scope.insert_customer.national_expairydate != "" && $scope.myFile) {
            $scope.national_file_ext = angular.element(document.getElementById("national_files")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var $button = $('.nat_id').clone();
                angular.element(document.getElementById("file_append")).append($button);
                $('#nationalllid').hide();
            } else {
                alert('National file should ba an Image');
            }
        } else {
            alert('All Values are Important');
        }
    };
    $scope.add_passportid = function() {
        if ($scope.insert_customer.passport_no != "" && $scope.insert_customer.passport_xdate != "" && $scope.passFile) {
            $scope.passport_file_ext = angular.element(document.getElementById("passport_file")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.passport_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var $button = $('.pas_id').clone();
                angular.element(document.getElementById("file_append")).append($button);
                $('#passport_id').hide();
            } else {
                alert('Passport file should ba an Image');
            }
        } else {
            alert('All Values are Important');
        }
    }
    $scope.add_visaid = function() {
        if ($scope.insert_customer.visa_no != "" && $scope.insert_customer.visa_xdate != "" && $scope.visafile) {
            $scope.visa_file_ext = angular.element(document.getElementById("visa_file")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.visa_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var $button = $('.visa_id').clone();
                angular.element(document.getElementById("file_append")).append($button);
                $('#visa_id').hide();
            } else {
                alert('VISA file should ba an Image');
            }
        } else {
            alert('All Values are Important');
        }
    }
    $scope.add_commercialid = function() {
        if ($scope.insert_customer.commercial_no != "" && $scope.insert_customer.commercial_xdate != "" && $scope.commercialfile) {
            $scope.commerical_file_ext = angular.element(document.getElementById("commercial_file")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.commerical_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var $button = $('.commercial_id').clone();
                angular.element(document.getElementById("file_append")).append($button);
                $('#commercial_id').hide();
            } else {
                alert('Commercial file should ba an Image');
            }
        } else {
            alert('All Values are Important');
        }
    }
    $scope.add_dedid = function() {
        if ($scope.insert_customer.ded_no != "" && $scope.insert_customer.ded_xdate != "" && $scope.dedfile) {
            $scope.ded_file_ext = angular.element(document.getElementById("ded_file")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.ded_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var $button = $('.ded_id').clone();
                angular.element(document.getElementById("file_append")).append($button);
                $('#ded_id').hide();
            } else {
                alert('DED file should ba an Image');
            }
        } else {
            alert('All Values are Important');
        }
    }
    $scope.add_ejariid = function() {
        if ($scope.insert_customer.ejari_no != "" && $scope.insert_customer.ejari_xdate != "" && $scope.ejarifile) {
            $scope.ejari_file_ext = angular.element(document.getElementById("ejari_file")).val().toString().split('.').pop().toLowerCase();
            if ($.inArray($scope.ejari_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var $button = $('.ejari_id').clone();
                angular.element(document.getElementById("file_append")).append($button);
                $('#ejari_id').hide();
            } else {
                alert('EJARI file should ba an Image');
            }
        } else {
            alert('All Values are Important');
        }
    }
    $scope.insertcus = function() {
        var file = $scope.myFile;
        var pass_file = $scope.passFile;
        var visa_file = $scope.visafile;
        var commercial_file = $scope.commercialfile;
        var ded_file = $scope.dedfile;
        var ejari_file = $scope.ejarifile;
        //var file = $scope.national_file;
        var formdata = new FormData();
        formdata.append('customer_name', $scope.insert_customer.name);
        formdata.append('customer_email', $scope.insert_customer.email);
        formdata.append('number', $scope.insert_customer.number);
        formdata.append('altno', $scope.insert_customer.altno);
        formdata.append('ids', $scope.insert_customer.id);
        formdata.append('owner_type', $scope.insert_customer.cus_owner);
        formdata.append('national_file', file);
        formdata.append('national_id', $scope.insert_customer.national_no);
        formdata.append('national_expairydate', $scope.insert_customer.national_expairydate);

        formdata.append('pass_file', pass_file);
        formdata.append('passport_no', $scope.insert_customer.passport_no);
        formdata.append('passport_xdate', $scope.insert_customer.passport_xdate);

        formdata.append('visa_file', visa_file);
        formdata.append('visa_no', $scope.insert_customer.visa_no);
        formdata.append('visa_xdate', $scope.insert_customer.visa_xdate);

        formdata.append('commercial_file', commercial_file);
        formdata.append('commercial_no', $scope.insert_customer.commercial_no);
        formdata.append('commercial_xdate', $scope.insert_customer.commercial_xdate);

        formdata.append('ded_file', ded_file);
        formdata.append('ded_no', $scope.insert_customer.ded_no);
        formdata.append('ded_xdate', $scope.insert_customer.ded_xdate);

        formdata.append('ejari_file', ejari_file);
        formdata.append('ejari_no', $scope.insert_customer.ejari_no);
        formdata.append('ejari_xdate', $scope.insert_customer.ejari_xdate);

        formdata.append('project_name', $scope.insert_customer.project_name);
        formdata.append('address', $scope.insert_customer.address);
        formdata.append('company_id', status.company_id);
        $scope.selected_node = angular.element(document.getElementById("selected_node")).val();
        formdata.append('selected_node', $scope.selected_node);

        $url = "insertcus";
        $method = "POST";
        $headers = { 'Content-Type': undefined };
        $data = formdata;
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.insert_customer = {};
                    angular.element(document.getElementById('add_customer')).modal('hide');
                    $scope.getcustomer();
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                }
            } else {
                toaster.pop({ type: "error", title: "No Data Available" });
            }
        });
    };

    $scope.clear = function() {
        //angular.element(document.getElementById("selected_node")).val(" ");
    };

    $scope.append_tree = function() {
        $url = 'get_assignedproject';
        $method = 'POST';

        $data = { 'company_id': '' };
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data.data['project_details'] != '') {
                console.log("data=" + data.data['project_details']);
                var projet_wbs = JSON.parse(data.data['project_details']);
                var data = JSON.parse(projet_wbs);
                angular.element(document.getElementById("tree_loaded")).jstree('destroy');
                angular.element(document.getElementById("tree_loaded")).jstree({
                    plugins: ["themes", "json", "grid", "dnd", "contextmenu", "search"],
                    core: {
                        "data": data,
                        "check_callback": true
                    },
                    grid: {
                        columns: [
                            { width: 370, header: "Create WBS", title: "_DATA_", contextmenu: true },
                            {
                                width: 100,
                                cellClass: "col1",
                                value: "value1",
                                header: "Node Type",
                                valueClass: "spanclass"
                            },
                        ],
                        resizable: true,
                        contextmenu: true
                    },
                });
                angular.element(document.getElementById("tree_loaded")).jstree(true).redraw(true);
                var tree = $("#tree_loaded");
                tree.bind("loaded.jstree", function(event, data) {
                    tree.jstree("open_all");
                    //tree.jstree("deselect_all");
                });

                angular.element(document.getElementById("tree_loading")).modal('show');
            }

        });

    };
    angular.element(document.getElementById("tree_loaded")).bind(
        "select_node.jstree",
        function(evt, data) {
            //$scope.selected_node = "";
            $scope.selectedtext = "";
            $scope.selectedtext = data.node.text;
            $scope.selectedid = data.node.id;
            $scope.parents = data.node.parents;
            //$scope.nodInfo = $("#" + $scope.selectedid);
            $scope.parent_node_id_array = "";
            $scope.value = [];
            $scope.values = "";
            $scope.value.push($scope.selectedtext);
            for ($scope.i = 0; $scope.i < $scope.parents.length; $scope.i++) {
                $scope.parent_id = $scope.parents[$scope.i];
                $scope.parent_name = "";
                if ($scope.parent_id != '#') {
                    $scope.parent_node_id_array += $scope.parent_id + ',';
                    if ($('#' + $scope.parent_id + '_anchor').text() != "") {
                        $scope.parent_name += $('#' + $scope.parent_id + '_anchor').text() + ',';
                        $scope.p_id = $('#' + $scope.parent_id + '_anchor').text();
                        //$scope.parent_ids = $('#tree_loaded').jstree().get_parent($scope.parent_id + '_anchor');                        
                        //console.log($scope.p_id);
                        $scope.value.push($scope.p_id);
                    }
                }
            }
            for ($scope.j = $scope.value.length - 1; $scope.j > 0; $scope.j--) {
                $scope.values += $scope.value[$scope.j] + '->';
            }
            $scope.selected_node = $scope.values + $scope.selectedtext;
            angular.element(document.getElementById("selected_node")).val($scope.selected_node);
            angular.element(document.getElementById("tree_loading")).modal("hide");
            //$scope.append_values($scope.selected_node);
            //$scope.insert_customer.selected_node = "values";

        }
    );
    /*$scope.$watch('insert_customer.selected_node', function (oldValue, newValue) {
        $scope.valuessss="";
        $scope.valuessss="welcome";
        $scope.append_values = function (values) {
            //angular.element(document.getElementById("selected_node")).val(values);
            $scope.insert_customer.selected_node = values;
        }
        
        //do something
    }); */

}]);