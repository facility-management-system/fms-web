myApp.controller('globalController_s', ['$scope', '$location',  function ($scope, $location){
    $scope.global = {};
    $scope.sglobal.navUrl = "app/templates/nav/nav_s.php";
    $scope.sglobal.sidebar = "app/templates/nav/sidebar_s.php";
    $scope.welcome= function () {
        $('#mb-signout').modal('show');
    };
    $scope.model_close = function () {
        $('#mb-signout').modal('hide');
    }; 
    $scope.doLogout= function() {
        userModel.doUserLogout();
        $location.path('/');
    };
}]);