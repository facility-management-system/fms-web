myApp.controller('tickets_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', function ($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService) {
  var status = $cookies.get('auth');
  status = JSON.parse(status);
  console.log(status);
  var comp_id = status['company_id'];

  $scope.formParams = {};
    $scope.stage = "";
    $scope.formValidation = false;
    $scope.toggleJSONView = false;
    $scope.toggleFormErrorsView = false;
    
    $scope.formParams = {
      ccEmail: '',
      ccEmailList: []
    };    
    // Navigation functions
    $scope.next = function (stage) {
      //$scope.direction = 1;
      //$scope.stage = stage;      
      $scope.formValidation = true;
      
      if ($scope.multiStepForm.$valid) {
        $scope.direction = 1;
        $scope.stage = stage;
        $scope.formValidation = false;
      }
    };  
    $scope.back = function (stage) {
      $scope.direction = 0;
      $scope.stage = stage;
    };
    
    // CC email list functions
    $scope.addCCEmail = function () {
      $scope.rowId++;
  
      var email = {
        email: $scope.formParams.ccEmail,
        row_id: $scope.rowId
      };
  
      $scope.formParams.ccEmailList.push(email);
  
      $scope.formParams.ccEmail = '';
    };
  
    $scope.removeCCEmail = function (row_id) {
      for (var i = 0; i < $scope.formParams.ccEmailList.length; i++) {
        if ($scope.formParams.ccEmailList[i].row_id === row_id) {
          $scope.formParams.ccEmailList.splice(i, 1);
          break;
        }
      }
    };
    
   $scope.get_newtickets = function () {
    $url = "get_newtickets";
    $method = "POST";
    $data = { 'company_id': comp_id };
    $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
        if (data['status'] == '200') {
            $scope.get_newtickets = data.data;
        } else {
            $scope.get_newtickets = data.data;
        }
    });
    $scope.get_newtickets();

   }
    // Post to desired exposed web service.
    $scope.submitForm = function () {
      //alert("Success");
      $input = $scope.tickets;
      console.log($input);
      $url = 'save_newticket';
      $method = 'POST';
      $data = {'input':$input};
      $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
          if (data['status'] == '200') {
            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
          } 
          else {
            $scope.error = 'Network error!! kindly try again';
          }
      });
      
     /* var wsUrl = "someURL";  
      // Check form validity and submit data using $http
      if ($scope.multiStepForm.$valid) {
        $scope.formValidation = false;
  
        $http({
          method: 'POST',
          url: wsUrl,
          data: JSON.stringify($scope.formParams)
        }).then(function successCallback(response) {
          if (response
            && response.data
            && response.data.status
            && response.data.status === 'success') {
            $scope.stage = "success";
          } else {
            if (response
              && response.data
              && response.data.status
              && response.data.status === 'error') {
              $scope.stage = "error";
            }
          }
        }, function errorCallback(response) {
          $scope.stage = "error";
          console.log(response);
        });
      } */
    };
    
    $scope.reset = function() {
      // Clean up scope before destorying
      $scope.formParams = {};
      $scope.stage = "";
    }
     
    $scope.model1_open = function() {      
      //$('#raise_ticket').modal('show');
      $('#contact_search').modal('show');
      //$('#view_tickets').modal('show');
      //angular.element(document.getElementById("contact_search")).modal('show');
    };

    $scope.view_sample = function (){
      $('#sample_image').modal('show');
    };

    $scope.upload_userexcel = function (){
      var file = $scope.ticket_File;
      var formdata = new FormData();
      formdata.append('bulk_file', file); 
          $url = "upload_ticketexcel";
          $method = "POST";       
          $headers = { 'Content-Type': undefined };
          $data = formdata;
          $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
            var number = data['data'].length;
            
            for(i=0;i<number;i++)
            {   
                if (data['status'] == '200') {
                    if (data['data'][i].type == 'success') {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                        $('#bulk_user').modal('hide');
                        //$scope.project = data.data.message;
                    } else {
                        toaster.pop({ type: data['data'][i].type, title: data['data'][i].message });
                    }
                } else {
                    toaster.pop({ type: "error", title: "Something went wrong!" });
                } 
            }
         // console.log(data['type']);
        }); 
    };

    //tickets load function
     $scope.loadTickets = function () {
      var vm = this;
      var resetPaging = false;
      $url = 'loadTickets';
      $method = 'GET';
      $data = "";
      $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
          console.log(data);
          if (data['status'] == '200') {
              $scope.tickets = data.data;
              $('#raise_ticket').modal('hide');
          } else {
              $scope.tickets = data.data;
          }
      });
  }; 
  $scope.loadTickets(); 

  $scope.tickets_Supporttype = function () {
    var vm = this;
    var resetPaging = false;
    $url = 'tickets_Supporttype';
    $method = 'POST';
    $data = { 'company_id': comp_id };        
    $scope.items = myService.submitForm($url, $method, $data).then(function (data){
        console.log(data);
        if (data['status'] == '200') {
            $scope.support_type = data.data;
        } else {
            $scope.support_type = data.data;
        }
    });
};
$scope.tickets_Supporttype(); 

    $scope.view_ticket = function (tick_details) {
      $scope.ticket_info = tick_details;
      $('#view_tickets').modal('show');
    };

    $scope.view_customer = function (cust_id) {
      $input = cust_id;
      $url = 'getcust_info';
      $method = 'POST';
      $data = {'input':$input};
      $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
          //console.log(data);
          if (data['status'] == '200') {
            $scope.view_user = data.data['0'];
            console.log($scope.view_user);
            $('#myModal').modal('show');
          }
          else {
              toaster.pop({ type: "error", title: "Network error !" });
          }
      });
     
    };
    
    $scope.delete_user = function (id) {
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this User!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then(function () {
        swal(
          'Deleted!',
          'User has been deleted.',
          'success'
        )
      }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelled',
            'User is safe :)',
            'error'
          )
        }
      })
    };

    $scope.check_contactnum = function() {
        $answer=this.customer.contact_num;      
        $url = 'retrieve_cust';
        $method = 'POST';
        $data ={'input':$answer};         
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {                             
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                   $scope.tickets = data.data.message[0];
                   $('#raise_ticket').modal('show');
                   //$scope.user_details(data.data.message[0]);
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
                
            } else {
                $scope.error = 'Network error!! kindly try again';
            }
        });
    };

    $scope.user_details = function(user_details){
      $scope.multiStepForm = user_details;
      $('#raise_ticket').modal('show');      
    }

}]);