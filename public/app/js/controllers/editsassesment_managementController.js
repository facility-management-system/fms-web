myApp.controller('editsassesment_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', '$timeout', '$routeParams', function ($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService, $timeout, $routeParams) {

    var status = $cookies.get('auth');
    status = JSON.parse(status);
    $scope.company_id = status.company_id;

    $scope.get_supportmodel = function () {
        $url = "get_supportmodel";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                $scope.get_supportmodel = data.data;
            } else {
                $scope.get_supportmodel = data.data;
            }
        });
    };
    $scope.get_supportmodel();  

    $scope.id = $routeParams.id;
    $scope.assessment_id = $routeParams.assessment_id;
    $scope.title = $routeParams.title;
    $scope.type = $routeParams.type;
    $scope.productcategory = $routeParams.productcategory;

    $scope.editassess = {
        id: $scope.id,
        assessment_id: $scope.assessment_id,
        title: $scope.title,
        type: $scope.type,
        productcategory: $scope.productcategory,
    };

    var ajaxResult = [];

    $scope.add_quizs = function(){
        $url = "get_quizid";
        $method = "GET";
        $data = "";
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            $scope.insertquiz={
                quiz_id_0:data.data
            };
            angular.element(document.getElementById('add_quiz')).modal('show');
        });
    };
    var counter = 0;
    var dells=0;
    $scope.add_newquiz = function () {
        counter += 1;
        dells = counter-1;
        $scope.suffix = $scope.insertquiz["quiz_id_0"];
        var i = parseInt($scope.suffix.replace(/\D/g, ''), 10);
        i = i + counter;
        $.strPad = function (i, l, s) {
            var o = i.toString();
            if (!s) { s = '0'; }
            while (o.length < l) {
                o = s + o;
            }
            return o;
        }; 
        var strr = $.strPad(i, 3);
        var str = ('Quiz_' + strr);
        $scope.insertquiz["quiz_id_"+counter] = str;
        
        var insert_quiz = angular.element(document.getElementById('new_append_quiz'));
        var el = $(`<div class="row">
                        <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.id_`+ counter + `.$invalid && !insert_quiz.id_` + counter +`.$pristine }">
                            <label class="col-md-3 control-label" for="form_control_1">Quiz id</label>
                            <div class="col-md-9">
                                <input type="text" name='id_` + counter + `' class="form-control" ng-model="insertquiz.quiz_id_` + counter + `" required>
                                <p ng-show="insert_quiz.id_` + counter + `.$invalid && !insert_quiz.id_` + counter +`.$pristine" class="help-block">Quiz ID is required.</p>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.question_`+ counter + `.$invalid && !insert_quiz.question_` + counter + `.$pristine }">
                                <label class="col-md-3 control-label" for="form_control_1">Question</label>
                                <div class="col-md-9">
                                    <input type="text" name='question_`+ counter + `' class="form-control" ng-model="insertquiz.question_` + counter + `" required>
                                    <p ng-show="insert_quiz.question_`+ counter + `.$invalid && !insert_quiz.question_`+ counter + `.$pristine" class="help-block">Question is Mandatory.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.option_a_`+ counter + `.$invalid && !insert_quiz.option_a_` + counter + `.$pristine }">
                                <label class="col-md-3 control-label" for="form_control_1">Option A</label>
                                <div class="col-md-9">
                                    <input type="text" name='option_a_`+ counter + `' class="form-control" ng-model="insertquiz.option_a_` + counter + `" required>
                                    <p ng-show="insert_quiz.option_a_`+ counter + `.$invalid && !insert_quiz.option_a_` + counter + `.$pristine" class="help-block">Option A is Mandatory.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.option_b_`+ counter + `.$invalid && !insert_quiz.option_b_` + counter + `.$pristine }">
                                <label class="col-md-3 control-label" for="form_control_1">Option B</label>
                                <div class="col-md-9">
                                    <input type="text" name='option_b_`+ counter + `' class="form-control" ng-model="insertquiz.option_b_` + counter + `" required>
                                    <p ng-show="insert_quiz.option_b_`+ counter + `.$invalid && !insert_quiz.option_b_` + counter + `.$pristine" class="help-block">Option B is Mandatory.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.option_c_`+ counter + `.$invalid && !insert_quiz.option_c_` + counter + `.$pristine }">
                                <label class="col-md-3 control-label" for="form_control_1">Option C</label>
                                <div class="col-md-9">
                                    <input type="text" name='option_c_`+ counter + `' class="form-control" ng-model="insertquiz.option_c_` + counter + `" required>
                                    <p ng-show="insert_quiz.option_c_`+ counter + `.$invalid && !insert_quiz.option_c_` + counter + `.$pristine" class="help-block">Option C is Mandatory.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.option_d_`+ counter + `.$invalid && !insert_quiz.option_d_` + counter + `.$pristine }">
                                <label class="col-md-3 control-label" for="form_control_1">Option D</label>
                                <div class="col-md-9">
                                    <input type="text" name='option_d_`+ counter + `' class="form-control" ng-model="insertquiz.option_d_` + counter + `" required>
                                    <p ng-show="insert_quiz.option_d_`+ counter + `.$invalid && !insert_quiz.option_d_` + counter + `.$pristine" class="help-block">Option D is Mandatory.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group form-md-line-input" ng-class="{ 'has-error' : insert_quiz.correct_ans_`+ counter + `.$invalid && !insert_quiz.correct_ans_` + counter + `.$pristine }">
                                <label class="col-md-3 control-label" for="form_control_1">Correct Ans</label>
                                <div class="col-md-9">
                                    <input type="text" name='correct_ans_`+ counter + `' class="form-control" ng-model="insertquiz.correct_ans_` + counter + `" required>
                                    <p ng-show="insert_quiz.correct_ans_`+ counter + `.$invalid && !insert_quiz.correct_ans_`+ counter + `.$pristine" class="help-block">Correct Answer is Mandatory.</p>
                                </div>
                            </div>
                        </div>
                    </div><br>`);
        //var result = angular.element(document.getElementById('insert_before')).before(el);
        var result = $(insert_quiz).append(el);
        $compile(result)($scope);
    };
    $scope.add_quiz = function(){
        console.log($scope.insert_quiz);
    }

}]);