myApp.controller('kb_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', function ($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService) {
    var status = $cookies.get('auth');
    status = JSON.parse(status);
	console.log(status);
	var comp_id = status['company_id'];

    $scope.get_supportmodel = function () {
        $url = "get_supportmodel";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                $scope.get_supportmodel = data.data;
            } else {
                $scope.get_supportmodel = data.data;
            }
        });
    };
    $scope.get_supportmodel();   
    
    $scope.approved_kblist = function () {
        $url = "approved_kblist";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                $scope.load_approvedkb = data.data;
            } else {
                $scope.load_approvedkb = data.data;
            }
        });
    };
    $scope.approved_kblist();   

    $scope.available_kblist = function () {
        $url = "available_kblist";
        $method = "POST";
        $data = { 'company_id': status.company_id };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            console.log(data);
            if (data['status'] == '200') {
                $scope.available_kb = data.data;
            } else {
                $scope.available_kb = data.data;
            }
        });
    };
    $scope.available_kblist();   

    $scope.view_spare = function (image){
        $scope.image = image;
        angular.element(document.getElementById('view_image')).modal('show');
    };

    $scope.view_video = function (video_content){
        $scope.video_file = video_content;
        angular.element(document.getElementById('view_video')).modal('show');
    }

    $scope.reset = function () {
        // Clean up scope before destorying
        $scope.formParams = {};
        $scope.knowledge = {};
        angular.element("input[type='file']").val(null);
       // $scope.add_kb.$setDirty();
    };

    $scope.update_status = function (input){
        $url = "update_status";
        $method = "POST";
        $data = { 'value':input };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    //$('#training').modal('hide');
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.available_kblist();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went wrong, kindly try again';
            }
        });
    };

    $scope.reject_kb = function (input){
        $url = "reject_kb";
        $method = "POST";
        $data = { 'value':input };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    //$('#training').modal('hide');
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.available_kblist();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went wrong, kindly try again';
            }
        }); 
    }
    $scope.delete_kb = function (input){
        $url = "delete_kb";
        $method = "POST";
        $data = { 'value':input };
        $scope.items = myService.submitForm($url, $method, $data).then(function (data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    //$('#training').modal('hide');
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.get_kblist();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Something went wrong, kindly try again';
            }
        });
    }

    $scope.edit_details = function (kb_value){
        $scope.edit_knowledge = kb_value;
        console.log($scope.edit_knowledge);
        $('#edit_modal').modal('show');
    }

    $scope.change_intoselect = function(){
        $("#load_supportid").html('');
  
        var el = $(`<select class="form-control" name="product" ng-model="edit_knowledge.support_category" ng-options="sup.id as sup.support_category for sup in get_supportmodel" required>
                      </select>
                      <p ng-show="edit_kb.product.$invalid && !edit_kb.product.$pristine" class="help-block">Product Category is required.</p>`).
                      appendTo("#load_supportid");;
        $compile(el)($scope);
        
      }

$scope.insert_kb = function () {
        // if($scope.knowledge.kb_type=="generic" || "technology"){
        //     $scope.knowledge.product="";
        // }
        // if( $scope.knowledge.product=="product"){
        //     $scope.knowledge.product= $scope.knowledge.product;
        // }
        // console.log($scope.knowledge.product);
        // return false;
        if(!$scope.knowledge.product){
            $scope.knowledge.product="";
        }
        
        $scope.trai_type = $scope.knowledge.kb_type;
        $scope.productcategory = $scope.knowledge.product;
        $scope.problem = $scope.knowledge.problem_desc;
        $scope.prob_solution = $scope.knowledge.kb_sol;
        $scope.trai_contenttype = $scope.knowledge.kb_contenttype;
        $scope.file = $scope.knowledge.content_file;
   
    // $scope.x2 = angular.isNumber($scope.productcategory);
    // console.log($scope.x2);
    //     if(angular.isNumber($scope.productcategory)=="true"){
    //         console.log("works correctlty");
    //     }
        
    //     return false;
        if ($scope.file) {
            $scope.file_name = $scope.knowledge.content_file.name;
            $scope.file_type = $scope.file_name.split('.');
            $scope.file_type = $scope.file_type[1];
            console.log($scope.file_type);
            //return false;
            if($scope.trai_contenttype=="image"){
                $scope.national_file_ext = angular.element(document.getElementById("file_attach")).val().toString().split('.').pop().toLowerCase();
                if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {

                    var formdata = new FormData();
                    formdata.append('file', $scope.file);
                    formdata.append('problem', $scope.problem);
                    formdata.append('type', $scope.trai_type);
                    formdata.append('product', $scope.productcategory);
                    formdata.append('solution', $scope.prob_solution);
                    formdata.append('content_type', $scope.trai_contenttype);
                    formdata.append('company_id', comp_id);
                } else {
                    toaster.pop({ type: "warning", title: 'Content Type and selected file is mismatched, Kindly select correct file!'});
                    return false;
                   
                }
            }
            if($scope.trai_contenttype=="video"){
                $scope.national_file_ext = angular.element(document.getElementById("file_attach")).val().toString().split('.').pop().toLowerCase();
                if ($.inArray($scope.national_file_ext, ['mp4', 'avi', 'flv', 'mov']) !== -1) {
                    
                    var formdata = new FormData();
                    formdata.append('file', $scope.file);
                    formdata.append('problem', $scope.problem);
                    formdata.append('type', $scope.trai_type);
                    formdata.append('product', $scope.productcategory);
                    formdata.append('solution', $scope.prob_solution);
                    formdata.append('content_type', $scope.trai_contenttype);
                    formdata.append('company_id', comp_id);
                } else {
                    toaster.pop({ type: "warning", title: 'Content Type and selected file is mismatched, Kindly select correct file!'});
                    return false;
                }
            }
            if($scope.trai_contenttype=="pdf"){
                $scope.national_file_ext = angular.element(document.getElementById("file_attach")).val().toString().split('.').pop().toLowerCase();
                if ($.inArray($scope.national_file_ext, ['pdf']) !== -1) {
                    
                    var formdata = new FormData();
                    formdata.append('file', $scope.file);
                    formdata.append('problem', $scope.problem);
                    formdata.append('type', $scope.trai_type);
                    formdata.append('product', $scope.productcategory);
                    formdata.append('solution', $scope.prob_solution);
                    formdata.append('content_type', $scope.trai_contenttype);
                    formdata.append('company_id', comp_id);
                } else {
                    toaster.pop({ type: "warning", title: 'Content Type and selected file is mismatched, Kindly select correct file!'});
                    return false;
                }
            }
            /*if ($scope.trai_contenttype != $scope.file_type) {
                alert('Content Type and selected file is mismatched, Kindly select correct file!');
            } else {
                var formdata = new FormData();
                formdata.append('file', $scope.file);
                formdata.append('problem', $scope.problem);
                formdata.append('type', $scope.trai_type);
                formdata.append('product', $scope.productcategory);
                formdata.append('solution', $scope.prob_solution);
                formdata.append('content_type', $scope.trai_contenttype);
                formdata.append('company_id', $scope.company_id);
            }*/
                $url = "uploadkb";
                $method = "POST";
                $data = formdata;
                $headers = { 'Content-Type': undefined };
                $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
                    if (data['status'] == '200') {
                        if (data['data']['type'] == 'success') {
                            //$('#training').modal('hide');
                            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                            $scope.reset();
                            $scope.get_kblist();
                        } else {
                            toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                        }
                    } else {
                        $scope.error = 'Knowledge Base is not inserted kindly try again';
                    }
                });
        }else{
            toaster.pop({ type: "warning", title: 'File is Mandatory'});
        }
}

$scope.edit_knowledgebase = function (){
    $scope.productcategory ="";
    if ( !isNaN($scope.edit_knowledge.support_category) && angular.isNumber($scope.edit_knowledge.support_category)) {
        $scope.productcategory = $scope.edit_knowledge.support_category;
    }
    else{
        $scope.productcategory = $scope.edit_knowledge.kb_productcategory;
    }
  
        $scope.kd_id = $scope.edit_knowledge.kb_id;
        $scope.trai_type = $scope.edit_knowledge.kb_type;
        $scope.problem = $scope.edit_knowledge.kb_problem;
        $scope.prob_solution = $scope.edit_knowledge.kb_solution;
        $scope.trai_contenttype = $scope.edit_knowledge.kb_contenttype;
        $scope.extfile = $scope.edit_knowledge.kb_content;
        //$scope.file = $scope.knowledge_b.content_file;
        console.log($scope.trai_contenttype);
        
            if ($scope.contentfile) {
                $scope.file = $scope.contentfile;
                $scope.file_name = $scope.contentfile.name;
                console.log($scope.file_name);
                $scope.file_type = $scope.file_name.split('.');
                $scope.file_type = $scope.file_type[1];
                console.log($scope.file_type);
                //return false;
                    if($scope.trai_contenttype=="image"){
                        $scope.national_file_ext = angular.element(document.getElementById("kbfile_attach")).val().toString().split('.').pop().toLowerCase();
                        console.log($scope.national_file_ext);
                        if ($.inArray($scope.national_file_ext, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
        
                            var formdata = new FormData();
                            formdata.append('file', $scope.file);
                            formdata.append('existing_file', $scope.extfile);                            
                            formdata.append('kb_id',  $scope.kd_id);                       
                            formdata.append('problem', $scope.problem);
                            formdata.append('type', $scope.trai_type);
                            formdata.append('product', $scope.productcategory);
                            formdata.append('solution', $scope.prob_solution);
                            formdata.append('content_type', $scope.trai_contenttype);
                            formdata.append('company_id', comp_id);
                            formdata.append('file_checker', 1);
                        }                    
                        else {
                            toaster.pop({ type: "warning", title: 'Content Type and selected file is mismatched, Kindly select correct file!'});
                            return false;
                        
                        }
                    }
                    if($scope.trai_contenttype=="video"){
                        $scope.national_file_ext = angular.element(document.getElementById("kbfile_attach")).val().toString().split('.').pop().toLowerCase();
                        if ($.inArray($scope.national_file_ext, ['mp4', 'avi', 'flv', 'mov']) !== -1) {
                            
                            var formdata = new FormData();
                            formdata.append('file', $scope.file);
                            formdata.append('existing_file', $scope.extfile);      
                            formdata.append('kb_id',  $scope.kd_id);    
                            formdata.append('problem', $scope.problem);
                            formdata.append('type', $scope.trai_type);
                            formdata.append('product', $scope.productcategory);
                            formdata.append('solution', $scope.prob_solution);
                            formdata.append('content_type', $scope.trai_contenttype);
                            formdata.append('company_id', comp_id);
                            formdata.append('file_checker', 1);
                        } 
                        else {
                            toaster.pop({ type: "warning", title: 'Content Type and selected file is mismatched, Kindly select correct file!'});
                            return false;
                        }
                    }
                    if($scope.trai_contenttype=="pdf"){
                        $scope.national_file_ext = angular.element(document.getElementById("kbfile_attach")).val().toString().split('.').pop().toLowerCase();
                        console.log($scope.national_file_ext);
                        if ($.inArray($scope.national_file_ext, ['pdf']) !== -1){
                            
                            var formdata = new FormData();
                            formdata.append('file', $scope.file);
                            formdata.append('existing_file', $scope.extfile);      
                            formdata.append('kb_id',  $scope.kd_id);    
                            formdata.append('problem', $scope.problem);
                            formdata.append('type', $scope.trai_type);
                            formdata.append('product', $scope.productcategory);
                            formdata.append('solution', $scope.prob_solution);
                            formdata.append('content_type', $scope.trai_contenttype);
                            formdata.append('company_id', comp_id);
                            formdata.append('file_checker', 1);
                        }                     
                        else {
                            toaster.pop({ type: "warning", title: 'Content Type and selected file is mismatched, Kindly select correct file!'});
                            return false;
                        }
                    } 
            }
            else{
                var formdata = new FormData();
                formdata.append('kb_id',  $scope.kd_id);                       
                formdata.append('problem', $scope.problem);
                formdata.append('type', $scope.trai_type);
                formdata.append('product', $scope.productcategory);
                formdata.append('solution', $scope.prob_solution);
                formdata.append('content_type', $scope.trai_contenttype);
                formdata.append('company_id', comp_id);
                formdata.append('file_checker', 0);
            }

            $url = "kb_edit";
            $method = "POST";
            $data = formdata;
            $headers = { 'Content-Type': undefined };
            $scope.items = myService.submitForm($url, $method, $data, $headers).then(function (data) {
                if (data['status'] == '200') {
                    if (data['data']['type'] == 'success') {
                        //$('#training').modal('hide');
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                        $('#edit_modal').modal('hide');
                        $scope.get_kblist();
                        $scope.clear_modal();
                    } else {
                        toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    }
                } else {
                    $scope.error = 'Knowledge Base is not updated kindly try again';
                }
            });
}

        $scope.clear_modal = function () {
            // Clean up scope before destorying
            $scope.edit_knowledge = {};
            angular.element("input[type='file']").val(null);
        };
}]);