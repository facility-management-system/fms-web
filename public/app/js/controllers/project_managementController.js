myApp.controller('project_managementController', ['$scope', '$location', '$cookies', 'userModel', '$http', '$compile', 'toaster', '$window', 'myService', '$timeout', function($scope, $location, $cookies, userModel, $http, $compile, toaster, $window, myService, $timeout) {


    var status = $cookies.get('auth');
    status = JSON.parse(status);

    $scope.loadproject = function() {
        $url = "getproject";
        $method = "GET";
        $data = "";
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $scope.project = data.data.message;
                } else {
                    toaster.pop({ type: "error", title: "No Data Available" });
                }
            } else {
                toaster.pop({ type: "error", title: "No Data Available" });
            }
        });
    };
    $scope.loadproject();


    $scope.formParams = {};
    $scope.stage = "";
    $scope.formValidation = false;
    $scope.toggleJSONView = false;
    $scope.toggleFormErrorsView = false;

    $scope.formParams = {
        ccEmail: '',
        ccEmailList: []
    };

    // Navigation functions
    $scope.next = function(tostage) {
        //$scope.direction = 1;
        //$scope.stage = stage;

        $scope.formValidation = true;
        console.log($scope.multiStepForm.$valid);
        console.log(tostage);
        // if ($scope.multiStepForm.$valid) {
        alert("hi");
        $scope.direction = 1;
        alert(tostage);
        $scope.stage = tostage;
        $scope.formValidation = false;
        //   }
    };

    $scope.back = function(stage) {
        $scope.direction = 0;
        $scope.stage = stage;
    };

    // Post to desired exposed web service.
    $scope.submitForm = function() {
        var wsUrl = "someURL";
        //console.log($scope.formParams);  
        $scope.reset();

        // Check form validity and submit data using $http
        /* if ($scope.multiStepForm.$valid) {
            $scope.formValidation = false;

            $http({
                method: 'POST',
                url: wsUrl,
                data: JSON.stringify($scope.formParams)
            }).then(function successCallback(response) {
                if (response
                    && response.data
                    && response.data.status
                    && response.data.status === 'success') {
                    $scope.stage = "success";
                } else {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status === 'error') {
                        $scope.stage = "error";
                    }
                }
            }, function errorCallback(response) {
                $scope.stage = "error";
                console.log(response);
            });
        } */
    };

    $scope.reset = function() {
        // Clean up scope before destorying
        $scope.formParams = {};
        $scope.stage = "";
    };
    $scope.adminlist = [];
    $scope.getadminlist = function() {
        alert("hello");

        $http({
            method: 'POST',
            url: 'index.php/getcompanybaseduserlist',
            data: { companyid: $scope.formParams.company_selected }
        }).then(function(response) {
            $scope.adminlist = response.data;
            console.log(response.data);
        });
    };
    $scope.init = function() {
        var data;
        data = [{
            text: $scope.formParams.pro_name,
            data: { value1: "Project", size: "30", spanclass: "root" },
            children: [{
                    text: "Phase-1",
                    data: { value1: "Phase", size: "50", spanclass: "first" },
                    children: [{
                            text: "Tower-1",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-1",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                    text: "Flat A1",
                                    data: { value1: "Flat", size: "50", spanclass: "third" },
                                    children: [
                                        { text: "Master Bedroom", data: { value1: "Area", size: "50", spanclass: "third" } }
                                    ]
                                }]
                            }]
                        },
                        {
                            text: "Tower-2",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-1",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                        text: "Flat A2",
                                        data: { value1: "Flat", size: "50", spanclass: "third" },
                                        children: [
                                            { text: "Balcony", data: { value1: "Area", size: "50", spanclass: "third" } }
                                        ]
                                    }

                                ]

                            }]

                        }

                    ],
                },
                {
                    text: "Phase-2",
                    data: { value1: "Phase", size: "50", spanclass: "first" },
                    children: [{
                            text: "Tower-1",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-1",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                    text: "Flat C1",
                                    data: { value1: "Flat", size: "50", spanclass: "third" },
                                    children: [
                                        { text: "Master Toilet", data: { value1: "Area", size: "50", spanclass: "third" } }
                                    ]
                                }]
                            }]
                        },
                        {
                            text: "Tower-2",
                            data: { value1: "Tower", size: "50", spanclass: "third" },
                            children: [{
                                text: "Floor-2",
                                data: { value1: "Floor", size: "50", spanclass: "third" },
                                children: [{
                                        text: "Flat A2",
                                        data: { value1: "Flat", size: "50", spanclass: "third" },
                                        children: [
                                            { text: "Kitchen", data: { value1: "Area", size: "50", spanclass: "third" } }
                                        ]
                                    }

                                ]

                            }]

                        }

                    ],
                },
            ]
        }];

        angular.element(document.getElementById("jstree")).jstree({
            plugins: ["themes", "json", "grid", "dnd", "contextmenu", "search"],
            core: {
                "data": data,
                "check_callback": true
            },
            grid: {
                columns: [
                    { width: 370, header: "Create WBS", title: "_DATA_", contextmenu: true },
                    {
                        width: 100,
                        cellClass: "col1",
                        value: "value1",
                        header: "Node Type",
                        valueClass: "spanclass"
                    },
                ],
                resizable: true,
                contextmenu: true
            },
            dnd: {
                drop_finish: function() {},
                drag_finish: function() {},
                drag_check: function(data) {
                    return {
                        after: true,
                        before: true,
                        inside: true
                    };
                }
            },
            contextmenu: {
                'items': function(node) {
                    var tmp = $.jstree.defaults.contextmenu.items();
                    delete tmp.create.action;
                    tmp.create.label = "New";
                    tmp.create.submenu = {
                        "create_folder": {
                            "separator_after": true,
                            "label": "Block",
                            "action": function(data) {
                                var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);

                                bootbox.prompt("Please Enter Block to add", function(result) {
                                    if (result == '' || isNaN(result) == true) {
                                        toastr["error"]("Please Enter Numbers", "Warning");
                                    } else {
                                        var cfnum = parseInt(result);
                                        for (i = 1; i <= cfnum; i++) {
                                            inst.create_node(obj, { type: "default", text: "Newnode" + i }, "last", function() {
                                                setTimeout(function() {
                                                    inst.edit();
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    };

                    return tmp;
                }
            }
        });
        var tree = $("#jstree");
        tree.bind("loaded.jstree", function(event, data) {
            tree.jstree("open_all");
            //tree.jstree("deselect_all");
        });
    };


    $scope.getcompanylist = function() {
        $http({
            method: 'GET',
            url: 'index.php/getcompanies',
        }).then(function(response) {
            $scope.companies = response.data;
        });
    };
    $scope.getcompanylist();



    $scope.model_open = function() {
        alert('welcome');
    };

    $scope.saveWbs = function() {
        $("#jstree").jstree("deselect_all");
        var treejson = $("#jstree").jstree(true).get_json('#', { flat: true });
        treejson = JSON.stringify(treejson);
        treejson = treejson.replace(/\\/g, "");
        //treejson=treejson.serialize();
        $data = {
            'project_name': $scope.formParams.pro_name,
            'project_location': $scope.formParams.pro_location,
            // 'project_manager': $scope.formParams.pro_manager,
            'project_wbs': treejson,
            'selectedcompany': $scope.formParams.company_selected,
            'selectedadmin': $scope.formParams.slctedUser
        };

        $url = "insertproject";
        $method = "POST";
        $scope.items = myService.submitForm($url, $method, $data).then(function(data) {
            if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $scope.reset();
                    //$scope.loadproject();
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadproject();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadproject();
                }
            } else {
                toaster.pop({ type: "error", title: "Project not created. Kindly try again!" });
            }
        });

        //console.log($scope.formParams.pro_name);

    };

    $scope.view_project = function(projects) {
        $scope.json_data = "";
        $scope.json_data = JSON.parse(projects.project_wbs);
        angular.element(document.getElementById("tree_sample")).jstree('destroy');
        var data = JSON.parse($scope.json_data);
        angular.element(document.getElementById("tree_sample")).jstree({
            core: {
                "data": data,
                "check_callback": true
            },
            plugins: ["themes", "json", "grid", "dnd", "contextmenu", "search", "json_data", "ui", "types", "data"],
            //plugins: ["themes","ui","json_data","json","grid","contextmenu"],
            grid: {
                columns: [
                    { width: 370, header: "Create WBS", title: "_DATA_", contextmenu: true },
                    {
                        width: 100,
                        cellClass: "col1",
                        value: "value1",
                        header: "Node Type",
                        valueClass: "spanclass"
                    },
                ],
                resizable: true,
                contextmenu: true
            },
            dnd: {
                drop_finish: function() {},
                drag_finish: function() {},
                drag_check: function(data) {
                    return {
                        after: true,
                        before: true,
                        inside: true
                    };
                }
            },
        });
        angular.element(document.getElementById("tree_sample")).jstree(true).redraw(true);
        var trees = $("#tree_sample");
        trees.bind("loaded.tree_sample", function(event, data) {
            trees.jstree("open_all");
            //tree.jstree("deselect_all");
        });
        angular.element(document.getElementById("view_projects")).modal('show');
    };

    $scope.uploadFile = function() {
        var file_data = angular.element(document.getElementById('fileInput')).prop("files")[0];
        var formdata = new FormData();
        formdata.append('pro_file', file_data);
        $url = "uploadtree";
        $method = "POST";
        $data = formdata;
        $headers = { 'Content-Type': undefined };
        $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data) {
            /* if (data['status'] == '200') {
                if (data['data']['type'] == 'success') {
                    $('#mymodel1').modal('hide');
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                    $scope.loadProduct();
                } else {
                    toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
                }
            } else {
                $scope.error = 'Product Category is not inserted kindly try again';
            } */
        });
    }

    //download tree
    var changed_array = [];
    var changed_array1 = [];
    var changed_array2 = [];
    var changed_array3 = [];
    var changed_array4 = [];
    var projectNameExcel;

    $scope.downloadTreeExcel = function() {

            projectNameExcel = $scope.formParams.pro_name;
            var tree_json1 = $("#jstree").jstree(true).get_json('#', { flat: true });
            var converted_json12 = JSON.stringify(tree_json1);
            var converted_json1 = JSON.parse(converted_json12);
            var child = [];
            var total_child = [];
            var last_data = "";
            var leaf_id1 = [];
            var leaf_id = [];
            var leaf_text = [];
            var a = [];
            var array1 = [];
            var array2 = [];
            var split_array = {};
            var split_array2 = [];
            var get_zero_array = [];
            var text_name = [];
            $('.jstree-leaf').each(function() {
                leaf_id1 += $(this).attr('id') + ',';

                leaf_text += $(this).children('a').text() + ',';

            });

            leaf_id1 = leaf_id1.split(',');

            leaf_id1 = leaf_id1.filter(function(e) {
                return e.replace(/(\r\n|\n|\r)/gm, "")
            });
            for (i = 0; i < converted_json1.length; i++) {
                text_name[i] = converted_json1[i]['id'];
            }
            var j = 0;
            for (i = 0; i < leaf_id1.length; i++) {

                if (text_name.indexOf(leaf_id1[i]) > -1) {

                    leaf_id[j] = leaf_id1[i];
                    j++;
                }
            }
            for (var i = 0; i < converted_json1.length; i++) {

                total_child.push(converted_json1[i]['id']);
                child[i] = converted_json1[i]['id'];

            }
            var par_node = "";
            for (i = 0; i < child.length; i++) {
                if ($("#jstree").jstree(true).get_parent(child[i]) == '#') {
                    par_node = child[i];
                }
            }

            var idx = 0;
            var f_array = [];

            for (var i = 0; i < leaf_id.length; i++) {
                var arry1 = [];
                for (var j = idx; j < child.length; j++) {
                    arry1 += child[j] + ',';


                    if (child[j] == leaf_id[i]) {
                        break;
                    }
                    idx++;


                }
                idx++;

                f_array[i] = arry1;


            }
            for (i = 0; i < f_array.length; i++) {
                if (f_array[i].indexOf(',') > -1) {
                    f_array[i] = f_array[i].split(',');
                    f_array[i] = f_array[i].filter(function(e) {
                        return e.replace(/(\r\n|\n|\r)/gm, "")
                    });
                }
            }
            get_zero_array[0] = $('#' + par_node + '_anchor').text() + "," + f_array[0];

            var parent_id = "";
            var get_parent_id = "";
            var add_parent = [];
            var reverse_add_parent = [];
            var e = 0;

            var split_each_value = [];
            var split_each_value1 = [];
            var split_text = "";
            var split_text1 = [];
            var get_p = "";

            for (i = 1; i < f_array.length; i++) {
                parent_id = f_array[i][0]; //j1_7
                get_p = "";
                for (j = 0; j < converted_json1.length; j++) //(j1_7,j1_8,j1_9)
                {

                    if (parent_id != "#") {
                        get_parent_id = $('#jstree').jstree().get_parent(parent_id); //j1_3


                        parent_id = get_parent_id;
                        if (get_parent_id != "#") {
                            get_p += get_parent_id + ':';
                        }

                    }

                    if (get_parent_id == par_node) {

                        break;
                    }
                }

                get_p = get_p.split(":");

                add_parent = get_p.filter(function(e) {
                    return e.replace(/#/gm, "")
                });


                reverse_add_parent = [];
                for (l = add_parent.length - 1; l >= 0; l--) {
                    reverse_add_parent[e] = add_parent[l];

                    e++;

                }
                reverse_add_parent = reverse_add_parent.filter(function(e) {
                    return e.replace(/(\r\n|\n|\r)/gm, "")
                });

                if (reverse_add_parent[0] == $('#' + par_node + '_anchor').text()) {
                    changed_array[i] = reverse_add_parent + "," + f_array[i];
                } else {
                    changed_array[i] = $('#' + par_node + '_anchor').text() + "," + reverse_add_parent + "," + f_array[i];
                }



            }


            changed_array[0] = get_zero_array[0];
            var typesrow = ["Project", "Phase", "Tower", "Floor", "Flat", "Area", "Sub Area"];
            var h = 0;
            for (i = 0; i < changed_array.length; i++) {
                if (i > 0) {
                    changed_array[i] = changed_array[i].split(",");
                    split_text = "";
                    for (f = 0; f < changed_array[i].length; f++) {
                        split_text += $('#' + changed_array[i][f] + '_anchor').text() + ':';
                    }


                } else {

                    changed_array[i] = changed_array[i].split(",");
                    split_text = "";

                    for (f = 0; f < changed_array[i].length; f++) {
                        split_text += $('#' + changed_array[i][f] + '_anchor').text() + ':';
                    }


                }
                changed_array[i] = split_text.split(':');
                changed_array[i] = changed_array[i].filter(function(e) {
                    return e.replace(/(\r\n|\n|\r)/gm, "")
                });

            }
            changed_array1 = changed_array.concat([typesrow]);
            var f = 0;
            var l = 1;

            for (h = changed_array1.length - 1; h >= 0; h--) {
                changed_array2[f] = changed_array1[h];
                f++;

            }
            for (i = changed_array2.length - 1; i >= 1; i--) {
                changed_array3[l] = changed_array2[i];
                l++;
            }
            changed_array4 = changed_array3;
            changed_array4[0] = typesrow;
            var get_prj_name = '"' + projectNameExcel + '.' + "xlsx" + '"';
            saveAsXlsx(get_prj_name, changed_array4);
        }
        //alasql for tree and linear
    function saveAsXlsx(name, test) {
        alasql('SELECT * INTO XLSX(' + name + ',{headers:false}) FROM ?', [test]);
    }

    $scope.getcompanyadminlist = function(selected_company) {
        $http({
            method: 'POST',
            url: 'index.php/getcompanybaseduserlist',
            data: { 'companyid': selected_company }
        }).then(function(response) {
            $scope.adminproject = response.data;
            console.log(response.data);
        });
    }
}]);