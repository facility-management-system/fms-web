myApp.controller('msocial_managementController', ['$scope', '$location', '$cookies', 'userModel', 'FileUploader', '$http', '$compile', 'toaster', '$window', 'myService', function ($scope, $location, $cookies, userModel, FileUploader, $http, $compile, toaster, $window, myService) {

  
  var status = $cookies.get('auth');
  status = JSON.parse(status);
  console.log(status);
  var comp_id = status['company_id'];
  var auth_user = status['user_id'];
  angular.element(document.getElementById('show_div')).hide();
  angular.element(document.getElementById('sub_div')).hide();
  angular.element(document.getElementById('guest_house')).hide();
  angular.element(document.getElementById('pg_div')).hide();
  angular.element(document.getElementById('shops_div')).hide();

  angular.element(document.getElementById('mobile_subdiv')).hide();

  angular.element(document.getElementById('carsub_div')).hide();
  angular.element(document.getElementById('show_carsubdivs')).hide();

  angular.element(document.getElementById('show_motorcycles')).hide();
  angular.element(document.getElementById('show_scooters')).hide();
  angular.element(document.getElementById('show_bicycles')).hide(); 
  

  var uploader = $scope.uploader = new FileUploader({
    url: 'upload.php'
  });

  // FILTERS

  uploader.filters.push({
    name: 'imageFilter',
    fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
});

// CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

  /*For Usermanagement */

 
 
  $scope.load = function (){
    
    $scope.myVar = false;
      $scope.photos=[];
    $scope.photos = [
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 01'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/images/cat.jpeg', desc: 'Image 02'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/blur-bg.jpg', desc: 'Image 03'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/nature1.jpeg', desc: 'Image 04'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-1.jpg', desc: 'Image 06'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/images/download.jpeg', desc: 'Image 07'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/images/cat.jpeg', desc: 'Image 08'}, 
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/nature1.jpeg', desc: 'Image 09'},
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 01'}
    ];
    console.log($scope.photos);

        $scope.photosec =[];
    $scope.photosec = [
        {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/nature1.jpeg', desc: 'Image 09'}
    ];
  };

  $scope.setformat = function (){
    //$scope.date = '12-03-2016';
   // $scope.date = $filter('date')(Date.now(),'yyyy-MM-dd'); 
  }
  $scope.setformat();
//   window.onload = function(){
//     document.getElementById("subm").onload=function(){
//         alert("Hello WOrld");
//         $scope.startFun = function () {
//             $scope.photos = [
//                 {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 01'},
//                 {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 02'},
//                 {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 03'},
//                 {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 04'},
//                 {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 05'},
//                 {src: 'http://localhost:8081/2018-03-12/FMS/public/assets/img/user-4.jpg', desc: 'Image 06'}
//             ];
//             console.log(photos);
//           };
//     }
// }
 
  // $scope.startFun();

  // initial image index
  $scope._Index = 0;   
  // if a current image is the same as requested image
  $scope.isActive = function (index) {
      return $scope._Index === index;
  };
  // show prev image
  $scope.showPrev = function () {
      $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.photos.length - 1;
  };
  // show next image
  $scope.showNext = function () {
      $scope._Index = ($scope._Index < $scope.photos.length - 1) ? ++$scope._Index : 0;
  };
  // show a certain image
  $scope.showPhoto = function (index) {
    $scope._Index = index;
};
 
$scope.ShowHide = function (){
    var myElement = angular.element( document.querySelector( '#myPopup' ) );
    myElement.classList.toggle("show");
}
$scope.image_preview = function (){
  if (typeof (FileReader) != "undefined")
   {
    var dvPreview = $("#dvPreview");
    dvPreview.html("");
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
    $($(this)[0].files).each(function () {
        var file = $(this);
        if (regex.test(file[0].name.toLowerCase())) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $("<img />");
                img.attr("style", "height:100px;width: 100px");
                img.attr("src", e.target.result);
                dvPreview.append(img);
            }
            reader.readAsDataURL(file[0]);
        } else {
            alert(file[0].name + " is not a valid image file.");
            dvPreview.html("");
            return false;
        }
    });
} 
else
 {
    alert("This browser does not support HTML5 FileReader.");
}
};

$scope.newValue = function(value) {

    console.log(value);
    if(value=="1"){
        $scope.social_postid = '1';
        $('#social_post').modal('show');
    }
    if(value=="2"){
        $scope.event_postid = '2';
        $('#event_post').modal('show');
    }
    if(value=="3"){
        $scope.classi_postid = '3';
        $('#classified_post').modal('show');
    }    
}
// SUBMIT SOCIAL POST
$scope.insertpost = function (){

    var formdata = new FormData();
    if($scope.uploadfiles){
        angular.forEach($scope.uploadfiles,function(file){
            console.log("file");
            formdata.append('file[]',file);
        });
        // var fd = new FormData();
        // angular.forEach($scope.logos, function (val, key) {
        //     console.log($scope.logos);
        //     fd.append('file'+key, val);						
        //   });
    }
   
    // return false;
    // if($scope.myFile){       
    //     var uploading_images = $scope.myFile;          
    //  }
    //  else{
    //    var uploading_images = "";
    //  }
    
   // var formdata = new FormData();
    formdata.append("ids",$scope.social_postid);
    formdata.append("title",$scope.insert_spost.s_title);
    formdata.append("desc",$scope.insert_spost.s_description);
    formdata.append("user_id",auth_user);   
    console.log(formdata);
    $url = 'insert_socialpost';
    $method = 'POST';
    $headers = { 'Content-Type': undefined };
    $data = formdata;
    $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data){ 
        //console.log(data.data);
          if(data['status']=='200'){
            if(data['data']['type']=='success'){
                console.log(data);
              $('#myModal').modal('hide');
              $scope.loadData();
              toaster.pop({ type: data['data']['type'], title: data['data']['message']});
            }else{
              toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
            }
          }else{
            $scope.error='User not Added, kindly try again';
          }
      });   
}
// SUBMIT EVENT POST
$scope.insert_evepost = function (){
    if($scope.myFile){       
        var uploading_images = $scope.evt_myFile;          
     }
     else{
       var uploading_images = "";
     }
    var formdata = new FormData();
    formdata.append("ids",$scope.event_postid);
    formdata.append("event_title",$scope.insert_eventpost.evt_title);
    formdata.append("event_desc",$scope.insert_eventpost.evt_desc);
    formdata.append("event_date",$scope.insert_eventpost.evt_date);   
    formdata.append("event_venue",$scope.insert_eventpost.evt_venue);     
    formdata.append("upload_images",uploading_images);
    formdata.append("user_id",auth_user);

    $url = 'insert_eventpost';
    $method = 'POST';
    $headers = { 'Content-Type': undefined };
    $data = formdata;
    $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data){ 
        //console.log(data.data);
          if(data['status']=='200'){
            if(data['data']['type']=='success'){
                console.log(data);
              $('#myModal').modal('hide');
              $scope.loadData();
              toaster.pop({ type: data['data']['type'], title: data['data']['message']});
            }else{
              toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
            }
          }else{
            $scope.error='User not Added, kindly try again';
          }
      }); 
}

$scope.insert_claspost = function() {
    var formdata = new FormData();
    formdata.append("user_id",auth_user);
    formdata.append("ids",$scope.classi_postid);
    formdata.append("cl_title",$scope.insert_clpost.cltitle_name);
    formdata.append("cl_desc",$scope.insert_clpost.description);
    formdata.append("cl_category",$scope.insert_clpost.value);   

    if($scope.insert_clpost.value=="1"){
        var prop_type = $scope.insert_clpost.prop_type;
        var prop_subtype = $scope.prop_subtype;
        formdata.append("proptype",prop_type);
        formdata.append("prop_subtype",prop_subtype);

        if(prop_subtype == '1' || prop_subtype == '2' || prop_subtype == '6' || prop_subtype == '7'){ 
           
            if($scope.prop_myFile){
                var prop_image = $scope.prop_myFile;
            }
            else{
                var prop_image = "";
            }      
            formdata.append("prop_price",$scope.insert_clpost.prop_price);
            formdata.append("bedrooms",$scope.insert_clpost.bedrooms);
            formdata.append("bath_room",$scope.insert_clpost.bath_room);
            formdata.append("furnish",$scope.insert_clpost.furnish);
            formdata.append("batchelor",$scope.insert_clpost.batchelor);
            formdata.append("construction",$scope.insert_clpost.construction); 
            formdata.append("maintainance",$scope.insert_clpost.maintainance); 
            formdata.append("parking",$scope.insert_clpost.parking); 
            formdata.append("facing",$scope.insert_clpost.facing); 
            formdata.append("proptotal_room",$scope.insert_clpost.proptotal_room); 
            formdata.append("floor",$scope.insert_clpost.floor); 
            formdata.append("prop_image",prop_image); 
        }

        if(prop_subtype == '3' || prop_subtype == '8'){
            if($scope.gh_myFile){
                var gh_image = $scope.gh_myFile;
            }
            else{
                var gh_image = "";
            }
            formdata.append("gh_price",$scope.insert_clpost.gh_price); 
            formdata.append("gh_parking",$scope.insert_clpost.gh_parking); 
            formdata.append("gh_furnish",$scope.insert_clpost.gh_furnish); 
            formdata.append("guesthouse_image",gh_image); 
        }

        if(prop_subtype == '5' || prop_subtype == '9'){
            if($scope.shops_myFile){
                var shop_image = $scope.shops_myFile;
            }
            else{
                var shop_image = "";
            }
            formdata.append("shop_price",$scope.insert_clpost.shop_price); 
            formdata.append("s_maintain",$scope.insert_clpost.s_maintain); 
            formdata.append("s_furnish",$scope.insert_clpost.s_furnish); 
            formdata.append("shop_parking",$scope.insert_clpost.shop_parking); 
            formdata.append("shop_room",$scope.insert_clpost.shop_room); 
            formdata.append("wash_room",$scope.insert_clpost.shop_wroom); 
            formdata.append("shop_image",shop_image); 
        }

        if(prop_subtype == '4'){ 
            if($scope.pg_myFile){
                var pg_images = $scope.pg_myFile;
            }
            else{
                var pg_images = "";
            }
            formdata.append("pg_price",$scope.insert_clpost.pg_pricevalue); 
            formdata.append("meals",$scope.insert_clpost.meals_type); 
            formdata.append("pg_furnish",$scope.insert_clpost.pg_furnish); 
            formdata.append("pg_images",pg_images); 
        }
        
    }

    if($scope.insert_clpost.value=="2"){
        if($scope.furn_myFile){       
            var furn_images = $scope.furn_myFile;          
         }
         else{
           var furn_images = "";
         }        
        formdata.append("furid",$scope.insert_clpost.furid);
        formdata.append("fur_price",$scope.insert_clpost.furniture_price);
        formdata.append("furn_images",furn_images);    
        
    }

    if($scope.insert_clpost.value=="3"){
        if($scope.mobile_myFile){       
            var mobile_images = $scope.mobile_myFile;          
         }
         else{
           var mobile_images = "";
         }        
        formdata.append("mobile_cat",$scope.insert_clpost.mobile_id);
        formdata.append("mobile_subcat",$scope.insert_clpost.mob_subid);
        formdata.append("mob_images",mobile_images);  
    }

    if($scope.insert_clpost.value=="4"){
        var cars = $scope.insert_clpost.cars;
        formdata.append("cars",cars);
        
        if(cars=="1"){
            if($scope.carimage_myImage){
                var car_image = $scope.carimage_myImage;
            }
            else{
                var car_image = "";
            }
            formdata.append("sub_cars",$scope.cars_div);
            formdata.append("car_model",$scope.insert_clpost.cars_mdl);
            formdata.append("car_brand",$scope.insert_clpost.car_brand);
            formdata.append("car_price",$scope.insert_clpost.car_price);
            formdata.append("car_year",$scope.insert_clpost.year_car);
            formdata.append("kilometers",$scope.insert_clpost.km_driven);
            formdata.append("car_image",car_image);
        }

        if(cars=="2"){
            if($scope.cars_myFile){
                var c_images = $scope.cars_myFile;
            }
            else{
                var c_images = "";
            }
            formdata.append("price",$scope.insert_clpost.price_forcar);
            formdata.append("year",$scope.insert_clpost.car_year);
            formdata.append("kilometer",$scope.insert_clpost.kilometer);
            formdata.append("c_images",c_images);
        }

        if(cars=="3" || cars=="4"){
            if($scope.carsub_myFile){
                var images = $scope.carsub_myFile;
            }
            else{
                var images = "";
            }
            formdata.append("price",$scope.insert_clpost.price_forcar);
            formdata.append("images",images);
            
        }
        // if(cars=="4"){
            
        // }
    }

    if($scope.insert_clpost.value=="5"){
        if($scope.elec_myFile){       
            var upload_images = $scope.elec_myFile;          
         }
         else{
           var upload_images = "";
         }        
        formdata.append("elec_type",insert_clpost.electronics);
        formdata.append("elec_price",insert_clpost.ele_price);
        formdata.append("upload_images",upload_images);        
    }

    if($scope.insert_clpost.value=="6"){
        if($scope.pet_myFile){       
            var uploading_images = $scope.pet_myFile;          
         }
         else{
           var uploading_images = "";
         }        
        formdata.append("pet_type",insert_clpost.pet_type);
        formdata.append("pet_price",insert_clpost.pets_price);
        formdata.append("uploading_images",uploading_images);

    }

    if($scope.insert_clpost.value=="7"){
        
    }

    $url = 'insert_classipost';
    $method = 'POST';
    $headers = { 'Content-Type': undefined };
    $data = formdata;
    $scope.items = myService.submitForm($url, $method, $data, $headers).then(function(data){ 
        //console.log(data.data);
          if(data['status']=='200'){
            if(data['data']['type']=='success'){
                console.log(data);
              $('#myModal').modal('hide');
              $scope.loadData();
              toaster.pop({ type: data['data']['type'], title: data['data']['message']});
            }else{
              toaster.pop({ type: data['data']['type'], title: data['data']['message'] });
            }
          }else{
            $scope.error='User not Added, kindly try again';
          }
      }); 
      
}

$scope.display_div = function(value) {
    if(value=="1"){
        $http({
            method: 'POST',
            url: 'getproptypes',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.prop_details = response.data;
                console.log($scope.prop_details);
            });
        $scope.myVar=true;
        $scope.hiddenvalue=value;
        $scope.furniture_div=false;
        $scope.mobile_div=false;
        $scope.cars_div=false;
        $scope.electronics_div=false;
        $scope.pets_div=false;
        $scope.bikes_div=false;
        // $scope.myVar = !$scope.myVar;
    }
    if(value=="2"){
        $http({
            method: 'POST',
            url: 'get_furntypes',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.furn_types = response.data;
                console.log($scope.furn_types);
        });

        $scope.myVar=false;
        $scope.furniture_div=true;
        $scope.mobile_div=false;
        $scope.cars_div=false;
        $scope.electronics_div=false;
        $scope.pets_div=false;
        $scope.bikes_div=false;
    }
    if(value=="3"){
        $http({
            method: 'POST',
            url: 'get_mobilecat',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.mobiles = response.data;
                console.log($scope.furn_types);
            });
        $scope.myVar=false;
        $scope.furniture_div=false;
        $scope.mobile_div=true;
        $scope.cars_div=false;
        $scope.electronics_div=false;
        $scope.pets_div=false;
        $scope.bikes_div=false;
    }
    if(value=="4"){
        $http({
            method: 'POST',
            url: 'get_cars',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.car_types = response.data;
                console.log($scope.car_types);
        });
        $scope.cars_div=true;
        $scope.myVar=false;
        $scope.furniture_div=false;
        $scope.mobile_div=false;        
        $scope.electronics_div=false;
        $scope.pets_div=false;
        $scope.bikes_div=false;
    }
    if(value=="5"){
        $http({
            method: 'POST',
            url: 'elec_types',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.elec_types = response.data;
                console.log($scope.elec_types);
        });
        $scope.myVar=false;
        $scope.furniture_div=false;
        $scope.mobile_div=false;
        $scope.cars_div=false;
        $scope.electronics_div=true;
        $scope.pets_div=false;
        $scope.bikes_div=false;
    }
    if(value=="6"){
        $http({
            method: 'POST',
            url: 'pets_types',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.pets_types = response.data;
                console.log($scope.pets_types);
        });
        $scope.myVar=false;
        $scope.furniture_div=false;
        $scope.mobile_div=false;
        $scope.cars_div=false;
        $scope.electronics_div=false;
        $scope.pets_div=true;
        $scope.bikes_div=false;
    }
    if(value=="7"){
        $http({
            method: 'POST',
            url: 'bike_types',
            data: {'input_val':value},
            }).then(function (response) {
                $scope.bike_types = response.data;
                console.log($scope.bike_types);
        });
        $scope.myVar=false;
        $scope.furniture_div=false;
        $scope.mobile_div=false;
        $scope.cars_div=false;
        $scope.electronics_div=false;
        $scope.pets_div=false;
        $scope.bikes_div=true;
    }

};

$scope.property_subtypes = function (val){

    angular.element(document.getElementById('guest_house')).hide();  
    angular.element(document.getElementById('pg_div')).hide(); 
    angular.element(document.getElementById('shops_div')).hide();        
    angular.element(document.getElementById('sub_div')).hide();        
    
    if(val == 'undefined' || val == "null" || val == ""){
        angular.element(document.getElementById('show_div')).hide();
    }else{
        angular.element(document.getElementById('show_div')).show();
    }
    $http({
        method: 'POST',
        url: 'getprop_subtypes',
        data: {'prop_id':val},
        }).then(function (response) {
            $scope.subtypes = response.data;
            console.log($scope.subtypes);
        });
};

$scope.load_divs = function (input){    
    $http({
        method: 'POST',
        url: 'get_bedrooms',
        data: {'input':input},
        }).then(function (response) {
            $scope.bedrooms = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_bathrooms',
        data: {'input':input},
        }).then(function (response) {
            $scope.bathrooms = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_furnishing',
        data: {'input':input},
        }).then(function (response) {
            $scope.furnishing = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_batchelors',
        data: {'input':input},
        }).then(function (response) {
            $scope.batch_types = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_carparking',
        data: {'input':input},
        }).then(function (response) {
            $scope.parkings = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_facing',
        data: {'input':input},
        }).then(function (response) {
            $scope.facing_types = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_consstatus',
        data: {'input':input},
        }).then(function (response) {
            $scope.construction = response.data;
            console.log($scope.subtypes);
        });

    $http({
        method: 'POST',
        url: 'get_meals',
        data: {'input':input},
        }).then(function (response) {
            $scope.meals_type = response.data;
            console.log($scope.subtypes);
        });

    if(input == '1' || input == '2' || input == '6' || input == '7'){       
        angular.element(document.getElementById('guest_house')).hide();  
        angular.element(document.getElementById('pg_div')).hide(); 
        angular.element(document.getElementById('shops_div')).hide();        
        angular.element(document.getElementById('sub_div')).show();        
    }
    if(input == '3' || input == '8'){
        angular.element(document.getElementById('sub_div')).hide();    
        angular.element(document.getElementById('pg_div')).hide();    
        angular.element(document.getElementById('shops_div')).hide();  
        angular.element(document.getElementById('guest_house')).show();                   
    }
    if(input == '4'){
        angular.element(document.getElementById('sub_div')).hide();    
        angular.element(document.getElementById('pg_div')).show();      
        angular.element(document.getElementById('guest_house')).hide(); 
        angular.element(document.getElementById('shops_div')).hide();                  
    }
    if(input == '5' || input == '9'){
        angular.element(document.getElementById('sub_div')).hide();    
        angular.element(document.getElementById('pg_div')).hide();      
        angular.element(document.getElementById('guest_house')).hide(); 
        angular.element(document.getElementById('shops_div')).show();                  
    }
}

$scope.mobile_subcat = function (val){
    if(val == 'undefined' || val == "null" || val == ""){
        angular.element(document.getElementById('mobile_subdiv')).hide();
    }else{
        angular.element(document.getElementById('mobile_subdiv')).show();
    }
    $http({
        method: 'POST',
        url: 'getmob_subtypes',
        data: {'cat_id':val},
        }).then(function (response) {
            $scope.mob_subtype = response.data;
            console.log($scope.mob_subtype);
        });
}

$scope.car_subtypes = function (val){
    if(val == 'undefined' || val == "null" || val == ""){
        angular.element(document.getElementById('carsub_div')).hide();
    }
    else{
        $http({
            method: 'POST',
            url: 'car_subtypes',
            data: {'cat_id':val},
            }).then(function (response) {
                $scope.car_subtype = response.data;
                console.log($scope.car_subtype);
            });
            //angular.element(document.getElementById('carsub_div')).show();
        $http({
            method: 'POST',
            url: 'car_fuels',
            data: {'cat_id':val},
            }).then(function (response) {
                $scope.car_fuels = response.data;
                console.log($scope.car_fuels);
            });
    }
    
}

$scope.loadbike_subtypes = function (val){
    $scope.bike_subtypes="";
    if(val == 'undefined' || val == "null" || val == ""){
        angular.element(document.getElementById('bikesub_div')).hide();
    }
    else{
        if(val == "1"){
           /* if motorcycles is select , load sub_divs according to that in div col 6 and start next row,
           do all this inside a div id show*/
        }
        if(val == "2"){
            // create a div, fill col md 6 accoding to scooters and next consecutive rows,
            // usind div id show
        }
        if(val == "3"){

        }
        if(val == "4"){

        }
        $http({
            method: 'POST',
            url: 'bike_subtypes',
            data: {'cat_id':val},
            }).then(function (response) {
                $scope.bike_subtypes = response.data;
                console.log($scope.bike_subtypes);
            });
            //angular.element(document.getElementById('carsub_div')).show();        
    }
}

$scope.motorcycles = function (val){
    $http({
        method: 'POST',
        url: 'bike_models',
        data: {'cat_id':val},
        }).then(function (response) {
            $scope.bikes_model = response.data;
            console.log($scope.bikes_model);
        });
    angular.element(document.getElementById('show_motorcycles')).show();
    angular.element(document.getElementById('show_scooters')).hide(); 
    angular.element(document.getElementById('show_bicycles')).hide(); 
}

$scope.scooters = function (val){
    $http({
        method: 'POST',
        url: 'bike_models',
        data: {'cat_id':val},
        }).then(function (response) {
            $scope.scooty_model = response.data;
            console.log($scope.scooty_model);
        });
    angular.element(document.getElementById('show_scooters')).show(); 
    angular.element(document.getElementById('show_motorcycles')).hide();  
    angular.element(document.getElementById('show_bicycles')).hide(); 
    
}

$scope.bicycles = function (val){
    angular.element(document.getElementById('show_bicycles')).show(); 
    angular.element(document.getElementById('show_motorcycles')).hide();
    angular.element(document.getElementById('show_scooters')).hide(); 
}

$scope.car_subdivs = function (input){
    $http({
        method: 'POST',
        url: 'get_carmodel',
        data: {'car_id':input},
        }).then(function (response) {
            $scope.car_models = response.data;
            console.log($scope.car_models);
        });
        angular.element(document.getElementById('show_carsubdivs')).show();
        
}

$scope.hide_thisdiv = function (){
    angular.element(document.getElementById('show_carsubdivs')).hide();
}

$scope.change_fur = function (){
    var input = $scope.furid;
    console.log(input);
}

  $scope.loadData = function (){
    this.myDate = new Date();
    
      this.minDate = new Date(
        this.myDate.getFullYear(),
        this.myDate.getMonth(),
        this.myDate.getDate()
      );
    
      this.maxDate = new Date(
        this.myDate.getFullYear(),
        this.myDate.getMonth(),
        this.myDate.getDate() + 7
      );
  };
  $scope.loadData();
  

  $scope.uploadFiles = function(files, errFiles) {
    $scope.files = files;
    $scope.errFiles = errFiles;
    angular.forEach(files, function(file) {
        file.upload = Upload.upload({
            url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
            data: {file: file}
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
            });
        }, function (response) {
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            file.progress = Math.min(100, parseInt(100.0 * 
                                     evt.loaded / evt.total));
        });
    });
}

  /* $scope.submitForm = function ($url, $method, $data) {
    
  }; */
//   $scope.data = 'none';
//   $scope.dataimg = 'none';
  $scope.add = function(){
  alert('d');
  var f = document.getElementById('file').files[0],
  r = new FileReader();
  r.onloadend = function(e){
  $scope.data = e.target.result;
  var img = document.createElement('img');
  img.src = 'data:image/jpeg;base64,' + btoa(e.target.result);
  document.body.appendChild(img);
 
  console.log(img);
 }
  r.readAsBinaryString(f);
  }
  
}]);

// create your directive
/*myApp.directive('myDirective', function() {
  return {
    // angular passes the element reference to you
    compile: function(element) {
      $(element).DataTable();
    }
  }
});*/
myApp.directive('ngFile', ['$parse', function ($parse) {
    return {
     restrict: 'A',
     link: function(scope, element, attrs) {
       element.bind('change', function(){
  
       $parse(attrs.ngFile).assign(scope,element[0].files)
       scope.$apply();
     });
    }
   };
  }]);
myApp.directive('customOnChange', function() {    
    return {
    restrict: 'A',
    link: function (scope, element, attrs) {
    var onChangeFunc = scope.$eval(attrs.customOnChange);
    element.bind('change', onChangeFunc);
    }
    };
});

//    function MyCtrl($scope) {
   
//    }