myApp.factory('userModel', ['$http','$cookies', function($http, $cookies){
    var userModel= {};
    userModel.doLogin=function(loginData){
        var formData = {
            'email'    : loginData.email,
            'password' : loginData.password
        };
        return $http({                
            url         :    'index.php/auth',
            method      :   "POST",
            data        :   formData,
            dataType    :   "JSON",
        }).then(function successCallback(response){
            //console.log(JSON.stringify(response.data));     
            $cookies.put('auth', JSON.stringify(response.data));
        },function errorCallback(data, status, headers){
            console.log(data, status, headers);
            alert('Login Error');
        });
    };    

    userModel.getAuthStatus = function(){
        var status = $cookies.get('auth');
        if(status){
            return true;
        }else{
            return false;
        }
    };  

    userModel.getUserObject = function() {
        var userObj =angular.fromJson($cookies.get('auth'));
        return userObj;
    };
    
    userModel.doUserLogout = function(){
        $cookies.remove('auth');
    };

    return userModel;
}])