-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 28, 2018 at 06:07 AM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fms`
--

DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `create_admin` (IN `p_compid` VARCHAR(250), IN `p_admin_id` VARCHAR(100), IN `p_ad_fname` VARCHAR(250), IN `p_ad_lname` VARCHAR(100), IN `p_ad_email` VARCHAR(250), IN `p_ad_cno` VARCHAR(250), IN `p_ad_altcno` VARCHAR(100), IN `p_ad_address` VARCHAR(250), IN `p_ad_userproof` VARCHAR(250), IN `p_ad_region` VARCHAR(250), IN `p_ad_area` VARCHAR(250), IN `p_ad_location` VARCHAR(250), IN `p_ad_role` VARCHAR(250), IN `p_ad_support` VARCHAR(250), IN `p_hashedpassword` TEXT, IN `p_temppassword` VARCHAR(250), IN `p_destinationpath` TEXT)  BEGIN
DECLARE newUserstatus VARCHAR(100);
SET @variable1=p_compid;
SET @variable2=(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fms' AND TABLE_NAME = 'add_users');

SET @variable4=CONCAT(@variable1,'_');


INSERT INTO fms.add_users(employee_id,user_id, company_id,first_name,last_name,email_id,contact_number,alternate_number,address,user_proof,region,area,location,role,support,updated_at,created_at) VALUES (p_admin_id,CONCAT(@variable4, LPAD(@variable2,4,'0')),@variable1,p_ad_fname,p_ad_lname,p_ad_email,p_ad_cno,p_ad_altcno,p_ad_address,
p_ad_userproof,p_ad_region,p_ad_area,p_ad_location,p_ad_role,p_ad_support,now(),now());
IF ROW_COUNT() > 0 THEN
INSERT INTO `users`(`name`, `user_id`, `email`, `password`, `temp_pwd`, `user_type`, `image`, `company_id`,  `created_at`, `updated_at`) VALUES (p_ad_fname,CONCAT(@variable4, LPAD(@variable2,4,'0')),p_ad_email,p_hashedpassword,p_temppassword,p_ad_role,p_destinationpath,@variable1,now(),now());
IF ROW_COUNT()>0 THEN
set newUserstatus=2;
ELSE
SET newUserstatus=1; 
END IF;
ELSE
SET newUserstatus=0;
END IF;

select newUserstatus;
END$$

CREATE  PROCEDURE `create_company` (IN `p_comp_name` VARCHAR(250), IN `p_comp_email` VARCHAR(250), IN `p_cont_num` VARCHAR(250), IN `p_alt_num` VARCHAR(250), IN `p_address` VARCHAR(250), IN `p_admin_id` VARCHAR(100), IN `p_ad_fname` VARCHAR(250), IN `p_ad_lname` VARCHAR(100), IN `p_ad_email` VARCHAR(250), IN `p_ad_cno` VARCHAR(250), IN `p_ad_altcno` VARCHAR(100), IN `p_ad_address` VARCHAR(250), IN `p_ad_userproof` VARCHAR(250), IN `p_ad_region` VARCHAR(250), IN `p_ad_area` VARCHAR(250), IN `p_ad_location` VARCHAR(250), IN `p_ad_role` VARCHAR(250), IN `p_ad_support` VARCHAR(250), IN `p_destinationpath` VARCHAR(250), IN `p_hashedpassword` TEXT, IN `p_password` TEXT)  BEGIN
DECLARE newUserstatus VARCHAR(100);
SET @variable1=(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fms' AND TABLE_NAME = 'company_details');
SET @variable2=(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fms' AND TABLE_NAME = 'add_users');
SET @variable3=CONCAT('company_', LPAD(@variable1,4,'0'));
SET @variable4=CONCAT(@variable3,'_');
INSERT INTO fms.company_details(company_id,company_name,company_email,company_num, alternate_num,address,updated_at,created_at) VALUES (@variable3,p_comp_name,p_comp_email,p_cont_num,p_alt_num,p_address,now(),now());
IF ROW_COUNT() > 0 THEN
INSERT INTO fms.add_users(employee_id,user_id, company_id,first_name,last_name,email_id,contact_number,alternate_number,address,user_proof,region,area,location,role,support,updated_at,created_at) VALUES (p_admin_id,CONCAT(@variable4, LPAD(@variable2,4,'0')),@variable3,p_ad_fname,p_ad_lname,p_ad_email,p_ad_cno,p_ad_altcno,p_ad_address,
p_ad_userproof,p_ad_region,p_ad_area,p_ad_location,p_ad_role,p_ad_support,now(),now());
IF ROW_COUNT() > 0 THEN

INSERT INTO fms.users(name, user_id, email, password, temp_pwd,user_type,image,company_id,created_at,updated_at) VALUES (p_ad_fname,CONCAT(@variable4, LPAD(@variable2,4,'0')),p_ad_email,p_hashedpassword,p_password,p_ad_role,p_destinationpath,@variable3,now(),now());

IF ROW_COUNT() > 0 THEN
set newUserstatus=3;
ELSE
SET newUserstatus=2;
END IF;
ELSE
SET newUserstatus=1;
END IF;
ELSE
SET newUserstatus=0;
END IF;


select newUserstatus;
END$$

CREATE  PROCEDURE `delete_adminuser` (IN `p_id` VARCHAR(250))  BEGIN
DECLARE querystatus VARCHAR(100);
update add_users set deleted_status=1,updated_at=now() WHERE id=p_id;
IF ROW_COUNT()>0 THEN
set @variable1=(select user_id from add_users WHERE id=p_id);
UPDATE users set deleted_status=0,updated_at=now() where user_id=@variable1;
set querystatus=1;
ELSE
set querystatus=0;
end IF;
select querystatus;
END$$

CREATE  PROCEDURE `edit_admin_user` (IN `p_employee_id` VARCHAR(250))  BEGIN
SELECT 
A.id,
A.employee_id,
A.user_id,
A.company_id,
A.first_name,
A.last_name,
A.email_id,
A.contact_number,
A.alternate_number,
A.address,
A.user_proof,
A.region, 
A.area,
A.location,
A.role,
A.project_id,
A.project_name,
A.support,
B.company_name
FROM fms.add_users A 
left JOIN fms.company_details B on B.company_id=A.company_id
WHERE A.employee_id=p_employee_id;
END$$

CREATE  PROCEDURE `get_users_all` (IN `p_deleted` VARCHAR(25))  BEGIN
SELECT A.id,
A.employee_id,
A.user_id,
A.company_id,
A.first_name,
A.last_name,
A.email_id,
A.contact_number,
A.alternate_number,
A.address,
A.user_proof,
A.region,
A.area,
A.location,
A.role,
A.project_id,
A.project_name,
A.support,
B.company_name
FROM fms.add_users A
LEFT JOIN fms.company_details B ON B.company_id=A.company_id;
END$$

CREATE  PROCEDURE `update_adminuser` (IN `p_id` VARCHAR(250), IN `p_employee_id` VARCHAR(250), IN `p_firstname` VARCHAR(250), IN `p_lastname` VARCHAR(250), IN `p_email` VARCHAR(250), IN `p_contactno` VARCHAR(250), IN `p_alternateno` VARCHAR(250), IN `p_address` VARCHAR(250), IN `p_region` VARCHAR(250), IN `p_area` VARCHAR(250), IN `p_location` VARCHAR(250), IN `p_role` VARCHAR(250), IN `p_support` VARCHAR(250))  BEGIN
DECLARE statusquery VARCHAR(100);
IF EXISTS(SELECT * from fms.add_users where id!=p_id AND (email_id=p_email OR contact_number=p_contactno)) THEN
IF EXISTS(SELECT * from fms.add_users where id!=p_id AND  contact_number=p_contactno) THEN
SET statusquery=3;
ELSE
IF EXISTS(SELECT * from fms.add_users where id!=p_id AND  email_id=p_email) THEN
SET statusquery=4;
END IF;
END IF;
ELSE
UPDATE fms.add_users 
SET
first_name=p_firstname,
last_name=p_lastname,
email_id=p_email,
contact_number=p_contactno,
alternate_number=p_alternateno,
address=p_address,
region=p_region,
area=p_area,
location=p_location,
role=p_role,
support=p_support,
updated_at=now()
WHERE employee_id=p_employee_id and id=p_id;
IF ROW_COUNT() > 0 THEN
SET @variable1=(select user_id from fms.add_users where employee_id=p_employee_id and id=p_id);
UPDATE fms.users SET name=p_firstname,
email=p_email,
updated_at=now()
WHERE user_id=@variable1;
IF ROW_COUNT() > 0 THEN
SET statusquery=2;
ELSE
SET statusquery=1;
END IF;

ELSE
SET statusquery=0;
END IF;
END if;
select statusquery;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `add_users`
--

CREATE TABLE `add_users` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `email_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_bin NOT NULL,
  `alternate_number` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(512) COLLATE utf8_bin NOT NULL,
  `user_proof` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_bin NOT NULL,
  `area` varchar(255) COLLATE utf8_bin NOT NULL,
  `location` varchar(255) COLLATE utf8_bin NOT NULL,
  `role` varchar(255) COLLATE utf8_bin NOT NULL,
  `project_id` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `project_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `support` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_status` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '0-not deleted,1-deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `add_users`
--

INSERT INTO `add_users` (`id`, `employee_id`, `user_id`, `company_id`, `first_name`, `last_name`, `email_id`, `contact_number`, `alternate_number`, `address`, `user_proof`, `region`, `area`, `location`, `role`, `project_id`, `project_name`, `support`, `updated_at`, `created_at`, `deleted_status`) VALUES
(1, 'EMP_0219', 'company1_0001', 'company1', 'Kailash', 'Keyar', 'kailash@gmail.com', '9911223355', '7700112233', 'Tharamani OMR Road', '', 'north', 'SRP Tools', 'Chennai', 'Technician', '', '', '2,3', '2018-03-14 04:48:07', '2018-03-14 04:48:07', '0'),
(2, 'Emp_0078', 'company1_0002', 'company1', 'we', 'ewr', 'nanthakumar.r@kaspontech.com', '8987678956', '7854754678', 'Kaspon techworks', '', 'north', 'Adyar', 'saidapet', 'Manager', '', '', '', '2018-05-07 11:21:59', '2018-05-07 11:21:59', '0'),
(3, 'emp109', 'company1_0003', 'company1', 'we', 'ewrwer', 'nantha93911@gmail.com', '8012563985', '7854754678', 'Kaspon techworks', '', 'north', 'Adyar', 'asd', 'Technician', '', '', '4', '2018-05-08 11:41:01', '2018-05-08 11:41:01', '0'),
(4, 'rdf', 'company1_0004', 'company1', 'rfd', 'dfs', 'ramya@gmail.com', '8789678677', '8787786777', 'Kaspon techworks', '', 'south', 'Adyar', 'asd', 'Manager', '', '', '', '2017-12-27 09:15:32', '2017-12-27 09:15:32', '0'),
(5, 'emp_00034', 'company1_0005', 'company1', 'Raju', 'bhai', 'ramyario@gmail.com', '9874563210', NULL, 'sdryhr', '', 'south', 'Adyar', 'guindy', 'Technician', '', '', '1', '2018-03-13 13:34:01', '2018-03-13 13:34:01', '0'),
(9, 'nantha', 'company1_0009', 'company1', 'nanthakumar', 'kumar', 'parentsarethegod@gmail.com', '9962987522', 'undefined', 'kaspontye', NULL, 'north', 'area', 'location', 'Manager', '', '', '', '2018-03-12 15:01:12', '2018-03-12 15:01:12', '0'),
(13, 'Emp_2001', 'company1_0013', 'company1', 'Haricharan', 'Srinivasan', 'hari@gmail.com', '7410236580', 'undefined', 'sdfsdf', NULL, 'south', 'chennai', 'Adayar depot', 'Manager', '', '', '', '2018-03-14 05:06:22', '2018-03-14 05:06:22', '0'),
(14, 'Emp_2003', 'company1_0014', 'company1', 'Vishnu', 'Kumar', 'vishnu@gmail.com', '9487770371', 'undefined', 'Erikarai, Velachery', NULL, 'east', 'chennai', 'saidapet', 'Admin', '', '', '', '2018-03-13 01:38:45', '2018-03-13 01:38:45', '0'),
(15, 'Emp_2003', 'company1_0015', 'company1', 'Vishnu', 'Kumar', 'vishnukumar@gmail.com', '9487770372', 'undefined', 'Erikarai, Velachery', NULL, 'east', 'chennai', 'saidapet', 'Admin', '', '', '', '2018-03-13 01:40:44', '2018-03-13 01:40:44', '0'),
(16, 'Emp_172', 'company1_0016', 'company1', 'nithya', 'b', 'deepika@gmail.com', '9637711440', 'undefined', 'uyruygsbhj', NULL, 'north', 'area', 'ewrewr', 'Admin', '', '', '', '2018-03-14 05:07:14', '2018-03-14 05:07:14', '0'),
(17, 'Emp_172', 'company1_0017', 'company1', 'deepika', 'b', 'nithya@gmail.com', '9637711442', 'undefined', 'uyruygsbhj', NULL, 'north', 'area', 'ewrewr', 'Admin', '', '', '', '2018-03-13 01:42:52', '2018-03-13 01:42:52', '0'),
(18, 'EMP_01210', 'company1_0018', 'company1', 'Kishanth', 'Kumar', 'kishanth@gmail.com', '9870022551', 'undefined', 'chennai', 'EMP_01210/Kishanthproof.JPG', 'south', 'we', 'we', 'Manager', '', '', '', '2018-03-13 01:48:49', '2018-03-13 01:48:49', '0'),
(19, 'KT0220', 'company1_0019', 'company1', 'Abinaya', 'S', 'abinaya.s@kaspontech.com', '9630214500', '7890021036', 'Tharamani 100 feet road, Chennai', 'KT0220/Abinayaproof.png', 'south', 'Adayar', 'Chennai', 'Technician', '', '', '1,3', '2018-03-24 03:38:11', '2018-03-24 03:38:11', '0'),
(25, 'dfgd', 'user', 'company1', 'dgvdr', 'qdfsed', 'nithya.n@kaspontech.com', '9857487500', 'undefined', 'ftrgyuhijk', '', 'north', 'yghujikl', 'dfvgbhnjkm', 'Admin', 'project_0022', 'nanthanam homes', '', '2018-03-26 00:36:43', '2018-03-26 00:36:43', '0'),
(26, 'Emp_0250', 'company1_0026', 'company1', 'Kirthika', 'Vairavan', 'kirthika.v@kaspontech.com', '9442155332', 'undefined', 'Kaspon Techworks', 'Emp_0250/Kirthikaproof.jpg', 'south', 'chennai', 'Kottivakkam', 'Technician', '0', '0', '3', '2018-05-08 11:21:50', '2018-05-08 11:21:50', '0'),
(27, 'Emp_0251', 'company1_0027', 'company1', 'Kishanth', 'S', 'kishanth.s@kaspontech.com', '8508230296', 'undefined', 'Kaspon Techworks, Chennai', 'Emp_0251/Kishanthproof.jpg', 'south', 'chennai', 'Perungudi', 'Technician', '0', '0', '6', '2018-05-08 11:27:53', '2018-05-08 11:27:53', '0'),
(28, 'EMP_0217', 'company1_0028', 'company1', 'Nanthakumar', 'Rajendran', 'nantha9391@gmail.com', '9685741203', 'undefined', 'Little mount, Saidapet, Chennai', '', 'south', 'chennai', 'Perungudi', 'Technician', '0', '0', '12', '2018-05-08 11:44:03', '2018-05-08 11:44:03', '0'),
(43, 'EMP_A100', 'company1_0042', 'company1', 'Manikandan', 'Durai', 'aadhimanikaspon@gmail.com', '9637410025', 'undefined', '357 First Cross Street, 5th Link Road, Nehru Nagar, Old Mahabalipuram Road, Kandancavadi, Nehru Nagar, Perungudi, Chennai, Tamil Nadu 600096, India', '', 'south', 'Perungudi', 'Chennai', 'Manager', '0', '0', '', '2018-05-18 21:43:02', '2018-05-18 21:43:02', '0'),
(44, 'KT0221', 'company1_0044', 'company1', 'Srihari', 'Ram', 'srihari.s@kaspontech.com', '9685741230', NULL, '357 First Cross Street, 5th Link Road, Nehru Nagar, Old Mahabalipuram Road, Kandancavadi, Nehru Nagar, Perungudi, Chennai, Tamil Nadu 600096, India', '/img/user_proofs/KT0221_Srihari_proof.jpg', 'south', 'Perungudi', 'CHENNAI', 'Servicedesk', '0', '0', '', '2018-05-23 05:01:47', '2018-05-23 05:01:47', '0'),
(46, 'jkhnjkjk', 'company_0008_0046', 'company_0008', 'jkljljh', 'lkjkljkl', 'lkljklj@ljlj.hjj', 'undefined', '4654564646', 'sdghsdghasdgsd', 'south', 'dfhdfhdfh', 'sdhgsdfhdfh', 'Admin', 'Admin', '0', '0', '', '2018-05-24 09:18:05', '2018-05-24 09:18:05', '0'),
(47, 'kjhjkhkj', 'company_0009_0047', 'company_0009', 'kjhjkhjk', 'kjhjkhkjh', 'kjhkjhjk@bkjhh.khjg', 'undefined', '6545646444', 'lhkljhlkjkjlk', 'kjhjkhkj/kjhjkhjkproof.JPG', 'south', 'dfhdfhdf', 'dfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 09:29:32', '2018-05-24 09:29:32', '0'),
(48, 'jlkjkljkl', 'company_0010_0048', 'company_0010', 'lkjkljklj', 'ljlkjlkjlk', 'kjkljkj@JKHJK.JIG', 'undefined', '6545645646', 'khkjhkjhkjh', 'jlkjkljkl/lkjkljkljproof.jpeg', 'north', 'dfhdfhdfh', 'dfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 09:32:21', '2018-05-24 09:32:21', '0'),
(49, 'fhdfhd', 'company_0011_0049', 'company_0011', 'fhdfh', 'dfhdfh', 'dfhdfhdf', 'hdfhdfh', 'dfhdfh', 'dfhdfh', 'dfhdfh', 'dfhdfh', 'dfhdfh', 'dfhdfh', 'dfhdfh', '0', '0', 'dfhdfh', '2018-05-24 09:34:48', '2018-05-24 09:34:48', '0'),
(50, 'klhkjlhlkj', 'company_0012_0050', 'company_0012', 'ljkljkljlkjkl', 'lhkljlkjlkj', 'lkjkljlk@lkjkj.hkh', 'undefined', '6456444455', ';kjkl;jpl;j;pkk', 'klhkjlhlkj/ljkljkljlkjklproof.png', 'east', 'dfhdfhdfh', 'dfhdfhdfh', 'Admin', '7', 'cvgjgfjgfj', 'Admin', '2018-05-24 08:23:52', '2018-05-24 13:53:52', '0'),
(51, 'ljkljkljlk', 'company_0013_0051', 'company_0013', 'kljlkjlkj', 'ljkkljklj', 'lkjkljlk@lkjkl.dfh', 'undefined', '5456655555', 'fgjfgjdfgjfgj', 'ljkljkljlk/kljlkjlkjproof.png', 'east', 'fgjfgjfgj', 'fgjfgjfgjfgj', 'Admin', '0', '0', 'Admin', '2018-05-24 09:44:10', '2018-05-24 09:44:10', '0'),
(52, 'ljkljkljlk', 'company_0014_0052', 'company_0014', 'kljlkjlkj', 'ljkkljklj', 'lkjkljlk@lkjkl.dfh', 'undefined', '5456655555', 'fgjfgjdfgjfgj', 'ljkljkljlk/kljlkjlkjproof.png', 'east', 'fgjfgjfgj', 'fgjfgjfgjfgj', 'Admin', '0', '0', 'Admin', '2018-05-24 09:44:41', '2018-05-24 09:44:41', '0'),
(53, 'kjhjkhjklhl', 'company_0015_0053', 'company_0015', 'ljkhlkjlkjlk', 'kljhlkjlkj', 'lhjlkjl@kljhkjl.hk', 'undefined', '6545646546', 'khgjkhjkhjkhkjh', 'kjhjkhjklhl/ljkhlkjlkjlkproof.jpg', 'east', 'hdfhdfh', 'dfhdfhdf', 'Admin', '8', 'dfhjdfjd', 'Admin', '2018-05-24 08:26:58', '2018-05-24 13:56:58', '0'),
(54, 'dfjdfjdfjdfj', 'company_0016_0054', 'company_0016', 'dfjdfjdfjdfj', 'dfjdfjdfjdfjdfj', 'xfhjdfjdf@gjfgdh.dfh', 'undefined', '4563453434', 'sdfghsdfhsdhsdfhsdfh', 'dfjdfjdfjdfj/dfjdfjdfjdfjproof.JPG', 'south', 'hsdhsdhsdhsdh', 'sdhsdhsd', 'Admin', '0', '0', 'Admin', '2018-05-24 10:00:31', '2018-05-24 10:00:31', '0'),
(55, 'kjhjkhkjlhkllk', 'company_0017_0055', 'company_0017', 'hkjhkjhkjlhjl', 'kghjghjgjkh', 'khjkhkjh@khk.sdg', 'undefined', '5645645655', 'ljhjklkljkljklj', 'kjhjkhkjlhkllk/hkjhkjhkjlhjlproof.png', 'south', 'cvncvncvn', 'cvncvncvncvn', 'Admin', '0', '0', 'Admin', '2018-05-24 10:02:11', '2018-05-24 10:02:11', '0'),
(56, 'klhkljhkljlkj', 'company_0018_0056', 'company_0018', 'jlkjkljl', 'klkjkjl;kj', 'jkhkjhjkh@kjh.sdg', 'undefined', '6546545645', 'kjbnkljnlkjlkjjl', 'klhkljhkljlkj/jlkjkljlproof.JPG', 'south', 'dfhdfhdfh', 'gjgfjfgj', 'Admin', '0', '0', 'Admin', '2018-05-24 10:05:13', '2018-05-24 10:05:13', '0'),
(57, 'dfhdfhdfh', 'company_0019_0057', 'company_0019', 'dfhdfhdfh', 'dfhdfhdfh', 'dfhdfh@dfh.fg', 'undefined', '3476346346', 'dhdfhdfhdfhdfh', 'dfhdfhdfh/dfhdfhdfhproof.JPG', 'east', 'dfhdfhdfh', 'dfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 10:08:16', '2018-05-24 10:08:16', '0'),
(58, 'dfhdfhdfh', 'company_0020_0058', 'company_0020', 'dfhdfhdfh', 'dfhdfh', 'dfhdfhdf@gfgf.gf', 'undefined', '3463463463', 'gjdfhjdfhdfh', 'dfhdfhdfh/dfhdfhdfhproof.JPG', 'east', 'dfhdfhdfh', 'dfhdfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 10:10:16', '2018-05-24 10:10:16', '0'),
(59, 'lhlkjl;kjkl', 'company_0021_0059', 'company_0021', 'kjlkjkljkl', 'kljkljlkj', 'ljlkjklj@ljhljk.sdg', 'undefined', '6545645655', 'lkjkljkljljklkjlkj', 'lhlkjl;kjkl/kjlkjkljklproof.JPG', 'west', 'cvncvncvn', 'cvncvncvnvc', 'Admin', '0', '0', 'Admin', '2018-05-24 10:13:08', '2018-05-24 10:13:08', '0'),
(60, 'fgjfgjfgjfgj', 'company_0022_0060', 'company_0022', 'fgjfgjfgjfgjfgj', 'dfhdfhdfh', 'dfhdfhdfh@dfhdf.dfh', 'undefined', '5745645654', 'dfhdfhdfhdfh', 'fgjfgjfgjfgj/fgjfgjfgjfgjfgjproof.JPG', 'north', 'dfhdfhdfh', 'dfhdfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 10:15:23', '2018-05-24 10:15:23', '0'),
(61, 'sdgsdgsdgsdg', 'company_0023_0061', 'company_0023', 'sdgsdgsdgsdg', 'sdgsdgsdgsdg', 'sdgsdgsdgs@djdfg.fd', 'undefined', '6845675674', 'dfdfhdfhdfhdfh', 'sdgsdgsdgsdg/sdgsdgsdgsdgproof.JPG', 'south', 'dfhdfhdfh', 'dfhdfhdfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 10:17:38', '2018-05-24 10:17:38', '0'),
(62, 'kljnhlkjlkjkl', 'company_0024_0062', 'company_0024', 'kljkljkljlkj', 'ljlkjkljlk', 'lkjljlkjlkjkl@dfhd.dg', 'undefined', '6795695689', 'dfhdfhdfh', 'kljnhlkjlkjkl/kljkljkljlkjproof.JPG', 'south', 'dhdfhdfhdfh', 'dfhdfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 10:28:19', '2018-05-24 10:28:19', '0'),
(63, 'sdsdgsd', 'company_0025_0063', 'company_0025', 'sdgsdgsdg', 'sdgsdgsdg', 'sdgsdgsd@gj.df', 'undefined', '4574555433', 'fhdfhdfh', 'sdsdgsd_sdgsdgsdg_proof.JPG', 'south', 'dfhdfh', 'fhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 14:12:22', '2018-05-24 14:12:22', '0'),
(64, 'sdfhdfh', 'company_0026_0064', 'company_0026', 'dfhdfhdfh', 'dfhdfhdfh', 'dfhdfh@fk.fgj', 'undefined', '3453463463', 'fjdfjhdfhdfhdfh', 'sdfhdfh_dfhdfhdfh_proof.JPG', 'south', 'sdhgsdgh', 'sdgsdgsdgsdgsdg', 'Admin', '0', '0', 'Admin', '2018-05-24 14:14:12', '2018-05-24 14:14:12', '0'),
(65, 'dfhdfhdfh', 'company_0027_0065', 'company_0027', 'dfhdfhdfh', 'dfhdfhdfh', 'dfhdfhdfh@fgj.fgj', 'undefined', '3463463463', 'fdfhdfhdfhdfh', 'dfhdfhdfh_dfhdfhdfh_proof.jpeg', 'south', 'dfhdfh', 'dfhdfhdfh', 'Admin', '0', '0', 'Admin', '2018-05-24 14:16:12', '2018-05-24 14:16:12', '0'),
(66, 'sdghsdghsdhg', 'company_0028_0066', 'company_0028', 'sdghsdgsdg', 'sdghsdgsdg', 'sdhgsdfg@fgf.sdfh', 'undefined', '3456346346', 'sdghsdsdghsdgh', 'sdghsdghsdhg_sdghsdgsdg_proof.png', 'west', 'sdghsdg', 'sdgsdgsdg', 'Admin', '0', '0', 'Admin', '2018-05-24 14:17:45', '2018-05-24 14:17:45', '0'),
(67, 'sdfhsdhsdh', 'company_0029_0067', 'company_0029', 'sdhsdhsdh', 'sdhsdhdsh', 'sdhsdh@gjf.df', '4376346344', '3456345655', 'sdhsdhsdh', 'sdfhsdhsdh_sdhsdhsdh_proof.png', 'south', 'sdhsdh', 'sdhsdhsdh', 'Admin', '0', '0', 'Admin', '2018-05-24 14:19:18', '2018-05-24 14:19:18', '0'),
(68, 'EMP_U1010', 'Company_0008_0068', 'Company_0008', 'Rakesh', 'Kumar', 'djdf@gfjf.fg', '4578434344', '9966220011', 'Ramalingam nagar,kottivakkam', NULL, 'east', 'saidapet', 'chennai', 'Manager', '0', '0', NULL, '2018-05-27 23:14:36', '2018-05-27 23:14:36', '0'),
(69, 'EMP_U1010', 'Company_0008_0069', 'Company_0008', 'dsf', 'sw', 'dfhjdf@dyer.fh', '3465776554', '', 'ftgvbhjnkyjhfg', NULL, 'tyfr', 'tygf', 'tygfty', 'admin', '0', '0', NULL, '2018-05-27 23:14:41', '2018-05-27 23:14:41', '0'),
(70, 'KT00201', 'company_0001_0070', 'company_0001', 'Hari', 'S', 'srihariram777@hotmail.com', '9585555888', '9555557112', 'gjdgjdjdfj', 'KT00201/Hariproof.jpeg', 'east', 'shsdhsdh', 'sdhsdhsdhsdhsdh', 'Admin', '9', 'prjname', '', '2018-05-28 01:18:32', '2018-05-28 06:48:32', '0'),
(71, 'sdfhsdh', 'company1_0071', 'company1', 'sdgsdg', 'sdgsdg', 'sdgsd@dfhjd.dfg', '4578457455', '3463463444', 'sdfhdfhdfh', '/img/user_proofs/sdfhsdh_sdgsdg_proof.JPG', 'south', 'dfhdfhdfh', 'fhdfhdfhfdhd', 'Technician', '0', '0', '8,11', '2018-05-28 07:18:37', '2018-05-28 07:18:37', '0');

-- --------------------------------------------------------

--
-- Table structure for table `assessment`
--

CREATE TABLE `assessment` (
  `id` int(11) NOT NULL,
  `assessment_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` varchar(255) COLLATE utf8_bin NOT NULL,
  `productcategory` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `assessment`
--

INSERT INTO `assessment` (`id`, `assessment_id`, `title`, `type`, `productcategory`, `company_id`, `updated_at`, `created_at`) VALUES
(3, 'assessment_001', 'Nanthakumars', 'product', 'wall tiling', 'company1', '2018-03-05 11:29:51', '2018-03-05 11:29:51'),
(4, 'assessment_004', 'Highcharts Demo', 'generic', 'undefined', 'company1', '2018-05-25 07:12:03', '2018-05-25 07:12:03'),
(5, 'assessment_005', 'Electrical', 'generic', 'undefined', 'company_0001', '2018-05-28 05:03:17', '2018-05-28 05:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(22) NOT NULL,
  `attendance_date` date NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time DEFAULT NULL,
  `employee_id` varchar(250) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `attendance_date`, `in_time`, `out_time`, `employee_id`, `created_at`, `updated_at`) VALUES
(1, '2018-05-08', '06:29:00', '16:04:10', 'Emp_0078', '2018-05-07 04:16:00', '2018-05-21 16:04:10'),
(3, '2018-05-07', '18:06:27', '16:04:10', 'Emp_0078', '2018-05-07 18:06:27', '2018-05-21 16:04:10'),
(4, '2018-05-09', '13:58:29', '16:04:10', 'Emp_0250', '2018-05-09 13:58:29', '2018-05-21 16:04:10'),
(7, '2018-05-09', '19:15:37', '16:04:10', 'Emp_0251', '2018-05-09 19:15:37', '2018-05-21 16:04:10'),
(10, '2018-05-10', '11:18:53', '16:04:10', 'Emp_0250', '2018-05-10 11:18:53', '2018-05-21 16:04:10'),
(11, '2018-05-10', '11:19:19', '16:04:10', 'Emp_0251', '2018-05-10 11:19:19', '2018-05-21 16:04:10'),
(12, '2018-05-11', '12:22:38', '16:04:10', 'Emp_0250', '2018-05-11 12:22:38', '2018-05-21 16:04:10'),
(13, '2018-05-14', '10:04:59', '16:04:10', 'Emp_0250', '2018-05-14 10:04:59', '2018-05-21 16:04:10'),
(14, '2018-05-14', '10:47:24', '16:04:10', 'Emp_0251', '2018-05-14 10:47:24', '2018-05-21 16:04:10'),
(15, '2018-05-17', '14:57:09', '16:04:10', 'Emp_0250', '2018-05-17 14:57:09', '2018-05-21 16:04:10'),
(16, '2018-05-19', '17:25:25', '16:04:10', 'Emp_0251', '2018-05-19 17:25:25', '2018-05-21 16:04:10'),
(17, '2018-05-21', '16:03:59', '16:04:10', 'Emp_0251', '2018-05-21 16:03:59', '2018-05-21 16:04:10');

-- --------------------------------------------------------

--
-- Table structure for table `batchelors`
--

CREATE TABLE `batchelors` (
  `id` int(20) NOT NULL,
  `batch_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_id` int(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `batchelors`
--

INSERT INTO `batchelors` (`id`, `batch_type`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, 'Yes', 1, '2018-04-24 08:40:12', '2018-04-24 08:40:12'),
(2, 'No', 1, '2018-04-24 08:40:12', '2018-04-24 08:40:12'),
(3, 'Yes', 2, '2018-04-24 08:40:24', '2018-04-24 08:40:24'),
(4, 'No', 2, '2018-04-24 08:40:24', '2018-04-24 08:40:24');

-- --------------------------------------------------------

--
-- Table structure for table `bathrooms`
--

CREATE TABLE `bathrooms` (
  `id` int(255) NOT NULL,
  `bathroom` varchar(255) COLLATE utf8_bin NOT NULL,
  `sub_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bathrooms`
--

INSERT INTO `bathrooms` (`id`, `bathroom`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, '1', 1, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(2, '2', 1, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(3, '3', 1, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(4, '3+', 1, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(5, '1', 2, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(6, '2', 2, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(7, '3', 2, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(8, '3+', 2, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(9, '1', 5, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(10, '2', 5, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(11, '3', 5, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(12, '3+', 5, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(13, '1', 6, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(14, '2', 6, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(15, '3', 6, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(16, '3+', 6, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(17, '1', 7, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(18, '2', 7, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(19, '3', 7, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(20, '3+', 7, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(21, '1', 9, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(22, '2', 9, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(23, '3', 9, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(24, '3+', 9, '2018-04-23 07:21:15', '2018-04-23 07:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `bedrooms`
--

CREATE TABLE `bedrooms` (
  `id` int(255) NOT NULL,
  `bedroom` varchar(255) COLLATE utf8_bin NOT NULL,
  `sub_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bedrooms`
--

INSERT INTO `bedrooms` (`id`, `bedroom`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, '1', 1, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(2, '2', 1, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(3, '3', 1, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(4, '3+', 1, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(5, '1', 2, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(6, '2', 2, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(7, '3', 2, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(8, '3+', 2, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(9, '1', 6, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(10, '2', 6, '2018-04-23 07:20:09', '2018-04-23 07:20:09'),
(11, '3', 6, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(12, '3+', 6, '2018-04-23 07:21:15', '2018-04-23 07:21:15'),
(13, '1', 7, '2018-04-24 06:39:17', '2018-04-24 06:39:17'),
(14, '2', 7, '2018-04-24 06:39:17', '2018-04-24 06:39:17'),
(15, '3+', 7, '2018-04-24 06:39:34', '2018-04-24 06:39:34'),
(16, '3+', 7, '2018-04-24 06:39:34', '2018-04-24 06:39:34');

-- --------------------------------------------------------

--
-- Table structure for table `be_product`
--

CREATE TABLE `be_product` (
  `id` int(11) NOT NULL,
  `be_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `product_category` varchar(255) COLLATE utf8_bin NOT NULL,
  `sub_category` varchar(255) COLLATE utf8_bin NOT NULL,
  `skill_level` varchar(255) COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bikes`
--

CREATE TABLE `bikes` (
  `id` int(20) NOT NULL,
  `bike_cat` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_id` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bikes`
--

INSERT INTO `bikes` (`id`, `bike_cat`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 'Motor Cycles', 7, '2018-04-24 10:23:36', '2018-04-24 10:23:36'),
(2, 'Scooters', 7, '2018-04-24 10:23:36', '2018-04-24 10:23:36'),
(3, 'Bicycles', 7, '2018-04-24 10:24:03', '2018-04-24 10:24:03'),
(4, 'Spare Parts', 7, '2018-04-24 10:24:03', '2018-04-24 10:24:03');

-- --------------------------------------------------------

--
-- Table structure for table `bikes_category`
--

CREATE TABLE `bikes_category` (
  `sub_id` int(20) NOT NULL,
  `sub_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_id` int(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bikes_category`
--

INSERT INTO `bikes_category` (`sub_id`, `sub_name`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 'Bajaj', 1, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(2, 'Hero Honda', 1, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(3, 'Yamaha', 1, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(4, 'Royal Enfield', 1, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(5, 'Honda', 1, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(6, 'Hero', 1, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(7, 'TVS', 1, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(8, 'Suzuki', 1, '2018-04-24 10:32:19', '2018-04-24 10:32:19'),
(9, 'KTM', 1, '2018-04-24 10:32:19', '2018-04-24 10:32:19'),
(10, 'Other Brands', 1, '2018-04-24 10:32:19', '2018-04-24 10:32:19'),
(11, 'Honda', 2, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(12, 'TVS', 2, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(13, 'Hero', 2, '2018-04-24 10:31:50', '2018-04-24 10:31:50'),
(14, 'Suzuki', 2, '2018-04-24 10:32:19', '2018-04-24 10:32:19'),
(15, 'Bajaj', 2, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(16, 'Mahindra', 2, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(17, 'Other Brands', 2, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(18, 'Hero', 3, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(19, 'Hercules', 3, '2018-04-24 10:26:47', '2018-04-24 10:26:47'),
(20, 'Other Brands', 3, '2018-04-24 10:26:47', '2018-04-24 10:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `bike_model`
--

CREATE TABLE `bike_model` (
  `id` int(20) NOT NULL,
  `model` varchar(250) COLLATE utf8_bin NOT NULL,
  `bike_subid` int(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bike_model`
--

INSERT INTO `bike_model` (`id`, `model`, `bike_subid`, `created_at`, `updated_at`) VALUES
(1, 'Avenger', 1, '2018-04-27 03:33:10', '2018-04-27 03:33:10'),
(2, 'CT 100', 1, '2018-04-27 03:33:10', '2018-04-27 03:33:10'),
(5, 'Discover', 1, '2018-04-27 03:33:10', '2018-04-27 03:33:10'),
(6, 'Platina', 1, '2018-04-27 03:33:10', '2018-04-27 03:33:10'),
(7, 'Pulsar', 1, '2018-04-27 03:33:10', '2018-04-27 03:33:10'),
(8, 'Others', 1, '2018-04-27 03:33:10', '2018-04-27 03:33:10'),
(9, 'Achiever', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(10, 'Ambition', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(13, 'CBZ', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(14, 'CD 100', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(15, 'CD Dawn', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(16, 'CD Deluxe', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(17, 'Dawn', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(18, 'Deluxe', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(19, 'Glamour', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(20, 'HX 250 R', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(21, 'Hunk', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(22, 'Ignitor', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(23, 'Impulse', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(24, 'Joy', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(25, 'Karizma', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(26, 'Passion', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(27, 'Sleek', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(28, 'Splendor', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(29, 'Super Splendor', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(30, 'Street', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(31, 'Others', 2, '2018-04-27 15:42:10', '2018-04-27 15:42:10'),
(32, 'Crux', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(33, 'Fazer', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(36, 'FZS', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(37, 'FZ', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(38, 'Saluto', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(39, 'SS 125', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(40, 'SZ', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(41, 'Vmax', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(42, 'YBR', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(43, 'YZF R', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(44, 'Others', 3, '2018-04-27 15:48:14', '2018-04-27 15:48:14'),
(45, 'Bullet', 4, '2018-04-27 15:53:23', '2018-04-27 15:53:23'),
(46, 'Classic', 4, '2018-04-27 15:53:23', '2018-04-27 15:53:23'),
(49, 'Continental GT', 4, '2018-04-27 15:53:23', '2018-04-27 15:53:23'),
(50, 'Thunderbird', 4, '2018-04-27 15:53:23', '2018-04-27 15:53:23'),
(51, 'Others', 4, '2018-04-27 15:53:23', '2018-04-27 15:53:23'),
(52, 'CB', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(53, 'CBF Stunner', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(56, 'CBR', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(57, 'CD 110 Dream', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(58, 'Dream Yuga', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(59, 'Goldwing GL 1800', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(60, 'VFR 1200 F', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(61, 'VT 1300 CX', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(62, 'Others', 5, '2018-04-27 15:56:31', '2018-04-27 15:56:31'),
(63, 'Achiever', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(64, 'Ambition', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(67, 'CBZ', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(68, 'CD 100', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(69, 'CD Dawn', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(70, 'CD Deluxe', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(71, 'Dawn', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(72, 'Deluxe', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(73, 'Glamour', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(74, 'HX 250 R', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(75, 'Hunk', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(76, 'Ignitor', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(77, 'Impulse', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(78, 'Joy', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(79, 'Karizma', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(80, 'Passion', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(81, 'Sleek', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(82, 'Splendor', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(83, 'Super Splendor', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(84, 'Street', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(85, 'Others', 6, '2018-04-27 16:07:37', '2018-04-27 16:07:37'),
(86, 'Apache RTR', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(87, 'Heavy Duty Super XL', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(88, 'Apache RTR', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(90, 'Phoenix', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(91, 'Star City Plus', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(92, 'Star Sport', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(93, 'Others', 7, '2018-04-27 16:04:21', '2018-04-27 16:04:21'),
(94, 'Bandit', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(95, 'Gixxer', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(98, 'GS', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(99, 'GSX', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(100, 'Hayate', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(101, 'Hayabusa', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(102, 'Inazuma', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(103, 'M1800 R', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(104, 'M800', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(105, 'Slingshot', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(106, 'V Storm', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(107, 'Others', 8, '2018-04-27 16:18:24', '2018-04-27 16:18:24'),
(108, 'Duke 200', 9, '2018-04-27 16:23:08', '2018-04-27 16:23:08'),
(109, '390 Duke ABS', 9, '2018-04-27 16:23:08', '2018-04-27 16:23:08'),
(112, 'RC', 9, '2018-04-27 16:23:08', '2018-04-27 16:23:08'),
(113, 'Others', 9, '2018-04-27 16:23:08', '2018-04-27 16:23:08'),
(114, 'Dio', 11, '2018-04-27 16:39:06', '2018-04-27 16:39:06'),
(115, 'Aviator', 11, '2018-04-27 16:39:06', '2018-04-27 16:39:06'),
(118, 'Activa', 11, '2018-04-27 16:39:06', '2018-04-27 16:39:06'),
(119, 'Others', 11, '2018-04-27 16:39:06', '2018-04-27 16:39:06'),
(120, 'Scooty', 12, '2018-04-27 16:42:11', '2018-04-27 16:42:11'),
(121, 'Wego', 12, '2018-04-27 16:42:11', '2018-04-27 16:42:11'),
(124, 'Jupiter', 12, '2018-04-27 16:42:11', '2018-04-27 16:42:11'),
(125, 'Others', 12, '2018-04-27 16:42:11', '2018-04-27 16:42:11'),
(126, 'Maestro', 13, '2018-04-27 16:44:16', '2018-04-27 16:44:16'),
(127, 'Pleasure', 13, '2018-04-27 16:44:16', '2018-04-27 16:44:16'),
(130, 'Others', 13, '2018-04-27 16:44:16', '2018-04-27 16:44:16'),
(131, 'Swish', 14, '2018-04-27 16:46:40', '2018-04-27 16:46:40'),
(132, 'Access', 14, '2018-04-27 16:46:40', '2018-04-27 16:46:40'),
(135, 'Let s', 14, '2018-04-27 16:46:40', '2018-04-27 16:46:40'),
(136, 'Others', 14, '2018-04-27 16:46:40', '2018-04-27 16:46:40'),
(137, 'Kine', 16, '2018-04-27 16:55:11', '2018-04-27 16:55:11'),
(138, 'Duro', 16, '2018-04-27 16:55:11', '2018-04-27 16:55:11'),
(141, 'Rodeo', 16, '2018-04-27 16:55:11', '2018-04-27 16:55:11'),
(142, 'Sym Flyte', 16, '2018-04-27 16:55:11', '2018-04-27 16:55:11'),
(143, 'Gusto', 16, '2018-04-27 16:55:11', '2018-04-27 16:55:11'),
(144, 'Others', 16, '2018-04-27 16:55:11', '2018-04-27 16:55:11');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(20) NOT NULL,
  `car_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `cat_id` int(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `car_type`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 'Cars', 4, '2018-04-24 09:41:12', '2018-04-24 09:41:12'),
(2, 'Commercial Vehicles', 4, '2018-04-24 09:41:12', '2018-04-24 09:41:12'),
(3, 'Other Vehicles', 4, '2018-04-24 09:41:35', '2018-04-24 09:41:35'),
(4, 'Spare Parts', 4, '2018-04-24 09:41:35', '2018-04-24 09:41:35');

-- --------------------------------------------------------

--
-- Table structure for table `cars_subtype`
--

CREATE TABLE `cars_subtype` (
  `id` int(20) NOT NULL,
  `car_subtype` varchar(200) COLLATE utf8_bin NOT NULL,
  `car_id` int(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cars_subtype`
--

INSERT INTO `cars_subtype` (`id`, `car_subtype`, `car_id`, `created_at`, `updated_at`) VALUES
(1, 'Maruti Suzuki', 1, '2018-04-24 09:48:29', '2018-04-24 09:48:29'),
(2, 'Hyundai', 1, '2018-04-24 09:48:29', '2018-04-24 09:48:29'),
(3, 'Tata', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(4, 'Mahindra', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(5, 'Toyota', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(6, 'Honda', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(7, 'Chevrolet', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(8, 'Ford', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(9, 'Skoda', 1, '2018-04-24 09:50:36', '2018-04-24 09:50:36'),
(10, 'Volkswagen', 1, '2018-04-24 09:51:17', '2018-04-24 09:51:17'),
(11, 'Fiat', 1, '2018-04-24 09:51:17', '2018-04-24 09:51:17'),
(12, 'Nissan', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(13, 'Renault', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(14, 'Mitsubishi', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(15, 'Mercedes-Bens', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(16, 'Audi', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(17, 'Mahindra Renault', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(18, 'BMW', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(19, 'Hindustan Motors', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20'),
(20, 'Others', 1, '2018-04-24 09:53:20', '2018-04-24 09:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `car_fuel`
--

CREATE TABLE `car_fuel` (
  `id` int(20) NOT NULL,
  `fuel_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `car_id` int(200) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `car_fuel`
--

INSERT INTO `car_fuel` (`id`, `fuel_type`, `car_id`, `updated_at`, `created_at`) VALUES
(1, 'Petrol', 1, '2018-05-03 09:18:00', '2018-05-03 09:18:00'),
(2, 'Diesel', 1, '2018-05-03 09:18:00', '2018-05-03 09:18:00'),
(3, 'LPG', 1, '2018-05-03 09:18:00', '2018-05-03 09:18:00'),
(4, 'CNG & Hybrids', 1, '2018-05-03 09:18:00', '2018-05-03 09:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `id` int(20) NOT NULL,
  `model` varchar(200) COLLATE utf8_bin NOT NULL,
  `car_subid` int(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`id`, `model`, `car_subid`, `created_at`, `updated_at`) VALUES
(1, '1000', 1, '2018-04-27 05:10:09', '2018-04-27 12:41:59'),
(2, '800', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(3, 'Alto', 1, '2018-04-27 05:10:09', '2018-04-27 12:41:59'),
(4, 'A Star', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(5, 'Alto 800', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(6, 'Baleno', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(7, 'Celerio', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(8, 'Ciaz', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(9, 'Eeco', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(10, 'Ertiga', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(11, 'Esteem', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(12, 'Grand Vitara', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(13, 'Gypsy', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(14, 'Kizashi', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(15, 'Omni', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(16, 'Ritz', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(17, 'Swift', 1, '2018-04-27 05:10:09', '2018-04-27 05:10:09'),
(18, 'Swift Dzire', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(19, 'Sx4', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(20, 'Versa', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(21, 'Wagon R', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(22, 'Wagon R 1.0', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(23, 'Wagon R Duo', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(24, 'Wagon R Stingray', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(25, 'Zen', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(26, 'Zen Estilo', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(27, 'Vitara Brezza', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(28, 'Others', 1, '2018-04-27 10:17:21', '2018-04-27 10:17:21'),
(29, 'Accent', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(30, 'Elantra', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(31, 'Elite i20', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(32, 'Eon', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(33, 'Getz', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(34, 'Getz Prime', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(35, 'Grand I 10', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(36, 'I10', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(37, 'I20', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(38, 'New Elantra', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(39, 'Santa Fe', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(40, 'Santro', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(41, 'Santro Xing', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(42, 'Sonata', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(43, 'Sonata Embera', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(44, 'Sonata Transform', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(45, 'Terracan', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(46, 'Tucson', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(47, 'Verna', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(48, 'Xcent', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(49, 'Others', 2, '2018-04-27 11:26:20', '2018-04-27 11:26:20'),
(50, 'Aria', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(51, 'Estate', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(52, 'Grande Dicor', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(53, 'Indica', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(54, 'Indica E V2', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(55, 'Indica Ev2', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(56, 'Indica V2', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(57, 'Indica V2 Turbo', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(58, 'Indica V2 Xeta', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(59, 'Indica Vista', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(60, 'Indicab', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(61, 'Indigo', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(62, 'Indigo Cs', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(63, 'Indigo Marina', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(64, 'Indigo Xl', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(65, 'Mansa', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(66, 'Movus', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(67, 'Nano', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(68, 'Safari', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(69, 'Safari Storme', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(70, 'Sierra', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(71, 'Sumo', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(72, 'Sumo Gold', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(73, 'Sumo Grande', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(74, 'Tiago', 3, '2018-04-27 11:42:25', '2018-04-27 11:42:25'),
(75, 'Venture', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(76, 'Vista', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(77, 'Vista Tech', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(78, 'Winger', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(79, 'Xenon Xt', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(80, 'Zest', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(81, 'Others', 3, '2018-04-27 11:49:53', '2018-04-27 11:49:53'),
(82, 'Voyeger', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(83, 'Xuv500', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(84, 'Xylo', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(85, 'TUV', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(86, 'Armada', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(87, 'Bolero', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(88, 'Armada', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(89, 'Bolero', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(90, 'E 20', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(91, 'Quanto', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(92, 'Reva', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(93, 'Scorpio', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(94, 'Thar', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(95, 'Verito', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(96, 'Verito Vibe Cs', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(97, 'Others', 4, '2018-04-27 09:32:00', '2018-04-27 09:32:00'),
(98, 'Camry', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(99, 'Corolla', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(100, 'Camry', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(101, 'Corolla', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(102, 'Corolla Altis', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(103, 'Etios', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(104, 'Etios Liva', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(105, 'Fortuner', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(106, 'Innova', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(107, 'Land Cruiser', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(108, 'Land Cruiser Prado', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(109, 'Prius', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(110, 'Qualis', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(111, 'Others', 5, '2018-04-27 12:43:09', '2018-04-27 12:43:09'),
(112, 'Accord', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(113, 'Amaze', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(114, 'Accord', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(115, 'Amaze', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(116, 'Brio', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(117, 'City', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(118, 'City Zx', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(119, 'Civic', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(120, 'Civic Hybrid', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(121, 'Crv', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(122, 'Jazz', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(123, 'Mobilio', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(124, 'Others', 6, '2018-04-27 12:50:11', '2018-04-27 12:50:11'),
(125, 'Aveo', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(126, 'Aveo U Va', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(127, 'Aveo', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(128, 'Aveo U Va', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(129, 'Beat', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(130, 'Captiva', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(131, 'Cruze', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(132, 'Enjoy', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(133, 'Forester', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(134, 'Optra', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(135, 'Optra Magnum', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(136, 'Optra Srv', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(137, 'Sail', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(138, 'Sail Uva', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(139, 'Spark', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(140, 'Tavera', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(141, 'Others', 7, '2018-04-27 12:55:36', '2018-04-27 12:55:36'),
(142, 'Classic', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(143, 'Ecosport', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(144, 'Classic', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(145, 'Ecosport', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(146, 'Endeavour', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(147, 'Escort', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(148, 'Fiesta', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(149, 'Fiesta Classic', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(150, 'Figo', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(151, 'Fusion', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(152, 'Ikon', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(153, 'Mondeo', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(154, 'Figo Aspire', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(155, 'Others', 8, '2018-04-27 13:02:23', '2018-04-27 13:02:23'),
(156, 'Fabia', 9, '2018-04-27 13:08:42', '2018-04-27 13:08:42'),
(157, 'Laura', 9, '2018-04-27 13:08:42', '2018-04-27 13:08:42'),
(158, 'Octavia', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(159, 'Octavia Combi', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(160, 'Octavia', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(161, 'Octavia Combi', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(162, 'Rapid', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(163, 'Superb', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(164, 'Yeti', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(165, 'Others', 9, '2018-04-27 14:05:28', '2018-04-27 14:05:28'),
(166, 'Passat', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(169, 'Phaeton', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(170, 'Polo', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(171, 'Touareg', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(172, 'Vento', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(173, 'Others', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(174, 'Beetle', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(175, 'Cross Polo', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(176, 'Jetta', 10, '2018-04-27 13:10:04', '2018-04-27 13:10:04'),
(177, '500', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(178, 'Adventure', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(181, 'Avvventura', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(182, 'Grand Punto', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(183, 'Linea', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(184, 'Palio', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(185, 'Palio D', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(186, 'Palio Nv', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(187, 'Palio Stile', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(188, 'Petra', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(189, 'Petra D', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(190, 'Punto Evo', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(191, 'Siena', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(192, 'Siena Weekend', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(193, 'Uno', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(194, 'Others', 11, '2018-04-27 02:20:44', '2018-04-27 02:20:44'),
(195, '370z', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(198, 'Evalia', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(199, 'Micra', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(200, 'Micra Active', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(201, 'Sunny', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(202, 'Teana', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(203, 'Terrano', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(204, 'X Trail', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(205, 'Datsun Redi Go', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(206, 'Others', 12, '2018-04-27 02:28:51', '2018-04-27 02:28:51'),
(207, 'Duster', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(208, 'Fluence', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(211, 'Koleos', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(212, 'Pulse', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(213, 'Scala', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(214, 'Kwid', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(215, 'Lodgy', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(216, 'Others', 13, '2018-04-27 14:34:20', '2018-04-27 14:34:20'),
(217, 'Cedia', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(218, 'Lancer', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(221, 'Lancer Evolution', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(222, 'Montero', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(223, 'Outlander', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(224, 'Pajero', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(225, 'Pajero Sport', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(226, 'Others', 14, '2018-04-27 02:38:42', '2018-04-27 02:38:42'),
(227, 'A Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(228, 'B Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(231, 'C Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(232, 'Cla Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(233, 'Cls', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(234, 'Cls Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(235, 'E Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(236, 'G Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(237, 'Gl Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(238, 'M Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(239, 'Ml Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(240, 'R Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(241, 'S Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(242, 'Sdl', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(243, 'Sl Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(244, 'Slk Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(245, 'Sls Class', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(246, 'Viano', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(247, 'Others', 15, '2018-04-27 14:42:06', '2018-04-27 14:42:06'),
(248, 'A7', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(249, 'A8', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(252, 'A8 L', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(253, 'A8l', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(254, 'Q3', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(255, 'Q5', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(256, 'Q7', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(257, 'R8', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(258, 'Rs 5', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(259, 'Rs 7', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(260, 'S6', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(261, 'Tt', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(262, 'Others', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(263, 'A3', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(264, 'A4', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(265, 'A6', 16, '2018-04-27 14:54:05', '2018-04-27 14:54:05'),
(266, 'Logan', 17, '2018-04-27 15:05:12', '2018-04-27 15:05:12'),
(267, 'Others', 17, '2018-04-27 15:05:12', '2018-04-27 15:05:12'),
(268, '1 Series', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(269, '3 Series', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(272, '5 Series', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(273, '5 Series Gt', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(274, '6 Series', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(275, '7 Series', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(276, 'Gran Turismo', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(277, 'M3', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(278, 'M5', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(279, 'X5 M', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(280, 'M6', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(281, 'X6 M', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(282, 'Mini', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(283, 'X1', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(284, 'X3', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(285, 'X5', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(286, 'X6', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(287, 'Z4', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(288, 'Others', 18, '2018-04-27 15:06:14', '2018-04-27 15:06:14'),
(289, 'Ambassador', 19, '2018-04-27 15:16:29', '2018-04-27 15:16:29'),
(290, 'Contessa', 19, '2018-04-27 15:16:29', '2018-04-27 15:16:29'),
(291, 'Ambassador', 19, '2018-04-27 15:16:29', '2018-04-27 15:16:29'),
(292, 'Contessa', 19, '2018-04-27 15:16:29', '2018-04-27 15:16:29'),
(293, 'Others', 19, '2018-04-27 15:16:29', '2018-04-27 15:16:29');

-- --------------------------------------------------------

--
-- Table structure for table `car_parkings`
--

CREATE TABLE `car_parkings` (
  `id` int(20) NOT NULL,
  `car_park` varchar(20) COLLATE utf8_bin NOT NULL,
  `sub_id` int(200) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `car_parkings`
--

INSERT INTO `car_parkings` (`id`, `car_park`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, '1', 1, '2018-04-24 06:02:37', '2018-04-24 06:02:37'),
(2, '2', 1, '2018-04-24 06:02:37', '2018-04-24 06:02:37'),
(3, '3', 1, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(4, '3+', 1, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(5, '1', 2, '2018-04-24 06:03:14', '2018-04-24 06:03:14'),
(6, '2', 2, '2018-04-24 06:03:14', '2018-04-24 06:03:14'),
(7, '3', 2, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(8, '3+', 2, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(9, '1', 3, '2018-04-24 06:03:14', '2018-04-24 06:03:14'),
(10, '2', 3, '2018-04-24 06:03:14', '2018-04-24 06:03:14'),
(11, '3', 3, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(12, '3+', 3, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(13, '1', 5, '2018-04-24 06:03:14', '2018-04-24 06:03:14'),
(14, '2', 5, '2018-04-24 06:03:14', '2018-04-24 06:03:14'),
(15, '3', 5, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(16, '3+', 5, '2018-04-24 06:02:52', '2018-04-24 06:02:52'),
(17, '1', 6, '2018-04-24 00:32:37', '2018-04-24 00:32:37'),
(18, '2', 6, '2018-04-24 00:32:37', '2018-04-24 00:32:37'),
(19, '3', 6, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(20, '3+', 6, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(21, '1', 7, '2018-04-24 00:33:14', '2018-04-24 00:33:14'),
(22, '2', 7, '2018-04-24 00:33:14', '2018-04-24 00:33:14'),
(23, '3', 7, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(24, '3+', 7, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(25, '1', 8, '2018-04-24 00:33:14', '2018-04-24 00:33:14'),
(26, '2', 8, '2018-04-24 00:33:14', '2018-04-24 00:33:14'),
(27, '3', 8, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(28, '3+', 8, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(29, '1', 9, '2018-04-24 00:33:14', '2018-04-24 00:33:14'),
(30, '2', 9, '2018-04-24 00:33:14', '2018-04-24 00:33:14'),
(31, '3', 9, '2018-04-24 00:32:52', '2018-04-24 00:32:52'),
(32, '3+', 9, '2018-04-24 00:32:52', '2018-04-24 00:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `category_details`
--

CREATE TABLE `category_details` (
  `id` int(11) NOT NULL,
  `cat_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `cat_image` varchar(500) COLLATE utf8_bin NOT NULL,
  `prod_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `cat_modal` varchar(150) COLLATE utf8_bin NOT NULL,
  `cat_desc` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `category_details`
--

INSERT INTO `category_details` (`id`, `cat_id`, `cat_name`, `cat_image`, `prod_id`, `company_id`, `cat_modal`, `cat_desc`, `last_update`) VALUES
(1, 'subcategory_0001', 'LED TV', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0001', 'product_0005', 'company1', '', 'A flat panel LCD TV set that uses LEDs (light emitting diodes) for its backlight source rather than the earlier cold cathode fluorescent lamps', '2017-09-15 08:29:11'),
(2, 'subcategory_0002', 'LCD TV', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0002', 'product_0005', 'company1', '', 'LCD TV is a television display technology based on a liquid crystal display', '2017-09-15 08:29:15'),
(3, 'subcategory_0003', 'Tube Type TV', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0003', 'product_0005', 'company1', '', 'The cathode ray tube (CRT) is a vacuum tube that contains one or more electron guns and a phosphorescent screen, and is used to display images.', '2017-09-15 08:29:20'),
(4, 'subcategory_0004', 'Bottom freezer', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0004', 'product_0003', 'company1', '', 'The freezer is located underneath the refrigerator section', '2017-09-15 08:15:28'),
(5, 'subcategory_0005', 'Side-by-side	', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0005', 'product_0003', 'company1', '', 'The freezer and refrigerator are placed directly next to each other', '2017-09-15 08:16:10'),
(6, 'subcategory_0006', 'Built-in	', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0006', 'product_0003', 'company1', '', '	Constructed to blend in with the rest of the kitchen cabinetry rather than being installed as a separate unit', '2017-09-15 08:16:43'),
(7, 'subcategory_0009', 'Polycrystalline Solar Panel SSI250W', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0009', 'product_0001', 'company1', '', 'Polycrystalline solar panels are also made from silicon. However, instead of using a single crystal of silicon, manufacturers melt many fragments of s', '2017-09-19 09:26:04'),
(8, 'subcategory_0010', 'Monocrystalline solar panels', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0010', 'product_0001', 'company1', '', 'To make solar cells for monocrystalline solar panels, silicon is formed into bars and cut into wafers. These types of panels are called “monocrystalli', '2017-09-15 08:29:27'),
(9, 'subcategory_0011', 'Grade Blue Monocrystalline cell', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0011', 'product_0001', 'company1', '', 'Image result for solar cell description\r\nSolar cells can be classified into first, second and third generation cells. ', '2017-09-15 08:29:30'),
(10, 'subcategory_0012', 'Window AC', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0012', 'product_0006', 'company1', '', 'The working of window air conditioner can be explained by separately considering the two cycles of air: room air cycle and the hot air cycle.', '2017-09-18 01:10:53'),
(11, 'subcategory_0014', 'mini solar panel', 'http://85.25.134.22/fieldpro/assets/images/subcategory_0014', 'product_0009', 'company1', '', 'solar panel fabric debris from tree', '2017-09-19 05:55:47'),
(12, 'subcategory_0015', 'welcome', 'http://localhost/fieldpro/assets/images/subcategory_0015', 'product_0001', 'company1', '', 'dsdjfhgsf', '2017-09-19 09:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `classified_categories`
--

CREATE TABLE `classified_categories` (
  `id` int(255) NOT NULL,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `classified_categories`
--

INSERT INTO `classified_categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Properties', '2018-04-23 09:53:54', '2018-04-23 09:53:54'),
(2, 'Furnitures', '2018-04-23 09:53:54', '2018-04-23 09:53:54'),
(3, 'Mobiles', '2018-04-23 09:54:22', '2018-04-23 09:54:22'),
(4, 'Cars', '2018-04-23 09:54:22', '2018-04-23 09:54:22'),
(5, 'Electronics', '2018-04-23 09:54:22', '2018-04-23 09:54:22'),
(6, 'Pets', '2018-04-23 09:55:12', '2018-04-23 09:55:12'),
(7, 'Bikes', '2018-04-23 09:55:12', '2018-04-23 09:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `classified_table`
--

CREATE TABLE `classified_table` (
  `id` int(20) NOT NULL,
  `post_title` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `post_desc` varchar(2500) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `classified_category` int(200) NOT NULL DEFAULT '0',
  `property_type` int(200) NOT NULL DEFAULT '0',
  `property_subtype` int(200) NOT NULL DEFAULT '0',
  `price` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `bedrooms` int(200) NOT NULL DEFAULT '0',
  `bathrooms` int(200) NOT NULL DEFAULT '0',
  `furnishing` int(200) NOT NULL DEFAULT '0',
  `cons_status` int(200) NOT NULL DEFAULT '0',
  `batchelors` int(200) NOT NULL DEFAULT '0',
  `maintanance` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `total_floors` int(200) DEFAULT '0',
  `floor_no` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `car_parking` int(200) NOT NULL DEFAULT '0',
  `facing` int(200) NOT NULL DEFAULT '0',
  `meals` int(200) NOT NULL DEFAULT '0',
  `furniture_type` int(200) NOT NULL DEFAULT '0',
  `mobile_category` int(200) NOT NULL DEFAULT '0',
  `mobile_subcat` int(200) NOT NULL DEFAULT '0',
  `cars` int(200) NOT NULL DEFAULT '0',
  `cars_subtype` int(200) NOT NULL DEFAULT '0',
  `model` int(200) NOT NULL DEFAULT '0',
  `year` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `fuel` int(200) NOT NULL DEFAULT '0',
  `km_driven` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `electronic_type` int(200) NOT NULL DEFAULT '0',
  `pets_category` int(200) NOT NULL DEFAULT '0',
  `bike_category` int(200) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(11) NOT NULL,
  `company_id` text COLLATE utf8_bin,
  `company_name` text COLLATE utf8_bin,
  `company_email` text COLLATE utf8_bin,
  `company_num` text COLLATE utf8_bin,
  `alternate_num` text COLLATE utf8_bin,
  `address` text COLLATE utf8_bin,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`id`, `company_id`, `company_name`, `company_email`, `company_num`, `alternate_num`, `address`, `updated_at`, `created_at`) VALUES
(1, 'company_0001', 'Kaspon Techworks', 'kaspon@gmail.com', '9632587410', NULL, 'Kottivakkam, Chennai', '2018-05-14 11:40:20', '2018-05-14 11:40:20'),
(2, 'company_0002', 'Infognana Solutions', 'infognana@gmail.com', '9632587410', NULL, 'Kottivakkam, Chennai', '2018-05-14 11:44:49', '2018-05-14 11:44:49'),
(8, 'company_0008', 'dfjdfjdfj', 'dfjdfj@kjhjk.hjg', '5464646444', '5545565655', 'ljkhljkklj', '2018-05-24 14:48:05', '2018-05-24 14:48:05'),
(9, 'company_0009', 'dfjdfjdfjdfj', 'dfjdfjdf@kjhjk.hjg', '5456456444', '4654565555', 'kjhjkhjkhkhkj', '2018-05-24 14:59:32', '2018-05-24 14:59:32'),
(10, 'company_0010', 'sdgsdgsdg', 'sdgsdgsd@dfhjdf.dfh', '5454655565', '5625555555', 'lhjiojiojoljkj', '2018-05-24 15:02:21', '2018-05-24 15:02:21'),
(11, 'company_0011', 'fgjkgdfg', 'dfjhdfhjdfh', 'dfhdfhd', 'fhdfhdfh', 'dfhdfhd', '2018-05-24 15:04:48', '2018-05-24 15:04:48'),
(12, 'company_0012', 'dfhdfhdfhdfh', 'dfhdfhdfh@dfhdf.dfh', '3146545555', '3415634654', 'lkjklhklhlkjlkjlkjlkj', '2018-05-24 15:09:39', '2018-05-24 15:09:39'),
(13, 'company_0013', 'dfhdfhdfhdfhd', 'fhdfhdf@khikj.khj', '5456444555', '6546546555', 'ljkhljkhkljkljlk', '2018-05-24 15:14:10', '2018-05-24 15:14:10'),
(14, 'company_0014', 'dfhdfhdfhdfhd', 'fhdfhdf@khikj.khj', '5456444555', '6546546555', 'ljkhljkhkljkljlk', '2018-05-24 15:14:41', '2018-05-24 15:14:41'),
(15, 'company_0015', 'dfhdfhdfhdf', 'hdfhdfh@ljl.jkg', '4654564654', '4654654445', 'kjhjkhjklhljklk', '2018-05-24 15:25:20', '2018-05-24 15:25:20'),
(16, 'company_0016', 'dfhjdfhjdfhj', 'dfhdfhdfh@dfhd.dfh', '6545646545', '6545645655', 'dfjdfjsdfjdfjdfj', '2018-05-24 15:30:31', '2018-05-24 15:30:31'),
(17, 'company_0017', 'sfghdfhdfh', 'dfhdfhd@dfhd.dfg', '5465464455', '5465456455', 'ohjujkohohjlojklk', '2018-05-24 15:32:11', '2018-05-24 15:32:11'),
(18, 'company_0018', 'dfhdfhdfh', 'dfhdfhdf@dgfhjd.df', '6545645645', '5464566566', 'jlhkljhkljkljkljlk', '2018-05-24 15:35:13', '2018-05-24 15:35:13'),
(19, 'company_0019', 'vhm,gfhkfgj', 'fgjfgj@dfhjf.fg', '3463463463', '3456346346', 'dfhdfhdfhdfh', '2018-05-24 15:38:16', '2018-05-24 15:38:16'),
(20, 'company_0020', 'sdhsdhdfh', 'dfhdfh@dfhd.fdg', '3763463465', '4574574574', 'gjdfjdfjhdfhdfh', '2018-05-24 15:40:16', '2018-05-24 15:40:16'),
(21, 'company_0021', 'nhdfhjdfhdfh', 'dfhdfhsdfh@fgjf.dfh', '6255555555', '6546545655', 'ljkhkljkljkl', '2018-05-24 15:43:08', '2018-05-24 15:43:08'),
(22, 'company_0022', 'sdhsdghsdgs', 'sdgsdgs@kjhjk.gjhg', '8767896897', '4565456455', 'gfjfgjfgjfgj', '2018-05-24 15:45:23', '2018-05-24 15:45:23'),
(23, 'company_0023', 'sdgsdgsdgsdg', 'sdgsdgsd@dfhdf.gfh', '7634634634', '5634634634', 'sdfghsdhgsdgsd', '2018-05-24 15:47:38', '2018-05-24 15:47:38'),
(24, 'company_0024', 'dhdfhdfh', 'dfhdfhdfh@dfhd.dfh', '6546545644', '6545645644', 'kjnjknklkljklj', '2018-05-24 15:58:18', '2018-05-24 15:58:18'),
(25, 'company_0025', 'sdghsdg', 'sdgsdg@sdgs.er', '3465345634', '2342342342', 'sdgsdgsdgsdg', '2018-05-24 19:42:22', '2018-05-24 19:42:22'),
(26, 'company_0026', 'dfhjdfhjsdfh', 'sdfhdf@gfj.fg', '4574574575', '3463464564', 'cdfhjdfjdfh', '2018-05-24 19:44:12', '2018-05-24 19:44:12'),
(27, 'company_0027', 'dfhdfhdfhd', 'dfhdfhdf@fgjf.dfhj', '4578456448', '4745745745', 'vfnjdfhdfhdfhdfh', '2018-05-24 19:46:12', '2018-05-24 19:46:12'),
(28, 'company_0028', 'sdhsdhsdh', 'sdhsdh@djf.dfh', '4362345235', '2352352358', 'sdhsdhgsdghsdh', '2018-05-24 19:47:45', '2018-05-24 19:47:45'),
(29, 'company_0029', 'sdgsdgsdgsdg', 'sdgsdg@rfer.sy', '3463463455', '4573457455', 'sdfhsdhsdfhs', '2018-05-24 19:49:18', '2018-05-24 19:49:18');

-- --------------------------------------------------------

--
-- Table structure for table `construction_status`
--

CREATE TABLE `construction_status` (
  `id` int(20) NOT NULL,
  `cons_status` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_id` int(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `construction_status`
--

INSERT INTO `construction_status` (`id`, `cons_status`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, 'Under Construction', 6, '2018-04-24 07:28:12', '2018-04-24 07:28:12'),
(2, 'New Launch', 6, '2018-04-24 07:28:12', '2018-04-24 07:28:12'),
(3, 'Ready to Move', 6, '2018-04-24 07:29:39', '2018-04-24 07:29:39'),
(4, 'Under Construction', 7, '2018-04-24 07:29:39', '2018-04-24 07:29:39'),
(5, 'New Launch', 7, '2018-04-24 07:29:39', '2018-04-24 07:29:39'),
(6, 'Ready to Move', 7, '2018-04-24 07:29:39', '2018-04-24 07:29:39'),
(9, 'Under Construction', 9, '2018-04-24 07:29:39', '2018-04-24 07:29:39'),
(10, 'New Launch', 9, '2018-04-24 07:29:39', '2018-04-24 07:29:39'),
(11, 'Ready to Move', 9, '2018-04-24 07:29:39', '2018-04-24 07:29:39');

-- --------------------------------------------------------

--
-- Table structure for table `contract_category`
--

CREATE TABLE `contract_category` (
  `id` int(200) NOT NULL,
  `cat_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(250) COLLATE utf8_bin NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contract_category`
--

INSERT INTO `contract_category` (`id`, `cat_name`, `company_id`, `updated_at`, `created_at`) VALUES
(1, 'useful', 'company1', '2018-01-09 07:28:23', '2018-01-09 07:28:23'),
(2, 'garde', 'company1', '2018-01-09 08:51:09', '2018-01-09 08:51:09'),
(3, 'global', 'company1', '2018-01-09 09:34:58', '2018-01-09 09:34:58'),
(4, 'sample', 'company1', '2018-01-09 10:04:54', '2018-01-09 10:04:54'),
(5, 'community', 'company1', '2018-01-23 09:47:22', '2018-01-23 09:47:22'),
(6, 'partial contract', 'company1', '2018-01-23 09:47:33', '2018-01-23 09:47:33'),
(7, 'dhgfdh', 'company1', '2018-01-25 06:48:35', '2018-01-25 06:48:35'),
(9, 'trsfcghz', 'company1', '2018-01-25 06:59:45', '2018-01-25 06:59:45');

-- --------------------------------------------------------

--
-- Table structure for table `contract_type`
--

CREATE TABLE `contract_type` (
  `id` int(200) NOT NULL,
  `contract_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `cont_cat` int(200) NOT NULL,
  `company_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contract_type`
--

INSERT INTO `contract_type` (`id`, `contract_type`, `cont_cat`, `company_id`, `updated_at`, `created_at`) VALUES
(1, 'fgf', 1, 'company1', '2018-01-24 05:59:39', '2018-01-24 05:59:39'),
(2, 'gfhfgh', 1, 'company1', '2018-01-24 06:40:01', '2018-01-24 06:40:01'),
(3, 'cvcvc', 3, 'company1', '2018-01-24 06:41:18', '2018-01-24 06:41:18'),
(4, 'udfhf', 1, 'company1', '2018-01-24 06:48:51', '2018-01-24 06:48:51'),
(5, 'yuik', 2, 'company1', '2018-01-24 06:58:42', '2018-01-24 06:58:42'),
(6, 'Part only', 2, 'company1', '2018-02-02 09:02:36', '2018-02-02 09:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer_id` text COLLATE utf8_bin NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_email` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_phone` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_alt` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `customer_address` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_nationalid` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_nationalexp` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_nationalfile` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_passport` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_passportexp` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_passportfile` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_visa` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_visaexp` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_visafile` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_commercial` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_commercialexp` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_commercialfile` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_ded` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_dedexp` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_dedfile` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_ejari` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_ejariexp` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_ejarifile` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_project` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_project_place` varchar(255) COLLATE utf8_bin NOT NULL,
  `customer_owner_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `enc_password` varchar(255) COLLATE utf8_bin NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customer_id`, `customer_name`, `customer_email`, `customer_phone`, `customer_alt`, `customer_address`, `customer_nationalid`, `customer_nationalexp`, `customer_nationalfile`, `customer_passport`, `customer_passportexp`, `customer_passportfile`, `customer_visa`, `customer_visaexp`, `customer_visafile`, `customer_commercial`, `customer_commercialexp`, `customer_commercialfile`, `customer_ded`, `customer_dedexp`, `customer_dedfile`, `customer_ejari`, `customer_ejariexp`, `customer_ejarifile`, `customer_project`, `customer_project_place`, `customer_owner_type`, `company_id`, `password`, `enc_password`, `updated_at`, `created_at`) VALUES
(1, 'Cust_0001', 'Nanthakumar', 'ramyariotz22@gmail.com', '9962987522', '8344586922', '357 First Cross Street, 5th Link Road, Nehru Nagar, Old Mahabalipuram Road, Kandancavadi, Nehru Nagar, Perungudi, Chennai, Tamil Nadu 600096, India', 'national_id', '2018-02-21', 'Nanthakumar/Nanthakumarnationalfile.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'nantha', 'nantha->Phase-1->Tower-1->Floor-1->Flat A1->Master Bedroom', '', 'company1', 'kaspon123', '3a861126f2d0d1f1d2a2805cf7765ed8', '2018-05-16 05:47:53', '2018-05-16 05:47:53'),
(3, 'Cust_0003', 'Ramya', 'parentsarethegod@gmail.com', '9003819501', '9962987522', 'Mahathma Gandhi Nagar, Taramani, Chennai, Tamil Nadu 600113, India', 'national_id', '2018-02-22', 'Ramya/Ramyanationalfile.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ramya', 'ramya->Phase-1->Tower-1->Floor-1->Flat A1->Master Bedroom', '', 'company1', '', '', '2018-05-14 10:31:49', '2018-05-14 10:31:49'),
(11, 'Cust_0005', 'Ramya', 'ramyakitesep22@gmail.com', '+919003819500', '8098709429', 'Taramani, Chennai, Tamil Nadu, India', 'national_id', '2018-05-23', 'Ramya/Ramyanationalfile.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '[object Object]', 'haricharan->Phase-2->Tower-2->Floor-2', 'Direct Owner', 'company1', 'PZ2hgUlA', '84f956613a9e8b752af0f1c675eef754', '2018-05-14 10:43:46', '2018-05-14 10:43:46'),
(13, 'Cust_0012', 'senthil', 'senthilcse2008@gmail.com', '2536987545', '569874563', 'Kumudam Nagar, Mugalivakkam, Chennai, Tamil Nadu, India', '', '', '', 'M9901689', '2018-09-27', 'senthil/senthilpassfile.png', '', '', '', '', '', '', '', '', '', '', '', '', '[object Object]', 'nantha->Phase-2->Tower-2->Floor-2', 'undefined', 'company1', 'BTSV2jzR', 'cf3add4389298b1d665c7e3172c103d1', '2018-05-22 06:53:53', '2018-05-22 06:53:53'),
(14, 'Cust_0014', 'kumar', 'senthilcse7830@gmail.com', '458697485', '142536987', 'Mugalivakkam, Chennai, Tamil Nadu, India', 'national_id', '2018-06-30', 'kumar/kumarnationalfile.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '[object Object]', 'Floor->Phase-1->Tower-1->Floor-1->Flat A1', 'Direct Owner', 'company1', 'qs3HRfhl', '965e2bdbb580bd5f95d235f087d134ff', '2018-05-22 07:31:00', '2018-05-22 07:31:00'),
(15, 'Cust_0015', 'kumar', 'senthilcse7830@gmail.com', '458697485', '142536987', 'Mugalivakkam, Chennai, Tamil Nadu, India', 'national_id', '2018-06-30', 'kumar/kumarnationalfile.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '[object Object]', 'Floor->Phase-1->Tower-1->Floor-1->Flat A1', 'Direct Owner', 'company1', 'r3lpGP8W', '34df7711742396d8bab4fcf082ce9181', '2018-05-22 07:31:01', '2018-05-22 07:31:01'),
(18, 'Cust_0016', 'sdhgsdh', 'sdhsdh@dfd.df', '3463463442', '3473476455', 'Stormont, Dundas and Glengarry County Road 2, Upper Canada Village, ON K0C 2G0, Canada', '', '', '', 'sdgsdgsdg', '2018-05-16', 'sdhgsdh/sdhgsdhpassfile.JPG', '', '', '', '', '', '', '', '', '', '', '', '', 'prjname', 'prjname->Phase-1->Tower-2->Floor-1->Flat A2->Balcony', 'Tenant', 'company_0001', 'VawPyWQc', '$2y$10$r3r977JvXR3XGjnY7w5MPuaEJYxjJNAxcWpsRAFfdd3c8E0wZKJvK', '2018-05-28 03:16:29', '2018-05-28 03:16:29');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(200) NOT NULL,
  `customer_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `customer_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `contact_number` varchar(200) COLLATE utf8_bin NOT NULL,
  `alternate_number` varchar(200) COLLATE utf8_bin NOT NULL,
  `email_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `customer_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `door_no` varchar(200) COLLATE utf8_bin NOT NULL,
  `floor` varchar(200) COLLATE utf8_bin NOT NULL,
  `building_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `street_number` varchar(200) COLLATE utf8_bin NOT NULL,
  `area` varchar(200) COLLATE utf8_bin NOT NULL,
  `Community` varchar(200) COLLATE utf8_bin NOT NULL,
  `District` varchar(200) COLLATE utf8_bin NOT NULL,
  `State` varchar(200) COLLATE utf8_bin NOT NULL,
  `Country` varchar(200) COLLATE utf8_bin NOT NULL,
  `pincode` varchar(100) COLLATE utf8_bin NOT NULL,
  `municipality_number` varchar(200) COLLATE utf8_bin NOT NULL,
  `water_connection` varchar(200) COLLATE utf8_bin NOT NULL,
  `electricity_connection` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_id`, `company_id`, `customer_name`, `contact_number`, `alternate_number`, `email_id`, `customer_type`, `door_no`, `floor`, `building_name`, `street_number`, `area`, `Community`, `District`, `State`, `Country`, `pincode`, `municipality_number`, `water_connection`, `electricity_connection`) VALUES
(1, 'Cust_0001', 'company1', 'Ramya', '9003819500', '8098709429', 'ramyariotz22@gmail.com', 'Direct Owner', '32A', '4th floor', 'Shanthi Apartments', '2nd cross street', 'Adayar', 'Munipality area', 'Chennai', 'TamilNadu', 'India', '6410120', 'MN_1000', 'W1000A', 'EB001A');

-- --------------------------------------------------------

--
-- Table structure for table `customertype`
--

CREATE TABLE `customertype` (
  `id` int(100) NOT NULL,
  `customer_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `updated_at` varchar(200) COLLATE utf8_bin NOT NULL,
  `created_at` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customertype`
--

INSERT INTO `customertype` (`id`, `customer_type`, `company_id`, `updated_at`, `created_at`) VALUES
(5, 'owner', 'company_0001', '2018-05-28 08:42:38', '2018-05-28 08:42:38'),
(6, 'Tenant', 'company_0001', '2018-05-28 08:42:43', '2018-05-28 08:42:43'),
(7, 'xcfhsdf', 'company1', '2018-05-28 12:29:20', '2018-05-28 12:29:20'),
(8, 'Tenant', 'company1', '2018-05-28 12:31:17', '2018-05-28 12:31:17');

-- --------------------------------------------------------

--
-- Table structure for table `electronics`
--

CREATE TABLE `electronics` (
  `id` int(20) NOT NULL,
  `electronic_items` varchar(200) COLLATE utf8_bin NOT NULL,
  `cat_id` int(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `electronics`
--

INSERT INTO `electronics` (`id`, `electronic_items`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 'Computers & Laptops', 5, '2018-04-24 10:00:22', '2018-04-24 10:00:22'),
(2, 'TVs, Video - Audio', 5, '2018-04-24 10:00:22', '2018-04-24 10:00:22'),
(3, 'Hard Disks, Printers & Monitors', 5, '2018-04-24 10:02:10', '2018-04-24 10:02:10'),
(4, 'ACs', 5, '2018-04-24 10:02:10', '2018-04-24 10:02:10'),
(5, 'Washing Machines', 5, '2018-04-24 10:02:10', '2018-04-24 10:02:10'),
(6, 'Refridgerators', 5, '2018-04-24 10:02:10', '2018-04-24 10:02:10'),
(7, 'Computer Accessories', 5, '2018-04-24 10:02:10', '2018-04-24 10:02:10'),
(10, 'Camera & Lenses', 5, '2018-04-24 10:03:14', '2018-04-24 10:03:14'),
(11, 'Kitchen & Other Appliances', 5, '2018-04-24 10:03:14', '2018-04-24 10:03:14'),
(12, 'Games & Entertainment', 5, '2018-04-24 10:03:46', '2018-04-24 10:03:46');

-- --------------------------------------------------------

--
-- Table structure for table `events_table`
--

CREATE TABLE `events_table` (
  `id` int(20) NOT NULL,
  `event_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `event_venue` varchar(255) COLLATE utf8_bin NOT NULL,
  `event_date` varchar(255) COLLATE utf8_bin NOT NULL,
  `event_desc` varchar(2500) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `events_table`
--

INSERT INTO `events_table` (`id`, `event_title`, `event_venue`, `event_date`, `event_desc`, `created_at`, `updated_at`) VALUES
(1, '', 'flat1', '2018-02-15', '', '2018-04-22 18:30:00', '2018-04-22 18:30:00'),
(2, 'rfffffrd', 'trygbuyi', '09-05-2018', 'dsfgrtdeeeeeeeeeggggdcvcccccccccccccccccccccccccc', '2018-05-04 23:14:08', '2018-05-04 23:14:08'),
(3, 'rfffffrd', 'trygbuyi', '09-05-2018', 'dsfgrtdeeeeeeeeeggggdcvcccccccccccccccccccccccccc', '2018-05-04 23:16:09', '2018-05-04 23:16:09');

-- --------------------------------------------------------

--
-- Table structure for table `facing_direction`
--

CREATE TABLE `facing_direction` (
  `id` int(255) NOT NULL,
  `sub_id` int(255) NOT NULL,
  `facing_direction` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `facing_direction`
--

INSERT INTO `facing_direction` (`id`, `sub_id`, `facing_direction`, `created_at`, `updated_at`) VALUES
(1, 1, 'East', '2018-04-23 07:39:45', '2018-04-23 07:39:45'),
(2, 1, 'West', '2018-04-23 07:39:45', '2018-04-23 07:39:45'),
(3, 1, 'North', '2018-04-23 07:40:04', '2018-04-23 07:40:04'),
(4, 1, 'South', '2018-04-23 07:40:04', '2018-04-23 07:40:04'),
(5, 1, 'North-East', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(6, 1, 'North-West', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(7, 1, 'South-East', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(8, 1, 'South-West', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(9, 2, 'East', '2018-04-23 07:39:45', '2018-04-23 07:39:45'),
(10, 2, 'West', '2018-04-23 07:39:45', '2018-04-23 07:39:45'),
(11, 2, 'North', '2018-04-23 07:40:04', '2018-04-23 07:40:04'),
(12, 2, 'South', '2018-04-23 07:40:04', '2018-04-23 07:40:04'),
(13, 2, 'North-East', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(14, 2, 'North-West', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(15, 2, 'South-East', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(16, 2, 'South-West', '2018-04-23 07:40:43', '2018-04-23 07:40:43'),
(17, 6, 'East', '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(18, 6, 'West', '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(19, 6, 'North', '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(20, 6, 'South', '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(21, 6, 'North-East', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(22, 6, 'North-West', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(23, 6, 'South-East', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(24, 6, 'South-West', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(25, 7, 'East', '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(26, 7, 'West', '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(27, 7, 'North', '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(28, 7, 'South', '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(29, 7, 'North-East', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(30, 7, 'North-West', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(31, 7, 'South-East', '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(32, 7, 'South-West', '2018-04-23 02:10:43', '2018-04-23 02:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `furnish_status`
--

CREATE TABLE `furnish_status` (
  `id` int(255) NOT NULL,
  `furnish` varchar(255) COLLATE utf8_bin NOT NULL,
  `sub_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `furnish_status`
--

INSERT INTO `furnish_status` (`id`, `furnish`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, 'Furnished', 1, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(2, 'Semi-Furnished', 1, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(3, 'Unfurnished', 1, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(4, 'Furnished', 2, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(5, 'Semi-Furnished', 2, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(6, 'Unfurnished', 2, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(7, 'Furnished', 3, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(8, 'Semi-Furnished', 3, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(9, 'Unfurnished', 3, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(10, 'Furnished', 4, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(11, 'Semi-Furnished', 4, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(12, 'Unfurnished', 4, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(13, 'Furnished', 5, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(14, 'Semi-Furnished', 5, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(15, 'Unfurnished', 5, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(16, 'Furnished', 6, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(17, 'Semi-Furnished', 6, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(18, 'Unfurnished', 6, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(19, 'Furnished', 7, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(20, 'Semi-Furnished', 7, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(21, 'Unfurnished', 7, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(22, 'Furnished', 8, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(23, 'Semi-Furnished', 8, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(24, 'Unfurnished', 8, '2018-04-23 07:35:09', '2018-04-23 07:35:09'),
(25, 'Furnished', 9, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(26, 'Semi-Furnished', 9, '2018-04-23 07:34:32', '2018-04-23 07:34:32'),
(27, 'Semi-Furnished', 9, '2018-04-23 07:34:32', '2018-04-23 07:34:32');

-- --------------------------------------------------------

--
-- Table structure for table `furniture`
--

CREATE TABLE `furniture` (
  `fur_id` int(20) NOT NULL,
  `fur_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `cat_id` int(200) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `furniture`
--

INSERT INTO `furniture` (`fur_id`, `fur_name`, `cat_id`, `updated_at`, `created_at`) VALUES
(1, 'Sofa & Dining', 2, '2018-04-24 08:59:47', '2018-04-24 08:59:47'),
(2, 'Beds & Wardrobes', 2, '2018-04-24 08:59:47', '2018-04-24 08:59:47'),
(3, 'Home Decor & Garden', 2, '2018-04-24 09:00:43', '2018-04-24 09:00:43'),
(4, 'Kids Furniture', 2, '2018-04-24 09:00:43', '2018-04-24 09:00:43'),
(5, 'Other Household Items', 2, '2018-04-24 09:00:43', '2018-04-24 09:00:43');

-- --------------------------------------------------------

--
-- Table structure for table `knowledge_base`
--

CREATE TABLE `knowledge_base` (
  `id` int(255) NOT NULL,
  `kb_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `kb_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `kb_productcategory` varchar(255) COLLATE utf8_bin NOT NULL,
  `kb_problem` varchar(2000) COLLATE utf8_bin NOT NULL,
  `kb_solution` varchar(2000) COLLATE utf8_bin NOT NULL,
  `kb_contenttype` varchar(255) COLLATE utf8_bin NOT NULL,
  `kb_content` varchar(2000) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `kb_status` varchar(20) COLLATE utf8_bin NOT NULL,
  `updated_at` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `knowledge_base`
--

INSERT INTO `knowledge_base` (`id`, `kb_id`, `kb_type`, `kb_productcategory`, `kb_problem`, `kb_solution`, `kb_contenttype`, `kb_content`, `company_id`, `kb_status`, `updated_at`, `created_at`) VALUES
(2, 'KB002', 'technology', '16', 'ramya', 'fgfdgee', 'pdf', 'assets/uploads/knowledge_base/KB002.jpg', 'company1', '1', '2018-03-21 11:35:51', '2018-03-21 11:35:51'),
(3, 'KB003', 'technology', '16', 'fdgfdg', 'fgfdg', 'image', 'assets/uploads/knowledge_base/KB0100.jpg', 'company1', '1', '2018-03-21 11:37:01', '2018-03-21 11:37:01'),
(4, 'KB004', 'product', '5', 'wds', 'esrfer', 'video', 'assets/uploads/knowledge_base/KB002.mp4', 'company1', '1', '2018-03-22 09:11:36', '2018-03-22 09:11:36'),
(8, 'KB008', 'generic', '16', 'we are', 'no solution', 'pdf', 'assets/uploads/knowledge_base/KB008.pdf', 'company1', '1', '2018-05-17 11:19:16', '2018-05-17 11:19:16'),
(9, 'KB009', 'generic', '16', 'dfrt', 'ret', 'image', 'assets/uploads/knowledge_base/KB009.jpg', 'company1', '1', '2018-05-17 11:44:37', '2018-05-17 11:44:37'),
(10, 'KB010', 'product', '4', 'dfsrrrrrrrrrrrrrrrrrrrrrrrrrrr', 'dfdsrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', 'video', 'assets/uploads/knowledge_base/KB010.mp4', 'company1', '1', '2018-05-17 11:48:37', '2018-05-17 11:48:37'),
(11, 'KB011', 'product', '4', 'dfd', 'dfdf', 'image', 'assets/uploads/knowledge_base/KB011.gif', 'company1', '0', '2018-05-17 11:51:13', '2018-05-17 11:51:13'),
(12, 'KB012', 'generic', '16', 'dgfdg', 'fdgfd', 'image', 'assets/uploads/knowledge_base/KB012.jpg', 'company1', '0', '2018-05-17 11:52:22', '2018-05-17 11:52:22'),
(15, 'KB013', 'generic', '16', 'ghf', 'gfhgf', 'image', 'assets/uploads/knowledge_base/KB013.jpg', 'company1', '2', '2018-05-17 12:26:53', '2018-05-17 12:26:53'),
(17, 'KB017', 'generic', '16', 'sdsa', 'sdsa', 'image', 'assets/uploads/knowledge_base/KB017.jpg', 'company1', '1', '2018-05-17 12:47:13', '2018-05-17 12:47:13'),
(18, 'KB018', 'generic', '16', 'wqew', 'wewq', 'image', 'assets/uploads/knowledge_base/KB018.jpg', 'company1', '1', '2018-05-18 04:21:49', '2018-05-18 04:21:49'),
(20, 'KB020', 'generic', '16', 'ramya', 'Kumar', 'image', 'assets/uploads/knowledge_base/KB020.jpg', 'company1', '1', '2018-05-18 09:20:12', '2018-05-18 09:20:12'),
(21, 'KB021', 'product', '15', 'screen colour fades out', 'always prefer light colour screens', 'pdf', 'assets/uploads/knowledge_base/KB021.pdf', 'company1', '1', '2018-05-22 09:19:58', '2018-05-22 09:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `maintanance_type`
--

CREATE TABLE `maintanance_type` (
  `id` int(200) NOT NULL,
  `maintainance` varchar(200) COLLATE utf8_bin NOT NULL,
  `cont_cat` int(200) NOT NULL,
  `price` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `maintanance_type`
--

INSERT INTO `maintanance_type` (`id`, `maintainance`, `cont_cat`, `price`, `company_id`, `updated_at`, `created_at`) VALUES
(1, 'fjnhg', 3, '4000', 'company1', '2018-01-09 13:07:24', '2018-01-09 13:07:24'),
(2, 'v', 1, '2560', 'company1', '2018-01-23 12:16:49', '2018-01-23 12:16:49'),
(3, 'hjvghn', 2, '2000', 'company1', '2018-01-23 12:18:06', '2018-01-23 12:18:06'),
(4, 'Labour only', 3, '1850', 'company1', '2018-02-02 08:59:39', '2018-02-02 08:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `meals`
--

CREATE TABLE `meals` (
  `id` int(20) NOT NULL,
  `meals_status` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_id` int(200) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `meals`
--

INSERT INTO `meals` (`id`, `meals_status`, `sub_id`, `updated_at`, `created_at`) VALUES
(1, 'Yes', 4, '2018-04-24 08:52:14', '2018-04-24 08:52:14'),
(2, 'No', 4, '2018-04-24 08:52:14', '2018-04-24 08:52:14');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` int(20) NOT NULL,
  `mobile_cat` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_id` int(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `mobile_cat`, `cat_id`, `updated_at`, `created_at`) VALUES
(1, 'Mobile Phones', 3, '2018-04-24 09:04:52', '2018-04-24 09:04:52'),
(2, 'Tablets', 3, '2018-04-24 09:04:52', '2018-04-24 09:04:52'),
(3, 'Accessories', 3, '2018-04-24 09:05:18', '2018-04-24 09:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_subcat`
--

CREATE TABLE `mobile_subcat` (
  `sub_id` int(20) NOT NULL,
  `mobile_subcat` varchar(200) COLLATE utf8_bin NOT NULL,
  `cat_id` int(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `mobile_subcat`
--

INSERT INTO `mobile_subcat` (`sub_id`, `mobile_subcat`, `cat_id`, `updated_at`, `created_at`) VALUES
(1, 'Samsung', 1, '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(2, 'iPhone', 1, '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(3, 'Micromax', 1, '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(4, 'Nokia', 1, '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(5, 'Sony', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(6, 'Mi', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(7, 'HTC', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(8, 'Lenovo', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(9, 'Motorola', 1, '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(10, 'Oppo', 1, '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(11, 'Vivo', 1, '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(12, 'LG', 1, '2018-04-23 02:10:04', '2018-04-23 02:10:04'),
(13, 'Asus', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(14, 'Blackberry', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(15, 'Lava', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(16, 'Other Mobiles', 1, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(17, 'Samsung', 2, '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(18, 'iPads', 2, '2018-04-23 02:09:45', '2018-04-23 02:09:45'),
(19, 'Other Tablets', 2, '2018-04-23 02:10:43', '2018-04-23 02:10:43'),
(20, 'Mobile', 3, '2018-04-24 09:34:06', '2018-04-24 09:34:06'),
(21, 'Tablets', 3, '2018-04-24 09:34:06', '2018-04-24 09:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(250) NOT NULL,
  `deleted_status` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ol_tickets_list`
--

CREATE TABLE `ol_tickets_list` (
  `id` int(200) NOT NULL,
  `ticket_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `tech_id` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `previous_techid` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `customer_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `contact_number` varchar(100) COLLATE utf8_bin NOT NULL,
  `email_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `problem` longtext COLLATE utf8_bin NOT NULL,
  `support_cat` varchar(200) COLLATE utf8_bin NOT NULL,
  `scheduled_arrival` varchar(200) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` varchar(100) COLLATE utf8_bin NOT NULL,
  `ticket_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ol_tickets_list`
--

INSERT INTO `ol_tickets_list` (`id`, `ticket_id`, `tech_id`, `previous_techid`, `customer_id`, `contact_number`, `email_id`, `problem`, `support_cat`, `scheduled_arrival`, `company_id`, `status`, `ticket_type`, `updated_at`, `created_at`) VALUES
(1, 'Ticket_0001', '', '', 'Cust_0001', '9003819500', '', 'Compressor leaks out gas when AC is On.', 'Air Conditioing', '2018-01-20', 'company1', '0', '', '2018-01-11 00:00:00', '2018-01-11 00:00:00'),
(2, 'Ticket_0002', '', '', 'Cust_0001', '9003819500', '', 'Compressor leaks out gas when AC is On.', 'Air Conditioing', '2018-01-20', 'company1', '1', '', '2018-01-11 00:00:00', '2018-01-11 00:00:00'),
(3, 'company1_0003', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'sdddddfgv', '3', '2018-01-19 11:25:00', 'company1', '0', '', '2018-01-18 06:01:03', '2018-01-18 06:01:03'),
(4, 'company1_0004', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'gfdxgdg', '2', '2018-01-17 11:32:00', 'company1', '0', '', '2018-01-18 06:02:26', '2018-01-18 06:02:26'),
(5, 'company1_0005', 'efdsf', NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'wall painting', '8', '2018-01-19 09:34:00', 'company1', '0', '', '2018-01-18 06:05:05', '2018-01-18 06:05:05'),
(6, 'Ticket_0006', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'dfgdf', '6', '2018-01-19 12:34:00', 'company1', '0', '', '2018-01-18 07:04:57', '2018-01-18 07:04:57'),
(7, 'Ticket_0007', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'dfgdf', '6', '2018-01-19 12:34:00', 'company1', '0', '', '2018-01-18 07:23:15', '2018-01-18 07:23:15');

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `id` int(20) NOT NULL,
  `pet_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_id` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `pet_name`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 'Fishes & Aquarium', 6, '2018-04-24 10:08:36', '2018-04-24 10:08:36'),
(2, 'Dogs', 6, '2018-04-24 10:08:36', '2018-04-24 10:08:36'),
(3, 'Other Pets', 6, '2018-04-24 10:09:45', '2018-04-24 10:09:45'),
(4, 'Pet Food & Accessoried', 6, '2018-04-24 10:09:45', '2018-04-24 10:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_management`
--

CREATE TABLE `product_management` (
  `id` int(11) NOT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_image` varchar(500) NOT NULL,
  `product_modal` varchar(150) NOT NULL,
  `product_desc` varchar(150) NOT NULL,
  `company_id` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_management`
--

INSERT INTO `product_management` (`id`, `product_id`, `product_name`, `product_image`, `product_modal`, `product_desc`, `company_id`, `updated_at`, `created_at`) VALUES
(1, 'product_company1_0001', 'nantha', 'http://localhost/nantha_sales/public/assets/images/nantha.jpg', 'nantha', 'nanthass', 'company1', '2017-12-05 09:42:47', '2017-12-05 09:42:47'),
(2, 'product_company1_0002', 'Machine', 'http://localhost/nantha_sales/public/assets/images/Machine.png', 'Machines', 'Machine', 'company1', '2017-12-07 09:18:35', '2017-12-07 09:18:35');

-- --------------------------------------------------------

--
-- Table structure for table `project_creation`
--

CREATE TABLE `project_creation` (
  `id` int(250) NOT NULL,
  `project_name` varchar(400) COLLATE utf8_bin NOT NULL,
  `project_location` varchar(250) COLLATE utf8_bin NOT NULL,
  `project_manager` varchar(250) COLLATE utf8_bin NOT NULL,
  `project_wbs` longtext COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `project_creation`
--

INSERT INTO `project_creation` (`id`, `project_name`, `project_location`, `project_manager`, `project_wbs`, `updated_at`, `created_at`) VALUES
(1, 'nantha', 'nantha', 'nantha', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"nantha\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-02-09 06:01:16', '2018-02-09 06:01:16'),
(2, 'haricharan', 'haricharan', 'haricharan', '\"[{\\\"id\\\":\\\"j2_1\\\",\\\"text\\\":\\\"haricharan\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j2_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j2_1\\\"},{\\\"id\\\":\\\"j2_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_2\\\"},{\\\"id\\\":\\\"j2_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_3\\\"},{\\\"id\\\":\\\"j2_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_4\\\"},{\\\"id\\\":\\\"j2_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_5\\\"},{\\\"id\\\":\\\"j2_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_2\\\"},{\\\"id\\\":\\\"j2_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_7\\\"},{\\\"id\\\":\\\"j2_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_8\\\"},{\\\"id\\\":\\\"j2_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_9\\\"},{\\\"id\\\":\\\"j2_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j2_1\\\"},{\\\"id\\\":\\\"j2_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_11\\\"},{\\\"id\\\":\\\"j2_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_12\\\"},{\\\"id\\\":\\\"j2_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_13\\\"},{\\\"id\\\":\\\"j2_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_14\\\"},{\\\"id\\\":\\\"j2_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_11\\\"},{\\\"id\\\":\\\"j2_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_16\\\"},{\\\"id\\\":\\\"j2_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_17\\\"},{\\\"id\\\":\\\"j2_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_18\\\"}]\"', '2018-02-12 06:08:54', '2018-02-12 06:08:54'),
(3, 'ramya', 'ramya', 'ramya', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"ramya\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-02-12 06:10:28', '2018-02-12 06:10:28'),
(4, 'df', 'dfd', 'dsfds', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"df\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-03-13 10:25:45', '2018-03-13 10:25:45'),
(5, 'edsrfs', 'dfrgtfd', 'frtgd', '\"[{\\\"id\\\":\\\"j2_1\\\",\\\"text\\\":\\\"edsrfs\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j2_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j2_1\\\"},{\\\"id\\\":\\\"j2_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_2\\\"},{\\\"id\\\":\\\"j2_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_3\\\"},{\\\"id\\\":\\\"j2_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_4\\\"},{\\\"id\\\":\\\"j2_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_5\\\"},{\\\"id\\\":\\\"j2_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_2\\\"},{\\\"id\\\":\\\"j2_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_7\\\"},{\\\"id\\\":\\\"j2_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_8\\\"},{\\\"id\\\":\\\"j2_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_9\\\"},{\\\"id\\\":\\\"j2_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j2_1\\\"},{\\\"id\\\":\\\"j2_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_11\\\"},{\\\"id\\\":\\\"j2_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_12\\\"},{\\\"id\\\":\\\"j2_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_13\\\"},{\\\"id\\\":\\\"j2_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_14\\\"},{\\\"id\\\":\\\"j2_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_11\\\"},{\\\"id\\\":\\\"j2_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_16\\\"},{\\\"id\\\":\\\"j2_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_17\\\"},{\\\"id\\\":\\\"j2_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j2_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j2_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j2_18\\\"}]\"', '2018-05-05 12:16:18', '2018-05-05 12:16:18'),
(6, 'Floor', 'mugalivakkam', 'nantha', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"Floor\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_20\\\",\\\"text\\\":\\\"Kitchen Room\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_20\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_20_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_21\\\",\\\"text\\\":\\\"Master bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_21\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_21_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_22\\\",\\\"text\\\":\\\"Visitor Room\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_22\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_22_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-05-22 12:50:39', '2018-05-22 12:50:39');
INSERT INTO `project_creation` (`id`, `project_name`, `project_location`, `project_manager`, `project_wbs`, `updated_at`, `created_at`) VALUES
(7, 'cvgjgfjgfj', 'gfjgfjgfjfgj', 'hjkgfjgfj@fghk.fh', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"cvgjgfjgfj\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-05-24 13:53:51', '2018-05-24 13:53:51'),
(8, 'dfhjdfjd', 'dfdf', 'dfhdfh@df.dfh', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"dfhjdfjd\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-05-24 13:56:58', '2018-05-24 13:56:58'),
(9, 'prjname', 'chennai', 'Hari', '\"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"prjname\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Project\\\",\\\"size\\\":\\\"30\\\",\\\"spanclass\\\":\\\"root\\\"},\\\"parent\\\":\\\"#\\\"},{\\\"id\\\":\\\"j1_2\\\",\\\"text\\\":\\\"Phase-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_2\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_2_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_3\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_3\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_3_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_4\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_4\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_4_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_3\\\"},{\\\"id\\\":\\\"j1_5\\\",\\\"text\\\":\\\"Flat A1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_5\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_5_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_4\\\"},{\\\"id\\\":\\\"j1_6\\\",\\\"text\\\":\\\"Master Bedroom\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_6\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_6_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_5\\\"},{\\\"id\\\":\\\"j1_20\\\",\\\"text\\\":\\\"bed room\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_20\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_20_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"parent\\\":\\\"j1_6\\\"},{\\\"id\\\":\\\"j1_21\\\",\\\"text\\\":\\\"toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_21\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_21_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"parent\\\":\\\"j1_6\\\"},{\\\"id\\\":\\\"j1_7\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_7\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_7_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_2\\\"},{\\\"id\\\":\\\"j1_8\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_8\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_8_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_7\\\"},{\\\"id\\\":\\\"j1_9\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_9\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_9_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_8\\\"},{\\\"id\\\":\\\"j1_10\\\",\\\"text\\\":\\\"Balcony\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_10\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_10_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_9\\\"},{\\\"id\\\":\\\"j1_11\\\",\\\"text\\\":\\\"Phase-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_11\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_11_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Phase\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"first\\\"},\\\"parent\\\":\\\"j1_1\\\"},{\\\"id\\\":\\\"j1_12\\\",\\\"text\\\":\\\"Tower-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_12\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_12_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_13\\\",\\\"text\\\":\\\"Floor-1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_12\\\"},{\\\"id\\\":\\\"j1_14\\\",\\\"text\\\":\\\"Flat C1\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_13\\\"},{\\\"id\\\":\\\"j1_15\\\",\\\"text\\\":\\\"Master Toilet\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_15\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_15_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_22\\\",\\\"text\\\":\\\"Hall\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_22\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_22_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"parent\\\":\\\"j1_14\\\"},{\\\"id\\\":\\\"j1_16\\\",\\\"text\\\":\\\"Tower-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_16\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_16_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Tower\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_11\\\"},{\\\"id\\\":\\\"j1_17\\\",\\\"text\\\":\\\"Floor-2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_17\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_17_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Floor\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_16\\\"},{\\\"id\\\":\\\"j1_18\\\",\\\"text\\\":\\\"Flat A2\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_18\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_18_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Flat\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_17\\\"},{\\\"id\\\":\\\"j1_19\\\",\\\"text\\\":\\\"Kitchen\\\",\\\"icon\\\":true,\\\"li_attr\\\":{\\\"id\\\":\\\"j1_19\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_19_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{\\\"value1\\\":\\\"Area\\\",\\\"size\\\":\\\"50\\\",\\\"spanclass\\\":\\\"third\\\"},\\\"parent\\\":\\\"j1_18\\\"}]\"', '2018-05-28 06:48:32', '2018-05-28 06:48:32');

-- --------------------------------------------------------

--
-- Table structure for table `property_subtype`
--

CREATE TABLE `property_subtype` (
  `sub_id` int(255) NOT NULL,
  `subtype_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `property_id` int(255) NOT NULL COMMENT '1-Rent , 2- sale',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `property_subtype`
--

INSERT INTO `property_subtype` (`sub_id`, `subtype_name`, `property_id`, `updated_at`, `created_at`) VALUES
(1, 'Apartments', 1, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(2, 'Houses/ Villas', 1, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(3, 'Guest Houses', 1, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(4, 'PG', 1, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(5, 'Shops/ Offices', 1, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(6, 'Apartments', 2, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(7, 'Houses/ Villas', 2, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(8, 'Guest Houses', 2, '2018-04-23 12:19:20', '2018-04-23 12:19:20'),
(9, 'Shops/ Offices', 2, '2018-04-23 12:19:20', '2018-04-23 12:19:20');

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE `property_type` (
  `cat_id` int(255) NOT NULL,
  `property_id` int(255) NOT NULL,
  `property_name` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `property_type`
--

INSERT INTO `property_type` (`cat_id`, `property_id`, `property_name`) VALUES
(1, 1, 'For Rent'),
(1, 2, 'For Sale');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL,
  `assessment_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `question` varchar(255) COLLATE utf8_bin NOT NULL,
  `option_a` varchar(255) COLLATE utf8_bin NOT NULL,
  `option_b` varchar(255) COLLATE utf8_bin NOT NULL,
  `option_c` varchar(255) COLLATE utf8_bin NOT NULL,
  `option_d` varchar(255) COLLATE utf8_bin NOT NULL,
  `correct_ans` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `related_table`
--

CREATE TABLE `related_table` (
  `id` int(20) NOT NULL,
  `related_id` int(20) NOT NULL,
  `post_type` int(11) NOT NULL COMMENT '1- social,2- events, 3- classified',
  `user_id` varchar(225) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `related_table`
--

INSERT INTO `related_table` (`id`, `related_id`, `post_type`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2', '2018-04-23 00:00:00', '2018-04-23 00:00:00'),
(2, 1, 2, '2', '2018-04-23 00:00:00', '2018-04-23 00:00:00'),
(3, 3, 2, 'company1_0002', '2018-05-05 04:46:09', '2018-05-05 04:46:09');

-- --------------------------------------------------------

--
-- Table structure for table `sample`
--

CREATE TABLE `sample` (
  `id` int(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sample`
--

INSERT INTO `sample` (`id`, `updated_at`, `name`) VALUES
(1, '2017-12-28 09:24:49', 'ramya'),
(2, '2017-12-28 09:24:49', 'abinaya');

-- --------------------------------------------------------

--
-- Table structure for table `spare`
--

CREATE TABLE `spare` (
  `id` int(11) NOT NULL,
  `spare_id` varchar(100) NOT NULL,
  `spare_code` varchar(255) NOT NULL,
  `spare_desc` varchar(150) NOT NULL,
  `spare_name` varchar(255) NOT NULL,
  `spare_modal` varchar(255) NOT NULL,
  `image` varchar(500) NOT NULL,
  `spare_location` varchar(100) NOT NULL,
  `price` varchar(250) NOT NULL,
  `quantity_purchased` varchar(255) NOT NULL,
  `quantity_used` varchar(255) DEFAULT NULL,
  `cat_id` int(200) NOT NULL,
  `category` text NOT NULL,
  `p_date` date NOT NULL,
  `expiry_date` text,
  `tech_id` varchar(250) DEFAULT NULL,
  `company_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spare`
--

INSERT INTO `spare` (`id`, `spare_id`, `spare_code`, `spare_desc`, `spare_name`, `spare_modal`, `image`, `spare_location`, `price`, `quantity_purchased`, `quantity_used`, `cat_id`, `category`, `p_date`, `expiry_date`, `tech_id`, `company_id`, `created_at`, `updated_at`) VALUES
(2, 'Spare_002', '286', 'yhujguyh', 'yghwrbwj', 'gujghtyfk', 'assets/uploads/spare/Spare_002.jpg', 'rt', 'er453', '23', NULL, 2, 'Piped Gas', '2018-05-10', '-', NULL, 'company1', '2018-05-22 04:12:55', '2018-05-22 04:12:55'),
(3, 'Spare_003', '2843625', 'small and handy', 'Spanner', 'undefined', 'assets/uploads/spare/Spare_003.jpg', 'undefined', 'fghgf', '23', NULL, 2, 'Gardening', '2018-05-09', '-', NULL, 'company1', '2018-05-22 04:12:52', '2018-05-22 04:12:52'),
(4, 'Spare_004', '234254', 'dsfds', 'air tight lids', 'dsfds', 'assets/uploads/spare/Spare_004.png', 'dsf', 'dsfd', '12', NULL, 1, 'Electrical', '2018-05-09', '2018-05-10', NULL, 'company1', '2018-05-22 09:48:20', '2018-05-22 09:48:20'),
(5, 'Spare_005', '82256', 'undefined', 'undefined', 'fgtuyhjik', 'assets/uploads/spare/Spare_005.jpg', 'undefined', 'sads', '100', NULL, 2, 'Painting', '2018-05-09', '2018-05-09', NULL, 'company1', '2018-05-22 04:13:03', '2018-05-22 04:13:03'),
(6, 'Spare_006', 'SPA103', 'ytfy', 'tfygh', 'gtyfgtyf', 'assets/uploads/spare/Spare_006.jpg', 'tyftf', 'dsfdsf', '56', NULL, 6, 'Air Conditioning', '2018-05-08', '2018-05-10', NULL, 'company1', '2018-05-22 04:13:06', '2018-05-22 04:13:06'),
(7, 'Spare_007', 'ghjfgh', 'gbb', 'ret', 'fggggggd', 'assets/uploads/spare/Spare_007.png', 'ret', '67', '123', NULL, 6, 'Air Conditioning', '2018-05-03', '2018-05-09', NULL, 'company1', '2018-05-11 07:01:56', '2018-05-11 07:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `subtype_forsale`
--

CREATE TABLE `subtype_forsale` (
  `sub_id` int(255) NOT NULL,
  `subtype_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `property_id` int(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `subtype_forsale`
--

INSERT INTO `subtype_forsale` (`sub_id`, `subtype_name`, `property_id`, `updated_at`, `created_at`) VALUES
(1, 'Apartments', 1, '2018-04-23 06:49:20', '2018-04-23 06:49:20'),
(2, 'Houses/ Villas', 1, '2018-04-23 06:49:20', '2018-04-23 06:49:20'),
(3, 'Guest Houses', 1, '2018-04-23 06:49:20', '2018-04-23 06:49:20'),
(4, 'Shops/ Offices', 1, '2018-04-23 06:49:20', '2018-04-23 06:49:20');

-- --------------------------------------------------------

--
-- Table structure for table `support_category`
--

CREATE TABLE `support_category` (
  `id` int(100) NOT NULL,
  `support_category` varchar(150) NOT NULL,
  `company_id` varchar(100) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `support_category`
--

INSERT INTO `support_category` (`id`, `support_category`, `company_id`, `updated_at`, `created_at`) VALUES
(1, 'Electrical', 'company1', '2018-05-10 01:34:56', '2018-05-10 01:34:56'),
(2, 'Piped Gas', 'company1', '2018-05-10 01:32:35', '2018-05-10 01:32:35'),
(3, 'Plumbing', 'company1', '2017-12-28 04:14:32', '2017-12-28 04:14:32'),
(4, 'Gym and Games room', 'company1', '2018-05-10 01:31:52', '2018-05-10 01:31:52'),
(5, 'Gardening', 'company1', '2017-12-28 04:14:55', '2017-12-28 04:14:55'),
(6, 'Air Conditioning', 'company1', '2017-12-28 04:15:52', '2017-12-28 04:15:52'),
(7, 'Plastering', 'company1', '2017-12-28 04:15:52', '2017-12-28 04:15:52'),
(8, 'Painting', 'company1', '2017-12-28 04:15:52', '2017-12-28 04:15:52'),
(9, 'Lift Issues', 'company1', '2018-05-10 01:32:26', '2018-05-10 01:32:26'),
(11, 'false ceiling', 'company1', '2018-01-08 22:32:28', '2018-01-08 22:32:28'),
(12, 'swimming pool', 'company1', '2018-01-08 22:34:29', '2018-01-08 22:34:29'),
(15, 'Interior decors', 'company1', '2018-02-01 22:04:16', '2018-02-01 22:04:16'),
(16, 'No support Category', '', '2018-02-01 22:04:16', '2018-02-01 22:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_list`
--

CREATE TABLE `tickets_list` (
  `id` int(200) NOT NULL,
  `ticket_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `tech_id` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `previous_techid` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `customer_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `contact_number` varchar(100) COLLATE utf8_bin NOT NULL,
  `email_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `problem` longtext COLLATE utf8_bin NOT NULL,
  `support_cat` varchar(200) COLLATE utf8_bin NOT NULL,
  `support_subcategory` varchar(200) COLLATE utf8_bin NOT NULL,
  `scheduled_arrival` varchar(200) COLLATE utf8_bin NOT NULL,
  `ticket_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `images` text COLLATE utf8_bin NOT NULL,
  `company_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` varchar(100) COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tickets_list`
--

INSERT INTO `tickets_list` (`id`, `ticket_id`, `tech_id`, `previous_techid`, `customer_id`, `contact_number`, `email_id`, `problem`, `support_cat`, `support_subcategory`, `scheduled_arrival`, `ticket_type`, `images`, `company_id`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Ticket_0001', '', '', 'Cust_0001', '9003819500', '', 'Compressor leaks out gas when AC is On.', 'Air Conditioing', '1', '2018-01-20', '', '', 'company1', '0', '2018-01-11 00:00:00', '2018-01-11 00:00:00'),
(2, 'Ticket_0002', '', '', 'Cust_0001', '9003819500', '', 'Compressor leaks out gas when AC is On.', 'Air Conditioing', '1', '2018-01-20', '', '', 'company1', '1', '2018-01-11 00:00:00', '2018-01-11 00:00:00'),
(3, 'Ticket_0003', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'sdddddfgv', '3', '', '2018-01-19 11:25:00', '', '', 'company1', '0', '2018-01-18 06:01:03', '2018-01-18 06:01:03'),
(4, 'Ticket_0004', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'gfdxgdg', '2', '', '2018-01-17 11:32:00', '', '', 'company1', '0', '2018-01-18 06:02:26', '2018-01-18 06:02:26'),
(5, 'Ticket_0005', 'efdsf', NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'wall painting', '8', '', '2018-01-19 09:34:00', '', '', 'company1', '0', '2018-01-18 06:05:05', '2018-01-18 06:05:05'),
(6, 'Ticket_0006', NULL, NULL, 'Cust_0001', '9003819500', 'ramyariotz22@gmail.com', 'dfgdf', '6', '', '2018-01-19 12:34:00', '', '', 'company1', '0', '2018-01-18 07:04:57', '2018-01-18 07:04:57'),
(25, 'Ticket_0007', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', '', '', 'company1', '0', '2018-05-10 11:31:03', '2018-05-10 11:31:03'),
(26, 'Ticket_0026', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', '/home/kaspon/lampstack-7.1.11-0/php/tmp/phptbJ8kQ', 'company1', '0', '2018-05-10 13:14:49', '2018-05-10 13:14:49'),
(27, 'TK_0027', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', 'TK_0027_10_05_201801_22_26.JPG', 'company1', '0', '2018-05-10 13:22:26', '2018-05-10 13:22:26'),
(28, 'TK_0028', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', 'img/ticket_images/TK_0028_10_05_201801_24_42.JPG', 'company1', '0', '2018-05-10 13:24:42', '2018-05-10 13:24:42'),
(31, 'TK_0031', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', 'img/ticket_images/TK_0031_11_05_201805_01_36.jpg', 'company1', '0', '2018-05-11 05:01:36', '2018-05-11 05:01:36'),
(32, 'TK_0032', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', 'img/ticket_images/TK_0032_11_05_201805_13_23.jpg', 'company1', '0', '2018-05-11 05:13:23', '2018-05-11 05:13:23'),
(33, 'TK_0033', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-11 05:20:46', '2018-05-11 05:20:46'),
(34, 'TK_0034', NULL, NULL, 'cust_003', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-11 05:21:18', '2018-05-11 05:21:18'),
(35, 'TK_0035', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 06:00:59', '2018-05-14 06:00:59'),
(36, 'TK_0036', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 06:05:16', '2018-05-14 06:05:16'),
(37, 'TK_0037', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 06:15:53', '2018-05-14 06:15:53'),
(38, 'TK_0038', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 06:46:03', '2018-05-14 06:46:03'),
(39, 'TK_0039', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 06:54:54', '2018-05-14 06:54:54'),
(40, 'TK_0040', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal/community', '', 'company1', '0', '2018-05-14 10:37:43', '2018-05-14 10:37:43'),
(41, 'TK_0041', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 10:46:56', '2018-05-14 10:46:56'),
(42, 'TK_0042', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-14 10:46:59', '2018-05-14 10:46:59'),
(43, 'TK_0043', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal', '', 'company1', '0', '2018-05-14 10:55:37', '2018-05-14 10:55:37'),
(44, 'TK_0044', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal', '', 'company', '0', '2018-05-14 10:57:02', '2018-05-14 10:57:02'),
(45, 'TK_0045', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', '', 'company1', '0', '2018-05-14 10:57:05', '2018-05-14 10:57:05'),
(46, 'TK_0046', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal', '', 'company1', '0', '2018-05-14 10:57:39', '2018-05-14 10:57:39'),
(47, 'TK_0047', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal/community', '', 'company1', '0', '2018-05-14 10:57:45', '2018-05-14 10:57:45'),
(48, 'TK_0048', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', '', 'company1', '0', '2018-05-14 11:04:47', '2018-05-14 11:04:47'),
(49, 'TK_0049', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal', '', 'company1', '0', '2018-05-14 11:05:19', '2018-05-14 11:05:19'),
(50, 'TK_0050', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal/community', '', 'company1', '0', '2018-05-14 11:28:24', '2018-05-14 11:28:24'),
(51, 'TK_0051', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07', 'personal/community', '', 'company1', '0', '2018-05-14 11:30:00', '2018-05-14 11:30:00'),
(52, 'TK_0052', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', 'yuy', 'personal', '', 'company1', '0', '2018-05-14 11:38:53', '2018-05-14 11:38:53'),
(53, 'TK_0053', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'when all fuses are damaged', '0', '1', 'May 14, 2018 5:30:10 PM', 'Personal', '', 'company1', '0', '2018-05-14 12:00:37', '2018-05-14 12:00:37'),
(54, 'TK_0054', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07 09:30:00', 'personal/community', '', 'company1', '0', '2018-05-14 13:10:48', '2018-05-14 13:10:48'),
(55, 'TK_0055', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', 'yuy', 'personal', '', 'company1', '0', '2018-05-15 04:18:32', '2018-05-15 04:18:32'),
(56, 'TK_0056', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'when door can lock pls helping to open', '0', '3', '17-05-201812:15:00PM', 'Personal', '', 'company1', '0', '2018-05-15 05:46:17', '2018-05-15 05:46:17'),
(57, 'TK_0057', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'when fan conditioner is failed to issue thee crashed\nthe fan', '0', '2', '2018-05-3112:30:00', 'PersonalCommunity', '', 'company1', '0', '2018-05-15 05:59:20', '2018-05-15 05:59:20'),
(58, 'TK_0058', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07 09:30:00', 'personal/community', '', 'company1', '0', '2018-05-15 06:05:24', '2018-05-15 06:05:24'),
(59, 'TK_0059', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '7654784678', '1', '3', '2018-03-07 09:30:00', 'personal/community', '', 'company1', '0', '2018-05-15 06:05:47', '2018-05-15 06:05:47'),
(60, 'TK_0060', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'djxhjvoblvhzhcjcjjck', '0', '3', '2018-05-31		06:00:00', '[Ljava.lang.String;@a6b9850', '', 'company1', '0', '2018-05-15 06:49:21', '2018-05-15 06:49:21'),
(61, 'TK_0061', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'no vhxysogodidpvuuzuzuckcjcjxjxvlvjc since I\'m sending sending M coffin', '0', '3', '2018-05-31		07:00:00', '[Ljava.lang.String;@55e9cfe', '', 'company1', '0', '2018-05-15 06:52:10', '2018-05-15 06:52:10'),
(62, 'TK_0062', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'shin dig did fun an adult Alice cs time for coffin', '0', '1', '2018-05-31		07:00:00', '[Ljava.lang.String;@94d9342', '', 'company1', '0', '2018-05-15 06:58:51', '2018-05-15 06:58:51'),
(63, 'TK_0063', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'side door can drive coming soon soon soon soon', '5', '1', '2018-05-31		06:00:00', '[Ljava.lang.String;@a3bbd89', '', 'company1', '0', '2018-05-15 07:03:14', '2018-05-15 07:03:14'),
(64, 'TK_0064', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'finish soon did shin find hmm soon soon all done Coco can', '8', '1', '2018-05-31		06:00:00', '[Ljava.lang.String;@c174397', '', 'company1', '0', '2018-05-15 07:09:35', '2018-05-15 07:09:35'),
(65, 'TK_0065', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'sensible and duck doin doin sick', '8', '1', '2018-05-31		06:00:00', '[Ljava.lang.String;@c94f09f', '', 'company1', '0', '2018-05-15 07:12:47', '2018-05-15 07:12:47'),
(66, 'TK_0066', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'shall focus m from Sim will stick doin doin', '0', '3', '2018-05-31		07:00:00', 'personalstring', '', 'company1', '0', '2018-05-15 08:22:28', '2018-05-15 08:22:28'),
(67, 'TK_0067', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'access card failure to open door', '0', '3', '2018-05-26		08:00:00', 'personalstring,communitystring', '', 'company1', '0', '2018-05-15 08:24:47', '2018-05-15 08:24:47'),
(68, 'TK_0068', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'studio duck doin shin soon soon please studio', '0', '3', '2018-05-24		06:00:00', '[Ljava.lang.String;@7f17ef6', '', 'company1', '0', '2018-05-15 09:08:24', '2018-05-15 09:08:24'),
(69, 'TK_0069', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'studio session driving saying studio studio', '1', '4', '2018-05-24		12:30:00', '[Ljava.lang.String;@f56a1ee', '', 'company1', '0', '2018-05-15 09:24:40', '2018-05-15 09:24:40'),
(70, 'TK_0070', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'studio doin soon soon soon soon as', '6', '2', '2018-05-31		12:45:00', '[Ljava.lang.String;@f56a1ee', '', 'company1', '0', '2018-05-15 09:25:28', '2018-05-15 09:25:28'),
(71, 'TK_0071', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'again soon soon though driving', '1', '5', '2018-05-26		06:00:00', '[Ljava.lang.String;@6da8e24', '', 'company1', '0', '2018-05-15 09:27:25', '2018-05-15 09:27:25'),
(72, 'TK_0072', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'account doin to doin sick soon since', '1', '3', '2018-05-24		12:30:00', '[Ljava.lang.String;@5e78277', '', 'company1', '0', '2018-05-15 09:30:27', '2018-05-15 09:30:27'),
(73, 'TK_0073', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'anxious soon soon soon soon soon', '1', '3', 'null', '[Ljava.lang.String;@126975a', '', 'company1', '0', '2018-05-15 09:32:23', '2018-05-15 09:32:23'),
(74, 'TK_0074', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'shin truck will email again soon and sick to get the your schedule.', '1', '3', '2018-05-18		06:00:00', '[Ljava.lang.String;@d6fe914', '', 'company1', '0', '2018-05-15 09:39:18', '2018-05-15 09:39:18'),
(75, 'TK_0075', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'speaker when not working in to they are.', '9', '2', '2018-05-23		06:00:00', '[Ljava.lang.String;@944feee', '', 'company1', '0', '2018-05-15 09:44:10', '2018-05-15 09:44:10'),
(76, 'TK_0076', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'when over hearing problem to my headphones.', '9', '1', '2018-05-31		10:20:00', '[Ljava.lang.String;@9689c19', '', 'company1', '0', '2018-05-15 09:46:29', '2018-05-15 09:46:29'),
(77, 'TK_0077', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'remote sensor in air conditioning with not working', '6', '8', '2018-05-22		09:00:00', '[Ljava.lang.String;@8767bb1', '', 'company1', '0', '2018-05-15 09:54:21', '2018-05-15 09:54:21'),
(78, 'TK_0078', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'all fuses combined to change the within date', '1', '2', '2018-05-25		07:00:00', '[Ljava.lang.String;@8767bb1', '', 'company1', '0', '2018-05-15 09:58:59', '2018-05-15 09:58:59'),
(79, 'TK_0079', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'yrtyt', '0', '5', '2018-15-04', 'yrtyt', '', 'company1', '0', '2018-05-15 10:04:08', '2018-05-15 10:04:08'),
(80, 'TK_0080', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'yrtyt', '0', '5', '2018-15-04', 'yrtyt', '', 'company1', '0', '2018-05-15 10:04:11', '2018-05-15 10:04:11'),
(81, 'TK_0081', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'Hi', '5', '1', '2018-26-04', 'Hi', '', 'company1', '0', '2018-05-15 10:05:26', '2018-05-15 10:05:26'),
(82, 'TK_0082', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'Lift Issues Over Heating', '8', '0', '2018-15-04		android.support.v7.widget.AppCompatTextView{a3c1ed9 V.ED..C.. ........ 20,0-302,90 #7f090154 app:id/selecttime}', 'Lift Issues Over Heating', '', 'company1', '0', '2018-05-15 10:14:44', '2018-05-15 10:14:44'),
(83, 'TK_0083', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'Lift Issues Over Heating', '8', '0', '2018-18-04		android.support.v7.widget.AppCompatTextView{a3c1ed9 V.ED..C.. ........ 20,0-302,90 #7f090154 app:id/selecttime}', 'Lift Issues Over Heating', '', 'company1', '0', '2018-05-15 10:14:54', '2018-05-15 10:14:54'),
(84, 'TK_0084', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'ewewqe', '5', '0', '2018-15-04		15:52:00', 'ewewqe', '', 'company1', '0', '2018-05-15 10:22:08', '2018-05-15 10:22:08'),
(85, 'TK_0085', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'ewewqe', '5', '0', '2018-25-04		15:52:00', 'ewewqe', '', 'company1', '0', '2018-05-15 10:24:04', '2018-05-15 10:24:04'),
(86, 'TK_0086', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'card access to door open failure to not work', '1', '4', '2018-05-31		06:00:00', '[Ljava.lang.String;@287e8f6', '', 'company1', '0', '2018-05-15 10:30:26', '2018-05-15 10:30:26'),
(87, 'TK_0087', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'yryer', '8', '1', '2018-15-04		16:09:00', 'yryer', '', 'company1', '0', '2018-05-15 10:39:07', '2018-05-15 10:39:07'),
(88, 'TK_0088', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'air conditioning sensor not working to within date', '6', '2', '2018-05-30', '[Ljava.lang.String;@547cf13', '', 'company1', '0', '2018-05-15 10:41:05', '2018-05-15 10:41:05'),
(89, 'TK_0089', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'when school doin doin sick doin also will', '9', '1', '2018-05-24', '[Ljava.lang.String;@8cab1c9', '', 'company1', '0', '2018-05-15 10:45:05', '2018-05-15 10:45:05'),
(90, 'TK_0090', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'eqwew', '8', '1', '2018-15-04		16:17:00', 'eqwew', '', 'company1', '0', '2018-05-15 10:47:49', '2018-05-15 10:47:49'),
(91, 'TK_0091', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'eqwew', '0', '3', '2018-15-04		16:21:00', 'eqwew', '', 'company1', '0', '2018-05-15 10:51:09', '2018-05-15 10:51:09'),
(92, 'TK_0092', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'sensible will assume', '9', '10', '2018-05-18', '[Ljava.lang.String;@d68c2cc', '', 'company1', '0', '2018-05-15 10:53:46', '2018-05-15 10:53:46'),
(93, 'TK_0093', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '5675', '0', '3', 'null', '5675', '', 'company1', '0', '2018-05-15 10:54:23', '2018-05-15 10:54:23'),
(94, 'TK_0094', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'anxious soon soon soon soon soon', '9', '10', '2018-05-30', '[Ljava.lang.String;@d28a738', '', 'company1', '0', '2018-05-15 11:01:45', '2018-05-15 11:01:45'),
(95, 'TK_0095', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'anxious Wilson soon soon soon', '6', '8', '2018-05-24 07:00:00', '[Ljava.lang.String;@e19a86a', '', 'company1', '0', '2018-05-15 11:07:40', '2018-05-15 11:07:40'),
(96, 'TK_0096', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'awsqww', '8', '1', '2018-15-04		16:55:00', 'awsqww', '', 'company1', '0', '2018-05-15 11:25:51', '2018-05-15 11:25:51'),
(97, 'TK_0097', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'e23e2', '0', '4', '2018-15-04		04:06:00', 'Personal', '', 'company1', '0', '2018-05-15 11:39:00', '2018-05-15 11:39:00'),
(98, 'TK_0098', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'ewqew', '8', '0', '2018-26-04		17:10:00', 'Community', '', 'company1', '0', '2018-05-15 11:40:49', '2018-05-15 11:40:49'),
(99, 'TK_0099', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'remote sensor has probably not working to within date.', '6', '8', '2018-05-24 06:00:00', 'Community', '', 'company1', '0', '2018-05-15 11:47:55', '2018-05-15 11:47:55'),
(100, 'TK_0100', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'weqwe', '5', '1', '2018-15-04 17:26:00', 'Personal', '', 'company1', '0', '2018-05-15 11:56:33', '2018-05-15 11:56:33'),
(101, 'TK_0101', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'eqeeq', '8', '1', '2018-15-04 17:31:00', 'Community', '', 'company1', '0', '2018-05-15 12:21:21', '2018-05-15 12:21:21'),
(102, 'TK_0102', NULL, NULL, 'cust_001', '997960876', '899', '7654784678', '1', '3', '2018-03-07', 'personal / community', '', 'company1', '0', '2018-05-16 06:10:12', '2018-05-16 06:10:12'),
(103, 'TK_0103', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'gcf', '0', '0', '2018-16-04 11:51:00', 'Personal', '', 'company1', '0', '2018-05-16 06:21:04', '2018-05-16 06:21:04'),
(104, 'TK_0104', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'vzvzvg', '0', '0', '2018-16-04 17:03:00', 'Personal', '', 'company1', '0', '2018-05-16 11:33:13', '2018-05-16 11:33:13'),
(105, 'TK_0105', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'vyt', '0', '0', '2018-16-04 17:46:00', 'Personal', '', 'company1', '0', '2018-05-16 12:16:55', '2018-05-16 12:16:55'),
(106, 'TK_0106', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'soon studio', '1', '6', '2018-05-30 08:10:00', 'Personal', '', 'company1', '0', '2018-05-19 09:45:32', '2018-05-19 09:45:32'),
(107, 'TK_0107', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'ghujgyuigyiygi', '0', '0', '2018-23-04 15:46:00', 'Personal', '', 'company1', '0', '2018-05-19 10:16:09', '2018-05-19 10:16:09'),
(108, 'TK_0108', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'cvsdfed', '8', '1', '2018-19-04 15:51:00', 'Personal', '', 'company1', '0', '2018-05-19 10:21:41', '2018-05-19 10:21:41'),
(109, 'TK_0109', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'safsafass', '8', '0', '2018-19-04 15:52:00', 'Community', '', 'company1', '0', '2018-05-19 10:22:32', '2018-05-19 10:22:32'),
(110, 'TK_0110', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', 'dvewwe', '5', '1', '2018-19-04 15:54:00', 'Personal', '', 'company1', '0', '2018-05-19 10:25:00', '2018-05-19 10:25:00'),
(111, 'TK_0111', NULL, NULL, 'Cust_0001', '9962987522', 'ramyariotz22@gmail.com', '2w2w', '8', '1', '2018-19-04 15:57:00', 'Community', '', 'company1', '0', '2018-05-19 10:28:06', '2018-05-19 10:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_subcategory`
--

CREATE TABLE `tickets_subcategory` (
  `subcat_id` int(11) NOT NULL,
  `sub_category` varchar(255) COLLATE utf8_bin NOT NULL,
  `category` int(11) NOT NULL,
  `priority` varchar(200) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(200) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tickets_subcategory`
--

INSERT INTO `tickets_subcategory` (`subcat_id`, `sub_category`, `category`, `priority`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'AC is not Working', 1, 'P2', 'company1', '2018-05-10 12:35:23', '2018-05-10 12:35:23'),
(2, 'Fuse Problem', 1, 'P1', 'company1', '2018-05-10 12:35:23', '2018-05-10 12:35:23'),
(3, 'Fan Problem', 1, 'P2', 'company1', '2018-05-10 12:35:23', '2018-05-10 12:35:23'),
(4, 'Door Access Card', 1, 'P1', 'company1', '2018-05-10 12:35:23', '2018-05-10 12:35:23'),
(5, 'Door Access Phone', 1, 'P1', 'company1', '2018-05-10 12:35:23', '2018-05-10 12:35:23'),
(6, 'Invertor and UPS', 1, 'P1', 'company1', '2018-05-10 12:35:23', '2018-05-10 12:35:23'),
(7, 'Cooling condenser problem', 6, 'P1', 'company1', '2018-05-11 11:24:22', '2018-05-11 11:24:22'),
(8, 'Remote sensor not working', 6, 'P1', 'company1', '2018-05-11 11:24:22', '2018-05-11 11:24:22'),
(9, 'Over Heating', 9, 'P1', 'company1', '2018-05-11 11:24:22', '2018-05-11 11:24:22'),
(10, 'Speaker Phones Not audible', 9, 'P1', 'company1', '2018-05-11 11:24:22', '2018-05-11 11:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `timeline_images`
--

CREATE TABLE `timeline_images` (
  `id` int(255) NOT NULL,
  `timeline_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `images` varchar(355) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `timeline_table`
--

CREATE TABLE `timeline_table` (
  `id` int(255) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` varchar(300) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `timeline_table`
--

INSERT INTO `timeline_table` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'example', 'testing timeline', '2018-04-23 19:17:16', '2018-04-23 19:17:16'),
(2, 'dfdsf', 'dsfdsf', '2018-04-25 09:20:02', '2018-04-25 09:20:02'),
(3, 'fgfd', 'fdgd', '2018-04-25 09:53:08', '2018-04-25 09:53:08'),
(4, 'fgfd', 'ramya', '2018-04-25 09:53:46', '2018-04-25 09:53:46'),
(5, '4et53', '3454353', '2018-04-25 09:54:48', '2018-04-25 09:54:48'),
(6, '78', '888888678', '2018-04-25 09:56:52', '2018-04-25 09:56:52'),
(7, '3453', '35435', '2018-04-25 09:59:26', '2018-04-25 09:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `id` int(11) NOT NULL,
  `training_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` varchar(255) COLLATE utf8_bin NOT NULL,
  `productcategory` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `contenttype` varchar(255) COLLATE utf8_bin NOT NULL,
  `content` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`id`, `training_id`, `title`, `type`, `productcategory`, `contenttype`, `content`, `company_id`, `updated_at`, `created_at`) VALUES
(3, 'training_001', 'Nanthakumar', 'product', 'swimming pool', 'pdf', 'C:\\xampp\\htdocs\\FMS\\public\\assets/uploads/training/training_001.pdf', 'company1', '2018-03-05 11:26:55', '2018-03-05 11:26:55'),
(4, 'training_004', 'nanthakumaeee', 'product', 'Air Conditioning', 'pdf', 'C:\\xampp\\htdocs\\FMS\\public\\assets/uploads/training/training_004.pdf', 'company1', '2018-03-05 11:26:45', '2018-03-05 11:26:45'),
(6, 'training_006', 'djdfhjd', 'generic', 'undefined', 'pdf', 'assets/uploads/training/training_006.pdf', 'company_0001', '2018-05-28 09:44:43', '2018-05-28 09:44:43'),
(7, 'training_007', 'sdgsdg', 'technology', 'undefined', 'video', '/home/new/Lampp/apache2/htdocs/FMS/public/assets/uploads/training/training_007.mp4', 'company_0001', '2018-05-28 05:28:17', '2018-05-28 05:28:17'),
(8, 'training_008', 'sdgsdg', 'technology', 'undefined', 'video', '/home/new/Lampp/apache2/htdocs/FMS/public/assets/uploads/training/training_008.mp4', 'company_0001', '2018-05-28 05:30:37', '2018-05-28 05:30:37'),
(9, 'training_009', 'sdgsdg', 'technology', 'undefined', 'pdf', '/home/new/Lampp/apache2/htdocs/FMS/public/assets/uploads/training/training_009.pdf', 'company_0001', '2018-05-28 05:33:23', '2018-05-28 05:33:23'),
(10, 'training_010', 'sgdgsd', 'generic', 'undefined', 'video', 'training_010.mp4', 'company_0001', '2018-05-28 05:35:43', '2018-05-28 05:35:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temp_pwd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_id`, `email`, `password`, `temp_pwd`, `user_type`, `image`, `company_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nanthakumar', 'company1_0002', 'nanthakumar.r@kaspontech.com', '$2y$10$uyu2724gJTyToyLA/Ti1nuX76aVIl33wf1HAfyOaxVHeoOe.V/txO', '', 'Admin', '', 'company1', '', '2017-10-08 23:01:18', '2017-10-08 23:01:18'),
(2, 'we', '', 'wer@gmn', '$2y$10$uyu2724gJTyToyLA/Ti1nuX76aVIl33wf1HAfyOaxVHeoOe.V/txO', '', 'Manager', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/FMS/public/images/noimage.png', 'company1', NULL, '2017-12-27 08:59:33', '2017-12-27 08:59:33'),
(3, 'we', '', 'nantha93911@gmail.com', '$2y$10$XpnWScf01FXAYv33.jM7F.KwE5xjfWcVHhzJJaavgYwHV41bHS24W', '', 'Manager', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/FMS/public/images/noimage.png', 'company1', NULL, '2017-12-27 09:03:57', '2017-12-27 09:03:57'),
(4, 'rfd', '', 'ramya@gmail.com', '$2y$10$U4ITy3WeAz0iPwLHb8PZcOXJwXbm0wwhLOekPhblk9KSmAcVJe1eW', '', 'Manager', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/FMS/public/images/noimage.png', 'company1', NULL, '2017-12-27 09:15:33', '2017-12-27 09:15:33'),
(5, 'Raju', '', 'ramyario@gmail.com', '$2y$10$uyu2724gJTyToyLA/Ti1nuX76aVIl33wf1HAfyOaxVHeoOe.V/txO', '', 'Servicedesk', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/FMS/public/images/noimage.png', 'company1', NULL, '2017-12-29 00:43:39', '2017-12-29 00:43:39'),
(21, 'Megala', '', 'sribalajidec19@gmail.com', '$2y$10$dQOqblO5.8GP69RDGMtnQu05oVprtNkUcF/TvTy9GP19zPGgll50K', '89KflfGe', 'Manager', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/2018-03-12/FMS/public/images/noimage.png', 'company1', NULL, '2018-03-13 23:33:49', '2018-03-13 23:33:49'),
(22, 'Abinaya', '', 'abinaya.s@kaspontech.com', '$2y$10$sRtp3lgZFDeWxzKze5e8ZuOpiinUo9RwJXhJ8evcsAQr6A9aauvpW', 'tzRx4wYM', 'Technician', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/2018-03-12/FMS/public/images/noimage.png', 'company1', NULL, '2018-03-24 03:38:11', '2018-03-24 03:38:11'),
(24, 'Vishnu', '', 'vishnukumar.s@kaspontech.com', '$2y$10$SQLvWHBYFPz61maBO5Dx8u.V9lMuN692vsaSPQ7QQ14rn1rUBLOE2', 'hoG5jxws', 'customer', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/2018-03-12/FMS/public/images/noimage.png', 'company1', NULL, '2018-03-24 07:14:36', '2018-03-24 07:14:36'),
(27, 'dgvdr', '', 'nithya.n@kaspontech.com', '$2y$10$SQcZbtYsFnepgMB/66xp3Ozzcn/cIclmci0n0UgV/5X5k68EnF3YG', 'aao7sIIG', 'Admin', '/home/kaspon/lampstack-7.1.11-0/apache2/htdocs/2018-03-12/FMS/public/images/noimage.png', 'company1', NULL, '2018-03-26 00:36:43', '2018-03-26 00:36:43'),
(28, 'Kirthika', 'company1_0026', 'kirthika.v@kaspontech.com', '$2y$10$sAnwb2uPYut5yVs5lnBjsuXXMpEFN9onlnZwy9wxbtjdC8J3Mn9iO', 'kaspon123', 'Technician', '/var/www/html/FMS/public/images/noimage.png', 'company1', NULL, '2018-05-08 11:21:50', '2018-05-08 11:21:50'),
(29, 'Kishanth', 'company1_0027', 'kishanth.s@kaspontech.com', '$2y$10$Iwvg21GGCzgNNt7AnaVjDOk5yxMNQOpkE6ph7NIm3lwoRnfiEmkMK', 'kaspon123', 'Technician', '/var/www/html/FMS/public/images/noimage.png', 'company1', NULL, '2018-05-08 11:27:53', '2018-05-08 11:27:53'),
(30, 'Nanthakumar', 'company1_0028', 'nantha9391@gmail.com', '$2y$10$LotS24HpZ8dRqHeaRNTipugs4Hdq4V6cmvVNkotX/lurp48ZWgJhu', '7f0kSOEm', 'Technician', '/var/www/html/FMS/public/images/noimage.png', 'company1', NULL, '2018-05-08 11:44:03', '2018-05-08 11:44:03'),
(43, 'Manikandan', 'company1_0042', 'aadhimanikaspon@gmail.com', '$2y$10$bdanFvYcoI5vwwwidjhcmeFLJJr08N3usZUcc0Cc9TBRnNg.lgh3q', 'gTGdGB6s', 'Manager', '/images/noimage.png', 'company1', NULL, '2018-05-18 21:43:03', '2018-05-18 21:43:03'),
(44, 'Srihari', 'company1_0044', 'srihari.s@kaspontech.com', '$2y$10$uldsHHdDz02MZbUrsvuQcOXxrgEyQrsjy87CvlhB.vk8Etl2GK8HG', 'q8DMQnKc', 'Servicedesk', '/images/noimage.png', 'company1', NULL, '2018-05-22 02:26:43', '2018-05-22 02:26:43'),
(45, 'SuperAdmin', 'company1_0045', 'superadmin@kaspontech.com', '$2y$10$8mUJcCySt30tbvYnlJAwaOpbQDPDSKZEZYz5fCuLhEv7cX9ThsHMC', 'kaspon123', '0', '/images/noimage.png', 'company1', NULL, '2018-05-22 23:36:01', '2018-05-22 23:36:01'),
(46, 'Rakesh', 'Company_0008_0068', 'djdf@gfjf.fg', '$2y$10$pQsjO8/thqtuEcP.72qCw.J5vSGVSd/5HU8BmoAVRNmKC7kAQUftW', 'kG2DwEiW', 'Manager', '/home/new/Lampp/apache2/htdocs/FMS/public/images/noimage.png', 'Company_0008', NULL, '2018-05-27 23:14:36', '2018-05-27 23:14:36'),
(47, 'dsf', 'Company_0008_0069', 'dfhjdf@dyer.fh', '$2y$10$HLcshXmK3OcccIBudHW63eS7VdQRTazcEOIEMUQP/1PILymjWzDAu', 'mlQ6Bm0z', 'admin', '/home/new/Lampp/apache2/htdocs/FMS/public/images/noimage.png', 'Company_0008', NULL, '2018-05-27 23:14:41', '2018-05-27 23:14:41'),
(48, 'Hari', 'company_0001_0070', 'srihariram777@hotmail.com', '$2y$10$4Qy3tzLSprnun0YPUQ/ml.rlo3Ic6VPjhwv8qZCzxKgDBNprtULJe', 'jPh5m0Qh', 'Admin', '/home/new/Lampp/apache2/htdocs/FMS/public/images/noimage.png', 'company_0001', NULL, '2018-05-28 06:39:20', '2018-05-28 06:39:20'),
(49, 'sdgsdg', 'company1_0071', 'sdgsd@dfhjd.dfg', '$2y$10$YYIs1b2Ye6s7GojlH8W3U.g725vIgD6YZ.O200WRL3LBXsd7DLpZW', 'xkj2uoWy', 'Technician', '/images/noimage.png', 'company1', NULL, '2018-05-28 07:18:37', '2018-05-28 07:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(200) NOT NULL,
  `user_roles` text COLLATE utf8_bin NOT NULL,
  `company_id` text COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_roles`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'company1', '2018-05-19 08:15:12', '2018-05-19 08:15:12'),
(2, 'Manager/Supervisor', 'company1', '2018-05-19 08:15:12', '2018-05-19 08:15:12'),
(3, 'Gate Security', 'company1', '2018-05-19 08:15:12', '2018-05-19 08:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_mapping`
--

CREATE TABLE `user_role_mapping` (
  `id` int(11) NOT NULL,
  `company_id` varchar(250) NOT NULL,
  `user_id` varchar(250) NOT NULL,
  `module_id` varchar(250) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_users`
--
ALTER TABLE `add_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment`
--
ALTER TABLE `assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batchelors`
--
ALTER TABLE `batchelors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bathrooms`
--
ALTER TABLE `bathrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bedrooms`
--
ALTER TABLE `bedrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `be_product`
--
ALTER TABLE `be_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bikes`
--
ALTER TABLE `bikes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bikes_category`
--
ALTER TABLE `bikes_category`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `bike_model`
--
ALTER TABLE `bike_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars_subtype`
--
ALTER TABLE `cars_subtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_fuel`
--
ALTER TABLE `car_fuel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_parkings`
--
ALTER TABLE `car_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_details`
--
ALTER TABLE `category_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classified_categories`
--
ALTER TABLE `classified_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classified_table`
--
ALTER TABLE `classified_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `construction_status`
--
ALTER TABLE `construction_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_category`
--
ALTER TABLE `contract_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_type`
--
ALTER TABLE `contract_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customertype`
--
ALTER TABLE `customertype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `electronics`
--
ALTER TABLE `electronics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events_table`
--
ALTER TABLE `events_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facing_direction`
--
ALTER TABLE `facing_direction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furnish_status`
--
ALTER TABLE `furnish_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furniture`
--
ALTER TABLE `furniture`
  ADD PRIMARY KEY (`fur_id`);

--
-- Indexes for table `knowledge_base`
--
ALTER TABLE `knowledge_base`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintanance_type`
--
ALTER TABLE `maintanance_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meals`
--
ALTER TABLE `meals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_subcat`
--
ALTER TABLE `mobile_subcat`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `ol_tickets_list`
--
ALTER TABLE `ol_tickets_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_management`
--
ALTER TABLE `product_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_creation`
--
ALTER TABLE `project_creation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_subtype`
--
ALTER TABLE `property_subtype`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `property_type`
--
ALTER TABLE `property_type`
  ADD PRIMARY KEY (`property_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `related_table`
--
ALTER TABLE `related_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample`
--
ALTER TABLE `sample`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spare`
--
ALTER TABLE `spare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subtype_forsale`
--
ALTER TABLE `subtype_forsale`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `support_category`
--
ALTER TABLE `support_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets_list`
--
ALTER TABLE `tickets_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets_subcategory`
--
ALTER TABLE `tickets_subcategory`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `timeline_images`
--
ALTER TABLE `timeline_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeline_table`
--
ALTER TABLE `timeline_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role_mapping`
--
ALTER TABLE `user_role_mapping`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_users`
--
ALTER TABLE `add_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `assessment`
--
ALTER TABLE `assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `batchelors`
--
ALTER TABLE `batchelors`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bathrooms`
--
ALTER TABLE `bathrooms`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `bedrooms`
--
ALTER TABLE `bedrooms`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `be_product`
--
ALTER TABLE `be_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bikes`
--
ALTER TABLE `bikes`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bikes_category`
--
ALTER TABLE `bikes_category`
  MODIFY `sub_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `bike_model`
--
ALTER TABLE `bike_model`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cars_subtype`
--
ALTER TABLE `cars_subtype`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `car_fuel`
--
ALTER TABLE `car_fuel`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=294;

--
-- AUTO_INCREMENT for table `car_parkings`
--
ALTER TABLE `car_parkings`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `category_details`
--
ALTER TABLE `category_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `classified_categories`
--
ALTER TABLE `classified_categories`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `classified_table`
--
ALTER TABLE `classified_table`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `construction_status`
--
ALTER TABLE `construction_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `contract_category`
--
ALTER TABLE `contract_category`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `contract_type`
--
ALTER TABLE `contract_type`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customertype`
--
ALTER TABLE `customertype`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `electronics`
--
ALTER TABLE `electronics`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `events_table`
--
ALTER TABLE `events_table`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `facing_direction`
--
ALTER TABLE `facing_direction`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `furnish_status`
--
ALTER TABLE `furnish_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `furniture`
--
ALTER TABLE `furniture`
  MODIFY `fur_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `knowledge_base`
--
ALTER TABLE `knowledge_base`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `maintanance_type`
--
ALTER TABLE `maintanance_type`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `meals`
--
ALTER TABLE `meals`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mobile_subcat`
--
ALTER TABLE `mobile_subcat`
  MODIFY `sub_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ol_tickets_list`
--
ALTER TABLE `ol_tickets_list`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_management`
--
ALTER TABLE `product_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_creation`
--
ALTER TABLE `project_creation`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `property_subtype`
--
ALTER TABLE `property_subtype`
  MODIFY `sub_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `property_type`
--
ALTER TABLE `property_type`
  MODIFY `property_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `related_table`
--
ALTER TABLE `related_table`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sample`
--
ALTER TABLE `sample`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `spare`
--
ALTER TABLE `spare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `support_category`
--
ALTER TABLE `support_category`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tickets_list`
--
ALTER TABLE `tickets_list`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `tickets_subcategory`
--
ALTER TABLE `tickets_subcategory`
  MODIFY `subcat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `timeline_images`
--
ALTER TABLE `timeline_images`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timeline_table`
--
ALTER TABLE `timeline_table`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_role_mapping`
--
ALTER TABLE `user_role_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
