<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_model extends Model
{
    protected $table = 'product_management';

    protected $fillable = [
        'id', 'product_id', 'product_name', 'product_image', 'product_model', 'product_desc', 'company_id'
    ];
}