<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpareModel extends Model
{
    protected $table = 'spare';
}
