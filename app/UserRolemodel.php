<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRolemodel extends Model
{
    protected $table = 'user_roles';
}
