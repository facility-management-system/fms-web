<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\User_model;
use App\UserRolemodel;
use App\Supportmodel;
use Mail;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use PHPExcel; 
use PHPExcel_Cell;
use PHPExcel_IOFactory;

class UserController extends Controller
{
    public function checkAuth(Request $request){
        $username=$request->email;
		$password=$request->password;
        $credintials= [
            'email'=>$username,
            'password'=>$password,
        ];
        if(!Auth::attempt(['email' => $username, 'password' => $password])){
            return response('Username and Password does not match', 403);            
        }
        return response(AUTH::user(),201);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $insert)
    {   
        /* Mail::raw('Text to e-mail', function ($message) {
            $message->to('nanthakumar.r@kaspontech.com');
        }); */
        $company_id=$insert->company_id;
        $user_id = DB::table('add_users') 
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$user_id==""){                        
            $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $user_ids = $company_id."_" . $datass;
        }
        else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $user_ids = $company_id."_" . $datass;
        };     
        if($insert->ids == 'undefined' || $insert->ids == null){
            
            if($insert->hasFile('user_proof')){ 
                $extension=$insert->user_proof->getClientOriginalExtension();
                $insert->file('user_proof')->move(public_path("/img/user_proofs/"), $insert->employeeid.'_'.$insert->firstname.'_proof'.'.'.$extension);
                $path= "/img/user_proofs/".$insert->employeeid.'_'.$insert->firstname.'_proof'.'.'.$extension;
                // $proof_images = $insert->file('user_proof');
                            // $destinationPath = public_path('img');
                            // $extension=$insert->user_proof->getClientOriginalExtension();
                            // $path = $insert->file('user_proof')->storeAs(
                            //     $insert->employeeid.'_'.$insert->firstname.'_proof'.'.'.$extension
                            // );
                            // $user_prooffile = $path;
               // $user_prooffile->move($destinationPath, $user_prooffile);
               // $proof_file = $destinationPath.'/'.$user_prooffile;
              // $proof_images->move($destinationPath,$insert->employeeid.'_'.$insert->firstname.'_proof'.'.'.$proof_images->getClientOriginalExtension());
            }else{
                
                $path = "";
            }

            $user_i= new User_model(); 
            if($insert->roles=="Technician"){
                $str =  $insert->support;
                //$arr = explode(',',$str);                     
                $user_i->employee_id=$insert->employeeid;
                $user_i->user_id=$user_ids;
                $user_i->company_id=$company_id;
                $user_i->first_name=$insert->firstname;
                $user_i->last_name=$insert->lastname;
                $user_i->email_id=$insert->emailid;
                $user_i->contact_number=$insert->contactnumber;
                $user_i->alternate_number=$insert->alternatenumber;        
                $user_i->address=$insert->addresses;
                $user_i->region=$insert->regions;
                $user_i->area=$insert->areas;
                $user_i->location=$insert->locations;
                $user_i->role=$insert->roles;
                $user_i->user_proof=$path;
                $user_i->support= $str;
            }
            else{
                $user_i->employee_id=$insert->employeeid;
                $user_i->company_id=$company_id;
                $user_i->user_id=$user_ids;
                $user_i->first_name=$insert->firstname;
                $user_i->last_name=$insert->lastname;
                $user_i->email_id=$insert->emailid;
                $user_i->contact_number=$insert->contactnumber;
                $user_i->alternate_number=$insert->alternatenumber;        
                $user_i->address=$insert->addresses;
                $user_i->region=$insert->regions;
                $user_i->area=$insert->areas;
                $user_i->location=$insert->locations;
                $user_i->role=$insert->roles;
                $user_i->user_proof=$path;
                $user_i->support="";
            }
            //for user insert            
            $whereData_emailid = [
                ['email_id', $insert->emailid],
                ['company_id', $company_id]
            ];
            $whereData_contact = [
                ['contact_number', $insert->contactnumber],
                ['company_id', $company_id]
            ];    
            $whereData = [
                ['employee_id', $insert->employeeid],
                ['company_id', $company_id]
            ];
            $whereData_empid = [
                ['employee_id', $insert->employeeid],
                ['id', '<>', $insert->ids],
                ['company_id',  $company_id]
            ];
            $check_emailid=$this->check_mailid($whereData_emailid);
            if($check_emailid){
                $json_array=['type'=>'warning','message'=>'Email ID already Exists!'];                
            }else{
                $contact_exists=$this->check_contactnumber($whereData_contact);
                if($contact_exists){
                    $json_array=['type'=>'warning','message'=>'Contact Number already Exists!'];
                }
                else{
                    $empid_exists=$this->check_empid($whereData_empid);
                    if($empid_exists){
                        $json_array=['type'=>'warning','message'=>'Employee ID already Exists!'];
                    }
                    else {
                        $user_i->save();
                        if($user_i->save()){  
                            $destinationPath = "/images";
                            $destinationPath=$destinationPath.'/noimage.png';
                            $string = str_random(8);
                            $hashed_random_password = Hash::make($string);
                            $login= new User();                        
                            $login->name=$insert->firstname;
                            $login->user_id=$user_ids;
                            $login->email=$insert->emailid;
                            $login->password=$hashed_random_password;
                            $login->temp_pwd=$string;
                            $login->user_type=$insert->roles;
                            $login->image=$destinationPath;   
                            $login->company_id=$company_id;                      
                            if($login->save()){
                                //$json_array=['type'=>'success','message'=>'User Added Successfully'];
                                $path='http://localhost/nantha_sales/public/';
                                $data = array( 'email' => $insert->emailid);
                                Mail::send('email',
                                array(
                                    'name' => $insert->firstname,
                                    'email' => $insert->emailid,
                                    'password' => $string
                                ), function($message) use ($data)
                                {
                                    $message->from('ramyariotz22@gmail.com');
                                    $message->to($data['email'], 'Admin')->subject('Credintials for FMS User Login');
                                });
                                if (Mail::failures()) {
                                    $json_array=['type'=>'error','message'=>'Mail not sent to the user, The password for this user is '.$hashed_random_password];
                                }else{
                                    $json_array=['type'=>'success','message'=>'User Added Successfully'];
                                }                           
                            }else{
                                $json_array=['type'=>'error','message'=>"User's Login Credentials Failed to Upload !!"];
                            }                        
                        }else{
                            $json_array=['type'=>'error','message'=>'User not Added!!'];
                        }
                    }
                }
            }
        }
        else{
            //for User Update
            $whereData_emailid = [
                ['email_id', $insert->emailid],
                ['id', '<>', $insert->ids],
                ['company_id', $company_id]
            ];
            $whereData_contact = [
                ['contact_number', $insert->contactnumber],
                ['id', '<>', $insert->ids],
                ['company_id',  $company_id]
            ];
            $whereData_empid = [
                ['employee_id', $insert->employeeid],
                ['id', '<>', $insert->ids],
                ['company_id',  $company_id]
            ];
            $check_emailid=$this->check_mailid($whereData_emailid);
            if($check_emailid){
                $json_array=['type'=>'warning','message'=>'Email ID already Exists!'];                
            }else{
                $contact_exists=$this->check_contactnumber($whereData_contact);
                if($contact_exists){
                    $json_array=['type'=>'warning','message'=>'Contact Number already Exists!'];
                }else{
                    $empid_exists=$this->check_empid($whereData_empid);
                    if($empid_exists){
                        $json_array=['type'=>'warning','message'=>'Employee ID already Exists!'];
                    }
                    else{
                        // if($insert->hasFile('user_proof')){ 
                        //     return $insert->user_proof->getClientOriginalExtension();
                        //     $extension=$insert->user_proof->getClientOriginalExtension();
                        //     $insert->file('user_proof')->move(public_path("/img/user_proofs/"), $insert->employeeid.'_'.$insert->firstname.'_proof'.'.'.$extension);
                        //     $path= "/img/user_proofs/".$insert->employeeid.'_'.$insert->firstname.'_proof'.'.'.$extension;                            
                        // }
                        // else{                            
                        //     $path = "";
                        // }
                        if($insert->roles=="Technician"){
                            $str =  $insert->support;
                            //$arr = implode(',',$str);
                            if(is_array( $str)){
                                $string_version = implode(',', $str);
                            }
                            else $string_version = $str;
                        
                            //echo $string_version;
                            $update_user=DB::table('add_users')
                                            ->where('id', $insert->ids)
                                            ->update(['employee_id'=>$insert->employeeid,
                                                    'first_name'=>$insert->firstname, 
                                                    'last_name'=>$insert->lastname,
                                                    'email_id'=>$insert->emailid,
                                                    'contact_number'=>$insert->contactnumber,
                                                    'alternate_number'=>$insert->alternatenumber,
                                                    'address'=>$insert->addresses,
                                                    'region'=>$insert->regions,
                                                    'area'=>$insert->areas,
                                                    'location'=>$insert->locations,
                                                    'role'=>$insert->roles,
                                                    'support'=>$string_version,
                                                    //'user_proof'=>$path
                                            ]);
                        }
                        else{                 
                            $update_user=DB::table('add_users')
                                            ->where('id', $insert->ids)
                                            ->update(['employee_id'=>$insert->employeeid,
                                                    'first_name'=>$insert->firstname, 
                                                    'last_name'=>$insert->lastname,
                                                    'email_id'=>$insert->emailid,
                                                    'contact_number'=>$insert->contactnumber,
                                                    'alternate_number'=>$insert->alternatenumber,
                                                    'address'=>$insert->addresses,
                                                    'region'=>$insert->regions,
                                                    'area'=>$insert->areas,
                                                    'location'=>$insert->locations,
                                                    'role'=>$insert->roles,
                                                    'support'=>"",
                                                    //'user_proof'=>$path
                                            ]);
                        }
                        if($update_user){
                            $json_array=['type'=>'success','message'=>'User Updated Successfully'];
                        }else{
                            $json_array=['type'=>'error','message'=>'No Changes made !!'];
                        }
                    }
                }
            }
        }
        return $json_array;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User_model::all();
        return $users;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function admincompanyupdate(Request $request)
    {

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    { 
        $userss = DB::table('add_users')->where('id',$request->input)->delete();
        if($userss){
            $userdata = DB::table('users')->where('id',$request->input)->delete();
            if($userdata){
                $json_array=['type'=>'success','message'=>'User Details Deleted!!'];
            }
            else{
                $json_array=['type'=>'error','message'=>'Not Deleted!!'];
            }
        }
        else{
            $json_array=['type'=>'error','message'=>'No Changes made !!'];
        }
        return $json_array;
    }

    public function getusers(){
        $company_id=Auth::user()->company_id;
        //$userss = User_model::all();
        $userss = DB::table('add_users')
        ->where('company_id',$company_id)
        ->select('*')
        ->get();
      

        return $userss;
    }
    public function updateadminuser(request $input)
    {
       
        $query1=DB::select('call update_adminuser ( ?,?,?,?,?,?,?,?,?,?,?,?,?)',array ( $input->ids,$input->employeeid,$input->firstname,$input->lastname,$input->emailid,$input->contactnumber,$input->alternatenumber,$input->addresses,$input->regions,$input->areas,$input->locations,$input->roles,$input->support));

        $c=get_object_vars($query1[0]);
        //echo $query1[0]->statusquery;
        $json_array=array();
        if($query1[0]->statusquery==4)
        {
        $json_array=['type'=>'error','message'=>'Email ID already Exists!'];
        }
        else if($query1[0]->statusquery==3)
        {
        $json_array=['type'=>'error','message'=>'Contact NO already Exists!'];
        }
        else if($query1[0]->statusquery==2)
        {
        $json_array=['type'=>'success','message'=>'Updated successfully!'];
        }
        else{
            $json_array=['type'=>'error','message'=>'Some error occured!'];
        }
        return $json_array;
    }
    public function deleteadminuser(request $input)
    {
        $query1=DB::select('call delete_adminuser ( ?)',array ( $input->ids));

       // $c=get_object_vars($query1[0]);
       $json_array=array();
       if($query1[0]->statusquery==1)
       {
       $json_array=['type'=>'success','message'=>'User successfully deleted'];
       }
       else{
        $json_array=['type'=>'error','message'=>'Error occured'];
       }
       return $json_array;
    }
 public function getuser_role(request $input){
        $customer = UserRolemodel::where('company_id', '=', $input->company_id)->get();
        /* $customer = Customerinsertmodel::all(); */
        return $customer;
    }

    public function insert_userrole(request $request){
        $where=[
            'user_roles'=>$request->owner_types,
        ];
        if(empty($request->owner_types) || $request->owner_types=="undefined"){
            $json_array=['type'=>'error','message'=>'User role is mandatory'];
        }else{
            $customer = new UserRolemodel();
            $customer->user_roles=$request->owner_types;        
            $customer->company_id=$request->company_id;        
            $role_exists =$this->checkuser_role($where);
            if($role_exists){            
                $json_array=['type'=>'warning','message'=>'User role already exists!'];
            }else{
                if($customer->save()){
                    $json_array=['type'=>'success','message'=>'User role is added!'];
                }else{
                    $json_array=['type'=>'error','message'=>'User role not added! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }
    public function checkuser_role($where){
        $user_role = UserRolemodel::where($where)->count() > 0;
        return $user_role;
    }
    public function companybaseduserlist(request $request)
    {
       // echo $request->companyid;
        $where=[
            "company_id"=>$request->companyid,
            "project_id"=>0,
            "project_name"=>0
        ];
       
        $userss = DB::table('add_users')
                    ->where($where)
                    
                    ->select('*')
                    ->get();
                    return $userss;
    }
    public function userlist(request $request)
    {
       
        $users=DB::select('call get_users_all (?)',array (0));
   
       // $c=get_object_vars($query1[0]);


        return $users;
    }
    public function edit_users(request $request){

        $company_id=Auth::user()->company_id;
        $original_array=array();
        $userss=array();
        $new_array=array();
        $where=[
            "employee_id"=>$request->input,
            "company_id"=>$company_id,
        ];
        $userss = DB::table('add_users')
                    ->where($where)
                    ->select('*')
                    ->get();            
        //    print_r($userss);
        //    die;
            if($userss[0]->role=="Technician"){
                $arr=explode(",",$userss[0]->support);

                for($i=0;$i<count($arr);$i++){
                    $sup_cat = DB::table('support_category')
                    ->where('support_category.id',$arr[$i])
                    ->select('*')
                    ->get();
                    $original_array[].=$sup_cat[0]->support_category;
                }
                    $string_version = implode(',', $original_array);
                    $array_input= array(
                        'id'=>$userss[0]->id,
                        'employee_id'=>$userss[0]->employee_id,
                        'first_name'=>$userss[0]->first_name,
                        'last_name'=>$userss[0]->last_name,
                        'email_id'=>$userss[0]->email_id,
                        'contact_number'=>$userss[0]->contact_number,
                        'alternate_number'=>$userss[0]->alternate_number,
                        'address'=>$userss[0]->address,
                        'region'=>$userss[0]->region,
                        'area'=>$userss[0]->area,
                        'location'=>$userss[0]->location,
                        'role'=>$userss[0]->role,
                        'support'=>$userss[0]->support,
                        'sup_name'=>$string_version
                    ); 
            }
            else{
                $array_input= array(
                    'id'=>$userss[0]->id,
                    'employee_id'=>$userss[0]->employee_id,
                    'first_name'=>$userss[0]->first_name,
                    'last_name'=>$userss[0]->last_name,
                    'email_id'=>$userss[0]->email_id,
                    'contact_number'=>$userss[0]->contact_number,
                    'alternate_number'=>$userss[0]->alternate_number,
                    'address'=>$userss[0]->address,
                    'region'=>$userss[0]->region,
                    'area'=>$userss[0]->area,
                    'location'=>$userss[0]->location,
                    'role'=>$userss[0]->role
                ); 
           

            }
            
            return $array_input;
            /*array_push($array_input,['id'=>$userss[0]->id]);
            array_push($array_input,['employee_id'=>$userss[0]->employee_id]);
            array_push($array_input,['first_name'=>$userss[0]->first_name]);
            array_push($array_input,array('last_name'=>$userss[0]->last_name));
            array_push($array_input,array('email_id'=>$userss[0]->email_id));
            array_push($array_input,array('contact_number'=>$userss[0]->contact_number));
            array_push($array_input,array('alternate_number'=>$userss[0]->alternate_number));
            array_push($array_input,array('address'=>$userss[0]->address));
            array_push($array_input,array('region'=>$userss[0]->region));
            array_push($array_input,array('area'=>$userss[0]->area));
            array_push($array_input,array('location'=>$userss[0]->location));
            array_push($array_input,array('role'=>$userss[0]->role));
            array_push($array_input,array('support'=>$userss[0]->support));*/
            // if($array_input['role']=="Technician"){
                
            // }
       
        //echo $string_version;
        
        // return $userss[0];
    }
    public function admin_edit_users(request $request){

      //  $company_id=Auth::user()->company_id;
        $original_array=array();
        $userss=array();
        $new_array=array();
      //  print_r($request->input);
        $query1=DB::select('call edit_admin_user (?)',array($request->input));
        $where=[
            "employee_id"=>$request->input
           // "company_id"=>$company_id,
        ];
        $userss = DB::table('add_users')
                    ->join('company_details', 'add_users.company_id', '=', 'company_details.company_id')
                    ->where($where)
                    ->select('*')
                    ->get();            
        //    print_r($userss);
        //    die;
     //   print_r($query1);
        $c=get_object_vars($query1[0]);
            if($c['role']=="Technician"){
                $arr=explode(",",$c['support']);

                for($i=0;$i<count($arr);$i++){
                    $sup_cat = DB::table('support_category')
                    ->where('support_category.id',$arr[$i])
                    ->select('*')
                    ->get();
                    $original_array[].=$sup_cat[0]->support_category;
                }
                    $string_version = implode(',', $original_array);
                   /* $query2 = DB::table('add_users')
                    ->where('employee_id',$request->input)
                    ->select('*')
                    ->get();*/
                    $array_input= array(
                        'id'=>$c['id'],
                        'employee_id'=>$c['employee_id'],
                        'company_id'=>$c['company_id'],
                        'first_name'=>$c['first_name'],
                        'last_name'=>$c['last_name'],
                        'email_id'=>$c['email_id'],
                        'contact_number'=>$c['contact_number'],
                        'alternate_number'=>$c['alternate_number'],
                        'address'=>$c['address'],
                        'region'=>$c['region'],
                        'area'=>$c['area'],
                        'location'=>$c['location'],
                        'role'=>$c['role'],
                        'support'=>$c['support'],
                        'sup_name'=>$string_version
                    ); 
            }
            else{
               /* $query2 = DB::table('add_users')
                ->where('employee_id',$request->input)
                ->select('*')
                ->get();*/
                $array_input= array(
                    'id'=>$c['id'],
                    'employee_id'=>$c['employee_id'],
                    'first_name'=>$c['first_name'],
                    'last_name'=>$c['last_name'],
                    'email_id'=>$c['email_id'],
                    'contact_number'=>$c['contact_number'],
                    'alternate_number'=>$c['alternate_number'],
                    'address'=>$c['address'],
                    'region'=>$c['region'],
                    'area'=>$c['area'],
                    'location'=>$c['location'],
                    'role'=>$c['role'],
                    'company_id'=>$c['company_id']
                ); 
           

            }
            
            return $array_input;
            /*array_push($array_input,['id'=>$userss[0]->id]);
            array_push($array_input,['employee_id'=>$userss[0]->employee_id]);
            array_push($array_input,['first_name'=>$userss[0]->first_name]);
            array_push($array_input,array('last_name'=>$userss[0]->last_name));
            array_push($array_input,array('email_id'=>$userss[0]->email_id));
            array_push($array_input,array('contact_number'=>$userss[0]->contact_number));
            array_push($array_input,array('alternate_number'=>$userss[0]->alternate_number));
            array_push($array_input,array('address'=>$userss[0]->address));
            array_push($array_input,array('region'=>$userss[0]->region));
            array_push($array_input,array('area'=>$userss[0]->area));
            array_push($array_input,array('location'=>$userss[0]->location));
            array_push($array_input,array('role'=>$userss[0]->role));
            array_push($array_input,array('support'=>$userss[0]->support));*/
            // if($array_input['role']=="Technician"){
                
            // }
       
        //echo $string_version;
        
        // return $userss[0];
    }
    public function loadWorktype(){
        $user = Supportmodel::all();
        return $user;
    }

    public function getapilogin(request $request){
        $username=$request->email;
		$password=$request->password;
        $credintials= [
            'email'=>$username,
            'password'=>$password,
        ];
        if(!Auth::attempt(['email' => $username, 'password' => $password])){
            echo response('Username and Password does not match', 403);
            //echo 'Username and Password does not matched';
        }else{
            echo response(AUTH::user(),201);
            //echo 'Password Matched';
        }  
    }

    public function check_mailid($whereData_emailid){
        $email_exists=User_model::where($whereData_emailid)->count() > 0;
        return $email_exists;
    }

    public function check_contactnumber($whereData_emailid){
        $email_exists=User_model::where($whereData_emailid)->count() > 0;
        return $email_exists;
    }

    public function check_empid($whereData_empid){
        $emp_exists=User_model::where($whereData_empid)->count() > 0;
        return $emp_exists;
    }

    public function upload_userexcel(request $request){
        
        $json_array=array();
        if ($request->hasFile('bulk_file')){
            $path = $request->file('bulk_file')->getRealPath();
            $ext = $request->file('bulk_file')->getClientOriginalExtension();
        if($ext=="xlsx" || $ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                    $objExcel = $reader->getExcel();
                    $sheet = $objExcel->getSheet(0);
                    // $highestColumn = $sheet->getHighestColumn(); 
                        $GLOBALS['$highestColumn']  = $sheet->getHighestDataColumn(); 
                    })->get();
                       // echo $GLOBALS['$highestColumn'];

                    if($GLOBALS['$highestColumn']=="N"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $value) {
                                if($value->company_id!=""&&$value->employee_id!="" && $value->firstname!="" && $value->lastname!="" && $value->email_id!="" && 
                                $value->contact_number!="" && $value->address!="" && $value->region!="" && $value->area!="" && $value->location!="" && $value->role!="" 
                                && $value->updated_at!="" && $value->created_at!=""){
                                    if($value->alternate_number==""){
                                        $value->alternate_number="";
                                    }
                                    $where=[
                                        'contact_number'=>$value->contact_number,
                                    ];
                                    $where_m=[
                                        'email_id'=>$value->email_id,
                                    ];
    
                                    $check_ifexists=$this->check_contactnumber($where);
                                    if($check_ifexists){     
                                        array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for Contact Number '.$value->contact_number ));       
                                        //$json_array[].=['type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'];
                                    }
                                    else{ 
                                        $check_mail= $this->check_mailid($where_m);
                                        if($check_mail){
                                            array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for Mail Id '.$value->email_id )); 
                                        }
                                        else{
                                          //  $company_id=Auth::user()->company_id;
                                          $company_id=$value->company_id;
                                            $user_id = DB::table('add_users') 
                                                ->orderBy("id", "desc")
                                                ->select('id')
                                                ->limit(1)
                                                ->get();
                                            if(!$user_id==""){                        
                                                $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
                                                $datass = $datass + 1;
                                                $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                $user_ids = $company_id."_" . $datass;
                                            }
                                            else{
                                                $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
                                                $datass = $datass + 1;
                                                $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                $user_ids = $company_id."_" . $datass;
                                            };   
                                            $user = new User_model();
                                            $user->employee_id=$value->employee_id;
                                            $user->user_id=$user_ids;
                                            $user->company_id=$value->company_id;
                                            $user->first_name=$value->firstname;
                                            $user->last_name=$value->lastname;
                                            $user->email_id=$value->email_id;
                                            $user->contact_number=$value->contact_number;
                                            $user->alternate_number=$value->alternate_number;        
                                            $user->address=$value->address;
                                            $user->region=$value->region;
                                            $user->area=$value->area;
                                            $user->location=$value->location;
                                            $user->role=$value->role;
                                            $user->save();
                                            if($user->save()){  
                                                $destinationPath = public_path('images');
                                                $destinationPath=$destinationPath.'/noimage.png';
                                                $string = str_random(8);
                                                $hashed_random_password = Hash::make($string);
                                                $login= new User();                        
                                                $login->name=$value->firstname;
                                                $login->user_id=$user_ids;
                                                $login->email=$value->email_id;
                                                $login->temp_pwd=$string;
                                                $login->password=$hashed_random_password;
                                                $login->user_type=$value->role;
                                                $login->image=$destinationPath;   
                                                $login->company_id=$value->company_id;                      
                                                if($login->save()){
                                                    $data = array( 'email' => $value->email_id);
                                                    Mail::send('email',
                                                    array(
                                                        'name' => $value->firstname,
                                                        'email' => $value->email_id,
                                                        'password' => $string
                                                    ), function($message) use ($data)
                                                    {
                                                        $message->from('ramyariotz22@gmail.com');
                                                        $message->to($data['email'], 'Admin')->subject('Credintials for FMS User Login');
                                                    });
                                                    if (Mail::failures()) {
                                                        array_push($json_array,array('type'=>'success','message'=>'Records inserted for User '.$value->employee_id)); 
                                                    }else{
                                                        array_push($json_array,array('type'=>'success','message'=>'Records inserted for User '.$value->employee_id)); 
                                                    }                                                    
                                                }
                                                else{
                                                    array_push($json_array,array('type'=>'error','message'=>'User Credentials For User '.$value->employee_id. ' not saved!' ));
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    array_push($json_array,array('type'=>'error','message'=>'Fill all fields For User '.$value->employee_id ));            
                                }
                           
                            }
                        }
                        else{
                            array_push($json_array,array('type'=>'warning','message'=>'Empty excel file')); 
                        }
                    }
                    else{
                        array_push($json_array,array('type'=>'error','message'=>'Unmatched file format!,View Sample template')); 
                    }
            }
            else{
                array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
            }    
        }
        else{
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
        return $json_array; 
    }

    public function upload_techexcel(request $request){
        $json_array=array();
        if ($request->hasFile('bulk_file')){
            $path = $request->file('bulk_file')->getRealPath();
            $ext = $request->file('bulk_file')->getClientOriginalExtension();
        if($ext=="xlsx" || $ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                    $objExcel = $reader->getExcel();
                    $sheet = $objExcel->getSheet(0);
                    // $highestColumn = $sheet->getHighestColumn(); 
                        $GLOBALS['$highestColumn']  = $sheet->getHighestDataColumn(); 
                    })->get();
                       // echo $GLOBALS['$highestColumn'];

                    if($GLOBALS['$highestColumn']=="N"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $value) {
                                if($value->employee_id!="" && $value->firstname!="" && $value->lastname!="" && $value->email_id!="" && 
                                $value->contact_number!="" && $value->address!="" && $value->region!="" && $value->area!="" && $value->location!="" && $value->role!="" 
                                && $value->support!=""&& $value->updated_at!="" && $value->created_at!=""){
                                    if($value->alternate_number==""){
                                        $value->alternate_number="";
                                    }
                                    $where=[
                                        'contact_number'=>$value->contact_number,
                                    ];
                                    $where_m=[
                                        'email_id'=>$value->email_id,
                                    ];
    
                                    $check_ifexists=$this->check_contactnumber($where);
                                    if($check_ifexists){     
                                        array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for Contact Number '.$value->contact_number ));       
                                        //$json_array[].=['type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'];
                                    }
                                    else{ 
                                        $check_mail= $this->check_mailid($where_m);
                                        if($check_mail){
                                            array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for Mail Id '.$value->email_id )); 
                                        }
                                        else{
                                            $company_id=Auth::user()->company_id;
                                            $user_id = DB::table('add_users') 
                                                ->orderBy("id", "desc")
                                                ->select('id')
                                                ->limit(1)
                                                ->get();
                                            if(!$user_id==""){                        
                                                $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
                                                $datass = $datass + 1;
                                                $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                $user_ids = $company_id."_" . $datass;
                                            }
                                            else{
                                                $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
                                                $datass = $datass + 1;
                                                $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                $user_ids = $company_id."_" . $datass;
                                            };   
                                            $user = new User_model();
                                            $user->employee_id=$value->employee_id;
                                            $user->user_id=$user_ids;
                                            $user->company_id=$company_id;
                                            $user->first_name=$value->firstname;
                                            $user->last_name=$value->lastname;
                                            $user->email_id=$value->email_id;
                                            $user->contact_number=$value->contact_number;
                                            $user->alternate_number=$value->alternate_number;        
                                            $user->address=$value->address;
                                            $user->region=$value->region;
                                            $user->area=$value->area;
                                            $user->location=$value->location;
                                            $user->role=$value->role;
                                            $user->support= $value->support;
                                            $user->save();
                                            if($user->save()){  
                                                $destinationPath = public_path('images');
                                                $destinationPath=$destinationPath.'/noimage.png';
                                                $string = str_random(8);
                                                $hashed_random_password = Hash::make($string);
                                                $login= new User();                        
                                                $login->name=$value->firstname;
                                                $login->user_id=$user_ids;
                                                $login->email=$value->email_id;
                                                $login->temp_pwd=$string;
                                                $login->password=$hashed_random_password;
                                                $login->user_type=$value->role;
                                                $login->image=$destinationPath;   
                                                $login->company_id=$company_id;                      
                                                if($login->save()){
                                                    $data = array( 'email' => $value->email_id);
                                                    Mail::send('email',
                                                    array(
                                                        'name' => $value->firstname,
                                                        'email' => $value->email_id,
                                                        'password' => $string
                                                    ), function($message) use ($data)
                                                    {
                                                        $message->from('ramyariotz22@gmail.com');
                                                        $message->to($data['email'], 'Admin')->subject('Credintials for FMS User Login');
                                                    });
                                                    if (Mail::failures()) {
                                                        array_push($json_array,array('type'=>'success','message'=>'Records inserted for User '.$value->employee_id)); 
                                                    }else{
                                                        array_push($json_array,array('type'=>'success','message'=>'Records inserted for User '.$value->employee_id)); 
                                                    }   
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    array_push($json_array,array('type'=>'error','message'=>'Fill all fields For User '.$value->employee_id ));            
                                }
                           
                            }
                        }
                        else{
                            array_push($json_array,array('type'=>'warning','message'=>'Empty excel file')); 
                        }
                    }
                    else{
                        array_push($json_array,array('type'=>'error','message'=>'Unmatched file format!,View Sample template')); 
                    }
            }
            else{
                array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
            }    
        }
        else{
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
        return $json_array; 
    }

}