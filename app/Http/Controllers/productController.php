<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Product_model;
use App\subcat_model;
use URL;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function getproduct(REQUEST $request){
        $company_id=$request->prod_id;
        $product = DB::table('product_management')
            ->where('product_management.company_id',$company_id)
            ->select('*')
            ->get(); 
        return json_encode($product);
    }
    public function getsubcategory(REQUEST $request){
        $prod_id=$request->prod_id; 
        $subcategory = DB::table('category_details')
            ->where('prod_id',$prod_id)
            ->select('*')
            ->get(); 
        return json_encode($subcategory);
    }
    public function productid_check(Request $request){
        $company_id=Auth::user()->company_id;
        $subcategory = DB::table('product_management')            
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$subcategory==""){                
            $datass = intval(preg_replace('/[^0-9]+/', '', $subcategory), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $datass = "product_" .$company_id."_". $datass;            
        }else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $subcategory), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $datass = "product_" .$company_id."_". $datass;           
        }

        // get the `UploadedFile` object
        $file = $request->file('pro_file');
        $ids = $request->ids;
        $file = $request->pro_file;
        $pro_name = $request->pro_name;
        $pro_model = $request->pro_model;
        $pro_desc = $request->pro_desc;
        $product_image = $request->product_image;
        $product_id = $request->product_id;

        if ($request->hasFile('pro_file')) {

            $ext=array('gif', 'jpg', 'jpeg', 'png');

            $extension=$request->pro_file->getClientOriginalExtension();
            if(in_array($extension, $ext)) {
                $upload_folder = '/assets/images/';

                if(!empty($ids) && !empty($product_image) && $ids != 'undefined' && $product_image != 'undefined'){
                    $whereData_emailid = [
                        ['product_name', $pro_name],
                        ['id', '<>', $ids],
                        ['product_id', '<>', $product_id],
                        ['company_id',  $company_id]
                    ];

                    $check_nameexists=$this->check_nameexists($whereData_emailid);
                    if($check_nameexists){
                        $json_array=['type'=>'error','message'=>'Product Category Name is already exists, Try different Name!'];
                    }else{
                        if($file->move(public_path() . $upload_folder, $pro_name.'.'.$extension)){
                            $update_product=DB::table('product_management')
                                ->where('id', $ids)
                                ->update(['product_id'=>$product_id,
                                        'product_name'=>$pro_name, 
                                        'product_image'=>URL::asset($upload_folder . $pro_name.'.'.$extension),
                                        'product_modal'=>$pro_model,
                                        'product_desc'=>$pro_desc,
                                        'company_id'=>$company_id,
                                        ]);
                            if($update_product){
                                $json_array=['type'=>'success','message'=>'Product Category is Updated Successfully'];                            
                            }else{
                                $json_array=['type'=>'error','message'=>'There is no changes happended!'];
                            }
                        }else{
                            $json_array=['type'=>'error','message'=>'File is not uploaded, Kindly Try again!'];
                        }                    
                    }
                }else{
                    $whereData_emailid = [
                        ['product_name', $pro_name],
                        ['company_id',  $company_id]
                    ];
                    $check_nameexists=$this->check_nameexists($whereData_emailid);
                    if($check_nameexists){
                        $json_array=['type'=>'error','message'=>'Product Category Name is already exists, Try different Name!'];
                    }else{             
                        if($file->move(public_path() . $upload_folder, $pro_name.'.'.$extension)){
                            $Product_model=new Product_model;
                            $Product_model->product_id=$datass;
                            $Product_model->product_name=$pro_name;
                            $Product_model->product_image=URL::asset($upload_folder . $pro_name.'.'.$extension);
                            $Product_model->product_modal=$pro_model;
                            $Product_model->product_desc=$pro_desc;
                            $Product_model->company_id=$company_id;
                            if($Product_model->save()){
                                $json_array=['type'=>'success','message'=>'Product Category inserted Successfully'];
                            }else{
                                $json_array=['type'=>'success','message'=>'Product Category not inserted, Kindly try again!'];
                            }
                        }else{
                            $json_array=['type'=>'error','message'=>'File is not uploaded, Kindly Try again!'];
                        }
                    }
                }
            } else {
                $json_array=['type'=>'error','message'=>'Product Category Image should be an Image file!'];
            }            

        }else{
            if(!empty($ids) && !empty($product_image) && $ids != 'undefined' && $product_image != 'undefined'){
                $whereData_emailid = [
                    ['product_name', $pro_name],
                    ['id', '<>', $ids],
                    ['product_id', '<>', $product_id],
                    ['company_id',  $company_id]
                ];

                $check_nameexists=$this->check_nameexists($whereData_emailid);
                if($check_nameexists){
                    $json_array=['type'=>'error','message'=>'Product Category Name is already exists, Try different Name!'];
                }else{
                    $update_product=DB::table('product_management')
                        ->where('id', $ids)
                        ->update(['product_id'=>$product_id,
                                'product_name'=>$pro_name, 
                                'product_image'=>$product_image,
                                'product_modal'=>$pro_model,
                                'product_desc'=>$pro_desc,
                                'company_id'=>$company_id,
                                ]);
                    if($update_product){
                        $json_array=['type'=>'success','message'=>'Product Category is Updated Successfully'];                            
                    }else{
                        $json_array=['type'=>'error','message'=>'There is no changes happended!'];
                    }                 
                }
            }else{
                $json_array=['type'=>'error','message'=>'Product Category Image is Mandatory!'];
            }

        }
        return $json_array;
    }
    public function get_product(REQUEST $request){
        $prod_id=$request->prod_id;
        $product = DB::table('product_management')
            ->where('product_management.id',$prod_id)
            ->select('*')
            ->get(); 
        return json_encode($product);
    }
    public function delete_product(REQUEST $pro){
        $id=$pro->prod_id;
        $del_sub = DB::table('category_details')->where('prod_id',$id)->delete();        
        if($del_sub){
            $blog = DB::table('product_management')->where('id',$id)->delete();
            if($blog){
                $json_array=['type'=>'success','message'=>'Product Category is Deleted Successfully!'];
            }else{
                $json_array=['type'=>'error','message'=>'Product Category is not Deleted, Kindly try again!'];
            }            
        }else{
            $json_array=['type'=>'error','message'=>'Product Category is not Deleted, Kindly try again!'];
        }
        return $json_array;
    }

    public function subcatid_check(Request $request){
        $company_id=Auth::user()->company_id;
        $subcategory = DB::table('category_details')            
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$subcategory==""){                
            $datass = intval(preg_replace('/[^0-9]+/', '', $subcategory), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $datass = "product_" .$company_id."_". $datass;            
        }else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $subcategory), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $datass = "product_" .$company_id."_". $datass;           
        }

        // get the `UploadedFile` object
        $file = $request->file('cat_file');
        $file = $request->cat_file;
        $ids = $request->ids;
        $product_id = $request->product_id;
        $product_name = $request->product_name;
        $cat_name = $request->cat_name;
        $cat_desc = $request->cat_desc;
        $cat_image = $request->cat_image;

        if ($request->hasFile('cat_file')) {

            $ext=array('gif', 'jpg', 'jpeg', 'png');

            $extension=$request->pro_file->getClientOriginalExtension();
            if(in_array($extension, $ext)) {
                $upload_folder = '/assets/images/';

                if(!empty($ids) && !empty($cat_image) && $ids != 'undefined' && $cat_image != 'undefined'){
                    $whereData_emailid = [
                        ['cat_name', $cat_name],
                        ['id', '<>', $ids],
                        ['cat_id', '<>', $cat_id],
                        ['company_id',  $company_id]
                    ];

                    $check_nameexists=$this->check_nameexists($whereData_emailid);
                    if($check_nameexists){
                        $json_array=['type'=>'error','message'=>'Product Category Name is already exists, Try different Name!'];
                    }else{
                        if($file->move(public_path() . $upload_folder, $cat_name.'.'.$extension)){
                            $update_cat=DB::table('category_details')
                                ->where('id', $ids)
                                ->update(['cat_id'=>$cat_id,
                                        'cat_name'=>$pro_name, 
                                        'cat_image'=>URL::asset($upload_folder . $pro_name.'.'.$extension),
                                        'prod_id'=>$pro_model,
                                        'cat_desc'=>$pro_desc,
                                        'company_id'=>$company_id,
                                        ]);
                            if($update_cat){
                                $json_array=['type'=>'success','message'=>'Product Category is Updated Successfully'];                            
                            }else{
                                $json_array=['type'=>'error','message'=>'There is no changes happended!'];
                            }
                        }else{
                            $json_array=['type'=>'error','message'=>'File is not uploaded, Kindly Try again!'];
                        }                    
                    }
                }else{
                    $whereData_emailid = [
                        ['cat_name', $cat_name],
                        ['company_id',  $company_id]
                    ];
                    $check_nameexists=$this->check_nameexists($whereData_emailid);
                    if($check_nameexists){
                        $json_array=['type'=>'error','message'=>'Sub Category Name is already exists, Try different Name!'];
                    }else{             
                        if($file->move(public_path() . $upload_folder, $pro_name.'.'.$extension)){
                            $Product_model=new Product_model;
                            $Product_model->product_id=$datass;
                            $Product_model->product_name=$pro_name;
                            $Product_model->product_image=URL::asset($upload_folder . $pro_name.'.'.$extension);
                            $Product_model->product_modal=$pro_model;
                            $Product_model->product_desc=$pro_desc;
                            $Product_model->company_id=$company_id;
                            if($Product_model->save()){
                                $json_array=['type'=>'success','message'=>'Product Category inserted Successfully'];
                            }else{
                                $json_array=['type'=>'success','message'=>'Product Category not inserted, Kindly try again!'];
                            }
                        }else{
                            $json_array=['type'=>'error','message'=>'File is not uploaded, Kindly Try again!'];
                        }
                    }
                }
            } else {
                $json_array=['type'=>'error','message'=>'Product Category Image should be an Image file!'];
            }            

        }else{
            if(!empty($ids) && !empty($product_image) && $ids != 'undefined' && $product_image != 'undefined'){
                $whereData_emailid = [
                    ['product_name', $pro_name],
                    ['id', '<>', $ids],
                    ['product_id', '<>', $product_id],
                    ['company_id',  $company_id]
                ];

                $check_nameexists=$this->check_nameexists($whereData_emailid);
                if($check_nameexists){
                    $json_array=['type'=>'error','message'=>'Product Category Name is already exists, Try different Name!'];
                }else{
                    $update_product=DB::table('product_management')
                        ->where('id', $ids)
                        ->update(['product_id'=>$product_id,
                                'product_name'=>$pro_name, 
                                'product_image'=>$product_image,
                                'product_modal'=>$pro_model,
                                'product_desc'=>$pro_desc,
                                'company_id'=>$company_id,
                                ]);
                    if($update_product){
                        $json_array=['type'=>'success','message'=>'Product Category is Updated Successfully'];                            
                    }else{
                        $json_array=['type'=>'error','message'=>'There is no changes happended!'];
                    }                 
                }
            }else{
                $json_array=['type'=>'error','message'=>'Product Category Image is Mandatory!'];
            }

        }
        return $json_array;
    }
    public function get_subcategory(REQUEST $request){
        $id=$request->id;
        $product = DB::table('category_details')
            ->where('category_details.id',$id)
            ->join('product_management','category_details.prod_id','=','product_management.product_id')
            ->select('category_details.*','product_management.product_name')
            ->get(); 
        return json_encode($product);
    }
    public function check_nameexists($wherename){
        $email_exists = Product_model::where($wherename)->count() > 0;
        return $email_exists;
    }
}
