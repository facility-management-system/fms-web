<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Trainingmodel;
use App\assessment;
use App\Supportmodel;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;
use Illuminate\Support\Facades\Storage;
use URL;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function get_supportmodel(REQUEST $request){   
        $supportmodel = Supportmodel::where('company_id', '=', $request->company_id)->get();
        return $supportmodel;
    }
    public function get_training(REQUEST $request){   
        $training = Trainingmodel::where('company_id', '=', $request->company_id)->get();
        return $training;
    }

    public function getassessment(REQUEST $request){   
        $assessment = assessment::where('company_id', '=', $request->company_id)->get();
        return $assessment;
    }    
    public function uploadtraining(Request $request){
        $json_array;$training_id;$url;$last_id;        
        $last = DB::table('training')->orderBy('id', 'DESC')->first();
        if($last==null){
            $last_id = (int)$last+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $training_id = "training_".$last_id;                     
        }else{
            $last_id = (int)$last->id+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $training_id = "training_".$last_id;
        }
        if($request->hasFile('file')){
            $extension=$request->file->getClientOriginalExtension();
            $url = public_path("assets/uploads/training/");
            if($request->file('file')->move(public_path("/assets/uploads/training/"), $training_id.'.'.$extension)){         
                $training= new Trainingmodel();
                $training->title = $request->title;
                $training->training_id = $training_id;
                $training->type = $request->type;
                $training->productcategory = $request->product;
                $training->contenttype = $request->content_type;
                $training->content = $training_id.'.'.$extension;
                $training->company_id = $request->company_id;
                if($training->save()){
                    $json_array=['type'=>'success','message'=>'Training Created Successfully!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Training not Created, Kindly try again!'];
                }
            }else{
                $json_array=['type'=>'error','message'=>'Uploaded file is not moved to server, Kindly try again!'];
            }
            
        }else{
            $json_array=['type'=>'error','message'=>'File is not uploaded, Kindly try again!'];
        }
        return $json_array;
    }
    public function edittraining(Request $request){
        $json_array;
        if($request->hasFile('file')){
            $extension=$request->file->getClientOriginalExtension();
            $url = public_path("assets/uploads/training/");
            if($request->file('file')->move(public_path("/assets/uploads/training/"), $request->training_id.'.'.$extension)){
                $update_training=DB::table('training')
                            ->where('id', $request->id)
                            ->update(['training_id'=>$request->training_id,
                                    'title'=>$request->title, 
                                    'type'=>$request->type,
                                    'productcategory'=>$request->product,
                                    'contenttype'=>$request->content_type,
                                    'company_id'=>$request->company_id,
                                    ]);
                $json_array=['type'=>'success','message'=>'Training Updated Successfully'];
            }else{
                $json_array=['type'=>'error','message'=>'Uploaded file is not moved to server, Kindly try again!'];
            }
        }else{
            $update_training=DB::table('training')
                            ->where('id', $request->id)
                            ->update(['training_id'=>$request->training_id,
                                    'title'=>$request->title, 
                                    'type'=>$request->type,
                                    'productcategory'=>$request->product,
                                    'contenttype'=>$request->content_type,
                                    'company_id'=>$request->company_id,
                                    ]);
                    if($update_training){
                        $json_array=['type'=>'success','message'=>'Training Updated Successfully'];
                    }else{
                        $json_array=['type'=>'error','message'=>'No changes happended!'];
                    }
        }
        return $json_array;
    }
    public function deletetraining(Request $delete){
        $id=$delete->id;
        $delete_training=DB::table('training')
                            ->where('id', $delete->id)
                            ->delete();
        if($delete_training){
            $json_array=['type'=>'success','message'=>'Training Deleted Successfully'];
        }else{
            $json_array=['type'=>'error','message'=>'Training Not Deleted, Kindly Try again!'];
        }
        return $json_array;
    }
    
    public function uploadassessment(Request $request){
        $json_array;$assessment_id;$url;$last_id;        
        $last = DB::table('assessment')->orderBy('id', 'DESC')->first();
        if($last==null){
            $last_id = (int)$last+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $assessment_id = "assessment_".$last_id;                     
        }else{
            $last_id = (int)$last->id+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $assessment_id = "assessment_".$last_id;
        }
        $assessment= new assessment();
        $assessment->title = $request->assess_title;
        $assessment->assessment_id = $assessment_id;
        $assessment->type = $request->assess_type;
        $assessment->productcategory = $request->assess_product;
        $assessment->company_id = $request->company_id;
        if($assessment->save()){
            $json_array=['type'=>'success','message'=>'Assessment Created Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Assessment not Created, Kindly try again!'];
        }
        return $json_array;
    }
    public function editassessment(Request $request){
        $json_array;
        $update_assessment=DB::table('assessment')
                        ->where('id', $request->id)
                        ->update(['assessment_id'=>$request->assessment_id,
                                'title'=>$request->title, 
                                'type'=>$request->type,
                                'productcategory'=>$request->product,
                                'company_id'=>$request->company_id,
                                ]);
                if($update_assessment){
                    $json_array=['type'=>'success','message'=>'Assessment Updated Successfully'];
                }else{
                    $json_array=['type'=>'error','message'=>'No changes happended!'];
                }
        return $json_array;
    }
    public function deleteassessment(Request $delete){
        $id=$delete->id;
        $delete_assessment=DB::table('assessment')
                            ->where('id', $delete->id)
                            ->delete();
        if($delete_assessment){
            $json_array=['type'=>'success','message'=>'Assessment Deleted Successfully'];
        }else{
            $json_array=['type'=>'error','message'=>'Assessment Not Deleted, Kindly Try again!'];
        }
        return $json_array;
    }

    public function get_quizid(){
        $quiz_id;
        $last = DB::table('quiz')->orderBy('id', 'DESC')->first();
        if($last==null){
            $last_id = (int)$last+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $quiz_id = "Quiz_".$last_id;                     
        }else{
            $last_id = (int)$last->id+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $quiz_id = "Quiz_".$last_id;
        }
        return $quiz_id;
    }
}
