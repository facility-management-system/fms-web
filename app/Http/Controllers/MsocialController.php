<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\Msocialmodel;
use App\Relatedtablemodel;
use App\Eventsmodel;
use App\User_model;
use App\Supportmodel;
use Mail;
use Excel;
use Illuminate\Support\Facades\Hash;
use PHPExcel; 
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use Carbon\Carbon;

class MsocialController extends Controller
{
    // public function getposttypes(){
    //     $userss = DB::table('post_type')
    //     ->select('*')
    //     ->get();
    //     return $userss;
    // }
    
    public function getproptypes(request $request){
        $input=$request->input_val;
        //$userss = User_model::all();
        $userss = DB::table('property_type')
        ->where('cat_id',$input)
        ->select('*')
        ->get();
        return $userss;
    }

    public function getprop_subtypes(request $req){
        $prop_id=$req->prop_id;
        $subtype = DB::table('property_subtype')
        ->where('property_id',$prop_id)
        ->select('*')
        ->get();
        return $subtype;
    }

    public function get_bedrooms(request $input){
        $val=$input->input;
        $output = DB::table('bedrooms')
        ->where('sub_id',$val)
        ->select('*')
        ->get();
        return $output;
    }

    public function get_bathrooms(request $input){
        $val=$input->input;
        $output = DB::table('bathrooms')
        ->where('sub_id',$val)
        ->select('*')
        ->get();
        return $output;
    }

    public function get_furnishing(request $input){
        $val=$input->input;
        $output = DB::table('furnish_status')
        ->where('sub_id',$val)
        ->select('id','furnish')
        ->get();
        return $output;
    }
    public function get_batchelors(request $input){
        $val=$input->input;
        $output = DB::table('batchelors')
        ->where('sub_id',$val)
        ->select('id','batch_type')
        ->get();
        return $output;
    }
    public function get_carparking(request $input){
        $val=$input->input;
        $output = DB::table('car_parkings')
        ->where('sub_id',$val)
        ->select('id','car_park')
        ->get();
        return $output;
    }
    public function get_facing(request $input){
        $val=$input->input;
        $output = DB::table('facing_direction')
        ->where('sub_id',$val)
        ->select('id','facing_direction')
        ->get();
        return $output;
    }

    public function get_consstatus(request $input){
        $val=$input->input;
        $output = DB::table('construction_status')
        ->where('sub_id',$val)
        ->select('*')
        ->get();
        return $output;
    }
    public function get_meals(request $input){
        $val=$input->input;
        $output = DB::table('meals')
        ->where('sub_id',$val)
        ->select('*')
        ->get();
        return $output;
    }

    public function get_furntypes(request $input){
        $val=$input->input_val;
        $output = DB::table('furniture')
        ->where('cat_id',$val)
        ->select('fur_id','fur_name')
        ->get();
        return $output;
    }

    public function get_mobilecat(request $req){
        $val=$req->input_val;
        $output = DB::table('mobiles')
        ->where('cat_id',$val)
        ->select('id','mobile_cat')
        ->get();
        return $output;
    }

    public function getmob_subtypes(request $req){
        $val=$req->cat_id;
        $output = DB::table('mobile_subcat')
        ->where('cat_id',$val)
        ->select('sub_id','mobile_subcat')
        ->get();
        return $output;
    }

    public function get_cars(request $input){
        $val=$input->input_val;
        $output = DB::table('cars')
        ->where('cat_id',$val)
        ->select('id','car_type')
        ->get();
        return $output;
    }

    public function car_subtypes(request $input){
        $val=$input->cat_id;
        $output = DB::table('cars_subtype')
        ->where('car_id',$val)
        ->select('id','car_subtype')
        ->get();
        return $output;
    }

    public function get_carmodel(request $req){
        $val=$req->car_id;
        $output = DB::table('car_model')
        ->where('car_subid',$val)
        ->select('id','model')
        ->get();
        return $output;
    }

    public function car_fuels(request $input){
        $val=$input->cat_id;
        $output = DB::table('car_fuel')
        ->where('car_id',$val)
        ->select('id','fuel_type')
        ->get();
        return $output;
    }

    public function elec_types(request $input){
        $val = $input->input_val;
        $output = DB::table('electronics')
        ->where('cat_id',$val)
        ->select('id','electronic_items')
        ->get();
        return $output;
    }

    public function pets_types(request $response){
        $val = $response->input_val;
        $output = DB::table('pets')
        ->where('cat_id',$val)
        ->select('id','pet_name')
        ->get();
        return $output;
    }

    public function bike_types(request $input){
        $val = $input->input_val;
        $output = DB::table('bikes')
        ->where('cat_id',$val)
        ->select('id','bike_cat')
        ->get();
        return $output;
    }
    
    public function bike_subtypes(request $input){
        $val=$input->cat_id;
        $output = DB::table('bikes_category')
        ->where('cat_id',$val)
        ->select('sub_id','sub_name')
        ->get();
        return $output;
    }

    public function bike_models(request $input){
        $val=$input->cat_id;
        $output = DB::table('bike_model')
        ->where('bike_subid',$val)
        ->select('id','model')
        ->get();
        return $output;
    }

    public function insert_socialpost(request $response){
        $json_array="";
        $now = Carbon::now();
        $user_i= new Msocialmodel(); 
        $user_i->title= $response->title;
        $user_i->description= $response->desc;
        $user_i->user_id= $response->user_id;
        $user_i->created_at= $now;
        $user_i->save();

        if($user_i->save()){
            $tl_id = DB::table('timeline_table') 
                ->orderBy("id", "desc")
                ->select('id')
                ->limit(1)
                ->get();                
           
            $new_post= new Relatedtablemodel();
            $new_post->related_id = $tl_id[0]->id;
            $new_post->post_type= $response->ids;
            $new_post->user_id= $response->user_id;
            $new_post->save();
            $i=0;
            if($new_post->save()){
                echo "timeline";
               
                //$json_array=['type'=>'success','message'=>'Your Post has been Saved!'];
                if($response->hasFile('upload_images')){     
                    echo "files";   
                    print_r($response->file('upload_images'));   
                    foreach($response->file('upload_images') as $image)
                    {
                        echo "foreach file";
                        //print_r($insert->upload_images); 
                       // die;
                        $destinationPath = public_path('img');
                                
                       $proof_images->move($destinationPath,$tl_id[0]->id.'_'.$response->user_id.'_image1'.'.'.$image[$i]->getClientOriginalExtension());
                       if($proof_images){
                        return "post saved";
                        die;
                       }
                       $json_array=['type'=>'success','message'=>'Your Post has been Saved!'];
                    $i=$i+1;
                    }
               
                }else{
                    $user_prooffile = "";
                    $proof_file = "";
                }
            }           
            
        }
        return $json_array; 
        
    }

    public function insert_eventpost(request $response){
        $user_i= new Eventsmodel(); 
        $user_i->event_title= $response->event_title;
        $user_i->event_desc= $response->event_desc;
        $user_i->event_date= $response->event_date;
        $user_i->event_venue= $response->event_venue;
        $user_i->save();

        if($user_i->save()){
            $tl_id = DB::table('events_table') 
                ->orderBy("id", "desc")
                ->select('id')
                ->limit(1)
                ->get();                
           
            $new_post= new Relatedtablemodel();
            $new_post->related_id = $tl_id[0]->id;
            $new_post->post_type= $response->ids;
            $new_post->user_id= $response->user_id;
            $new_post->save();

            if($new_post->save()){
                $json_array=['type'=>'success','message'=>'Your Post has been Saved!'];
            }
            return $json_array;
        }
    }

    public function insert_classipost(request $response){
        $new_post = new Msocialmodel();
    }
}
