<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Contractmodel;
use App\Supportmodel;
use App\Maintanancemodel;
use App\Maintananceload;
use App\Contract_typemodel;
use Illuminate\Support\Facades\Hash;
use Mail;
use Excel;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;
use PHPExcel; 
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use Illuminate\Support\Facades\Validator;


class ContractController extends Controller
{
    //create new
    public function create(Request $request)
    {
        //echo $request->value;
        //die;
        $company_id=Auth::user()->company_id;
        $where=[
            'cat_name'=>$request->value,
        ];
        if(empty($request->value) || $request->value=="undefined"){
            $json_array=['type'=>'error','message'=>'Contract Category is mandatory'];
        }else{
            $customer = new Contractmodel();
            $customer->cat_name=$request->value; 
            $customer->company_id=$company_id;       
            $check_nameexists=$this->check_nameexists($where);
            if($check_nameexists){            
                $json_array=['type'=>'error','message'=>'Contract Category is already exists!'];
            }else{
                if($customer->save()){
                    $json_array=['type'=>'success','message'=>'Contract Category is Successfully inserted!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Contract Category is not inserted! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }
        //new support
    public function create_support(Request $request)
    {   
        $company_id=Auth::user()->company_id;
        $where=[
            'support_category'=>$request->input,
        ];
        if(empty($request->input) || $request->input=="undefined"){
            $json_array=['type'=>'error','message'=>'Support Category is mandatory'];
        }else{
           $support = new Supportmodel();
            $support->support_category=$request->input;     
            $support->company_id= $company_id;
            $check_ifexists=$this->check_ifexists($where);
            if($check_ifexists){            
                $json_array=['type'=>'error','message'=>'Support Category already exists!'];
            }else{
                if($support->save()){
                    $json_array=['type'=>'success','message'=>'Support Category is Successfully inserted!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Support Category is not inserted! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }

    public function create_maintanance(Request $request){
        $company_id=Auth::user()->company_id;
        $where=[
            'maintainance'=>$request->input,
            'cont_cat'=>$request->cont,
            'company_id'=>$company_id,
        ];   
        
        if(empty($request->input) || $request->input=="undefined" || empty($request->cont) || $request->cont=="undefined"){
            $json_array=['type'=>'error','message'=>'All fields are mandatory'];
        }else{
           $maintain_save = new Maintanancemodel();
            $maintain_save->maintainance=$request->input;     
            $maintain_save->cont_cat=$request->cont;     
            $maintain_save->company_id= $company_id;
            $check_exists=$this->check_exists($where);
            if($check_exists){            
                $json_array=['type'=>'error','message'=>'The same values already exists!'];
            }else{
                if($maintain_save->save()){
                    $json_array=['type'=>'success','message'=>'Maintanace type is Successfully inserted!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Maintanace type is not inserted! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }

    public function create_conttype(Request $request){
        $company_id=Auth::user()->company_id;   
        $where=[
            'contract_type'=>$request->input,
            'cont_cat'=>$request->cont,
            'company_id'=>$company_id,
        ];
        if(empty($request->input) || $request->input=="undefined" || empty($request->cont) || $request->cont=="undefined"){
            $json_array=['type'=>'error','message'=>'All fields are mandatory'];
        }else{
            $maintain_save = new Contract_typemodel();
            $maintain_save->contract_type=$request->input;     
            $maintain_save->cont_cat=$request->cont;     
            $maintain_save->company_id= $company_id;
            $already_exists=$this->check_alreadyexists($where);
            if($already_exists){            
                $json_array=['type'=>'error','message'=>'Same values already exists!'];
            }else{
                if($maintain_save->save()){
                    $json_array=['type'=>'success','message'=>'Contract Type is Successfully inserted!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Contract Type is not inserted! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }
    
    //get contract types
    public function loadConttype(Request $request){
		//$company_id=Auth::user()->company_id;
        //$contract = Contractmodel::all();
		 $contract = DB::table('contract_category')
                    ->where('contract_category.company_id',$request->comp_id)
                    ->select("*" )
                    ->get(); 
        return $contract;
    }

    public function loadSupporttype(Request $request){
        //$support_type = Supportmodel::all();
		$support_type =	DB::table('support_category')
                    ->where('support_category.company_id',$request->comp_id)
                    ->select("*" )
                    ->get(); 
        return $support_type;
    }

    public function loadMaintaintype(Request $request){
        //$support_type = Maintanancemodel::all();
        $company_id=Auth::user()->company_id;
		$result_data = DB::table('maintanance_type')
                    ->where('maintanance_type.company_id',$request->comp_id)
                    ->select("maintanance_type.id", "maintanance_type.maintainance","maintanance_type.updated_at","maintanance_type.price","maintanance_type.cont_cat","contract_category.cat_name" )
                    ->join("contract_category", "maintanance_type.cont_cat", "=", "contract_category.id")
                    ->get();  
        return $result_data;
    }

    public function loadContracttype(Request $request){
        $company_id=Auth::user()->company_id;
        $cont_data = DB::table('contract_type')
                    ->where('contract_type.company_id',$request->comp_id)
                    ->select("contract_type.id", "contract_type.contract_type","contract_type.updated_at","contract_type.cont_cat","contract_category.cat_name" )
                    ->join("contract_category", "contract_type.cont_cat", "=", "contract_category.id")
                    ->get();   
        //$cont_data = loadContracttype::all();
        return $cont_data;
    }

    public function update_contract(Request $request){
        $where1=[
            'cat_name'=>$request->cat_name,
        ];
        if(empty($request->cat_name) || $request->cat_name=="undefined"){
            $json_array=['type'=>'error','message'=>'Contract Category is mandatory'];
        }else{                   
            $check_updateexists=$this->check_updateexists($where1);            
            if($check_updateexists){            
                $json_array=['type'=>'warning','message'=>'Same values exists!'];
            }else{
                $update_owner=DB::table('contract_category')
                            ->where('id', $request->id)
                            ->update(['cat_name'=>$request->cat_name,
                        ]);
                if($update_owner){
                    $json_array=['type'=>'success','message'=>'Contract Category is Successfully Updated!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Contract Category not updated!'];
                }
            }
        }        
        return $json_array;
    }

    public function update_support(Request $request){
        $where=[
            'support_category'=>$request->sup_cat,
        ];
        if(empty($request->sup_cat) || $request->sup_cat=="undefined"){
            $json_array=['type'=>'error','message'=>'Support Category is mandatory'];
        }else{                   
            $check_updateexists=$this->check_ifexists($where);            
            if($check_updateexists){            
                $json_array=['type'=>'warning','message'=>'Same values exists!'];
            }
            else{
                $update_sup =DB::table('support_category')
                                ->where('id', $request->id)
                                ->update([
                                    'support_category'=>$request->sup_cat,
                                ]);
                    if($update_sup){
                        $json_array=['type'=>'success','message'=>'Support Category is Successfully Updated!'];
                    }
                    else{
                        $json_array=['type'=>'error','message'=>'Support Category not updated!'];
                    }
            }
        }        
        return $json_array;
    }

    public function update_contracttype(Request $request){

        // $answer = DB::table('contract_category')
        //     ->where('cat_name',$request->cat_name)
        //     ->select('*')
        //     ->get(); 
        $where1=[
            'contract_type'=>$request->cont_type,
            'cont_cat'=>$request->dyn_id
        ];
        $already_exists=$this->check_alreadyexists($where1);
            if($already_exists){            
                $json_array=['type'=>'error','message'=>'Same Values already exists!'];
            }
            else{
                $update_sup=DB::table('contract_type')
                                    ->where('id', $request->id)
                                    ->update(['contract_type'=>$request->cont_type,
                                              'cont_cat'=>$request->dyn_id,
                                     ]);
                    if($update_sup){
                        $json_array=['type'=>'success','message'=>'Contract Type is Successfully Updated!'];
                    }
                    else{
                        $json_array=['type'=>'error','message'=>'Contract Type not updated!'];
                    }
            }
        return $json_array;
    }
    
    public function update_maintaintype(Request $request){
        
                // $answer = DB::table('contract_category')
                //     ->where('cat_name',$request->cat_name)
                //     ->select('*')
                //     ->get(); 
                $where1=[
                    'maintainance'=>$request->m_type,
                    'cont_cat'=>$request->dyn_mid
                ];
                $already_exists=$this->check_exists($where1);
                    if($already_exists){            
                        $json_array=['type'=>'error','message'=>'Same Values already exists!'];
                    }
                    else{
                        $update_sup=DB::table('maintanance_type')
                                            ->where('id', $request->id)
                                            ->update(['maintainance'=>$request->m_type,
                                                      'cont_cat'=>$request->dyn_mid,
                                             ]);
                            if($update_sup){
                                $json_array=['type'=>'success','message'=>'Maintanance Type is Successfully Updated!'];
                            }
                            else{
                                $json_array=['type'=>'error','message'=>'Maintanance Type not updated!'];
                            }
                    }
                return $json_array;
    }
    
    public function delete_contract(Request $request)
    {
        $id=$request->id;
        $deleteownertype = DB::table('contract_category')->where('id',$id)->delete();
        if($deleteownertype){
            $json_array=['type'=>'success','message'=>'Contract Category is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Something went wrong, Kindly try again!'];
        }
        return $json_array;    
    }

    public function delete_conttype(Request $request)
    {
        $id=$request->id;
        $deleteownertype = DB::table('contract_type')->where('id',$id)->delete();
        if($deleteownertype){
            $json_array=['type'=>'success','message'=>'Contract Type is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Contract Type is not Deleted, Kindly try again!'];
        }
        return $json_array;    
    }

    public function delete_mtype(Request $request)
    {
        $id=$request->id;
        $deleteownertype = DB::table('maintanance_type')->where('id',$id)->delete();
        if($deleteownertype){
            $json_array=['type'=>'success','message'=>'Maintanance Type is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Maintanance Type is not Deleted, Kindly try again!'];
        }
        return $json_array;    
    }

    public function delete_suptype(Request $request)
    {
        $id=$request->id;
        $deleteownertype = DB::table('support_category')->where('id',$id)->delete();
        if($deleteownertype){
            $json_array=['type'=>'success','message'=>'Support Category is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Support Category is not Deleted, Kindly try again!'];
        }
        return $json_array;    
    }
    
    public function check_nameexists($where){
        $customer = Contractmodel::where($where)->count() > 0;
        return $customer;
    }
    public function check_ifexists($where){
        $check_ifexists = Supportmodel::where($where)->count() > 0;
        return $check_ifexists;
    }
    public function check_exists($where){
        $check_exists = Maintanancemodel::where($where)->count() > 0;
        return $check_exists;
    }
    public function check_alreadyexists($where){
        $already_exists = Contract_typemodel::where($where)->count() > 0;
        return $already_exists;
    }
    public function check_updateexists($where){
        $already_exists = Contractmodel::where($where)->count() > 0;
        return $already_exists;
    }
    //Bulk Upload
    public function import_contract(Request $request)
    {
        $json_array=array();
        if ($request->hasFile('contract_file')){
            $path = $request->file('contract_file')->getRealPath();
            $ext = $request->file('contract_file')->getClientOriginalExtension();
        if($ext=="xlsx" || $ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                    $objExcel = $reader->getExcel();
                    $sheet = $objExcel->getSheet(0);
                    // $highestColumn = $sheet->getHighestColumn(); 
                        $GLOBALS['$highestColumn']  = $sheet->getHighestColumn(); 
                    })->get();
                    if($GLOBALS['$highestColumn']=="D"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $rows) {
    
                              
                                   // print_r($r_array);
                                    //echo $r_array['items']['category_name'];
                                  /*  $validator = Validator::make($r_array, [
                                        'category_name'=> 'required',
                                        'company_id'=> 'required',
                                        'updated_at'=>'required',
                                        'created_at'=>'required',
                                    ]);
                                    if ($validator->fails()) {  
                                        $json_array[].=['type'=>'warning','message'=>'Some field(s) missing in excel!'];
                                        //array_push($result,array("Some field(s) missing in excel!"));
                                    }
                                    else{*/
                                        $where=[
                                            'cat_name'=>$rows->category_name,
                                        ];
                                        $check_nameexists=$this->check_nameexists($where);
                                        if($check_nameexists){     
                                            array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'));       
                                            //$json_array[].=['type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'];
                                        }
                                        else{
                                            $customer = new Contractmodel();
                                            $customer->cat_name=$rows->category_name; 
                                            $customer->company_id=$rows->company_id;  
                                            $customer->updated_at=$rows->updated_at;
                                            $customer->created_at=$rows->created_at;
                                                $customer->save();
                        
                                                array_push($json_array,array('type'=>'success','message'=>'Records inserted Successfully.')); 
                                               // $json_array[].=['type'=>'success','message'=>'Records inserted Successfully.'];   
                                   // array_push($result,array("Users added successfully"));
                                         }
                                    /*else part if duplicate data available
                                    else{
                                    array_push($result,array("Duplicate entry for <strong>$row->userid</strong> "));
                                    } */
                
                                   // }
                           
                            }
                        }
                    }
                    else{
                        //$json_array[].=['type'=>'error','message'=>'Unmatched file format,View Sample template'];
                        array_push($json_array,array('type'=>'warning','message'=>'Unmatched file format,View Sample template')); 
                    }
                    //$this->PHPExcel->getActiveSheet();
                
                }
                else{
                    //$json_array[].=['type'=>"error",'message'=>"Upload an Excel file!"];
                    array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
                }
                   /* if($GLOBALS['$highestColumn'] == "B")
                    { */
                               /* start if(!empty($data) && $data->count()){
                                        foreach ($data as $key => $value) {
                                            if(!empty($value)){
                                                foreach ($value as $s) {		
                                                    $insert[] = ['cat_name' => $value['category_name'], 
                                                                'company_id' => $value['company_id'],
                                                                'updated_at' => $value['updated_at'],
                                                                'created_at' => $value['created_at']
                                                                ];
                                                }
                                            }
                                        }
                                        if(!empty($insert)){
                                            $record_insert= Contractmodel::insert($insert);
                                            if($record_insert){
                                                $json_array=['type'=>'success','message'=>'Records inserted Successfully.'];
                                            }
                                        }
                                }
                                else{
                                $json_array=['type'=>'warning','message'=>'Empty excel file'];
                                } end */
                   /* }
                    else{
                        $json_array=['type'=>'error','message'=>'Unmatched Excel file. See the template Once again'];
                    }   */           
        }
        else{
            //$json_array.=['type'=>'warning','message'=>'File not included!'];
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
    return $json_array; 
    }

    public function import1_contract(Request $request){
        $json_array=[];
        $input="";
        $highestColumn="";
        if($request->hasFile('contract_file')){
            $path = $request->file('contract_file')->getRealPath();
            $data = \Excel::load($path, function($reader) {
                $objExcel = $reader->getExcel();
                 $sheet = $objExcel->getSheet(0);
            //     $input=getActiveSheetIndex($objExcel);
            // echo $input;
            // die;
               // $highestColumn = $path->getHighestColumn(); 
                    $GLOBALS['$highestColumn']  = $sheet->getHighestColumn(); 
                })->get();
                // echo $highestColumn;
                // die;
                /*if($GLOBALS['$highestColumn'] == "D")
                {*/ 
                    if($data->count()){
                        foreach ($data as $key => $value) {
                            $arr[] = ['cat_name' => $value->category_name, 
                                    'company_id' => $value->company_id, 
                                    'updated_at' => $value->updated_at,
                                    'created_at' => $value->created_at
                                    ];
                        }
                        if(!empty($arr)){
                            \DB::table('contract_category')->insert($arr);
                            $json_array=['type'=>'success','message'=>'Records inserted Successfully.'];
                        }
                    }
                    else{
                        $json_array=['type'=>'warning','message'=>'Empty excel file'];
                    }
                /*}
                else{
                    echo "error";
                } */
        }
        else{
            $json_array=['type'=>'warning','message'=>'File not included!'];
        }
        return $json_array;
    }

    public function upload_excel(Request $request){
        $json_array=array();
        if ($request->hasFile('contract_type')){
            $path = $request->file('contract_type')->getRealPath();
            $file_ext = $request->file('contract_type')->getClientOriginalExtension();
        if($file_ext=="xlsx" || $file_ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                            $objExcel = $reader->getExcel();
                            $sheet = $objExcel->getSheet(0);
                            $GLOBALS['$highestColumn']  = $sheet->getHighestColumn(); 
                        })->get();

                        
                    if($GLOBALS['$highestColumn']=="E"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $value) {
                                        $where=[
                                            'contract_type'=>$value->contract_type,
                                            'cont_cat' => $value->contract_category, 
                                        ];
                                        $check_nameexists=$this->check_alreadyexists($where);
                                        if($check_nameexists){     
                                            array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for <strong>'.$value->contract_type.' & Category '.$value->contract_category.'</strong>'));       
                                            //$json_array[].=['type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'];
                                        }
                                        else{ 
                                            $maintain_save = new Contract_typemodel();
                                            $maintain_save->contract_type=$value->contract_type;     
                                            $maintain_save->cont_cat= $value->contract_category;     
                                            $maintain_save->company_id= $value->company_id; 
                                            $maintain_save->updated_at = $value->updated_at;
                                            $maintain_save->created_at= $value->created_at;
                                                $maintain_save->save();
                        
                                                array_push($json_array,array('type'=>'success','message'=>'Records inserted Successfully.')); 
                           
                                        }
                            }
                        }
                        else{
                            //$json_array=['type'=>'warning','message'=>'Empty excel file'];
                            array_push($json_array,array('type'=>'warning','message'=>'Empty excel file')); 
                        }
                    }
                    else{
                        //$json_array[].=['type'=>'error','message'=>'Unmatched file format,View Sample template'];
                        array_push($json_array,array('type'=>'error','message'=>'Unmatched file format,View Sample template')); 
                    }
                
                }
                else{
                    //$json_array[].=['type'=>"error",'message'=>"Upload an Excel file!"];
                    array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
                }
                   
        }
        else{
            //$json_array.=['type'=>'warning','message'=>'File not included!'];
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
        return $json_array;
      
        }
    
    public function upload_maintype(Request $request)
    {
        $json_array=array();
        if ($request->hasFile('main_type')){
            $path = $request->file('main_type')->getRealPath();
            $file_ext = $request->file('main_type')->getClientOriginalExtension();
        if($file_ext=="xlsx" || $file_ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                            $objExcel = $reader->getExcel();
                            $sheet = $objExcel->getSheet(0);
                            $GLOBALS['$highestColumn']  = $sheet->getHighestColumn(); 
                        })->get();

                        
                    if($GLOBALS['$highestColumn']=="E"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $value) {
                                        $where=[
                                            'maintainance'=>$value->maintainance,
                                            'cont_cat' => $value->contract_category, 
                                        ];
                                        $check_nameexists=$this->check_exists($where);
                                        if($check_nameexists){     
                                            array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for <strong>'.$value->maintainance.' & Category '.$value->contract_category.'</strong>'));       
                                            //$json_array[].=['type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'];
                                        }
                                        else{           
                                            $cont_type = new Maintanancemodel();
                                            $cont_type->maintainance = $value->maintainance; 
                                            $cont_type->cont_cat = $value->contract_category;
                                            $cont_type->company_id = $value->company_id;  
                                            $cont_type->updated_at = $value->updated_at;
                                            $cont_type->created_at= $value->created_at;
                                                $cont_type->save();
                        
                                                array_push($json_array,array('type'=>'success','message'=>'Records inserted Successfully.')); 
                           
                                        }
                            }
                        }
                        else{
                            //$json_array=['type'=>'warning','message'=>'Empty excel file'];
                            array_push($json_array,array('type'=>'warning','message'=>'Empty excel file')); 
                        }
                    }
                    else{
                        //$json_array[].=['type'=>'error','message'=>'Unmatched file format,View Sample template'];
                        array_push($json_array,array('type'=>'error','message'=>'Unmatched file format,View Sample template')); 
                    }
                
                }
                else{
                    //$json_array[].=['type'=>"error",'message'=>"Upload an Excel file!"];
                    array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
                }
                   
        }
        else{
            //$json_array.=['type'=>'warning','message'=>'File not included!'];
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
        return $json_array;
      
    }

    public function upload_supexcel(Request $request){
        $json_array=array();
        if ($request->hasFile('support_file')){
            $path = $request->file('support_file')->getRealPath();
            $file_ext = $request->file('support_file')->getClientOriginalExtension();
            if($file_ext=="xlsx" || $file_ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                            $objExcel = $reader->getExcel();
                            $sheet = $objExcel->getSheet(0);
                            $GLOBALS['$highestColumn']  = $sheet->getHighestColumn(); 
                        })->get();
                        
                    if($GLOBALS['$highestColumn']=="D"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $value) {
                                        $where=[
                                            'support_category'=>$value->support_category,
                                            'company_id' => $value->company_id, 
                                        ];
                                        $check_ifexists=$this->check_ifexists($where);
                                        if($check_ifexists){     
                                            array_push($json_array,array('type'=>'error','message'=>'Duplicate entry for <strong>'.$value->support_category .'</strong>'));       
                                            //$json_array[].=['type'=>'error','message'=>'Duplicate entry for <strong>'.$rows->category_name.'</strong>'];
                                        }
                                        else{           
                                            $cont_type = new Supportmodel();
                                            $cont_type->support_category = $value->support_category; 
                                            $cont_type->company_id = $value->company_id;  
                                            $cont_type->updated_at = $value->updated_at;
                                            $cont_type->created_at= $value->created_at;
                                                $cont_type->save();
                                                array_push($json_array,array('type'=>'success','message'=>'Records inserted Successfully.')); 
                                        }
                            }
                        }
                        else{
                            array_push($json_array,array('type'=>'warning','message'=>'Empty excel file')); 
                        }
                    }
                    else{
                        array_push($json_array,array('type'=>'error','message'=>'Unmatched file format,View Sample template')); 
                    }
            }
            else{
                array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
            }    
        }
        else{
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
        return $json_array; 
    }

}
