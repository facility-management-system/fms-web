<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Companymodel;
use App\User_model;
use App\User;
use Mail;


class Companycontroller extends Controller
{
    public function insertcompany(Request $insert){
       /* $user_id = DB::table('company_details') 
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$user_id==""){                        
            $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $comp_id = "company"."_" . $datass;
        }
        else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $comp_id = "company"."_" . $datass;
        };  */
     //  echo $insert->company_name;
       // die();
       /* $user_i= new Companymodel(); 



        $user_i->company_id=$comp_id;
        $user_i->company_name=$insert->company_name;
        $user_i->company_email=$insert->company_email;
        $user_i->company_num=$insert->company_number;
        $user_i->alternate_num=$insert->alternate_number;        
        $user_i->address=$insert->comapny_address;
        $user_i->save();*/
       // ($insert->company_name,$insert->company_email,$insert->company_number, $insert->alternate_number,$insert->company_address,$insert->comapny_address,$insert->admin_fname,$insert->admin_lname,$insert->admin_emailid,$insert->admin_contactno,$insert->alternatecontactno ,$insert->admin_address,$insert->admin_region,$insert->admin_area,$insert->admin_location,"Admin")

      /*  $new_user->employee_id=$insert->comp_adminid;
        $new_user->company_id=$comp_id;
        $new_user->user_id=$users_id;
        $new_user->first_name=$insert->admin_fname;
        $new_user->last_name=$insert->admin_lname;
        $new_user->email_id=$insert->admin_emailid;
        $new_user->contact_number=$insert->admin_contactno;
        $new_user->alternate_number=$insert->admin_alternatecontactno;        
        $new_user->address=$insert->admin_address;
        $new_user->region=$insert->admin_region;
        $new_user->area=$insert->admin_area;
        $new_user->location=$insert->admin_location;
        $new_user->role="Admin";
        $user_i->user_proof=$user_prooffile;*/


       
        //SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fms' AND TABLE_NAME = 'company_details'
       /* if($user_i->save()){
            $id = DB::table('company_details') 
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$id==""){                        
            $datass = intval(preg_replace('/[^0-9]+/', '', $id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $users_id = $comp_id."_" . $datass;
        }
        else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $users_id = $comp_id."_" . $datass;
        };    */
        $output=""; 
        if($insert->hasFile('user_proof')){   
           
            //$basic = "http://loalhost:8080/2018-03-12/FMS/public/assets/img";  
            $images = $insert->file('user_proof');
            $image_target = 'img/user_proofs/';
            $image_path = $image_target .'/' . $insert->comp_adminid . '_' . $insert->admin_fname . '_'.'proof' .'.'.$images->getClientOriginalExtension();
         
            $images->move($image_target,$insert->comp_adminid . '_' . $insert->admin_fname . '_'.'proof' .'.'.$images->getClientOriginalExtension());
            $user_prooffile=$insert->comp_adminid . '_' . $insert->admin_fname . '_'.'proof' .'.'.$images->getClientOriginalExtension();
        }else{
            $user_prooffile = "";
            $proof_file = "";
           
        }
        $whereData_emailid = [
            ['email_id', $insert->admin_emailid]
        ];
        $whereData_contact = [
            ['contact_number', $insert->admin_contactno]
        ];    
        $whereData = [
            ['employee_id', $insert->comp_adminid]
        ];
        $whereData_empid = [
            ['employee_id', $insert->comp_adminid],
            ['id', '<>', $insert->ids]
        ];
      
       
           $check_emailid=$this->check_mailid($whereData_emailid);
           if($check_emailid){
               $json_array=['type'=>'warning','message'=>'Email ID already Exists!'];                
           }else{
               $contact_exists=$this->check_contactnumber($whereData_contact);
               if($contact_exists){
                   $json_array=['type'=>'warning','message'=>'Contact Number already Exists!'];
               }
               else{
                   $empid_exists=$this->check_empid($whereData_empid);
                   if($empid_exists){
                       $json_array=['type'=>'warning','message'=>'Employee ID already Exists!'];
                   }
                   else {
                        $destinationPath = "/images";
                        $destinationPath=$destinationPath.'/noimage.png';
                            $string = str_random(8);
                            $hashed_random_password = Hash::make($string);
                    $query1=DB::select('call create_company (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',array($insert->company_name,$insert->company_email,$insert->company_number, $insert->alternate_number, $insert->company_address,$insert->comp_adminid, $insert->admin_fname, $insert->admin_lname,$insert->admin_emailid, $insert->admin_contactno,$insert->admin_alternatecontactno ,$insert->admin_address,$user_prooffile, $insert->admin_region,$insert->admin_area,$insert->admin_location,"Admin","",$destinationPath,$hashed_random_password,$string));
                    $c=get_object_vars($query1[0]);
            if($c['newUserstatus']==3){
              $data = array( 'email' => $insert->admin_emailid);
                                Mail::send('email',
                                array(
                                    'name' => $insert->admin_fname,
                                    'email' => $insert->admin_emailid,
                                    'password' => $string
                                ), function($message) use ($data)
                                {
                                    $message->from('ramyariotz22@gmail.com');
                                    $message->to($data['email'], 'Admin')->subject('Credintials for FMS User Login');
                                });
                                if (Mail::failures()) {
                                    $json_array=['type'=>'error','message'=>'Mail not sent to the user, The password for this user is '.$hashed_random_password];
                                }else{
                                    $json_array=['type'=>'success','message'=>'User Added Successfully'];
                                }                           
                   
            }
            else if($c['newUserstatus']==2){
                 $json_array=['type'=>'error','message'=>"User's Login Credentials Failed to Upload !!"];
            }
      else if($c['newUserstatus']==1){
                  $json_array=['type'=>'error','message'=>'User not Added!!'];
            }
        else{
            $json_array=['type'=>'error','message'=>'Something went Wrong !!'];
        }
    }
}
    return $json_array;
    }
}

    public function getcompanies(){
        $userss = DB::table('company_details')
        ->select('*')
        ->get('company_id','company_name');
        return $userss;
    }

    public function insert_adminuser(Request $insert){
         $user_id = DB::table('add_users') 
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
       /* if(!$user_id==""){                        
            $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $pro_ids = "project"."_" . $datass;
        }
        else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $user_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $pro_ids = "project"."_" . $datass;
        };  */   
        if($insert->ids == 'undefined' || $insert->ids == null){

            if($insert->hasFile('user_proof')){   
                //$basic = "http://loalhost:8080/2018-03-12/FMS/public/assets/img";  
                $destinationPath = public_path('img');
                $extension=$insert->user_proof->getClientOriginalExtension();
                $path = $insert->file('user_proof')->storeAs(
                     $insert->employeeid, $insert->firstname.'proof'.'.'.$extension
                );
                $user_prooffile = $path;
               // $user_prooffile->move($destinationPath, $user_prooffile);
               // $proof_file = $destinationPath.'/'.$user_prooffile;
            }else{
                $user_prooffile = "";
                $proof_file = "";
            }

          /*  $user_i= new User_model(); 
            
                $user_i->employee_id=$insert->employeeid;
                $user_i->company_id=$insert->company_id;
                $user_i->user_id="user";
                $user_i->first_name=$insert->firstname;
                $user_i->last_name=$insert->lastname;
                $user_i->email_id=$insert->emailid;
                $user_i->contact_number=$insert->contactnumber;
                $user_i->alternate_number=$insert->alternatenumber;        
                $user_i->address=$insert->addresses;
                $user_i->region=$insert->regions;
                $user_i->area=$insert->areas;
                $user_i->location=$insert->locations;
                $user_i->role=$insert->roles;
                $user_i->user_proof=$user_prooffile;
                $user_i->project_id=$pro_ids;
                $user_i->project_name=$insert->project;
                $user_i->support="";*/
         
            //for user insert            
            $whereData_emailid = [
                ['email_id', $insert->emailid]
            ];
            $whereData_contact = [
                ['contact_number', $insert->contactnumber]
            ];    
            $whereData = [
                ['employee_id', $insert->employeeid]
            ];
            $whereData_empid = [
                ['employee_id', $insert->employeeid],
                ['id', '<>', $insert->ids]
            ];
            $check_emailid=$this->check_mailid($whereData_emailid);
            if($check_emailid){
                $json_array=['type'=>'warning','message'=>'Email ID already Exists!'];                
            }else{
                $contact_exists=$this->check_contactnumber($whereData_contact);
                if($contact_exists){
                    $json_array=['type'=>'warning','message'=>'Contact Number already Exists!'];
                }
                else{
                    $empid_exists=$this->check_empid($whereData_empid);
                    if($empid_exists){
                        $json_array=['type'=>'warning','message'=>'Employee ID already Exists!'];
                    }
                    else {


                        $string = str_random(8);
                        $hashed_random_password = Hash::make($string);
                        $destinationPath = public_path('images');
                        $destinationPath=$destinationPath.'/noimage.png';

                        
     $query1=DB::select('call create_admin (?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?)',array ($insert->company_id, $insert->employeeid,$insert->firstname, $insert->lastname, 
     $insert->emailid, $insert->contactnumber,$insert->alternatenumber,$insert->addresses,$user_prooffile, $insert->regions,
     $insert->areas,$insert->locations,$insert->roles,"",$hashed_random_password,$string,$destinationPath));

     $c=get_object_vars($query1[0]);
   


                      //  $user_i->save();
                        if($c['newUserstatus']!=0){  
//$query1=User_model::where('email_id',$insert->emailid)->where('employee_id',$insert->employeeid)->get();



                       /*     $destinationPath = public_path('images');
                            $destinationPath=$destinationPath.'/noimage.png';
                            $string = str_random(8);
                            $hashed_random_password = Hash::make($string);
                            $login= new User();                        
                            $login->name=$insert->firstname;
                            $login->=$query1[0]['user_id'];
                            $login->email=$insert->emailid;
                            $login->password=$hashed_random_password;
                            $login->temp_pwd=$string;
                            $login->user_type=$insert->roles;
                            $login->image=$destinationPath;
                            $login->company_id=$insert->company_id;      */                
                            if($c['newUserstatus']==2){
                                //$json_array=['type'=>'success','message'=>'User Added Successfully'];
                                $path='http://localhost/nantha_sales/public/';
                                $data = array( 'email' => $insert->emailid);
                                Mail::send('email',
                                array(
                                    'name' => $insert->firstname,
                                    'email' => $insert->emailid,
                                    'password' => $string
                                ), function($message) use ($data)
                                {
                                    $message->from('ramyariotz22@gmail.com');
                                    $message->to($data['email'], 'Admin')->subject('Credintials for FMS User Login');
                                });
                                if (Mail::failures()) {
                                    $json_array=['type'=>'error','message'=>'Mail not sent to the user, The password for this user is '.$string];
                                }else{
                                    $json_array=['type'=>'success','message'=>'User Added Successfully'];
                                }                           
                            }else{
                                $json_array=['type'=>'error','message'=>"User's Login Credentials Failed to Upload !!"];
                            }                        
                        }else{
                            $json_array=['type'=>'error','message'=>'User not Added!!'];
                        }
                    }
                }
            }
        }
        return $json_array; 
    }

    public function check_mailid($whereData_emailid){
        $email_exists=User_model::where($whereData_emailid)->count() > 0;
        return $email_exists;
    }

    public function check_contactnumber($whereData_emailid){
        $email_exists=User_model::where($whereData_emailid)->count() > 0;
        return $email_exists;
    }

    public function check_empid($whereData_empid){
        $emp_exists=User_model::where($whereData_empid)->count() > 0;
        return $emp_exists;
    }

}
