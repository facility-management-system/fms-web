<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\User_model;
use App\Customermodel;
use App\Customerinsertmodel;
use App\ProjectModel;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;
use Illuminate\Support\Facades\Storage;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getcustomer(Request $request){
        $customer = Customermodel::where('company_id', '=', $request->company_id)->get();
        return $customer;
    }
    public function get_customer(Request $request){
        $customer = Customerinsertmodel::where('company_id', '=', $request->company_id)->get();
        /* $customer = Customerinsertmodel::all(); */
        return $customer;
    }
    public function get_assignedproject()
    {
        $userid=Auth::user()->user_id;
     //   echo $userid;
     $query1=User_model::where('user_id','=',$userid)->get();
    
     $result=array();
     if($query1[0]['project_id'])
     {
        $query2=ProjectModel::where('id','=',$query1[0]['project_id'])->get();
      $result=array("projectid"=>$query1[0]['project_id'],"projectname"=>$query1[0]['project_name'],"project_details"=>$query2[0]['project_wbs']);
     }
     else{
        $result=array("projectid"=>'',"projectname"=>'',"project_details"=>'');
     }
      return json_encode($result);
    }
    public function create(Request $request)
    {
       /*  Schema::connection('mysql')->create('mytable_welcome', function($table)
        {
            $table->increments('id');
            $table->timestamps();
        });
        die; */
        $where=[
            'customer_type'=>$request->owner_types,
           'company_id'=> Auth::user()->company_id,
        ];
        if(empty($request->owner_types) || $request->owner_types=="undefined"){
            $json_array=['type'=>'error','message'=>'Owner type is mandatory'];
        }else{
           $customer = new Customermodel();
            $customer->customer_type=$request->owner_types;        
          //  $customer->company_id=$request->company_id;        
          $company_id=Auth::user()->company_id;
          $customer->company_id=Auth::user()->company_id;
            $check_nameexists=$this->check_nameexists($where);
            if($check_nameexists){            
                $json_array=['type'=>'error','message'=>'Owner type is already exists!'];
            }else{
                if($customer->save()){
                    $json_array=['type'=>'success','message'=>'Owner type is Successfully inserted!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Owner type is not inserted! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }
    public function updateownertype(Request $request){
        $where=[
            ['customer_type', $request->customer_type],
            ['id', '<>', $request->id],
        ];
        if(empty($request->customer_type) || $request->customer_type=="undefined"){
            $json_array=['type'=>'error','message'=>'Owner type is mandatory'];
        }else{                   
            $check_nameexists=$this->check_nameexists($where);            
            if($check_nameexists){            
                $json_array=['type'=>'error','message'=>'Owner type is already exists!'];
            }else{
                $update_owner=DB::table('customertype')
                    ->where('id', $request->id)
                    ->update(['customer_type'=>$request->customer_type,
                            ]);
                if($update_owner){
                    $json_array=['type'=>'success','message'=>'Owner type is Successfully Updated!'];
                }else{
                    $json_array=['type'=>'error','message'=>'There is no changes happend!'];
                }
            }
        }        
        return $json_array;
    }
    public function deleteownertype(Request $request)
    {
        $id=$request->id;
        $deleteownertype = DB::table('customertype')->where('id',$id)->delete();
        if($deleteownertype){
            $json_array=['type'=>'success','message'=>'Owner Type is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Owner Type is not Deleted, Kindly try again!'];
        }
        return $json_array;    
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function check_nameexists($where){
        $customer = Customermodel::where($where)->count() > 0;
        return $customer;
    }
    public function create_customer(Request $request){        
        $customer_name=$request->customer_name;
        $customer_email=$request->customer_email;
        $customer_phone=$request->number;
        $customer_alt=$request->altno;
        $customer_ownertype=$request->owner_type;
        $customer_address=$request->address;        
        $customer_project=$request->project_name;
        $customer_project_place=$request->selected_node;
        if($request->hasFile('national_file')){
            $customer_national=$request->ids;
            $customer_nationalexp=$request->national_expairydate;            
            $extension=$request->national_file->getClientOriginalExtension();
            $path = $request->file('national_file')->storeAs(
                $request->customer_name, $request->customer_name.'nationalfile'.'.'.$extension
            );
            $customer_nationalfile = $path;
        }else{
            $customer_national = "";
            $customer_nationalexp = "";
            $customer_nationalfile = "";
        };
        if($request->hasFile('pass_file')){
            $customer_passport=$request->passport_no;
            $customer_passportexp=$request->passport_xdate;            
            $extension=$request->pass_file->getClientOriginalExtension();
            $path = $request->file('pass_file')->storeAs(
                $request->customer_name, $request->customer_name.'passfile'.'.'.$extension
            );
            $customer_passportfile = $path;
        }else{
            $customer_passport = "";
            $customer_passportexp = "";
            $customer_passportfile = "";
        };
        if($request->hasFile('visa_file')){
            $customer_visa=$request->visa_no;
            $customer_visaexp=$request->visa_xdate;            
            $extension=$request->national_file->getClientOriginalExtension();
            $path = $request->file('visa_file')->storeAs(
                $request->customer_name, $request->customer_name.'visafile'.'.'.$extension
            );
            $customer_visafile = $path;
        }else{
            $customer_visa = "";
            $customer_visaexp = "";
            $customer_visafile = "";
        };
        if($request->hasFile('comercial_file')){
            $customer_comercial=$request->commercial_no;
            $customer_comercialexp=$request->commercial_xdate;            
            $extension=$request->national_file->getClientOriginalExtension();
            $path = $request->file('comercial_file')->storeAs(
                $request->customer_name, $request->customer_name.'comercialfile'.'.'.$extension
            );
            $customer_comercialfile = $path;
        }else{
            $customer_comercial = "";
            $customer_comercialexp = "";
            $customer_comercialfile = "";
        };
        if($request->hasFile('ded_file')){
            $customer_ded=$request->ded_no;
            $customer_dedexp=$request->ded_xdate;            
            $extension=$request->national_file->getClientOriginalExtension();
            $path = $request->file('ded_file')->storeAs(
                $request->customer_name, $request->customer_name.'dedfile'.'.'.$extension
            );
            $customer_dedfile = $path;
        }else{
            $customer_ded = "";
            $customer_dedexp = "";
            $customer_dedfile = "";
        };
        if($request->hasFile('ejari_file')){
            $customer_ejari=$request->ejari_no;
            $customer_ejariexp=$request->ejari_xdate;            
            $extension=$request->national_file->getClientOriginalExtension();
            $path = $request->file('ejari_file')->storeAs(
                $request->customer_name, $request->customer_name.'ejarifile'.'.'.$extension
            );
            $customer_ejarifile = $path;
        }else{
            $customer_ejari = "";
            $customer_ejariexp = "";
            $customer_ejarifile = "";
        };
        $cust_id = DB::table('customer') 
        ->orderBy("id", "desc")
        ->select('id')
        ->limit(1)
        ->get();
    if(!$cust_id==""){                        
        $datass = intval(preg_replace('/[^0-9]+/', '', $cust_id), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $customer = "Cust"."_" . $datass;
    }else{
        $datass = intval(preg_replace('/[^0-9]+/', '', $cust_id), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $customer = "Cust"."_" . $datass;
    }; 
        $string = str_random(8);
        $hashed_random_password = Hash::make($string);
        $random_password = md5($string);
        $Customermodel= new Customerinsertmodel();
        $Customermodel->customer_id = $customer;
        $Customermodel->customer_name = $customer_name;
        $Customermodel->customer_email = $customer_email;
        $Customermodel->customer_phone = $customer_phone;
        $Customermodel->customer_alt = $customer_alt;
        $Customermodel->customer_address = $customer_address;
        $Customermodel->customer_owner_type = $customer_ownertype;
        $Customermodel->customer_nationalid = $customer_national;
        $Customermodel->customer_nationalexp = $customer_nationalexp;
        $Customermodel->customer_nationalfile = $customer_nationalfile;
        $Customermodel->customer_passport = $customer_passport;
        $Customermodel->customer_passportexp = $customer_passportexp;
        $Customermodel->customer_passportfile = $customer_passportfile;
        $Customermodel->customer_visa = $customer_visa;
        $Customermodel->customer_visaexp = $customer_visaexp;
        $Customermodel->customer_visafile = $customer_visafile;
        $Customermodel->customer_commercial = $customer_comercial;
        $Customermodel->customer_commercialexp = $customer_comercialexp;
        $Customermodel->customer_commercialfile = $customer_comercialfile;
        $Customermodel->customer_ded = $customer_ded;
        $Customermodel->customer_dedexp = $customer_dedexp;
        $Customermodel->customer_dedfile = $customer_dedfile;
        $Customermodel->customer_ejari = $customer_ejari;
        $Customermodel->customer_ejariexp = $customer_ejariexp;
        $Customermodel->customer_ejarifile = $customer_ejarifile;
        $Customermodel->customer_project = $customer_project;
        $Customermodel->customer_project_place = $customer_project_place;
        $Customermodel->company_id = $request->company_id;
        $Customermodel->password = $string;
        $Customermodel->enc_password = $hashed_random_password;
        if($Customermodel->save()){
            // $destinationPath = "/images";
            // $destinationPath=$destinationPath.'/noimage.png';            
            // $login= new User();                        
            // $login->name=$customer_name;
            // $login->email=$customer_email;
            // $login->password=$hashed_random_password;
            // $login->temp_pwd=$string;
            // $login->user_type="customer";
            // $login->image=$destinationPath;   
            // $login->company_id=$company_id;                      
            // if($login->save()){
            // }
            $data = array( 'email' => $customer_email);
            Mail::send('customer_email',
            array(
                'name' => $customer_name,
                'email' => $customer_email,
                'password' => $string
            ), function($message) use ($data)
            {
                $message->from('ramyariotz22@gmail.com');
                $message->to($data['email'], 'Admin')->subject('Credintials for FMS Customer Login');
            });
            if (Mail::failures()) {
                $json_array=['type'=>'error','message'=>'Mail not sent to the user, The password for this user is '.$string];
            }else{
                $json_array=['type'=>'success','message'=>'Customer Created Successfully!'];
            }            
        }else{
            $json_array=['type'=>'error','message'=>'Customer not Created, Kindly try again!'];
        }
        return $json_array;   
    }
}
