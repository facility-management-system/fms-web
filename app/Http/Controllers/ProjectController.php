<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\ProjectModel;
use App\User_model;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $projects = projectModel::all();
        $json_array=['type'=>'success','message'=>$projects];
        return $json_array;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(REQUEST $project)
    {
        $json_array;
        $project_name=$project->project_name;
        $project_location=$project->project_location;
        $project_manager=$project->project_manager;

        $adminuser=$project->selectedadmin;
        $company=$project->selectedcompany;
        $wbs=json_encode($project->project_wbs);
        //unserialize($wbs);
        $project_nameexists=projectModel::where('project_name',$project_name)->count() > 0;
        if($project_nameexists){
            $json_array=['type'=>'warning','message'=>'Project Name is already Exists!'];
        }else{
            $query1=User_model::where('user_id', $adminuser)->get();
            $project_manager=$query1[0]['first_name'];
            $project_creation= new projectModel();
            $project_creation->project_name=$project_name;
            $project_creation->project_location=$project_location;
            $project_creation->project_manager=$project_manager;
            $project_creation->project_wbs=$wbs;
           
            if($project_creation->save()){
                $get_projectid=projectModel::where('project_name',$project_name)->get();
                //echo $adminuser;
               // echo $get_projectid[0]['id'];
                $query1=User_model::where('user_id', $adminuser)
                ->update(['project_id' =>$get_projectid[0]['id'] ,'project_name'=>$get_projectid[0]['project_name']]);

                $json_array=['type'=>'success','message'=>'Project Created Successfully!'];
            }else{
                $json_array=['type'=>'error','message'=>'Project not Created, Kindly try again!'];
            }
        }
        return $json_array;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
