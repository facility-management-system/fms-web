<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Kbmodel;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;
use Illuminate\Support\Facades\Storage;
use URL;

class KbController extends Controller
{
    public function approved_kblist(Request $request){
        $cont_data = DB::table('knowledge_base')
        ->where('knowledge_base.company_id',$request->company_id)
        ->where('knowledge_base.kb_status','1')
        ->select("knowledge_base.kb_id","knowledge_base.kb_type","knowledge_base.kb_productcategory","knowledge_base.kb_problem","knowledge_base.kb_contenttype","knowledge_base.kb_solution","knowledge_base.kb_content","support_category.support_category" )
        ->join("support_category", "knowledge_base.kb_productcategory", "=", "support_category.id")
        ->get();   
//$cont_data = loadContracttype::all();
        return $cont_data;
    }
    // ->where(function($q) {
    //     $q->where('knowledge_base.kb_status', '0')
    //       ->orWhere('knowledge_base.kb_status', '2');
    // })
    public function available_kblist(Request $request){
        $cont_data = DB::table('knowledge_base')
        ->where('knowledge_base.company_id',$request->company_id)
        ->where('knowledge_base.kb_status','0')
        ->select("knowledge_base.kb_id","knowledge_base.kb_type","knowledge_base.kb_productcategory","knowledge_base.kb_problem","knowledge_base.kb_contenttype","knowledge_base.kb_solution","knowledge_base.kb_content","support_category.support_category" )
        ->join("support_category", "knowledge_base.kb_productcategory", "=", "support_category.id")
        ->get();  
        return $cont_data;
    }
    
    public function update_status(Request $insert){
        $status="1";
        $update_user=DB::table('knowledge_base')
        ->where('knowledge_base.kb_id', $insert->value)
        ->update([
                'knowledge_base.kb_status'=>$status
        ]);
        if($update_user){
            $json_array=['type'=>'success','message'=>'Knowledge Base Approved Successfully'];
        }
        else{
            $json_array=['type'=>'error','message'=>'Error while Updating !!'];
        }
        return $json_array;
    }

    public function delete_kb(Request $input){
        $userdata = DB::table('knowledge_base')->where('kb_id',$input->value)->delete();
        if($userdata){
            $json_array=['type'=>'success','message'=>'Knowledge Base Deleted!!'];
        }
        else{
            $json_array=['type'=>'error','message'=>'Not Deleted!!'];
        }
        return $json_array;
    }  
    
    public function reject_kb(Request $input){
        $status="2";
        $userdata = DB::table('knowledge_base')->where('kb_id',$input->value)->update([
            'knowledge_base.kb_status'=>$status
        ]);
        if($userdata){
            $json_array=['type'=>'success','message'=>'Knowledge Base Rejected!!'];
        }
        else{
            $json_array=['type'=>'error','message'=>'Not Rejected!!'];
        }
        return $json_array;
    }  

    public function uploadkb(Request $request){
        $json_array;
        $training_id;
        $url;
        $last_id;        
        $last = DB::table('knowledge_base')->orderBy('id', 'DESC')->first();
        if($last==null){
            $last_id = (int)$last+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $training_id = "KB".$last_id;                     
        }else{
            $last_id = (int)$last->id+1;
            $last_id = (string)$last_id;
            $last_id=sprintf("%03s", $last_id);
            $training_id = "KB".$last_id;
        }
        if($request->product){
            $product=$request->product;
        }
        else{
            $product ='16';
        }
        if($request->hasFile('file')){
            $extension=$request->file->getClientOriginalExtension();
            if($request->file('file')->move(public_path("/assets/uploads/knowledge_base/"), $training_id.'.'.$extension)){         
                $training= new Kbmodel();
                $training->kb_id = $training_id;
                $training->kb_type = $request->type;
                $training->kb_productcategory = $product;
                $training->kb_problem = $request->problem;
                $training->kb_solution = $request->solution;
                $training->kb_contenttype = $request->content_type;
                $training->kb_content = 'assets/uploads/knowledge_base/'.$training_id.'.'.$extension;
                $training->company_id = $request->company_id;
                $training->kb_status = 1;
                if($training->save()){
                    $json_array=['type'=>'success','message'=>'Knowledge Base added Successfully!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Knowledge Base not added, Kindly try again!'];
                }
            }else{
                $json_array=['type'=>'error','message'=>'File not Uploaded, Kindly try again!'];
            }
            
        }else{
            $json_array=['type'=>'error','message'=>"File doesn't exists, Kindly try again!"];
        }
        return $json_array;
    }

    public function kb_edit(Request $request){
        if($request->file_checker=="1"){
            if($request->hasFile('file')){
                $result= $request->existing_file;
                if (unlink($result)){
                    $extension=$request->file->getClientOriginalExtension();
                    if($request->file('file')->move(public_path("/assets/uploads/knowledge_base/"), $request->kb_id.'.'.$extension)){
                        $update_user=DB::table('knowledge_base')
                            ->where('kb_id', $request->kb_id)
                            ->update([
                                'kb_type'=>$request->type,
                                'kb_productcategory'=>$request->product, 
                                'kb_problem'=>$request->problem,
                                'kb_solution'=>$request->solution,
                                'kb_contenttype'=>$request->content_type,
                                'kb_content'=>'assets/uploads/knowledge_base/'.$request->kb_id.'.'.$extension
                            ]);  
                        if($update_user){    
                            $json_array=['type'=>'success','message'=>'Knowledge Base Updated Successfully!'];
                        }              
                    }
                    else{
                        $update_user=DB::table('knowledge_base')
                        ->where('kb_id', $request->kb_id)
                        ->update([
                            'kb_type'=>$request->type,
                            'kb_productcategory'=>$request->product, 
                            'kb_problem'=>$request->problem,
                            'kb_solution'=>$request->solution,
                            'kb_contenttype'=>$request->content_type
                        ]);
                        if($update_user){    
                            $json_array=['type'=>'success','message'=>'Knowledge Base Updated, Content File not replaced!!'];
                        }
                        else{
                            $json_array=['type'=>'error','message'=>'No Changes made!!'];   
                        }

                    }                
                }
            }
        }
        
        if($request->file_checker=="0"){
            $update_user=DB::table('knowledge_base')
            ->where('kb_id', $request->kb_id)
            ->update([
                    'kb_type'=>$request->type,
                    'kb_productcategory'=>$request->product, 
                    'kb_problem'=>$request->problem,
                    'kb_solution'=>$request->solution,
                    'kb_contenttype'=>$request->content_type
                ]);
            if($update_user){
                $json_array=['type'=>'success','message'=>'Knowledge Base Updated!!'];
            }
            else{
                $json_array=['type'=>'error','message'=>'No Changes made!!'];
            }               
        }  
        return $json_array;      
    }
}
