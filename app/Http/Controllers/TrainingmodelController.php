<?php

namespace App\Http\Controllers;

use App\Trainingmodel;
use Illuminate\Http\Request;
class TrainingmodelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trainingmodel  $trainingmodel
     * @return \Illuminate\Http\Response
     */
    public function show(Trainingmodel $trainingmodel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trainingmodel  $trainingmodel
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainingmodel $trainingmodel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trainingmodel  $trainingmodel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainingmodel $trainingmodel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trainingmodel  $trainingmodel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainingmodel $trainingmodel)
    {
        //
    }
}
