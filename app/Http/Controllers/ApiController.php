<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Mail;
use App\User;
use App\User_model;
use App\Ticketmodel;
use App\Attendancemodel;
use App\SubCategorymodel;
use App\Customerinsertmodel;

class ApiController extends Controller
{
    public function login_API(request $request)
    {
            $username = $request->username;
            $password = $request->password;
            $json="";
            $json_array="";
            $user_array=array();
            $original_array=array();
          //$hashed_password = Hash::make($password);
           //$encrypted = Crypt::encryptString($password);
           
            $where_mail=[
                'email'=>$request->username,
            ];
            $where_usermail=[
                'email_id'=>$request->username,
            ];
            $credintials= [
                'email'=>$username,
                'password'=>$password,
            ];
            
                $customer = new User(); 
                $check_mailexists=$this->check_nameexists($where_mail);
                if($check_mailexists){ 
                   /*$user_ext = DB::table('users')
                    ->where($where_mail)
                    ->select('*')
                    ->get(); */
                    $info = DB::table('add_users')
                    ->where($where_usermail)
                    ->select('*')
                    ->get(); 
                    
                   if(!Auth::attempt(['email' => $username, 'password' => $password])){       
                        $json_array = array(
                            "status" => "0",
                            "msg" => "Username and Password does not match!!!",
                            "data" => ""
                        );
                    }
                    else{
                        //array_push($user_array,AUTH::user());
                        $searchString = ',';
                        
                         if( strpos($info[0]->support, $searchString) !== false ) {
                            $string_version="";
                            $arr = explode(',', $info[0]->support);        
                            for($i=0;$i<count($arr);$i++){
                                $sup_cat = DB::table('support_category')
                                ->where('support_category.id',$arr[$i])
                                ->select('*')
                                ->get();
                                $original_array[].=$sup_cat[0]->support_category;
                            }
                                $string_version = implode(',', $original_array);   
                         }
                        
                        else {
                            $string_version="";
                           $sup_cat = DB::table('support_category')
                           ->where('support_category.id',$info[0]->support)
                           ->select('*')
                           ->get();
                           $string_version = $sup_cat[0]->support_category;
                        }
                        //echo $string_version;
                       $user_array=AUTH::user()->toArray();
                       $array2=array(   
                                    "employee_id"=>$info[0]->employee_id,
                                    "user_id"=>$info[0]->user_id,
                                    "contact_number"=>$info[0]->contact_number,
                                    "address"=>$info[0]->address,
                                    "region"=>$info[0]->region,
                                    "area"=>$info[0]->area,
                                    "location"=>$info[0]->location,
                                    "Support-Category"=>$string_version

                                );
                       $merged = array_merge($user_array, $array2);
                        $json_array = array(
                            "status" => "1",
                            "msg" =>"Login Successful",
                            "data" => $merged
                        );
                    }
                }
                else{
                    $json_array = array(
                        "status" => "0",
                        "msg" => "Invalid User !!!",
                        "data" => ""
                    );
                }
        // echo json_encode($json);
        return json_encode($json_array);
    }

    public function change_pwd(request $request){
        $username = $request->username;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $where_mail=[
            'email'=>$request->username,
        ];
        $check_mailexists=$this->check_nameexists($where_mail);
        if($check_mailexists){ 

            if(User::where('email', 'LIKE', $username)->where('temp_pwd', $old_password)->count() > 0) {                
                $hashed_random_password = Hash::make($new_password); 
                
                    $update_user=DB::table('users')
                        ->where('email',$request->username)
                        ->update([
                            'temp_pwd'=>$new_password,
                            'password'=>$hashed_random_password, 
                        ]);                    
                    
                    if ($update_user) {
                        $json = array(
                            "status"=>"1",
                            "msg"=>"Password reset Successfully!! ",
                            "data"=>""
                        );                    
                    }
                    else{
                        $json = array(
                            "status"=>"0",
                            "msg"=>'Password not Updated!',
                            "data"=>""
                        );
                    }            
            }
            else{
                $json = array(
                    "status" => "0",
                    "msg" => "Current Password doesn't match!!!",
                    "data" => ""
                );  
            }
          
        }
        else{
            $json = array(
                "status" => "0",
                "msg" => "Invalid User!!!",
                "data" => ""
            );
        }
        return json_encode($json);
    }

    public function forgot_password(request $request){
        $email = $request->mail_id;
        $where_mail=[
            'email'=>$request->mail_id,
        ];
        $json="";
        $check_mailexists=$this->check_nameexists($where_mail);
            if($check_mailexists){ 
                $new_pwd = str_random(8);
                $hashed_random_password = Hash::make($new_pwd); 

                $update_user=DB::table('users')
                ->where('email',$request->mail_id)
                ->update([
                    'temp_pwd'=>$new_pwd,
                    'password'=>$hashed_random_password, 
                ]);
                //$path='localhost:8080/FMS_updated/FMS/public/';
                $data = array( 'email' => $email);
                Mail::send('pwdmail',
                    array(
                        'email' => $request->mail_id,
                        'password' => $new_pwd,

                    ), function($message) use ($data)
                    {
                        $message->from('kaspondevelopers@gmail.com');
                        $message->to($data['email'], 'Admin')->subject('Credintials for FMS');
                    }
                );
                if (Mail::failures()) {
                    $json = array(
                        "status"=>"0",
                        "msg"=>'Mail not sent to the user, The password for this user is '.$string,
                        "data"=>""
                    );                    
                }else{
                    $json = array(
                        "status"=>"1",
                        "msg"=>'New Password sent to your mail',
                        "data"=>""
                    );
                }
            }
            else{
                $json = array(
                    "status" => "0",
                    "msg" => "Invalid User!!!",
                    "data" => ""
                );
            }
        return json_encode($json);
    }

    public function update_punchstatus(request $request){
        $emp_id = $request->employee_id;
        $punch_type = $request->punch_type;
        $json="";
       // $check_emp = $this->check_empexists($emp_id);

        if (User_model::where('employee_id', '=', $emp_id)->exists()) {
            $today = Carbon::now('Asia/Kolkata');
            $pieces = explode(" ", $today);
            $new_date = $pieces[0];
            $in_time = $pieces[1];
          
            if($punch_type=="punch_in"){
                if(Attendancemodel::where('employee_id', 'LIKE', $emp_id)->where('attendance_date', $new_date)->count() > 0) {                
                        $json = array(
                            "status" => "0",
                            "msg" => "Already Punch-In done!!!",
                            "data" => ""
                        );                
                }
                else{
                    $user_i= new Attendancemodel();                  
                    $user_i->employee_id= $emp_id;
                    $user_i->in_time= $in_time;
                    $user_i->attendance_date= $new_date;
                    $user_i->created_at= $today;
                    $user_i->updated_at= $today;
                    $user_i->save();
                    if($user_i->save()){
                        $json = array(
                            "status" => "1",
                            "msg" => "Punch-In done!!!",
                            "data" => ""
                        );  
                    }
                    
                }
            }
            else if($punch_type=="punch_out"){
                $out_date = Carbon::now('Asia/Kolkata');
                $whole = explode(" ", $out_date);
                //$new_date = $whole[0];
                $out_time = $whole[1];
                if(Attendancemodel::where('employee_id', 'LIKE', $emp_id)->where('attendance_date', $new_date)->whereNotNull('in_time')->count() > 0) {    
                    $update_user=DB::table('attendance')
                                ->update(['out_time'=>$out_time,
                                          'updated_at'=>$out_date,
                    ]);            
                    $json = array(
                        "status" => "1",
                        "msg" => "Punch-Out done!!!",
                        "data" => ""
                    );                
                }
                else{
                    $json = array(
                        "status" => "0",
                        "msg" => "Punch-In not done to Punch out!!!",
                        "data" => ""
                    );  
                }
    
            }
            else{
                    $json = array(
                        "status" => "0",
                        "msg" => "Check Punch type!!!",
                        "data" => ""
                    );
            }
        }
        else{
            $json = array(
                "status" => "0",
                "msg" => "Invalid Emp id!!!",
                "data" => ""
            ); 
        }
        
        return json_encode($json);
    }

    public function get_tickets(request $request){
        $emp_id = $request->emp_id;
        $info = DB::table('add_users')
        ->where('employee_id',$emp_id)
        ->select('*')
        ->get(); 
       // $string_version = implode(',', $info[0]->support);
        /*if(is_array( $info[0]->support)){
            $string_version = implode(',', $info[0]->support);
        }
        else 
        {
            $string_version = $info[0]->support;
        }*/
        $searchString = ',';
        
         if( strpos($info[0]->support, $searchString) !== false ) {
            $myArray = explode(',', $info[0]->support);
         }
         else if(strpos($info[0]->support, $searchString) == false){
             $myArray = $info[0]->support;
         }
         return $myArray;
    }

    public function cust_login(request $request){
        $username = $request->username;
        $password = md5($request->password);
        $json="";
        $json_array="";
        $user_array=array();
      //$hashed_password = Hash::make($password);
        if (Customerinsertmodel::where('customer_email', '=', $username)->exists()) {
            
            if (Customerinsertmodel::where('customer_email', '=', $username)->where('enc_password', '=', $password)->exists()) {
                $where_usermail= [
                    'customer_email'=>$username,
                    'enc_password'=>$password
                ];
                $info = DB::table('customer')
                ->where($where_usermail)
                ->select('*')
                ->get(); 

                $json_array = array(
                    "status" => "1",
                    "msg" =>"Login Successful",
                    "data" => $info
                );
            }
            else{
                $json_array = array(
                    "status" => "0",
                    "msg" => "Username and Password does not match!!!",
                    "data" => ""
                );
            }       
        }
        else{
            $json_array = array(
                "status" => "0",
                "msg" => "Invalid User !!!",
                "data" => ""
            );
        }            
    // echo json_encode($json);
    return json_encode($json_array);
    }

    public function load_category(request $insert){
        $userss = DB::table('support_category')
        ->where('company_id',$insert->company_id)
        ->select('*')
        ->get();
        if($userss){
            $json_array = array(
                "status" => "1",
                "msg" => "Support Categories !!!",
                "data" => $userss
            );
        }
        else{
            $json_array = array(
                "status" => "0",
                "msg" => "No Support Category !!!",
                "data" => ""
            );
        }
        return json_encode($json_array);
    }
    public function load_subcategory(request $request){
        if (SubCategorymodel::where('company_id', '=', $request->company_id)->where('category', '=', $request->category)->exists()) {
            $where_usermail= [
                'company_id'=>$request->company_id,
                'category'=>$request->category
            ];
            $info = DB::table('tickets_subcategory')
            ->where($where_usermail)
            ->select('subcat_id','sub_category')
            ->get(); 

            $json_array = array(
                "status" => "1",
                "msg" =>"sub categories !!",
                "data" => $info
            );
        }
        else{
            $json_array = array(
                "status" => "0",
                "msg" => "No Sub-Category!!!",
                "data" => ""
            );
        }  
        return json_encode($json_array);     
    }

    public function cust_raiseticket(request $insert){
        $ticket_id = DB::table('tickets_list') 
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$ticket_id==""){                        
            $datass = intval(preg_replace('/[^0-9]+/', '', $ticket_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $tick_id = "TK_" . $datass;
        }
        else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $ticket_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $tick_id = "TK_" . $datass;
        }; 
        $user_i= new Ticketmodel();   
        $date_folder = Carbon::now()->format('d_m_Yh:i:s');

        if($insert->hasFile('ticket_image')){ 
            $proof_images = $insert->file('ticket_image');
            $destinationPath = 'img/ticket_images/';
            $extension=$proof_images->getClientOriginalExtension();
            $original=  str_replace(":","_",$tick_id.'_'.$date_folder);
            $string_version = $original.'.'.$extension;
         $image=$destinationPath.''.$original.'.'.$extension;
          
           $proof_images->move($destinationPath, $original.'.'.$proof_images->getClientOriginalExtension());
        }else{
            $image="";
            $proof_images = "";
        }    
            $user_i->ticket_id=$tick_id;     
            $user_i->customer_id=$insert->customer_id;
            $user_i->contact_number=$insert->contactnumber;
            $user_i->email_id=$insert->emailid;
            $user_i->support_cat=$insert->support_cat;
            $user_i->support_subcategory=$insert->support_subcategory;
            $user_i->problem=$insert->problem;
            $user_i->scheduled_arrival=$insert->visit_time; 
            $user_i->company_id=$insert->company_id;
            $user_i->ticket_type=$insert->ticket_type;
            $user_i->images=$image;
            $user_i->status= 0;
            $user_i->save();
            if($user_i->save()){  

                $json_array = array(
                    "status" => "1",
                    "msg" =>"Your Issue has been Raised!!!",
                    "data" => ""
                );
            }
            else{
                $json_array = array(
                    "status" => "0",
                    "msg" =>"Issue not Saved!!!",
                    "data" => ""
                );
            }
            return json_encode($json_array);     
    }

    public function cust_forgotpwd(request $request){
        $email = $request->mail_id;
        $where_mail=[
            'customer_email'=>$request->mail_id,
        ];
        $json="";
        if (Customerinsertmodel::where('customer_email', '=', $email)->exists()) {
                $new_pwd = str_random(8);
                $hashed_random_password = md5($new_pwd); 

                $update_user=DB::table('customer')
                ->where('customer_email',$request->mail_id)
                ->update([
                    'password'=>$new_pwd,
                    'enc_password'=>$hashed_random_password, 
                ]);
                //$path='localhost:8080/FMS_updated/FMS/public/';
                $data = array( 'email' => $email);
                Mail::send('pwdmail',
                    array(
                        'email' => $request->mail_id,
                        'password' => $new_pwd,

                    ), function($message) use ($data)
                    {
                        $message->from('kaspondevelopers@gmail.com');
                        $message->to($data['email'], 'Admin')->subject('Credintials for FMS');
                    }
                );
                if (Mail::failures()) {
                    $json = array(
                        "status"=>"0",
                        "msg"=>'Mail not sent to the user, The password for this user is '.$string,
                        "data"=>""
                    );                    
                }else{
                    $json = array(
                        "status"=>"1",
                        "msg"=>'New Password sent to your mail',
                        "data"=>""
                    );
                }
            }
            else{
                $json = array(
                    "status" => "0",
                    "msg" => "Invalid User!!!",
                    "data" => ""
                );
            }
        echo json_encode($json);
    }

    public function customerchange_pwd(request $request){
        $username = $request->username;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $where_mail=[
            'customer_email'=>$request->username,
        ];
        
        if(Customerinsertmodel::where('customer_email', 'LIKE', $username)->count() > 0){ 
            if(Customerinsertmodel::where('customer_email', 'LIKE', $username)->where('password', $old_password)->count() > 0) {                
                $hashed_random_password = md5($new_password); 
                
                    $update_user=DB::table('customer')
                        ->where('customer_email',$request->username)
                        ->update([
                            'password'=>$new_password,
                            'enc_password'=>$hashed_random_password, 
                        ]);                    
                    
                    if ($update_user) {
                        $json = array(
                            "status"=>"1",
                            "msg"=>"Password reset Successfully!! ",
                            "data"=>""
                        );                    
                    }
                    else{
                        $json = array(
                            "status"=>"0",
                            "msg"=>'Password not Updated!',
                            "data"=>""
                        );
                    }            
            }
            else{
                $json = array(
                    "status" => "0",
                    "msg" => "Current Password doesn't match!!!",
                    "data" => ""
                );  
            }          
        }
        else{
            $json = array(
                "status" => "0",
                "msg" => "Invalid User!!!",
                "data" => ""
            );
        }
        return json_encode($json);
    }

    public function check_nameexists($where){
        $customer = User::where($where)->count() > 0;
        return $customer;
    }

    public function check_empexists($emp){
        $mail_id = User_model::where($emp)->count();
        return $mail_id;
    }
}
