<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Customermodel;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getcustomer(){
        $customer = Customermodel::all();
        return $customer;
    }
    public function create(Request $request)
    {
       /*  Schema::connection('mysql')->create('mytable_welcome', function($table)
        {
            $table->increments('id');
            $table->timestamps();
        });
        die; */
        $where=[
            'customer_type'=>$request->owner_types,
        ];
        if(empty($request->owner_types) || $request->owner_types=="undefined"){
            $json_array=['type'=>'error','message'=>'Owner type is mandatory'];
        }else{
           $customer = new Customermodel();
            $customer->customer_type=$request->owner_types;        
            $check_nameexists=$this->check_nameexists($where);
            if($check_nameexists){            
                $json_array=['type'=>'error','message'=>'Owner type is already exists!'];
            }else{
                if($customer->save()){
                    $json_array=['type'=>'success','message'=>'Owner type is Successfully inserted!'];
                }else{
                    $json_array=['type'=>'error','message'=>'Owner type is not inserted! Kindly try again'];
                }
            }
        }        
        return $json_array;
    }
    public function updateownertype(Request $request){
        $where=[
            ['customer_type', $request->customer_type],
            ['id', '<>', $request->id],
        ];
        if(empty($request->customer_type) || $request->customer_type=="undefined"){
            $json_array=['type'=>'error','message'=>'Owner type is mandatory'];
        }else{                   
            $check_nameexists=$this->check_nameexists($where);            
            if($check_nameexists){            
                $json_array=['type'=>'error','message'=>'Owner type is already exists!'];
            }else{
                $update_owner=DB::table('customertype')
                    ->where('id', $request->id)
                    ->update(['customer_type'=>$request->customer_type,
                            ]);
                if($update_owner){
                    $json_array=['type'=>'success','message'=>'Owner type is Successfully Updated!'];
                }else{
                    $json_array=['type'=>'error','message'=>'There is no changes happend!'];
                }
            }
        }        
        return $json_array;
    }
    public function deleteownertype(Request $request)
    {
        $id=$request->id;
        $deleteownertype = DB::table('customertype')->where('id',$id)->delete();
        if($deleteownertype){
            $json_array=['type'=>'success','message'=>'Owner Type is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Owner Type is not Deleted, Kindly try again!'];
        }
        return $json_array;    
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function check_nameexists($where){
        $customer = Customermodel::where($where)->count() > 0;
        return $customer;
    }
}
