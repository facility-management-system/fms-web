<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Custmodel;
use App\Ticketmodel;

class TicketController extends Controller
{
    public function loadTickets()
    {
        $company_id=Auth::user()->company_id;
        $tickets = new Ticketmodel();
        $where=[
            'company_id'=>$company_id,
        ];
        // $tickets_check = Ticketmodel::where($where)->get();
        $tickets_check = DB::table('tickets_list')
        ->where('tickets_list.company_id',$company_id)
        ->join('support_category', 'tickets_list.support_cat', '=', 'support_category.id')
        ->select('tickets_list.id', 'tickets_list.ticket_id', 'tickets_list.customer_id', 'tickets_list.contact_number', 'tickets_list.scheduled_arrival', 'tickets_list.problem', 'support_category.support_category', 'tickets_list.status')
        ->get();

        return $tickets_check;
    }
    
    public function tickets_Supporttype(Request $request){
        //$support_type = Supportmodel::all();
		$support_type =	DB::table('support_category')
                    ->where('support_category.company_id',$request->company_id)
                    ->select("*" )
                    ->get(); 
        return $support_type;
    }

    public function retrieve_cust(Request $request){
        $where=[
            'contact_number'=>$request->input,
        ];
        if(empty($request->input) || $request->value=="undefined"){
            $json_array=['type'=>'error','message'=>'Fill Contact Number'];
        }
        else{
           $customer = new Custmodel();
            $customer->contact_number=$request->input;        
            $check_nameexists=$this->contact_chk($where);
            if($check_nameexists){  
                
                $retrieve_data = Custmodel::where($where)->get();
                //echo $retrieve_data;
                //die;
                    $json_array=['type'=>'success','message'=>$retrieve_data];
            }
            else{
                $json_array=['type'=>'warning','message'=>'Customer Information not available'];
            }
        }        
        return $json_array;
    }

    public function getcust_info(Request $request)
    {
        $where=[
            'customer_id'=>$request->input,
        ];
        $retrieve_data = Custmodel::where($where)->get();
        return $retrieve_data;
    }

    public function contact_chk($where){
        $contact_chk = Custmodel::where($where)->count() > 0;
        return $contact_chk;
    }
    public function save_newticket(Request $request){
        //print_r($request->input);
        //echo $request->input['customer_id'];
        $company_id=Auth::user()->company_id;
        $ticket_id = DB::table('tickets_list') 
            ->orderBy("id", "desc")
            ->select('id')
            ->limit(1)
            ->get();
        if(!$ticket_id==""){                        
            $datass = intval(preg_replace('/[^0-9]+/', '', $ticket_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $user_ids = "Ticket"."_" . $datass;
        }else{
            $datass = intval(preg_replace('/[^0-9]+/', '', $ticket_id), 10);
            $datass = $datass + 1;
            $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
            $user_ids = "Ticket"."_" . $datass;
        }; 
    
        $new_ticket= new Ticketmodel();
            $new_ticket->ticket_id=$user_ids;
            $new_ticket->customer_id=$request->input['customer_id'];
            $new_ticket->contact_number=$request->input['contact_number'];
            $new_ticket->email_id=$request->input['email_id'];   
            $new_ticket->problem=$request->input['problem']; 
            $new_ticket->support_cat=$request->input['support_typ']; 
            $new_ticket->scheduled_arrival=$request->input['tic_date']; 
            $new_ticket->ticket_type=$request->input['ticket_type']; 
            $new_ticket->status=0;
            $new_ticket->company_id=$company_id;
            if($new_ticket->save()){
                $json_array=['type'=>'success','message'=>'Ticket details is Successfully inserted!'];
            }else{
                $json_array=['type'=>'error','message'=>'Ticket details not inserted! Kindly try again'];
            }
            return $json_array;
    }

    public function upload_ticketexcel(request $request){
        $json_array=array();
        if ($request->hasFile('bulk_file')){
            $path = $request->file('bulk_file')->getRealPath();
            $ext = $request->file('bulk_file')->getClientOriginalExtension();
        if($ext=="xlsx" || $ext=="xls"){
                //$data = Excel::load($path, function($reader) {})->get();
                $data = Excel::load($path, function($reader) {
                    $objExcel = $reader->getExcel();
                    $sheet = $objExcel->getSheet(0);
                    // $highestColumn = $sheet->getHighestColumn(); 
                        $GLOBALS['$highestColumn']  = $sheet->getHighestDataColumn(); 
                    })->get();
                       // echo $GLOBALS['$highestColumn'];

                    if($GLOBALS['$highestColumn']=="F"){
                        if (!empty($data) && $data->count()>0)  {
                            foreach ($data as $key => $value) {
                                // $where=[
                                //     'contact_number'=>$value->contact_number,
                                // ];
                                // $where_m=[
                                //     'email_id'=>$value->email_id,
                                // ];
                                $company_id=Auth::user()->company_id;
                                $ticket_id = DB::table('tickets_list') 
                                    ->orderBy("id", "desc")
                                    ->select('id')
                                    ->limit(1)
                                    ->get();
                                if(!$ticket_id==""){                        
                                    $datass = intval(preg_replace('/[^0-9]+/', '', $ticket_id), 10);
                                    $datass = $datass + 1;
                                    $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                    $user_ids = "Ticket"."_" . $datass;
                                }else{
                                    $datass = intval(preg_replace('/[^0-9]+/', '', $ticket_id), 10);
                                    $datass = $datass + 1;
                                    $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                    $user_ids = "Ticket"."_" . $datass;
                                }; 
                            
                                $new_ticket= new Ticketmodel();
                                    $new_ticket->ticket_id=$user_ids;
                                    $new_ticket->customer_id=$value->customer_id;
                                    $new_ticket->contact_number=$vslue->contact_number;
                                    $new_ticket->email_id=$value->email_id;   
                                    $new_ticket->problem=$value->problem; 
                                    $new_ticket->support_cat=$value->support_cat; 
                                    $new_ticket->scheduled_arrival=$value->scheduled_arrival; 
                                    $new_ticket->status=0;
                                    $new_ticket->company_id=$company_id;
                                    if($new_ticket->save()){
                                        array_push($json_array,array('type'=>'success','message'=>'Ticket details is Successfully inserted!')); 
                                       // $json_array=['type'=>'success','message'=>'Ticket details is Successfully inserted!'];
                                    }
                                    // else{
                                    //     $json_array=['type'=>'error','message'=>'Ticket details not inserted! Kindly try again'];
                                    // }
                            }
                        }
                        else{
                            array_push($json_array,array('type'=>'warning','message'=>'Empty excel file')); 
                        }
                    }
                    else{
                        array_push($json_array,array('type'=>'error','message'=>'Unmatched file format!,View Sample template')); 
                    }
            }
            else{
                array_push($json_array,array('type'=>'error','message'=>'Upload an Excel file!')); 
            }    
        }
        else{
            array_push($json_array,array('type'=>'warning','message'=>'File not included!')); 
        }
        return $json_array; 
    }
}
