<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\SpareModel;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;
use Illuminate\Support\Facades\Storage;

class SpareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function get_spare(Request $spare)    
    {
        $customer = SpareModel::where('company_id', '=', $spare->company_id)->get();
        return $customer;
    }    
    public function insertspare(Request $spare){
        $spare_id;
        $json_array;        
        $result = DB::table('support_category') 
        ->where("id", $spare->sparecategory)
        ->select('support_category')
        ->get();
        $category = $result[0]->support_category;
        /* Insert New Spare */
        if( ($spare->id == "undefined" || $spare->id == null ) && ($spare->spare_id == "undefined" || $spare->spare_id == null ) && ($spare->spareimage == "undefined" || $spare->spareimage == null )  ){ 
            $last = DB::table('spare')->orderBy('id', 'DESC')->first();
            if($last==null){
                $last_id = (int)$last+1;
                $last_id = (string)$last_id;
                $last_id=sprintf("%03s", $last_id);
                $spare_id = "Spare_".$last_id;                     
            }else{
                $last_id = (int)$last->id+1;
                $last_id = (string)$last_id;
                $last_id=sprintf("%03s", $last_id);
                $spare_id = "Spare_".$last_id;
            };            
            $sparemodel= new SpareModel();            
            $sparemodel->spare_id = $spare_id;
            $sparemodel->spare_code = $spare->sparecode;
            $sparemodel->spare_desc = $spare->sparedesc;
            $sparemodel->spare_name = $spare->sparename;
            $sparemodel->spare_modal = $spare->sparemodal;
            $sparemodel->spare_location = $spare->sparelocation;        
            $sparemodel->price = $spare->spareprice;
            $sparemodel->quantity_purchased = $spare->sparequantity;        
            $sparemodel->cat_id = $spare->sparecategory;
            $sparemodel->category = $category;
            $sparemodel->p_date = $spare->sparepurchase;
            $sparemodel->expiry_date = $spare->spareexpairy;
            $sparemodel->company_id = $spare->company_id;

            if($spare->hasFile('sparefile')){
                $extension=$spare->sparefile->getClientOriginalExtension();
                //$url = public_path("assets/uploads/spare/");
                if($spare->file('sparefile')->move(public_path("/assets/uploads/spare/"), $spare_id.'.'.$extension)){
                    $sparemodel->image = "assets/uploads/spare/".$spare_id.'.'.$extension;
                    if($sparemodel->save()){
                        $json_array=['type'=>'success','message'=>'Spare details Inserted !'];
                    }else{
                        $json_array=['type'=>'error','message'=>'Spare not Added, Kindly try again!'];
                    }
                }else{
                    $json_array=['type'=>'error','message'=>'Spare Image is Created, Kindly try again!'];
                }
            }else{
                $json_array=['type'=>'error','message'=>'Spare Image is Mandatory!'];
            }
        }
        /* Insert New Spare */

        /* Edit Spare */
        else{
         if(($spare->sparecode == "undefined" || $spare->sparecode == null ) || ($spare->sparedesc == "undefined" || $spare->sparedesc == null ) || ($spare->sparemodal == "undefined" || $spare->sparemodal == null )
         || ($spare->sparename == "undefined" || $spare->sparename == null ) || ($spare->sparelocation == "undefined" || $spare->sparelocation == null ) || ($spare->spareprice == "undefined" || $spare->spareprice == null ) ||
         ($spare->sparequantity == "undefined" || $spare->sparequantity == null ) || ($spare->sparecategory == "undefined" || $spare->sparecategory == null ) || ($spare->sparepurchase == "undefined" || $spare->sparepurchase == null )) 
            {
             
                $json_array=['type'=>'error','message'=>'All Fields are Mandatory!'];
            }
            else{
                 /* Edit with New spare Image */
                    if($spare->hasFile('sparefile')){
                        $extension=$spare->sparefile->getClientOriginalExtension();
                        if($spare->file('sparefile')->move(public_path("/assets/uploads/spare/"), $spare->spare_id.'.'.$extension)){
                            $sparemodel=DB::table('spare')
                                            ->where('id', '=', $spare->id)
                                            ->update([ 'spare_code'=>$spare->sparecode,
                                                    'spare_desc'=>$spare->sparedesc, 
                                                    'spare_name'=>$spare->sparename, 
                                                    'spare_modal'=>$spare->sparemodal, 
                                                    'spare_location'=>$spare->sparelocation,
                                                    'price'=>$spare->spareprice,
                                                    'quantity_purchased'=>$spare->sparequantity,
                                                    'cat_id'=>$spare->sparecategory,
                                                    'category'=>$category,
                                                    'p_date'=>$spare->sparepurchase,
                                                    'expiry_date'=>$spare->spareexpairy,
                                                    'image' => "assets/uploads/spare/".$spare->spare_id.'.'.$extension
                                                    ]);
                            $json_array=['type'=>'success','message'=>'Spare Deatils Updated!'];
                        }else{
                            $json_array=['type'=>'error','message'=>'Spare Image is not updated, Kindly try again!'];
                        }
                    }
                    /* Edit with New spare Image */
                    /* Edit without Image */
                    else{
                        $sparemodel=DB::table('spare')
                                            ->where('id', '=', $spare->id)
                                            ->update([ 'spare_code'=>$spare->sparecode,
                                                        'spare_desc'=>$spare->sparedesc, 
                                                        'spare_name'=>$spare->sparename, 
                                                        'spare_modal'=>$spare->sparemodal,
                                                        'spare_location'=>$spare->sparelocation,
                                                        'price'=>$spare->spareprice,
                                                        'quantity_purchased'=>$spare->sparequantity,
                                                        'cat_id'=>$spare->sparecategory,
                                                        'category'=>$category,
                                                        'p_date'=>$spare->sparepurchase,
                                                        'expiry_date'=>$spare->spareexpairy,
                                                        'image' => $spare->spareimage
                                                    ]);
                        if($sparemodel){
                            $json_array=['type'=>'success','message'=>'Spare Deatils Updated!'];
                        }else{
                            $json_array=['type'=>'warning','message'=>'No Changes Made!'];
                        }
                    }
                /* Edit without Image */
            }
           
        }
        return $json_array;
    }
    public function deletespare(request $spare){
        $id=$spare->id;
        $deletespare = DB::table('spare')->where('id',$id)->delete();
        if($deletespare){
            $json_array=['type'=>'success','message'=>'Spare is Deleted Successfully!'];
        }else{
            $json_array=['type'=>'error','message'=>'Spare is not Deleted, Kindly try again!'];
        }
        return $json_array; 
    }    
}
