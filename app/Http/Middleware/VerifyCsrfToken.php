<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    
    
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    
    protected $except = [
        '/forgot_password',
        '/update_punchstatus',
        '/login_API',
        '/get_tickets',
        '/change_pwd',
        '/cust_login',
        '/load_category',
        '/load_subcategory',
        '/cust_raiseticket',
        '/cust_forgotpwd',
        '/customerchange_pwd'
    ];    
}
