<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Supportmodel extends Model
{
    //
    protected $table = 'support_category';
    
    protected $fillable = [
        'id', 'support_category', 'updated_at','created_at'
    ];
}
