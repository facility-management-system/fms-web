<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategorymodel extends Model
{
    protected $table = 'tickets_subcategory';
}
