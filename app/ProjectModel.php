<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectModel extends Model
{
    protected $table = 'project_creation';
    
    protected $fillable = [
        'id', 'project_name', 'project_location', 'project_manager', 'project_wbs', 'updated_at','created_at'
    ];
}
