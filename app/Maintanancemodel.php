<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Maintanancemodel extends Model
{
    protected $table = 'maintanance_type';

    protected $fillable = [
        'id', 'maintainance', 'cont_cat', 'contract_category.cat_name', 'company_id', 'updated_at'
    ];
    public function cont_r_ship(){
     $user= DB::table('maintanance_type')
        ->join('contract_category', 'maintanance_type.cont_cat', '=', 'contract_category.id')
        ->select('maintanance_type.*', 'contract_category.cat_name as name')
        ->get();
    }
}
