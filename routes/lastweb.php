<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::post('/auth','UserController@checkAuth');
Route::post('/insertcompany','Companycontroller@insertcompany');
Route::post('/insert_adminuser','Companycontroller@insert_adminuser');
Route::GET('/getallusers','Companycontroller@getallusers');



Route::POST('/insertownertype','CustomerController@create');
Route::POST('/updateownertype', 'CustomerController@updateownertype');
Route::POST('/deleteownertype', 'CustomerController@deleteownertype');
Route::POST('/getcustomer', 'CustomerController@getcustomer');
Route::POST('/getproduct', 'productController@getproduct');
Route::POST('/getsubcategory', 'productController@getsubcategory');
Route::POST('/productid_check', 'productController@productid_check');
Route::POST('/get_product', 'productController@get_product');
Route::POST('/delete_product', 'productController@delete_product');
Route::POST('/subcatid_check', 'productController@subcatid_check');
Route::POST('/get_subcategory', 'productController@get_subcategory');
Route::POST('/getapilogin','UserController@getapilogin');

//User Mgmt module
Route::resource('/user','UserController');
Route::POST('/getusers', 'UserController@getusers');
Route::GET('/loadWorktype', 'UserController@loadWorktype');
Route::POST('/insertuser','UserController@create');
Route::POST('/edit_users','UserController@edit_users');
Route::POST('/upload_userexcel','UserController@upload_userexcel');
 
// Contract full page
Route::POST('/loadConttype', 'ContractController@loadConttype');
Route::POST('/loadSupporttype', 'ContractController@loadSupporttype');
Route::POST('/loadMaintaintype', 'ContractController@loadMaintaintype');
Route::POST('/loadContracttype', 'ContractController@loadContracttype');

Route::POST('/insertcattype','ContractController@create');
Route::POST('/insertsup_category','ContractController@create_support');
Route::POST('/insert_maintaintype','ContractController@create_maintanance');
Route::POST('/insert_conttype','ContractController@create_conttype');

Route::POST('update_contract','ContractController@update_contract');
Route::POST('update_support','ContractController@update_support');
Route::POST('update_contracttype','ContractController@update_contracttype');
Route::POST('update_maintaintype','ContractController@update_maintaintype');

Route::POST('/delete_contract','ContractController@delete_contract');
Route::POST('/delete_conttype','ContractController@delete_conttype');
Route::POST('/delete_mtype','ContractController@delete_mtype');
Route::POST('/delete_suptype','ContractController@delete_suptype');

Route::get('downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');

//Bulk Upload
Route::post('import_contract', 'ContractController@import_contract');
Route::post('upload_excel', 'ContractController@upload_excel');
Route::post('upload_maintype', 'ContractController@upload_maintype');
Route::post('upload_supexcel', 'ContractController@upload_supexcel');

// Raise Ticket

Route::GET('/loadTickets','TicketController@loadTickets');
Route::POST('/retrieve_cust','TicketController@retrieve_cust');
Route::POST('/getcust_info','TicketController@getcust_info');
Route::POST('/save_newticket','TicketController@save_newticket');
Route::POST('/upload_ticketexcel','TicketController@upload_ticketexcel');


//Nantha
Route::GET('/getproject','ProjectController@index');
Route::POST('/insertproject','ProjectController@create');
Route::resource('/uploadtree','jstreeexcelController');
Route::POST('/insertcus','CustomerController@create_customer');
Route::POST('/get_projects_id','ProjectController@get_projects_id');
Route::POST('/get_customer','CustomerController@get_customer');

//Training Management
Route::POST('/gettraining','TrainingController@get_training');
Route::POST('/get_supportmodel','TrainingController@get_supportmodel');
Route::POST('/getassessment','TrainingController@getassessment');
Route::POST('/uploadtraining','TrainingController@uploadtraining');
Route::POST('/edittraining','TrainingController@edittraining');
Route::POST('/deletetraining','TrainingController@deletetraining');
Route::POST('/uploadassessment','TrainingController@uploadassessment');
Route::POST('/editassessment','TrainingController@editassessment');
Route::POST('/deleteassessment','TrainingController@deleteassessment');

//Mail Sending
Route::GET('/send','mailcontroller@send');

//Edit Training with Quiz
Route::GET('/get_quizid','TrainingController@get_quizid ');

//Spare Management
Route::POST('/getspare','SpareController@get_spare');
Route::POST('/insertspare','SpareController@insertspare');
Route::POST('/deletespare','SpareController@deletespare');

//Knowledge base
Route::POST('/uploadkb','KbController@uploadkb');
Route::POST('/get_kblist','KbController@get_kblist');

//M-Social Page
//social posts
Route::POST('/insert_socialpost', 'MsocialController@insert_socialpost');
Route::POST('/insert_eventpost', 'MsocialController@insert_eventpost');
Route::POST('/insert_classipost', 'MsocialController@insert_classipost');

// Route::POST('/getposttypes', 'MsocialController@getposttypes');

//Classified Post
Route::POST('/getproptypes', 'MsocialController@getproptypes');
Route::POST('/getprop_subtypes', 'MsocialController@getprop_subtypes');
Route::POST('/get_bedrooms', 'MsocialController@get_bedrooms');
Route::POST('/get_bathrooms', 'MsocialController@get_bathrooms');
Route::POST('/get_furnishing', 'MsocialController@get_furnishing');
Route::POST('/get_batchelors', 'MsocialController@get_batchelors');
Route::POST('/get_carparking', 'MsocialController@get_carparking');
Route::POST('/get_facing', 'MsocialController@get_facing');
Route::POST('/get_consstatus', 'MsocialController@get_consstatus');
Route::POST('/get_meals', 'MsocialController@get_meals');

Route::POST('/get_furntypes', 'MsocialController@get_furntypes');

Route::POST('/get_mobilecat', 'MsocialController@get_mobilecat');
Route::POST('/getmob_subtypes', 'MsocialController@getmob_subtypes');

Route::POST('/get_cars', 'MsocialController@get_cars');
Route::POST('/car_subtypes', 'MsocialController@car_subtypes');
Route::POST('/car_fuels', 'MsocialController@car_fuels');
Route::POST('/get_carmodel', 'MsocialController@get_carmodel');

Route::POST('/elec_types', 'MsocialController@elec_types');

Route::POST('/pets_types', 'MsocialController@pets_types');

Route::POST('/bike_types', 'MsocialController@bike_types');
Route::POST('/bike_subtypes', 'MsocialController@bike_subtypes');
Route::POST('/bike_models', 'MsocialController@bike_models');




// Technician Mobile API
Route::POST('/login_API','ApiController@login_API');
Route::POST('/change_pwd','ApiController@change_pwd');
Route::POST('/forgot_password','ApiController@forgot_password');
Route::POST('/update_punchstatus','ApiController@update_punchstatus');
Route::POST('/get_tickets','ApiController@get_tickets');

// Customer Login API
Route::POST('/cust_login','ApiController@cust_login');
Route::POST('/load_category','ApiController@load_category');
Route::POST('/load_subcategory','ApiController@load_subcategory');
Route::POST('/cust_raiseticket','ApiController@cust_raiseticket');
Route::POST('/cust_forgotpwd','ApiController@cust_forgotpwd');
Route::POST('/customerchange_pwd','ApiController@customerchange_pwd');