<head>
    <meta charset="utf-8" />
    <title>Facility Management</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css'" /> -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme-default.css') }}" type="text/css" />    
    <!-- END GLOBAL MANDATORY STYLES -->      
    <link rel="stylesheet" href="{{ asset('bower_components/sweetalert2/dist/sweetalert2.min.css') }}" type="text/css" />   
    <link rel="stylesheet" href="{{ asset('bower_components/AngularJS-Toaster/toaster.min.css')}}" />
    <!-- <link rel="stylesheet" href="{{ asset('bower_components/angular_datatables/css/angular-datatables.css')}}" type="text/css"/> -->
    <link rel="stylesheet" href="{{ asset('bower_components/jquery.jstree.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('bower_components/jquery.dataTables.min.css') }}" />  
    <link rel="stylesheet" href="{{ asset('bower_components/angular-material.min.css') }}" type="text/css" /> 
    <link rel="stylesheet" href="{{ asset('bower_components/angularjs-datetime-picker/angularjs-datetime-picker.css')}}" type="text/css"/>    
    <link rel="stylesheet" href="{{ asset('bower_components/AngularJS-Toaster/toaster.min.css')}}" />    
    <link rel="stylesheet" href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.css')}}" />  
    <link rel="stylesheet" href="{{ asset('bower_components/ng-tags-input/ng-tags-input.min.css')}}" /> 
    <link rel="stylesheet" href="{{ asset('bower_components/ng-image-input-with-preview/dist/ng-image-input-with-preview.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/image_slide.css') }}" type="text/css" />    
    
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.css">
</head>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />   
    <!-- BEGIN CORE PLUGINS -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jquery/jquery-ui.min.js') }}"></script>   
    <script type="text/javascript" src="{{asset('assets/js/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>   
    <script type="text/javascript" src="{{asset('assets/js/plugins/jquery-validation/js/additional-methods.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jstree/jstree.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jstree/jstreegrid.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/icheck/icheck.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/fileinput/fileinput.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('assets/js/plugins/dropzone/dropzone.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('assets/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/morris/raphael-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/morris/morris.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/owl/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/alasql/console/alasql.min.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/alasql/console/xlsx.core.min.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pdfobject.min.js')}}" ></script>
    <!-- END CORE PLUGINS -->
    <script>var baseUrl = "{{ url ('/') }}";</script>

   <!-- Angular js -->
    <script type="text/javascript" src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
    

    <script type="text/javascript" src="{{ asset('bower_components/angular-material.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-idle.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('bower_components/angular-animate/angular-animate.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('bower_components/angular-touch.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('bower_components/angular-route/angular-route.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-cookies/angular-cookies.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/ng-tags-input/ng-tags-input.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.directive.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.factory.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.instances.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.options.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.renderer.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular_datatables/angular-datatables.util.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-pdf/dist/angular-pdf.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/ng-image-input-with-preview/dist/ng-image-input-with-preview.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/angular-validate.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/ngSweetAlert/SweetAlert.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/sweetalert2/dist/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-bind-html-compile/angular-bind-html-compile.min.js')}}"></script>    
    <script type="text/javascript" src="{{ asset('bower_components/ui.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-ui-router/release/angular-ui-router.min.js')}}"></script> 
    <!-- Google Maps JavaScript API -->

  




    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcwWLTDgwn8A4FAynSJJR6xeu9PkrWq60&libraries=places"></script>
    <!-- vsGoogleAutocomplete -->    
    <script type="text/javascript" src="{{ asset('bower_components/vsGoogleAutocomplete/dist/vs-google-autocomplete.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/vsGoogleAutocomplete/dist/vs-autocomplete-validator.min.js')}}"></script>
    <!--multiple file upload-->    
    <script type="text/javascript" src="{{ asset('bower_components/angular-file-upload/dist/angular-file-upload.js')}}"></script>  
    <!-- <script type="text/javascript" src="{{ asset('bower_components/ng-file-upload/ng-file-upload.min.js')}}"></script>   -->
    <!-- Angular NGTOAST -->
    <script type="text/javascript" src="{{ asset('bower_components/AngularJS-Toaster/toaster.min.js')}}"></script>  
    <script type="text/javascript" src="{{ asset('bower_components/jstree/dist/jstree.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/ng-js-tree/dist/ngJsTree.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/jstree.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootbox/bootbox.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/ngBootbox/dist/ngBootbox.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-file-model/angular-file-model.js')}}"></script> 
  <!--  <script type="text/javascript" src="{{ asset('bower_components/file-saver/FileSaver.min.js')}}"></script>  -->
    
    <!-- date time picker-->    
    <script type="text/javascript" src="{{ asset('bower_components/angularjs-datetime-picker/angularjs-datetime-picker.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('app/app.js')}}"></script> <!-- Route File -->
    <script type="text/javascript" src="{{ asset('app/js/controllers/UserController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/user_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/msocial_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/customer_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/contract_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/project_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/tickets_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/training_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/kb_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/company_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/editsassesment_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/spare_managementController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/globalController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/navController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/controllers/superadminController.js')}}"></script>
    <script type="text/javascript" src="{{ asset('app/js/models/userModel.js')}}"></script> 
    <!-- Angular js -->
    <style>
      .mandatory{
        color:red;
      }
    </style>
</head>
 <!-- AIzaSyCcwWLTDgwn8A4FAynSJJR6xeu9PkrWq60  -->