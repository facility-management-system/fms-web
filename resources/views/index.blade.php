<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @extends('cssscript')
    <style>
        .loading-dialog {
            display: none;
            position: fixed;
            z-index: 999999;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 0, 0, 0, .2 ) url('https://upload.wikimedia.org/wikipedia/commons/7/7d/Pedro_luis_romani_ruiz.gif') 50% 50% no-repeat;            
        }
    </style>
    <body ng-app="myApp" ng-controller="globalController" class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <toaster-container toaster-options="{'time-out': 3000,'close-button': true}"></toaster-container>
        <div class="loading-dialog" data-loading>            
        </div>        
        <div class="page-container">
            <div ng-view>            
            </div> 
        </div>
    </body>
</html>